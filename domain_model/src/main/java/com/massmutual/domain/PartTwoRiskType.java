package com.massmutual.domain;

public enum PartTwoRiskType implements RiskType{
	
	APS,
	BLOOD_DISORDER,
	CANCER,
	CHRONIC_FATIGUE,
	CURRENTLY_PREGNANT,
	DIABETES,
	DIGESTIVE_DISORDER,
	EARS_NOSE_THROAT,
	FAMILY_HISTORY,
	HEALTHY,
	HEART_CONDITION,
	HIV,
	MENTAL_HEALTH,
	MUSCULAR_DISORDER,
	NO_TESTS,
	NOT_STATED,
	NERVOUS_SYSTEM_DISORDER,
	OTHER_MEDICATIONS,
	OTHER_SURGERY,
	OTHER_TREATMENTS,
	PAST_TEN_YEAR_WINDOW,
	PAST_FIVE_YEAR_WINDOW,
	PAST_THREE_YEAR_WINDOW,
	PERSONAL_INFORMATION,
	PREGNANCY,
	REPRODUCTIVE_DISORDER,
	RESPIRATORY_DISORDER,
	SKIN_DISORDER,
	SUGGESTED_TESTING,
	URINARY_TRACT,
	ALL;
}

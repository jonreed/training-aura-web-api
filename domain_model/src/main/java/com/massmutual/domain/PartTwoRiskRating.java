package com.massmutual.domain;

public class PartTwoRiskRating extends RiskRating implements java.io.Serializable {

	static final long serialVersionUID = 1L;
	
	private PartTwoRiskType partTwoRiskType;
	
	public PartTwoRiskRating() {
		super();
	}

	public PartTwoRiskRating(RiskRating rating) {

		super(rating);	
		this.partTwoRiskType = PartTwoRiskType.valueOf(rating.getRiskType());

	}

	public PartTwoRiskType getPartTwoRiskType() {
		return partTwoRiskType;
	}

	public void setPartTwoRiskType(PartTwoRiskType partTwoRiskType) {
		this.partTwoRiskType = partTwoRiskType;
		
		if (partTwoRiskType != null){
			riskType = partTwoRiskType.toString();
		}
	}
	
	public RiskRating createRiskRating(){
		RiskRating rating = new RiskRating((RiskRating) this);
		rating.setRiskType(String.valueOf(partTwoRiskType));
		return rating;
	}

	@Override
	public String toString() {
		return "PartTwoRiskRating [partTwoRiskType=" + partTwoRiskType + "]";
	}
	
	

}

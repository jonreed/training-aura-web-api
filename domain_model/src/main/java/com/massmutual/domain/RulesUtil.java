package com.massmutual.domain;

import java.util.Calendar;
import java.util.Date;

//Static Helper Method Class that can be used by all for common utilities
public class RulesUtil 
{

	/**
	 * endDate will be subtracted from startDate and the difference in days will be
	 * returned.  
	 * 
	 * If endDate is before startDate then the value returned will be 
	 * negative days.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return Long number of days (negative or positive)
	 */
	public static Long calculateDiffInDays(Date startDate, Date endDate)
	{
		try
		{
		    System.err.println("Start Date: " + startDate);
		    System.err.println("End Date: " + endDate);
		    if(startDate == null || endDate == null)
		    {
				return null;
			}
			
			Calendar calStart = Calendar.getInstance();
			calStart.setTime(startDate);
			Calendar calEnd = Calendar.getInstance();
			calEnd.setTime(endDate);
	
			if(calStart == null || calEnd == null)
			{
				return null;
			}
	
			long diff = calEnd.getTimeInMillis() - calStart.getTimeInMillis();
			long diffDays = diff / (1000 * 60 * 60 * 24);
		    System.err.println("Difference in days: " + Long.valueOf(diffDays));
	
			return Long.valueOf(diffDays);
		}
		catch(Exception e)
		{
			System.err.println("Exception raised in RulesUtil::calculateDiffInDays(...) - will be returning null, error message:  " + e.getMessage());
			return null;
		}
	}
}
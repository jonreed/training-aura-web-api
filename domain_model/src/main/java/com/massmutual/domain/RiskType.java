package com.massmutual.domain;


/**
 * Just a Marker interface for RiskType<br>
 * Use Concrete type PartOneRiskType is for PartOne<br>
 * Use Concrete type PartTwoRiskType is for PartTwo<br>
 * @author ashakya
 *
 */
public interface RiskType {
	
}

package com.massmutual.domain.partone;

public enum RelationshipType {
	
	OLI_OTHER (2147483647), 
	OLI_REL_FORMERSPOUSE (174), 
	OLI_REL_SPOUSE (5),
	OLI_REL_CHILD (2), 
	OLI_REL_PARENT (3),
	OLI_REL_SIBLING (4), 	
	OLI_REL_EMPLOYER (7), 
	OLI_REL_PARTNER (9),
	OLI_REL_DOMPART (15),
	OLI_REL_GRANDPARENT (92), 
	OLI_REL_LEGALGUARD (27), 	 
	OLI_REL_BUSINESSASSOCIATE (146),
		
	OLI_PARTICROLE_PRIMARY (1),
	
	// From OLI_LU_ORGFORM
	OLI_ORG_TRUST (16), // Trust
	OLI_ORG_SOLEP (14), // Sole Proporietorship , i.e., Non-incorporated entity
	OLI_ORG_CTP (13); // C Corporation, i.e., Incorporated entity
 
	private int relationshipValueValue = 0;
	
	RelationshipType(int value) {
        this.relationshipValueValue = value;

    }
	
	public int getRelationshipValue(){
		return relationshipValueValue;
	}
	
	public static RelationshipType getRelationshipType(int value){
		
		for (RelationshipType p : RelationshipType.values()){
	           
			if (p.getRelationshipValue() == value){
				return p;
			}
	    }
		
		return null;
	}
}

package com.massmutual.domain.partone;

import com.massmutual.domain.RiskBase;

public class MilitaryRisk extends RiskBase implements java.io.Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 507060297487854542L;
	
	private String memberOfArmedForces;
	
	private String memberOfSpecialForces;
	
	private String activatedForDuty;
	
	private String activatedForDutyAsNationalGuardOrReserve;
	
	private String duties;
	
	private String hazardOrIncentivePay;
	
	private String dutyOutsideOfCountry;
	
	

	public String getMemberOfSpecialForces() {
		return memberOfSpecialForces;
	}

	public void setMemberOfSpecialForces(String memberOfSpecialForces) {
		this.memberOfSpecialForces = memberOfSpecialForces;
	}

	public String getActivatedForDuty() {
		return activatedForDuty;
	}

	public void setActivatedForDuty(String activatedForDuty) {
		this.activatedForDuty = activatedForDuty;
	}

	public String getActivatedForDutyAsNationalGuardOrReserve() {
		return activatedForDutyAsNationalGuardOrReserve;
	}

	public void setActivatedForDutyAsNationalGuardOrReserve(
			String activatedForDutyAsNationalGuardOrReserve) {
		this.activatedForDutyAsNationalGuardOrReserve = activatedForDutyAsNationalGuardOrReserve;
	}

	public String getDuties() {
		return duties;
	}

	public void setDuties(String duties) {
		this.duties = duties;
	}

	public String getHazardOrIncentivePay() {
		return hazardOrIncentivePay;
	}

	public void setHazardOrIncentivePay(String hazardOrIncentivePay) {
		this.hazardOrIncentivePay = hazardOrIncentivePay;
	}

	

	public String getDutyOutsideOfCountry() {
		return dutyOutsideOfCountry;
	}

	public void setDutyOutsideOfCountry(String dutyOutsideOfCountry) {
		this.dutyOutsideOfCountry = dutyOutsideOfCountry;
	}

	public MilitaryRisk() {
		
	}

	public String getMemberOfArmedForces() {
		return memberOfArmedForces;
	}

	public void setMemberOfArmedForces(String memberOfArmedForces) {
		this.memberOfArmedForces = memberOfArmedForces;
	}

	@Override
	public String toString() {
		return "MilitaryRisk [memberOfArmedForces=" + memberOfArmedForces
				+ ", memberOfSpecialForces=" + memberOfSpecialForces
				+ ", activatedForDuty=" + activatedForDuty
				+ ", activatedForDutyAsNationalGuardOrReserve="
				+ activatedForDutyAsNationalGuardOrReserve + ", duties="
				+ duties + ", hazardOrIncentivePay=" + hazardOrIncentivePay
				+ ", dutyOutsideOfCountry=" + dutyOutsideOfCountry + "]";
	}
	
	

}

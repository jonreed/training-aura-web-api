package com.massmutual.domain.partone;

public enum PremiumSourceType {
	OLI_PREMSRC_SALARY (8), 
	OLI_PREMSRC_INVESTMENT (9), 
	OLI_PREMSRC_SAVINGS (10), 
	OLI_PREMSRC_GIFTS  (11),
	OLI_PREMSRC_CUST_LOAN_PREM(1013200002),
	OLI_UNKNOWN(0);
	
	private int value;
	private PremiumSourceType(int value){
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
		
	public static PremiumSourceType getPremiumSourceType(int value){
		
		for (PremiumSourceType p : PremiumSourceType.values()){
	           
			if (p.getValue() == value){
				return p;
			}
	    }
		
		return null;
	}
}

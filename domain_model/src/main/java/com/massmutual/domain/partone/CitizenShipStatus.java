package com.massmutual.domain.partone;

public enum CitizenShipStatus {

	CITIZEN(8), NON_CITIZEN(2147483647);
		
	private int citizenShipStatusValue = 0;
	
	CitizenShipStatus(int value) {
        this.citizenShipStatusValue = value;

    }
	
	public int getCitizenShipStatusValue(){
		return citizenShipStatusValue;
	}
	
	public static CitizenShipStatus getCitizenShipStatus(int value){
		
		for (CitizenShipStatus s : CitizenShipStatus.values()){
	           
			if (s.getCitizenShipStatusValue() == value){
				return s;
			}
	    }
		
		return null;
	}
}

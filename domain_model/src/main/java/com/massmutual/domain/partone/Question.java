package com.massmutual.domain.partone;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.massmutual.domain.RiskType;

//import org.kie.api.definition.type.PropertyReactive;

public class Question implements Serializable {
	
	private static final long serialVersionUID = -2811773784154293610L;
		
	private String questionId;
	private RiskType riskType;
	private String groupId;
	private String strAnswer;
	private boolean updatedValue;
	private Date lastUpdated;
	private final String dateFormat = "yyyy-MM-dd";
	
	public Question() {
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public RiskType getRiskType() {
		return riskType;
	}

	public void setRiskType(RiskType riskType) {
		this.riskType = riskType;
	}

	public String getStrAnswer() {
		return strAnswer;
	}

	public void setStrAnswer(String strAnswer) {
		this.lastUpdated = new Date();
		this.strAnswer = strAnswer;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public int getNumValue() {
		if (strAnswer != null && !strAnswer.isEmpty()){
			try{
				return Integer.parseInt(strAnswer);
			}
			catch(Exception e){
				return -1;
			}
		}
		return -1;
	}

	public void setNumValue(int numValue) {
		setStrAnswer(Integer.toString(numValue));
	}
	
	public float getDecValue() {
		if (strAnswer != null && !strAnswer.isEmpty()){
			try{
				return Float.parseFloat(strAnswer);
			}
			catch(Exception e){
				return -1.0f;
			}
		}
		return 0.0f;
	}

	public void setDecValue(float decValue) {
		setStrAnswer(Float.toString(decValue));
	}
	
	// pass in Yes/yes, No/no and true/false
	public Boolean getBoolValue() {
		if (strAnswer != null && !strAnswer.isEmpty()){
			
			if (strAnswer.equalsIgnoreCase("Yes")){
				return true;
			} 
			else if (strAnswer.equalsIgnoreCase("No")){
				return false;
			} 
				
			return Boolean.parseBoolean(strAnswer);
			
		}
		
		return null;
				
	}
	
	public void setBoolValue(Boolean boolValue) {
		setStrAnswer(Boolean.toString(boolValue));
	}
	
	public Date getDateValue() {
		if (strAnswer != null && strAnswer.length() == dateFormat.length()){
			
			try{
				return new SimpleDateFormat(dateFormat).parse(strAnswer);
			}
			catch(Exception e){
				return null;
			}
			
		}
		return null;
	}
	
	// use date format "2015-08-18":yyyy-MM-dd
	public void setDateValue(Date date) {
		
		if (date != null){
			setStrAnswer(new SimpleDateFormat(dateFormat).format(date));
		}
		else{
			setStrAnswer("");
		}
	}

	public boolean isUpdatedValue() {
		return updatedValue;
	}

	public void setUpdatedValue(boolean updatedValue) {
		this.updatedValue = updatedValue;
	}
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	@Override
	public String toString() {
		return "Answer [questionId=" + questionId + ", groupId=" + groupId
				+ ", strAnswer=" + strAnswer + ", updatedValue=" + updatedValue
				+ ", lastUpdated=" + lastUpdated + ", dateFormat=" + dateFormat
				+ "]";
	}

	
	
}

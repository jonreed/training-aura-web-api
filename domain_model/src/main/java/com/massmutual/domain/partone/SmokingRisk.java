package com.massmutual.domain.partone;

import com.massmutual.domain.RiskBase;

public class SmokingRisk extends RiskBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	// "Used tobacco (except cigars) within past 12 months"
	private String usedPast12Months;

	// "Used tobacco (except cigars) within past 24 months"
	private String usedPast24Months;

	// "Used Cigars within past 12 months"
	private String usedCigarsPast12Months;	// Y/N

	// "Annual cigar count"
	private int annualCigarCount;
	
	// months smoked in the past: 12 or more
	private Integer smokingDuration;

	// "Used tobacco cessation aides in the past 12 months"
	private String usedTobaccoCessationAides;

	public SmokingRisk() {
	}

	public SmokingRisk(String usedPast12Months, String usedPast24Months,
			String usedCigarsPast12Months, int annualCigarCount,
			String usedTobaccoCessationAides) {

		this.usedPast12Months = usedPast12Months;
		this.usedPast24Months = usedPast24Months;
		this.usedCigarsPast12Months = usedCigarsPast12Months;
		this.annualCigarCount = annualCigarCount;
		this.usedTobaccoCessationAides = usedTobaccoCessationAides;
	}

	public String getUsedPast12Months() {
		return usedPast12Months;
	}

	public void setUsedPast12Months(String usedPast12Months) {
		this.usedPast12Months = usedPast12Months;
	}

	public String getUsedPast24Months() {
		return usedPast24Months;
	}

	public void setUsedPast24Months(String usedPast24Months) {
		this.usedPast24Months = usedPast24Months;
	}

	public Integer getSmokingDuration() {
		return smokingDuration;
	}

	public void setSmokingDuration(Integer smokingDuration) {
		this.smokingDuration = smokingDuration;
	}

	public String getUsedCigarsPast12Months() {
		return usedCigarsPast12Months;
	}

	public void setUsedCigarsPast12Months(String usedCigarsPast12Months) {
		this.usedCigarsPast12Months = usedCigarsPast12Months;
	}

	public int getAnnualCigarCount() {
		return annualCigarCount;
	}

	public void setAnnualCigarCount(int annualCigarCount) {
		this.annualCigarCount = annualCigarCount;
	}

	public String getUsedTobaccoCessationAides() {
		return usedTobaccoCessationAides;
	}

	public void setUsedTobaccoCessationAides(String usedTobaccoCessationAides) {
		this.usedTobaccoCessationAides = usedTobaccoCessationAides;
	}

	@Override
	public String toString() {
		return "SmokingRisk [usedPast12Months=" + usedPast12Months
				+ ", usedPast24Months=" + usedPast24Months
				+ ", usedCigarsPast12Months=" + usedCigarsPast12Months
				+ ", annualCigarCount=" + annualCigarCount
				+ ", smokingDuration=" + smokingDuration
				+ ", usedTobaccoCessationAides=" + usedTobaccoCessationAides
				+ "]";
	}

}

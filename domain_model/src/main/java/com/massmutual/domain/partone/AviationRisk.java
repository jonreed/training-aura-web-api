package com.massmutual.domain.partone;

import com.massmutual.domain.RiskBase;

public class AviationRisk extends RiskBase implements java.io.Serializable{
	static final long serialVersionUID = 1L;
	
	private String flying;

	public AviationRisk() {
		
	}

	public String getFlying() {
		return flying;
	}

	public void setFlying(String flying) {
		this.flying = flying;
	}

	@Override
	public String toString() {
		return "AviationRisk [flying=" + flying + "]";
	}

}

package com.massmutual.domain.partone;

import java.io.Serializable;
import java.util.ArrayList;

import com.massmutual.domain.Insured;
import com.massmutual.domain.PartOneRiskType;
import com.massmutual.domain.PolicyInformation;
import com.massmutual.domain.RiskInputBase;

//@ApiModel
public class PrimaryInsured extends RiskInputBase implements Serializable {
  
	private static final long serialVersionUID = -2L;
	
	// This is for the part one demo app... refactor?
	private PartOneRiskType riskType;
	private String id;
	
	//IMPORTANT: There should be no single variable fields in this class. Only risk group classes.
	
	//TODO: needs to move this field to PolicyAppliedFor
	//private int insuredAge;
	
	private Insured insured;
	private SmokingRisk smoking;
	private MilitaryRisk military;
	private AviationRisk aviation;
	private ForeignTravelRisk travel;
	private AvocationRisk avocation;
	private CitizenShip citizenShip;
	private Disability disability;
	private Felony felony;
	private Dui dui;
	//private OccupationRisk occupation;
	private FinancialHistory financialHistory;
	private PolicyAppliedFor policyAppliedFor;
	private ArrayList<Question> riskQuestions;
	private TemporaryLifeInsuranceReceipt temporaryLifeInsuranceReceipt;
	private PaymentInformation paymentInformation;
	
	private PurposeOfInsuranceRisk purposeOfInsurance;
	private OwnerBeneficaryRisk ownerBeneficary;
	private PartOneMedicalHistory medicalHistory;
	private AgentRisk agent;
	private PolicyInformation policyInformation;
	
	
	public PrimaryInsured() {
	}

	public PartOneRiskType getRiskType() {
		return riskType;
	}

	public void setRiskType(PartOneRiskType riskType) {
		this.riskType = riskType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public Insured getInsured() {
		return insured;
	}

	public void setInsured(Insured insured) {
		this.insured = insured;
	}

	//@RiskMarker(riskType=PartOneRiskType.SMOKER)
	public SmokingRisk getSmoking() {
		return smoking;
	}

	public void setSmoking(SmokingRisk smoking) {
		this.smoking = smoking;
	}

	//@RiskMarker(riskType=PartOneRiskType.MILITARY)
	public MilitaryRisk getMilitary() {
		return military;
	}

	public void setMilitary(MilitaryRisk military) {
		this.military = military;
	}

	//@RiskMarker(riskType=PartOneRiskType.AVIATION)
	public AviationRisk getAviation() {
		return aviation;
	}

	public void setAviation(AviationRisk aviation) {
		this.aviation = aviation;
	}

	//@RiskMarker(riskType=PartOneRiskType.FOREIGN_TRAVEL)
	public ForeignTravelRisk getTravel() {
		return travel;
	}

	public void setTravel(ForeignTravelRisk travel) {
		this.travel = travel;
	}

	//@RiskMarker(riskType=PartOneRiskType.AVOCATION)
	public AvocationRisk getAvocation() {
		return avocation;
	}

	public void setAvocation(AvocationRisk avocation) {
		this.avocation = avocation;
	}

	
	public ArrayList<Question> getRiskQuestions() {
		return riskQuestions;
	}

	public void setRiskQuestions(ArrayList<Question> riskQuestions) {
		this.riskQuestions = riskQuestions;
	}
	
	

	//@RiskMarker(riskType=PartOneRiskType.CITIZEN)
	public CitizenShip getCitizenShip() {
		return citizenShip;
	}

	public void setCitizenShip(CitizenShip citizenShip) {
		this.citizenShip = citizenShip;
	}

	//@RiskMarker(riskType=PartOneRiskType.DISABILITY)
	public Disability getDisability() {
		return disability;
	}

	public void setDisability(Disability disability) {
		this.disability = disability;
	}

	//@RiskMarker(riskType=PartOneRiskType.FELONY)
	public Felony getFelony() {
		return felony;
	}

	public void setFelony(Felony felony) {
		this.felony = felony;
	}

	//@RiskMarker(riskType=PartOneRiskType.DUI)
	public Dui getDui() {
		return dui;
	}

	public void setDui(Dui dui) {
		this.dui = dui;
	}

	//@RiskMarker(riskType=PartOneRiskType.OCCUPATION)
//	public OccupationRisk getOccupation() {
//		return occupation;
//	}
//
//	public void setOccupation(OccupationRisk occupation) {
//		this.occupation = occupation;
//	}

	
	//@RiskMarker(riskType=PartOneRiskType.ANNUAL_INCOME)
	public FinancialHistory getFinancialHistory() {
		return financialHistory;
	}

	public void setFinancialHistory(FinancialHistory financialHistory) {
		this.financialHistory = financialHistory;
	}

	
	//@RiskMarker(riskType=PartOneRiskType.ALL)
	public PolicyAppliedFor getPolicyAppliedFor() {
		return policyAppliedFor;
	}

	public void setPolicyAppliedFor(PolicyAppliedFor policyAppliedFor) {
		this.policyAppliedFor = policyAppliedFor;
	}

	
	public TemporaryLifeInsuranceReceipt getTemporaryLifeInsuranceReceipt() {
		return temporaryLifeInsuranceReceipt;
	}

	public void setTemporaryLifeInsuranceReceipt(
			TemporaryLifeInsuranceReceipt temporaryLifeInsuranceReceipt) {
		this.temporaryLifeInsuranceReceipt = temporaryLifeInsuranceReceipt;
	}

	public PaymentInformation getPaymentInformation() {
		return paymentInformation;
	}

	public void setPaymentInformation(PaymentInformation paymentInformation) {
		this.paymentInformation = paymentInformation;
	}

	public PurposeOfInsuranceRisk getPurposeOfInsurance() {
		return purposeOfInsurance;
	}

	public void setPurposeOfInsurance(PurposeOfInsuranceRisk purposeOfInsurance) {
		this.purposeOfInsurance = purposeOfInsurance;
	}

	public OwnerBeneficaryRisk getOwnerBeneficary() {
		return ownerBeneficary;
	}

	public void setOwnerBeneficary(OwnerBeneficaryRisk ownerBeneficary) {
		this.ownerBeneficary = ownerBeneficary;
	}

	public PartOneMedicalHistory getMedicalHistory() {
		return medicalHistory;
	}

	public void setMedicalHistory(PartOneMedicalHistory medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	public AgentRisk getAgent() {
		return agent;
	}

	public void setAgent(AgentRisk agent) {
		this.agent = agent;
	}

	public PolicyInformation getPolicyInformation() {
		return policyInformation;
	}

	public void setPolicyInformation(PolicyInformation policyInformation) {
		this.policyInformation = policyInformation;
	}

	@Override
	public String toString() {
		return "PrimaryInsured [riskType=" + riskType + ", id=" + id + ", insured=" + insured + ", smoking=" + smoking
				+ ", military=" + military + ", aviation=" + aviation + ", travel=" + travel + ", avocation="
				+ avocation + ", citizenShip=" + citizenShip + ", disability=" + disability + ", felony=" + felony
				+ ", dui=" + dui + ", financialHistory=" + financialHistory + ", policyAppliedFor=" + policyAppliedFor
				+ ", riskQuestions=" + riskQuestions + ", temporaryLifeInsuranceReceipt="
				+ temporaryLifeInsuranceReceipt + ", paymentInformation=" + paymentInformation + ", purposeOfInsurance="
				+ purposeOfInsurance + ", ownerBeneficary=" + ownerBeneficary + ", medicalHistory=" + medicalHistory
				+ ", agent=" + agent + ", policyInformation=" + policyInformation + "]";
	}

}

package com.massmutual.domain.partone;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.massmutual.domain.RiskBase;
import com.massmutual.domain.verification.MedicalHistory;

public class PartOneMedicalHistory extends MedicalHistory implements Serializable{
	
	private static final long serialVersionUID = -705991172507124136L;

	@Override
	public String toString() {
		return "PartOneMedicalHistory [" + super.toString() + "]";
	}
}

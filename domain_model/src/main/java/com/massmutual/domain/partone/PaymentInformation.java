package com.massmutual.domain.partone;

import java.io.Serializable;

import com.massmutual.domain.RiskBase;

public class PaymentInformation extends RiskBase implements Serializable {

	private static final long serialVersionUID = 2192527845979495751L;
	
	private PremiumSourceType type;
	private int premiumSource;
	private int premiumPayer;
	
	// new fields for Source of Premium
	private String earnedIncome; // Y/N
	private String giftsOrInheritance; // Y/N
	private String investmentIncome; // Y/N
	private String loanOrPremiumFinancing; // Y/N
	private String investmentOrSavings; // Y/N
	private String otherIncome; // Y/N
	
	public int getPremiumPayer() {
		return premiumPayer;
	}

	public void setPremiumPayer(int premiumPayer) {
		this.premiumPayer = premiumPayer;
	}

	public int getPremiumSource() {
		return premiumSource;
	}

	public void setPremiumSource(int premiumSource) {
		this.premiumSource = premiumSource;
		this.type = PremiumSourceType.getPremiumSourceType(premiumSource);
	}

	public PremiumSourceType getType() {
		return type;
	}

	public void setType(PremiumSourceType type) {
		this.type = type;
		this.premiumSource = this.type.getValue();
	}

	public String getEarnedIncome() {
		return earnedIncome;
	}

	public void setEarnedIncome(String earnedIncome) {
		this.earnedIncome = earnedIncome;
	}

	public String getGiftsOrInheritance() {
		return giftsOrInheritance;
	}

	public void setGiftsOrInheritance(String giftsOrInheritance) {
		this.giftsOrInheritance = giftsOrInheritance;
	}

	public String getInvestmentIncome() {
		return investmentIncome;
	}

	public void setInvestmentIncome(String investmentIncome) {
		this.investmentIncome = investmentIncome;
	}

	public String getLoanOrPremiumFinancing() {
		return loanOrPremiumFinancing;
	}

	public void setLoanOrPremiumFinancing(String loanOrPremiumFinancing) {
		this.loanOrPremiumFinancing = loanOrPremiumFinancing;
	}

	public String getInvestmentOrSavings() {
		return investmentOrSavings;
	}

	public void setInvestmentOrSavings(String investmentOrSavings) {
		this.investmentOrSavings = investmentOrSavings;
	}

	public String getOtherIncome() {
		return otherIncome;
	}

	public void setOtherIncome(String otherIncome) {
		this.otherIncome = otherIncome;
	}

	@Override
	public String toString() {
		return "PaymentInformation [type=" + type + ", premiumSource=" + premiumSource + ", premiumPayer="
				+ premiumPayer + ", earnedIncome=" + earnedIncome + ", giftsOrInheritance=" + giftsOrInheritance
				+ ", investmentIncome=" + investmentIncome + ", loanOrPremiumFinancing=" + loanOrPremiumFinancing
				+ ", investmentOrSavings=" + investmentOrSavings + ", otherIncome=" + otherIncome + "]";
	}

}

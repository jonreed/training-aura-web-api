package com.massmutual.domain.partone;

final public class  RuleControlAdvice {

	
	public static enum ADVICE{
		NONE,
		DO_SCHEDULED_ALIR_AMOUNTS_AT_RISK,
		DO_UNSCHEDULED_ALIR_AMOUNTS_AT_RISK,
		DO_SCHEDULED_ALIR_MIN,
		DO_ALL_ALIR_AMOUNTS_AT_RISK
	}
	
	private ADVICE advice;	
	private String adviceValue;
	
	
	private String adviceEvaluated;
	private String allowActivation;
	
	private double scheduledAlirRiskMultiplier = 0;	
	private double unscheduledAlirRiskMultiplier = 0;
	
	private double scheduledAlirAtRiskAmount = 0;
	private double unscheduledAlirAtRiskAmount = 0;
	
	private double scheduledAlirMinAmount = 0;
	
	private double amount = 0.0;

	public ADVICE getAdvice() {
		return advice;
	}

	public void setAdvice(ADVICE advice) {
		this.advice = advice;
	}

	public String getAdviceEvaluated() {
		return adviceEvaluated;
	}

	public void setAdviceEvaluated(String adviceEvaluated) {
		this.adviceEvaluated = adviceEvaluated;
	}

	public String getAllowActivation() {
		return allowActivation;
	}

	public void setAllowActivation(String allowActivation) {
		this.allowActivation = allowActivation;
	}

	public String getAdviceValue() {
		return adviceValue;
	}

	public void setAdviceValue(String adviceValue) {
		this.adviceValue = adviceValue;
	}

	public double getScheduledAlirRiskMultiplier() {
		return scheduledAlirRiskMultiplier;
	}

	public void setScheduledAlirRiskMultiplier(double scheduledAlirRiskMultiplier) {
		this.scheduledAlirRiskMultiplier = scheduledAlirRiskMultiplier;
	}

	public double getUnscheduledAlirRiskMultiplier() {
		return unscheduledAlirRiskMultiplier;
	}

	public void setUnscheduledAlirRiskMultiplier(double unscheduledAlirRiskMultiplier) {
		this.unscheduledAlirRiskMultiplier = unscheduledAlirRiskMultiplier;
	}

	public double getScheduledAlirAtRiskAmount() {
		return scheduledAlirAtRiskAmount;
	}

	public void setScheduledAlirAtRiskAmount(double scheduledAlirAtRiskAmount) {
		this.scheduledAlirAtRiskAmount = scheduledAlirAtRiskAmount;
	}

	public double getUnscheduledAlirAtRiskAmount() {
		return unscheduledAlirAtRiskAmount;
	}

	public void setUnscheduledAlirAtRiskAmount(double unscheduledAlirAtRiskAmount) {
		this.unscheduledAlirAtRiskAmount = unscheduledAlirAtRiskAmount;
	}
	
	public double getTotalAlirAtRiskAmount() {
		return scheduledAlirAtRiskAmount + unscheduledAlirAtRiskAmount;
	}

	public double getScheduledAlirMinAmount() {
		return scheduledAlirMinAmount;
	}

	public void setScheduledAlirMinAmount(double scheduledAlirMinAmount) {
		this.scheduledAlirMinAmount = scheduledAlirMinAmount;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "RuleControlAdvice [advice=" + advice + ", adviceValue=" + adviceValue + ", adviceEvaluated="
				+ adviceEvaluated + ", allowActivation=" + allowActivation + ", scheduledAlirRiskMultiplier="
				+ scheduledAlirRiskMultiplier + ", unscheduledAlirRiskMultiplier=" + unscheduledAlirRiskMultiplier
				+ ", scheduledAlirAtRiskAmount=" + scheduledAlirAtRiskAmount + ", unscheduledAlirAtRiskAmount="
				+ unscheduledAlirAtRiskAmount + ", scheduledAlirMinAmount=" + scheduledAlirMinAmount + ", amount="
				+ amount + "]";
	}
	
}

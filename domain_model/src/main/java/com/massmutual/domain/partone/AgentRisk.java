package com.massmutual.domain.partone;

import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.RiskBase;

public class AgentRisk extends RiskBase implements java.io.Serializable
{
	private static final long serialVersionUID = 5127128048294674061L;
	
	/**
	 * List of agents on this policy/case - soliciting, servicing, writing - will be populated by caller (aura-rules)
	 * Numeric portion of the id only including zero's (ex: 002154)
	 */
	private List<String> agentIdList = new ArrayList<String>();

	/**
	 * *****NOTE:  THIS IS SUCH A SMALL LIST IT CAN BE INCLUDED IN THIS OBJECT, IF THIS
	 * WAS A LARGE LIST YOU WOULD WANT IT TO BE IT'S OWN SEPARATE OBJECT OUTSIDE OF THIS OBJECT****
	 * List of agents that are on the watchList - will be populated by the brms engine
	 * Numeric portion of the id only including zero's (ex: 002154)
	 */
	private List<String> agentIdWatchList = new ArrayList<String>();
	
	/** More efficient to save a bad id as soon as it's found, doesn't need to keep checking and used to return
	 * for multiple calls 
	 */
	private String badAgentIdFound = null;
		
	/*******************************************************************/
	/*            STANDARD GET/SET METHODSBRMS HELPER METHODS          */
	/*******************************************************************/

	/**
	 * List of agents on this policy/case - soliciting, servicing, writing - will be populated by caller (aura-rules)
	 * 
	 * @return list of agent id's - numeric portion of the id only including zero's (ex: 002154)
	 */
	public List<String> getAgentIdList() {
		return agentIdList;
	}

	/**
	 * List of agents on this policy/case - soliciting, servicing, writing - will be populated by caller (aura-rules)
	 * 
	 * @param agentIdList - numeric portion of the id only including zero's (ex: 002154)
	 */
	public void setAgentIdList(List<String> agentIdList) {
		this.agentIdList = agentIdList;
	}

	/**
	 * List of agents on this policy/case - soliciting, servicing, writing - will be populated by caller (aura-rules)
	 * 
	 * @param agentId - numeric portion of the id only including zero's (ex: 002154)
	 */
	public void addAgentIdList(String agentId) {
		if(agentIdList == null)
		{
			agentIdList = new ArrayList<String>();
		}
		agentIdList.add(agentId);
	}

	/**
	 * List of agents that are on the watchList - will be populated by the brms engine
	 * 
	 * @return list of agent id's - numeric portion of the id only including zero's (ex: 002154)
	 */
	public List<String> getAgentIdWatchList() {
		return agentIdWatchList;
	}

	/**
	 * List of agents that are on the watchList - will be populated by the brms engine
	 * 
	 * @param agentIdWatchList - numeric portion of the id only including zero's (ex: 002154)
	 */
	public void setAgentIdWatchList(List<String> agentIdWatchList) {
		this.agentIdWatchList = agentIdWatchList;
	}
	
	/**
	 * If one of the id's in the agentIdList is found in the badAgentWatchList, then the first id
	 * found will be saved and this method will return that id.
	 * 
	 * @return String agent id - numeric portion of the id only including zero's (ex: 002154) 
	 * or null if there is no bad agent id in the list of agents on this case.
	 */
	public String getBadAgentIdFound() {
		return badAgentIdFound;
	}

	/**
	 * If one of the id's in the agentIdList is found in the badAgentWatchList, then the first id
	 * found will be saved using this method.
	 * 
	 * @param badAgentIdFound - numeric portion of the id only including zero's (ex: 002154)
	 */
	public void setBadAgentIdFound(String badAgentIdFound) {
		this.badAgentIdFound = badAgentIdFound;
	}
	
	/*******************************************************************/
	/*                    BRMS HELPER METHODS                          */
	/*******************************************************************/
	
	/**
	 * BRMS helper method, a gdst uses this method to populate the agentIdWatchList.
	 * Do NOT change the method name intentionally setup as a "get" method in order
	 * for it to work in BRMS the way it is coded.
	 * 
	 * @param agentId - numeric portion of the id only including zero's (ex: 002154)
	 */
	public void setAddAgentIdWatchList(String agentId) {
		if(agentIdWatchList == null)
		{
			agentIdWatchList = new ArrayList<String>();
		}
		agentIdWatchList.add(agentId);
	}
	
	/**
	 * BRMS helper method, aura-rules populates the agentIdList and a BRMS
	 * gdst populates the agentIdWatchList.  Then this method is called to handled
	 * BRMS rules to determine if any of the id's from the case (passed in by aura-rules)
	 * exist in the watch list.  First one found is returned, if none are found then 
	 * null is returned - do NOT change that processing or the rules will break it
	 * expects and looks for null.
	 * Do NOT change the method name intentionally setup as a "get" method in order
	 * for it to work in BRMS the way it is coded.
	 * 
	 * Note:  All id's are the numeric portion of the id only including zero's (ex:  002154)
	 * 
	 * @return 	first agent found on our agentIdList that is contained within the agentIdWatchList
	 * 			else null.
	 */
	public String getAgentInWatchList()
	{	
		if(badAgentIdFound != null && badAgentIdFound.trim().length() > 0)
		{
			return badAgentIdFound;
		}
		
		if(agentIdWatchList != null && agentIdWatchList.size() > 0 && agentIdList != null && agentIdList.size() > 0)
		{
			for(String agent : agentIdList)
			{
				if(agentIdWatchList.contains(agent))
				{
					setBadAgentIdFound(agent);
					return badAgentIdFound;
				}
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return "AgentRisk [agentIdList=" + agentIdList + ", agentIdWatchList="
				+ agentIdWatchList + super.toString() + "]";
	}
	
	/*******************************************************************/
	/* DUMMY METHODS TO AVOID JACKSON ISSUES - DO NOT REMOVE OR CHANGE */
	/*******************************************************************/
	
	/**
	 * *** Do not change or remove ****
	 * Dummy method to avoid jackson issues when having a set method without a get method.
	 * **********************************
	 * 
	 */
	public void setAgentInWatchList(String dummy)
	{
		//do nothing
	}
	
	/**
	 * *** Do not change method name****
	 * Dummy method to avoid jackson issues when having a set method without a get method.
	 * **********************************
	 * 
	 */
	public String getAddAgentIdWatchList()
	{
		return "";
	}
}

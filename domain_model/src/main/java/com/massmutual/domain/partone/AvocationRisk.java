package com.massmutual.domain.partone;

import com.massmutual.domain.RiskBase;

public class AvocationRisk extends RiskBase implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7594622678006464139L;
	
	private String lifeStyleActivities;
	
	private String underWaterDiving;
	
	private String underWaterDiving100Ft;
	
	private String underWaterDivingCertification;
	
	private String underWaterDiveWithABuddy;
	
	private Integer underWaterDiveFrequency;
	
	private String underWaterDiveFewerThanFifty;

	public String getUnderWaterDiveFewerThanFifty() {
		return underWaterDiveFewerThanFifty;
	}

	public void setUnderWaterDiveFewerThanFifty(String underWaterDiveFewerThanFifty) {
		this.underWaterDiveFewerThanFifty = underWaterDiveFewerThanFifty;
	}

	public String getLifeStyleActivities() {
		return lifeStyleActivities;
	}

	public void setLifeStyleActivities(String lifeStyleActivities) {
		this.lifeStyleActivities = lifeStyleActivities;
	}

	public String getUnderWaterDiving() {
		return underWaterDiving;
	}

	public void setUnderWaterDiving(String underWaterDiving) {
		this.underWaterDiving = underWaterDiving;
	}

	public String getUnderWaterDiving100Ft() {
		return underWaterDiving100Ft;
	}

	public void setUnderWaterDiving100Ft(String underWaterDiving100Ft) {
		this.underWaterDiving100Ft = underWaterDiving100Ft;
	}

	public String getUnderWaterDivingCertification() {
		return underWaterDivingCertification;
	}

	public void setUnderWaterDivingCertification(
			String underWaterDivingCertification) {
		this.underWaterDivingCertification = underWaterDivingCertification;
	}

	public String getUnderWaterDiveWithABuddy() {
		return underWaterDiveWithABuddy;
	}

	public void setUnderWaterDiveWithABuddy(String underWaterDiveWithABuddy) {
		this.underWaterDiveWithABuddy = underWaterDiveWithABuddy;
	}

	public Integer getUnderWaterDiveFrequency() {
		return underWaterDiveFrequency;
	}

	public void setUnderWaterDiveFrequency(Integer underWaterDiveFrequency) {
		this.underWaterDiveFrequency = underWaterDiveFrequency;
	}

	@Override
	public String toString() {
		return "AvocationRisk [lifeStyleActivities=" + lifeStyleActivities
				+ ", underWaterDiving=" + underWaterDiving
				+ ", underWaterDiving100Ft=" + underWaterDiving100Ft
				+ ", underWaterDivingCertification="
				+ underWaterDivingCertification + ", underWaterDiveWithABuddy="
				+ underWaterDiveWithABuddy + ", underWaterDiveFrequency="
				+ underWaterDiveFrequency + ", underWaterDiveFewerThanFifty="
				+ underWaterDiveFewerThanFifty + "]";
	}

	
	
	
	
}

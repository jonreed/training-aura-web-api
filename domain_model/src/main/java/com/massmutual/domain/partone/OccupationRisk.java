package com.massmutual.domain.partone;

import com.massmutual.domain.RiskRatingBase;

public class OccupationRisk extends RiskRatingBase implements java.io.Serializable {
	
	private static final long serialVersionUID = -8818592692242538820L;

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "OccupationRisk [type=" + type + ", description=" + description + ", ruleName=" + ruleName
				+ ", rollupComplete=" + rollupComplete + ", riskSummaryType=" + riskSummaryType + ", UPNT=" + UPNT
				+ ", SPNT=" + SPNT + ", NONT=" + NONT + ", SPT=" + SPT + ", TOB=" + TOB + ", WP=" + WP + ", DIIO="
				+ DIIO + "]";
	}

	
		
}

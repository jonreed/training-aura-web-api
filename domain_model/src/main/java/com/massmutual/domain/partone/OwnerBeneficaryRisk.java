package com.massmutual.domain.partone;

import com.massmutual.domain.RiskBase;

public class OwnerBeneficaryRisk extends RiskBase implements java.io.Serializable {
	
	private static final long serialVersionUID = -8818592692242538820L;
	
	private RelationshipType beneficiaryRelationship;
	private RelationshipType ownerRelationship;
	private String ownerUSPerson;
	
	public RelationshipType getBeneficiaryRelationship() {
		return beneficiaryRelationship;
	}

	public void setBeneficiaryRelationship(RelationshipType beneficiaryRelationship) {
		this.beneficiaryRelationship = beneficiaryRelationship;
	}
	
	public int getBeneficiaryRelationshipValue(){
		return this.beneficiaryRelationship.getRelationshipValue();
	}
	
	public void setBeneficiaryRelationshipValue(int value) {
		this.beneficiaryRelationship = RelationshipType.getRelationshipType(value);
	}

	public RelationshipType getOwnerRelationship() {
		return ownerRelationship;
	}

	public void setOwnerRelationship(RelationshipType ownerRelationship) {
		this.ownerRelationship = ownerRelationship;
	}

	public int getOwnerRelationshipValue(){
		return this.ownerRelationship.getRelationshipValue();
	}
	
	public void setOwnerRelationshipValue(int value) {
		this.ownerRelationship = RelationshipType.getRelationshipType(value);
	}

	public String getOwnerUSPerson() {
		return this.ownerUSPerson;
	}

	public void setOwnerUSPerson(String ownerUSPerson) {
		this.ownerUSPerson = ownerUSPerson;
	}
	
	@Override
	public String toString() {
		return "OwnerBeneficaryRisk [beneficiaryRelationship=" + beneficiaryRelationship + ", ownerRelationship="
				+ ownerRelationship + ", ownerUSPerson=" + ownerUSPerson + "]";
	}
	
}

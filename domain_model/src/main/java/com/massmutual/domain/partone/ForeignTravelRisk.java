package com.massmutual.domain.partone;

import java.util.Calendar;
import java.util.Date;

import com.massmutual.domain.RiskBase;

public class ForeignTravelRisk extends RiskBase implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3148830365000260913L;

	private String country;
	
	private int durationOfStayInWeeks;
	private int durationOfStayInDays;
	private String anyTravelNext2Years;	
	private Date dateOfDeparture;

	public String getCountry() {
		return country;
	}
	
	// this is a helper method so that we can have multiple country enum dropdowns in the guided decision table
	public String getExcludedCountry() {
		return country;
	}
	
	// this is a helper method so that we can have multiple country enum dropdowns in the guided decision table
	public String geACountry() {
		return country;
	}
	
	// this is a helper method so that we can have multiple country enum dropdowns in the guided decision table
	public String getBCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public int getDurationOfStayInWeeks() {
		return durationOfStayInWeeks;
	}

	public void setDurationOfStayInWeeks(int durationOfStayInWeeks) {
		this.durationOfStayInWeeks = durationOfStayInWeeks;
	}

	public int getDurationOfStayInDays() {
		return durationOfStayInDays;
	}

	public void setDurationOfStayInDays(int durationOfStayInDays) {
		this.durationOfStayInDays = durationOfStayInDays;
	}

	public String getAnyTravelNext2Years() {
		return anyTravelNext2Years;
	}

	public void setAnyTravelNext2Years(String anyTravelNext2Years) {
		this.anyTravelNext2Years = anyTravelNext2Years;
	}

	public Date getDateOfDeparture() {
		return dateOfDeparture;
	}

	public void setDateOfDeparture(Date dateOfDeparture) {
		this.dateOfDeparture = dateOfDeparture;
	}
	
	public long calculateDiffinDays(Date date){
		
		if(dateOfDeparture == null || date == null){
			return -1;
		}
		
		Calendar calEnd = Calendar.getInstance();
		calEnd.setTime(dateOfDeparture);
		cleanDate(calEnd);
		
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(date);
		cleanDate(calStart);
		
		long diff = calEnd.getTimeInMillis() - calStart.getTimeInMillis();
		
		long diffDays = diff / (1000 * 60 * 60 * 24);
		
		if(diffDays < 1){
			diffDays = diffDays * -1;
		}
				
		return diffDays;
	}
	
	//clean hour minute and second
	private  void cleanDate(Calendar date){
			
		date.clear(Calendar.HOUR);
		date.clear(Calendar.MINUTE);
		date.clear(Calendar.SECOND);
		date.clear(Calendar.MILLISECOND);
	
	}

	@Override
	public String toString() {
		return "ForeignTravelRisk [country=" + country + ", durationOfStayInWeeks=" + durationOfStayInWeeks
				+ ", durationOfStayInDays=" + durationOfStayInDays + ", anyTravelNext2Years=" + anyTravelNext2Years
				+ ", dateOfDeparture=" + dateOfDeparture + "]";
	}

}

package com.massmutual.domain.partone;

import com.massmutual.domain.RiskBase;

public class CitizenShip extends RiskBase implements java.io.Serializable{

	private static final long serialVersionUID = -305155500826583183L;
	
	private CitizenShipStatus immigrationStatus;
	private String insuredUSPerson;

	public CitizenShipStatus getImmigrationStatus() {
		return immigrationStatus;
	}

	public void setImmigrationStatus(CitizenShipStatus immigrationStatus) {
		this.immigrationStatus = immigrationStatus;
	}
	
	public int getCitizenShipStatusValue(){
		return this.immigrationStatus.getCitizenShipStatusValue();
	}
	
	public void setCitizenShipStatusValue(int value) {
		this.immigrationStatus = CitizenShipStatus.getCitizenShipStatus(value);
	}

	public void setInsuredUSPerson(String insuredUSPerson) {
		this.insuredUSPerson = insuredUSPerson;
	}
	
	public String getInsuredUSPerson(){
		return this.insuredUSPerson;
	}
	
	@Override
	public String toString() {
		return "CitizenShip [immigrationStatus=" + immigrationStatus + ", insuredUSPerson=" + insuredUSPerson + "]";
	}
	
	
	
	
	


}

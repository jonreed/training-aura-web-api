package com.massmutual.domain.partone;

import java.io.Serializable;

import org.kie.api.definition.type.PropertyReactive;

/* This class is only used internally to the rule engine and is
 *  not used in any inputs or outputs
 */
@PropertyReactive
public class PolicyCalculationResults implements Serializable{

	private static final long serialVersionUID = -5081253854204964510L;

	private double policyRiskAmount = 0.0;
	private double totalRiskAmount = 0.0;
	private double maximumRiskAmount = 0.0;
	
	private double scheduledAlirAtRiskAmount = 0.0;	
	private double unscheduledAlirAtRiskAmount = 0.0;
	
	private boolean pra_updated = false;
	private boolean tra_updated = false;
	
	public double getPolicyRiskAmount() {
		//System.out.println("getPolicyRiskAmount: " + policyRiskAmount );

		return policyRiskAmount;
	}
	
	public double getTotalRiskAmount() {
		//System.out.println("getTotalRiskAmount: " + totalRiskAmount );

		return totalRiskAmount; //totalRiskAmount;
	}

	public void setPolicyRiskAmount(double policyRiskAmount) {
		//System.out.println("setPolicyRiskAmount: " + policyRiskAmount );

		this.policyRiskAmount = policyRiskAmount;
		pra_updated = true;
	}
	
	public void setTotalRiskAmount(double totalRiskAmount) {
		//System.out.println("setTotalRiskAmount: " + totalRiskAmount );

		this.totalRiskAmount = totalRiskAmount;
		tra_updated = true;
	}
	
	public double getMaximumRiskAmount() {
		return maximumRiskAmount;
	}
	
	public void setMaximumRiskAmount(double maximumRiskAmount) {
		//System.out.println("setMaximumRiskAmount: " + maximumRiskAmount );

		this.maximumRiskAmount = maximumRiskAmount;
		pra_updated = true;
	}
	
	public double getScheduledAlirAtRiskAmount() {
		return scheduledAlirAtRiskAmount;
	}
	
	public void setScheduledAlirAtRiskAmount(double scheduledAlirAtRiskAmount) {
		this.scheduledAlirAtRiskAmount = scheduledAlirAtRiskAmount;
		pra_updated = true;
	}
	
	public double getUnscheduledAlirAtRiskAmount() {
		return unscheduledAlirAtRiskAmount;
	}
	
	public void setUnscheduledAlirAtRiskAmount(double unscheduledAlirAtRiskAmount) {
		this.unscheduledAlirAtRiskAmount = unscheduledAlirAtRiskAmount;
		pra_updated = true;
	}
	
	public boolean isUpdated() {
		return pra_updated;
	}

	public void setPRAUpdated(boolean updated) {
		this.pra_updated = updated;
	}

	public void setTRAUpdated(boolean updated) {
		this.tra_updated = updated;
	}

	@Override
	public String toString() {
		return "PolicyCalculationResults [policyRiskAmount=" + policyRiskAmount + ", maximumRiskAmount="
				+ maximumRiskAmount + ", scheduledAlirAtRiskAmount=" + scheduledAlirAtRiskAmount
				+ ", unscheduledAlirAtRiskAmount=" + unscheduledAlirAtRiskAmount + ", pra_updated=" + pra_updated + "]";
	}
	
	
}

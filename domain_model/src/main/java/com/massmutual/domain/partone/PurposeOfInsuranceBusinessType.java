package com.massmutual.domain.partone;

import java.util.ArrayList;
import java.util.List;

public enum PurposeOfInsuranceBusinessType {
	
	KEY_EMPLOYEE,
	LOANGUARANTEE_COVERAGE,
	PLAN_457,
	KEOGH,
	TARGET_BENIFIT_PLAN,
	STOCK_REDEMPTION,
	K_401,
	DEFERRED_COMPENSATION,
	PROFIT_SHARING,
	PENSION_TRUST,
	CROSS_PURCHASE,
	I_412,
	ESOP,
	SPLIT_DOLLAR_ASSIGNMENT,
	SPLIT_DOLLAR_ENDORSEMENT,
	OTHER_BUSINESS; 
	
	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		for (PurposeOfInsuranceBusinessType t : values()) {
			
			names.add(t.name());
	    }
		
		return names;
	}
}

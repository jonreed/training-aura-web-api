package com.massmutual.domain.partone;

import com.massmutual.domain.RiskBase;

public class Dui extends RiskBase implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2001147129530643863L;

	private String convictedLastFiveYears;
	
	private Integer movingViolationsCount;
	
	private String suspendedLicenseDueToNonPmtChildSupport;
	
	private String movingVoilationList;

	public String getConvictedLastFiveYears() {
		return convictedLastFiveYears;
	}

	public void setConvictedLastFiveYears(String convictedLastFiveYears) {
		this.convictedLastFiveYears = convictedLastFiveYears;
	}

	public Integer getMovingViolationsCount() {
		return movingViolationsCount;
	}

	public void setMovingViolationsCount(Integer movingViolationsCount) {
		this.movingViolationsCount = movingViolationsCount;
	}

	public String getSuspendedLicenseDueToNonPmtChildSupport() {
		return suspendedLicenseDueToNonPmtChildSupport;
	}

	public void setSuspendedLicenseDueToNonPmtChildSupport(
			String suspendedLicenseDueToNonPmtChildSupport) {
		this.suspendedLicenseDueToNonPmtChildSupport = suspendedLicenseDueToNonPmtChildSupport;
	}

	public String getMovingVoilationList() {
		return movingVoilationList;
	}

	public void setMovingVoilationList(String movingVoilationList) {
		this.movingVoilationList = movingVoilationList;
	}

	@Override
	public String toString() {
		return "Dui [convictedLastFiveYears="
				+ convictedLastFiveYears + ", movingViolationsCount="
				+ movingViolationsCount
				+ ", suspendedLicenseDueToNonPmtChildSupport="
				+ suspendedLicenseDueToNonPmtChildSupport
				+ ", movingVoilationList=" + movingVoilationList + "]";
	}
	
	
	
	
	
	
	


}

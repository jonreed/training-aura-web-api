package com.massmutual.domain.partone;
import java.io.Serializable;

import com.massmutual.domain.RiskBase;


public class TemporaryLifeInsuranceReceipt extends RiskBase  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1635214307415065104L;
	
	//TLIR_Q1a1,TLIR_Q1b1,TLIR_Q21
	
	// treated for conditions
	private String question1a;
	
	private String question1b;
	
	private String question2;

	public String getQuestion1a() {
		return question1a;
	}

	public void setQuestion1a(String question1a) {
		this.question1a = question1a;
	}

	public String getQuestion1b() {
		return question1b;
	}

	public void setQuestion1b(String question1b) {
		this.question1b = question1b;
	}

	public String getQuestion2() {
		return question2;
	}

	public void setQuestion2(String question2) {
		this.question2 = question2;
	}

	@Override
	public String toString() {
		return "TemporaryLifeInsuranceReceipt [question1a=" + question1a
				+ ", question1b=" + question1b + "]";
	}
	
	

}

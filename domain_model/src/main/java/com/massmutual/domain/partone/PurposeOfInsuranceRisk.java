package com.massmutual.domain.partone;

import java.util.List;

import com.massmutual.domain.RiskBase;

public class PurposeOfInsuranceRisk extends RiskBase implements java.io.Serializable {
	
	private static final long serialVersionUID = -8818592692242538820L;
	
	//private ProposedPurposeType proposedPurpose;
	
	private String personalUse;	// Y/N
	private String businessUse; // Y/N
	
	private String collaterallyAssigned; // Y/N	
	private String economicIncentive; // Y/N
	private String plannedLifeSettlement; // Y/N
	
	private String captiveInsurer; // Y/N
	private String premFromCaptive; // Y/N
	
	// new Private fields for Purpose of Insurance
	private List<String> purposeOfInsurancePrivate;
//	private String incomeOfDependents; // Y/N
//	private String futureInsurability; // Y/N
//	private String charitableGiving; // Y/N
//	private String revocableTrust; // Y/N
//	private String estateTaxes; // Y/N
//	private String retirementPlanning; // Y/N
//	private String assetProtection; // Y/N
//	private String mortgage; // Y/N
//	private String educationFund; // Y/N
//	private String irrevocableTrust; // Y/N
//	private String otherPersonalNeeds; // free text
	
	// new fields for Business
	private List<String> purposeOfInsuranceBusiness;
//	private String keyEmployee; // Y/N
//	private String loanGuaranteeCoverage; // Y/N
//	private String plan457; // Y/N
//	private String keogh; // Y/N
//	private String targetBenifitPlan; // Y/N
//	private String stockRedemption; // Y/N
//	private String k401; // Y/N
//	private String deferredCompensation; // Y/N
//	private String profitSharing; // Y/N
//	private String pensionTrust; // Y/N
//	private String crossPurchase; // Y/N
//	private String i412; // Y/N
//	private String esop; // Y/N
//	private String splitDollarAssignment; // Y/N
//	private String splitDollarEndorsement; // Y/N
//	private String otherBusiness; // free text
		
	public List<String> getPurposeOfInsurancePrivate() {
		return purposeOfInsurancePrivate;
	}

	public void setPurposeOfInsurancePrivate(List<String> purposeOfInsurancePrivate) {
		this.purposeOfInsurancePrivate = purposeOfInsurancePrivate;
	}

	public List<String> getPurposeOfInsuranceBusiness() {
		return purposeOfInsuranceBusiness;
	}

	public void setPurposeOfInsuranceBusiness(List<String> purposeOfInsuranceBusiness) {
		this.purposeOfInsuranceBusiness = purposeOfInsuranceBusiness;
	}

	public String getPersonalUse() {
		return personalUse;
	}

	public void setPersonalUse(String personalUse) {
		this.personalUse = personalUse;
	}

	public String getBusinessUse() {
		return businessUse;
	}

	public void setBusinessUse(String businessUse) {
		this.businessUse = businessUse;
	}

	public String getCollaterallyAssigned() {
		return collaterallyAssigned;
	}

	public void setCollaterallyAssigned(String collaterallyAssigned) {
		this.collaterallyAssigned = collaterallyAssigned;
	}

	public String getEconomicIncentive() {
		return economicIncentive;
	}

	public void setEconomicIncentive(String economicIncentive) {
		this.economicIncentive = economicIncentive;
	}

	public String getPlannedLifeSettlement() {
		return plannedLifeSettlement;
	}

	public void setPlannedLifeSettlement(String plannedLifeSettlement) {
		this.plannedLifeSettlement = plannedLifeSettlement;
	}

	public String getCaptiveInsurer() {
		return captiveInsurer;
	}

	public void setCaptiveInsurer(String captiveInsurer) {
		this.captiveInsurer = captiveInsurer;
	}

	public String getPremFromCaptive() {
		return premFromCaptive;
	}

	public void setPremFromCaptive(String premFromCaptive) {
		this.premFromCaptive = premFromCaptive;
	}

	@Override
	public String toString() {
		return "PurposeOfInsuranceRisk [personalUse=" + personalUse + ", businessUse=" + businessUse
				+ ", collaterallyAssigned=" + collaterallyAssigned + ", economicIncentive=" + economicIncentive
				+ ", plannedLifeSettlement=" + plannedLifeSettlement + ", captiveInsurer=" + captiveInsurer
				+ ", premFromCaptive=" + premFromCaptive + ", purposeOfInsurancePrivate=" + purposeOfInsurancePrivate
				+ ", purposeOfInsuranceBusiness=" + purposeOfInsuranceBusiness + "]";
	}
	
}

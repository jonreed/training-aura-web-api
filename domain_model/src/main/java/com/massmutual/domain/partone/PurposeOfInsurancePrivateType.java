package com.massmutual.domain.partone;

import java.util.ArrayList;
import java.util.List;

public enum PurposeOfInsurancePrivateType {
	
	INCOME_OF_DEPENDENTS (0),
	FUTURE_INSURABILITY (0), 
	CHARITABLE_GIVING (0),
	REVOCABLE_TRUST (0),
	ESTATE_TAXES (0),
	RETIREMENT_PLANNING (0), 
	ASSET_PROTECTION (0),
	MORTGAGE (0),
	EDUCATION_FUND (0), 
	IRREVOCABLE_TRUST (0), 
	OTHER_PERSONAL_NEEDS (0);
	
	
	private int purposeOfInsuranceValue = 0;
	
	PurposeOfInsurancePrivateType(int value) {
        this.purposeOfInsuranceValue = value;

    }
	
	public int getPurposeOfInsuranceValue(){
		return purposeOfInsuranceValue;
	}
	
	public static PurposeOfInsurancePrivateType getProposedPurposeType(int value){
		
		for (PurposeOfInsurancePrivateType p : PurposeOfInsurancePrivateType.values()){
	           
			if (p.getPurposeOfInsuranceValue() == value){
				return p;
			}
	    }
		
		return null;
	}
	
	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		for (PurposeOfInsurancePrivateType t : values()) {
			
			names.add(t.name());
	    }
		
		return names;
	}
}

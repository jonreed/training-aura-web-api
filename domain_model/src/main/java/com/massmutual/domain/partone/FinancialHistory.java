package com.massmutual.domain.partone;

import java.io.Serializable;

import com.massmutual.domain.RiskBase;
import com.massmutual.domain.partone.PolicyAppliedFor;

public class FinancialHistory  extends RiskBase  implements Serializable{
	
	private static final long serialVersionUID = 5591959265913876828L;
	
	private double earnedIncomeCurrentYear;
	private double unearnedIncomeCurrentYear;
	
	private double annualHouseHoldEarnedIncome;
	private double annualHouseHoldUnEarnedIncome;
	
	private double householdNetWorth;
	private double coverageAmountOntheirSpouse;

	public double getCoverageAmountOntheirSpouse() {
		return coverageAmountOntheirSpouse;
	}

	public void setCoverageAmountOntheirSpouse(double coverageAmountOntheirSpouse) {
		this.coverageAmountOntheirSpouse = coverageAmountOntheirSpouse;
	}

	public double getEarnedIncomeCurrentYear() {
		return earnedIncomeCurrentYear;
	}

	public void setEarnedIncomeCurrentYear(double earnedIncomeCurrentYear) {
		this.earnedIncomeCurrentYear = earnedIncomeCurrentYear;
	}

	public double getUnearnedIncomeCurrentYear() {
		return unearnedIncomeCurrentYear;
	}

	public void setUnearnedIncomeCurrentYear(double unearnedIncomeCurrentYear) {
		this.unearnedIncomeCurrentYear = unearnedIncomeCurrentYear;
	}

	public double getAnnualHouseHoldEarnedIncome() {
		return annualHouseHoldEarnedIncome;
	}

	public void setAnnualHouseHoldEarnedIncome(double annualHouseHoldEarnedIncome) {
		this.annualHouseHoldEarnedIncome = annualHouseHoldEarnedIncome;
	}

	public double getAnnualHouseHoldUnEarnedIncome() {
		return annualHouseHoldUnEarnedIncome;
	}

	public void setAnnualHouseHoldUnEarnedIncome(double annualHouseHoldUnEarnedIncome) {
		this.annualHouseHoldUnEarnedIncome = annualHouseHoldUnEarnedIncome;
	}

	public double getHouseholdNetWorth() {
		return householdNetWorth;
	}

	public void setHouseholdNetWorth(double householdNetWorth) {
		this.householdNetWorth = householdNetWorth;
	}

	public double getTotalRiskAmount() {
		return earnedIncomeCurrentYear + unearnedIncomeCurrentYear;
	}

	public void setTotalRiskAmount(double totalRiskAmount) {
		// dummy for brms
	}

	@Override
	public String toString() {
		return "FinancialHistory [earnedIncomeCurrentYear="
				+ earnedIncomeCurrentYear + ", unearnedIncomeCurrentYear="
				+ unearnedIncomeCurrentYear + ", annualHouseHoldEarnedIncome="
				+ annualHouseHoldEarnedIncome
				+ ", annualHouseHoldUnEarnedIncome="
				+ annualHouseHoldUnEarnedIncome + ", householdNetWorth="
				+ householdNetWorth + ", coverageAmountOntheirSpouse="
				+ coverageAmountOntheirSpouse + "]";
	}


	
	
	
	
}

package com.massmutual.domain.partone;

public enum PremiumPayorType {
	OLI_PARTICROLE_PRIMARY(1), 
	OLI_PARTICROLE_JOINT(6), 
	OLI_PARTICROLE_OWNER(18),
	OLI_UNKNOWN(
			0);

	private int value;

	private PremiumPayorType(int value) {
		this.value = value;
	}

	public int val() {
		return this.value;
	}
}

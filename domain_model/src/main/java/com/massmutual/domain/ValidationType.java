package com.massmutual.domain;

public enum ValidationType {

	REQUIRED, ERROR, WARNING, INFO;

}

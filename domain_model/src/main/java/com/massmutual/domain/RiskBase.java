package com.massmutual.domain;

public abstract class RiskBase {
	
	//Will hold the schema version of the json - currently used for Part II/CMI only but may be expanded.
	protected String schemaVersion;	
	//Will hold the pom/build version of the application
	protected String rulesPomVersion;
	protected RiskSummaryType riskSummaryType;

	
	public RiskSummaryType getRiskSummaryType() {
		return riskSummaryType;
	}
	
	public void setRiskSummaryType(RiskSummaryType riskSummaryType) {
		this.riskSummaryType = riskSummaryType;
	}

	public String getSchemaVersion() {
		return schemaVersion;
	}

	public void setSchemaVersion(String schemaVersion) {
		this.schemaVersion = schemaVersion;
	}

	public String getRulesPomVersion() {
		return rulesPomVersion;
	}

	public void setRulesPomVersion(String rulesPomVersion) {
		this.rulesPomVersion = rulesPomVersion;
	}

	/*******************************************************************/
	/*                    BRMS HELPER METHODS                          */
	/*******************************************************************/
	
	public double getSchemaVersionAsDouble() {
		if(schemaVersion != null)
		{
			try
			{
				return Double.parseDouble(schemaVersion);
			}
			catch(Exception e){}
		}
		return 0.0;
	}
	
	public double getRulesPomVersionAsDouble() {
		if(rulesPomVersion != null)
		{
			try
			{
				return Double.parseDouble(rulesPomVersion);
			}
			catch(Exception e){}
		}
		return 0.0;
	}
	
	/**
	 * Dummy method to avoid gson/jackson issues, doesn't happen 100% of the time but better to prevent - 
	 * for every get method that is not a real property create a fake set method - do not remove or change.
	 * Note:  This get method has been out there for nearly a year and is on the exception list, but this
	 * class is now being extended by other classes so that is why I've added it to be safe rather than sorry.
	 */
	public void setSchemaVersionAsDouble(double schemaVersion){}
	
	/**
	 * Dummy method to avoid gson/jackson issues, doesn't happen 100% of the time but better to prevent - 
	 * for every get method that is not a real property create a fake set method - do not remove or change.
	 * * Note:  This get method has been out there for nearly a year and is on the exception list, but this
	 * class is now being extended by other classes so that is why I've added it to be safe rather than sorry.
	 */
	public void setRulesPomVersionAsDouble(double pomVersion){}
}

package com.massmutual.domain;

public enum RiskSummaryType {
	
	PART_I, 	// EZ App
	PART_II, 	// CMI
	RX_RULES,
	VERIFICATION,
	MVR,
	MIB, 
	BLENDER,
	FINAL;
}


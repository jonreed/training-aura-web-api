package com.massmutual.domain;

import java.util.List;

import com.massmutual.domain.RiskRatingBase;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskRatingSummary;
import com.massmutual.domain.ValidationMessage;

public class RiskResponse {

	private List<RiskRatingBase> riskRatings;
	private List<RiskRatingRollup> riskRollups;
	private List<ValidationMessage> messages;
	private List<RiskRatingSummary> summary;
	
	public List<RiskRatingBase> getRiskRatings() {
		return riskRatings;
	}
	
	public void setRiskRatings(List<RiskRatingBase> riskRatings) {
		this.riskRatings = riskRatings;
	}
	
	public List<RiskRatingRollup> getRiskRollups() {
		return riskRollups;
	}
	
	public void setRiskRollups(List<RiskRatingRollup> riskRollups) {
		this.riskRollups = riskRollups;
	}
	
	public List<ValidationMessage> getMessages() {
		return messages;
	}
	
	public void setMessages(List<ValidationMessage> messages) {
		this.messages = messages;
	}

	public List<RiskRatingSummary> getSummary() {
		return summary;
	}

	public void setSummary(List<RiskRatingSummary> summary) {
		this.summary = summary;
	}

	@Override
	public String toString() {
		return "RiskResponse [riskRatings=" + riskRatings + ", riskRollups="
				+ riskRollups + ", messages=" + messages + ", summary="
				+ summary + "]";
	}
	
}

package com.massmutual.domain;

import java.util.List;

public class ValidationMessage implements java.io.Serializable {

	static final long serialVersionUID = 1L;
	
	private String questionIdentifier;
	private RiskType riskType;	
	private String message;
	private Integer priority;
	private List<String> affectedRules;

	private ValidationType type;

	public ValidationMessage() {
		
	}

	public String getQuestionIdentifier() {
		return questionIdentifier;
	}

	public void setQuestionIdentifier(String questionIdentifier) {
		this.questionIdentifier = questionIdentifier;
	}


	public RiskType getRiskType() {
		return riskType;
	}

	public void setRiskType(RiskType riskType) {
		this.riskType = riskType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public List<String> getAffectedRules() {
		return affectedRules;
	}

	public void setAffectedRules(List<String> affectedRules) {
		this.affectedRules = affectedRules;
	}

	public ValidationType getType() {
		return type;
	}

	public void setType(ValidationType type) {
		this.type = type;
	}
	
	
}

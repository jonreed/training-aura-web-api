package com.massmutual.domain;

import java.util.Date;
import com.massmutual.domain.partone.OccupationType;

public class Insured extends RiskBase implements java.io.Serializable{

	private static final long serialVersionUID = -5519686780765714592L;
	
	private int insuredAge;	// Insurance Age
	private String occupationType;
	private String contractState;
	private String licenseState;
	private String licenseAvailable;	//Has a drivers license (Y or N)	
	//Drivers License Status as sent to us by source system, currently only concerned with 1 value ("1  VALID"), 
	//should that increase add an enum associated with this property
	private String licenseStatus;	
	private String otherGovtIdProvided; //Y/N
	private Date birthDate;
	private String gender;
	
	public int getInsuredAge() {
		return insuredAge;
	}
	public void setInsuredAge(int insuredAge) {
		this.insuredAge = insuredAge;
	}

	public String getOccupationType() {
		return occupationType;
	}
	
	public void setOccupationType(String occupationType) {
		this.occupationType = occupationType;
	}
	
	public boolean isValidOccupation() {    
        return OccupationType.getNames().contains(occupationType); 
	}
	
	public String getContractState() {
		return contractState;
	}
	
	public void setContractState(String contractState) {
		this.contractState = contractState;
	}
	
	public String getLicenseState() {
		return licenseState;
	}
	public void setLicenseState(String licenseState) {
		this.licenseState = licenseState;
	}
	public String getLicenseAvailable() {
		return licenseAvailable;
	}
	
	public void setLicenseAvailable(String licenseAvailable) {
		this.licenseAvailable = licenseAvailable;
	}
	
	public String getLicenseStatus() {
		return licenseStatus;
	}
	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
	
	public String getOtherGovtIdProvided() {
		return otherGovtIdProvided;
	}
	public void setOtherGovtIdProvided(String otherGovtIdProvided) {
		this.otherGovtIdProvided = otherGovtIdProvided;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}
	@Override
	public String toString() {
		return "Insured [insuredAge=" + insuredAge + ", occupationType="
				+ occupationType + ", contractState=" + contractState + ", licenseState=" + licenseState
				+ ", licenseAvailable=" + licenseAvailable + ", licenseStatus=" + licenseStatus
				+ ", otherGovtIdProvided=" + otherGovtIdProvided + ", birthDate=" + birthDate + ", gender=" + gender
				+ "]";
	}
}
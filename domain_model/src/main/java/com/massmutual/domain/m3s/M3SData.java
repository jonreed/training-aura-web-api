package com.massmutual.domain.m3s;

import java.io.Serializable;

public class M3SData implements Serializable {
	private static final long serialVersionUID = -4846762547271845105L;

	/** This is the value passed back from the M3S Service (HMM/Haven Mortality Model) */
	private String responseRiskClass;

	public String getResponseRiskClass() {
		return responseRiskClass;
	}

	public void setResponseRiskClass(String responseRiskClass) {
		this.responseRiskClass = responseRiskClass;
	}

	@Override
	public String toString() {
		return "M3SData [responseRiskClass=" + responseRiskClass + "]";
	}
}
package com.massmutual.domain;

public class RiskRating extends RiskRatingBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;
	
	protected String riskType;
	
	public RiskRating() {
		super();
	}
	
	public RiskRating(RiskRating copy) {
		super(copy);
		this.riskType = copy.riskType;
		
	}
	
	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	@Override
	public String toString() {
		return "RiskRating [riskType=" + riskType + "], " + super.toString();
	}	

}


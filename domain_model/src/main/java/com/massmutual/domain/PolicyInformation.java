package com.massmutual.domain;

import java.util.Date;

public class PolicyInformation extends RiskBase implements java.io.Serializable {
	private static final long serialVersionUID = 7026090891615572804L;
	
	//base face amount, currently held in the following table:
	//UW_POL_ATTR table UW_POL_FACE_AMT column
	private Double faceAmount;
	
	//See req's but very rough definition for overall understanding of value contained
	//TAR = base(face) + rtr + lisr + alir + 12 months of pcr (does not include gir), currently held in the following table:
	//UW_POL_ATTR table TOTAL_RISK_AMT column
	private Double totalAmountAtRisk;
	
	// Preferred points are considered if sum of face amount plus RTR amount exceeds 50K
	private Double renewableTermRiderAmount;
	
	//The Application Submit Date, currently held in the following table:
	//UW_POL_ATTR table SUBM_DATE column
	private Date applicationSubmitDate;
	

	public Double getFaceAmount() {
		return faceAmount;
	}

	public void setFaceAmount(Double faceAmount) {
		this.faceAmount = faceAmount;
	}
	
	public Double getTotalAmountAtRisk() {
		return totalAmountAtRisk;
	}

	public void setTotalAmountAtRisk(Double totalAmountAtRisk) {
		this.totalAmountAtRisk = totalAmountAtRisk;
	}
	
	public Double getRenewableTermRiderAmount() 
	{
		return renewableTermRiderAmount;
	}
	public void setRenewableTermRiderAmount(Double renewableTermRiderAmount) 
	{
		this.renewableTermRiderAmount = renewableTermRiderAmount;
	}
	
	public Date getApplicationSubmitDate() {
		return applicationSubmitDate;
	}

	public void setApplicationSubmitDate(Date applicationSubmitDate) {
		this.applicationSubmitDate = applicationSubmitDate;
	}

	@Override
	public String toString() {
		return "PolicyInformation [faceAmount=" + faceAmount
				+ ", totalAmountAtRisk=" + totalAmountAtRisk
				+ ", applicationSubmitDate=" + applicationSubmitDate + "]";
	}

}
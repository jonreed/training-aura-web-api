package com.massmutual.domain;

public abstract class RiskRatingBase {

		//private RiskType riskType;
		protected String description;
		protected String ruleName;
		protected boolean rollupComplete = false;
		protected RiskSummaryType riskSummaryType;
	
		//"Ultra-preferred non tobacco"
		protected int UPNT;
		
		//"Select preferred non-tobacco"
		protected int SPNT;
		
		//"Non-tobacco"
		protected int NONT;
		
		//"Select preferred tobacco"
		protected int SPT;
		
		//"Standard tobacco"
		protected int TOB;
		
		//"Waiver of Premium"
		protected int WP;
		
		//"Deny Instance of Offer"
		protected int DIIO;
		
		
		public RiskRatingBase() {
		}
		
		public RiskRatingBase(RiskRatingBase base) {

			this(base.getDescription(), base.getRuleName(), base.getRiskSummaryType(), 
				 base.getUPNT(), base.getSPNT(), base.getNONT(), 
				 base.getSPT(), base.getTOB(), base.getWP(), base.getDIIO());

		}
		
		public RiskRatingBase(String description, String ruleName, RiskSummaryType riskSummaryType,
	 			  int uPNT, int sPNT, int nONT,
	 			  int sPT, int tOB, int wP, int dIIO) {

			this.description = description;
			this.ruleName = ruleName;
			this.riskSummaryType = riskSummaryType;
			
			UPNT = uPNT;
			SPNT = sPNT;
			NONT = nONT;
			SPT = sPT;
			TOB = tOB;
			WP = wP;
			DIIO = dIIO;
		
		}
		
		public void setAllRiskClasses(int classValue) {
			UPNT = classValue;
			SPNT = classValue;
			NONT = classValue;
			SPT = classValue;
			TOB = classValue;
			WP = classValue;
			DIIO = classValue;
		}
	
		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
		
		public String getRuleName() {
			return ruleName;
		}

		public void setRuleName(String ruleName) {
			this.ruleName = ruleName;
		}
		
		public boolean isRollupComplete() {
			return rollupComplete;
		}

		public void setRollupComplete(boolean rollupComplete) {
			this.rollupComplete = rollupComplete;
		}
		
		public RiskSummaryType getRiskSummaryType() {
			return riskSummaryType;
		}

		public void setRiskSummaryType(RiskSummaryType riskSummaryType) {
			this.riskSummaryType = riskSummaryType;
		}

		public int getUPNT() {
			return UPNT;
		}

		public void setUPNT(int uPNT) {
			UPNT = uPNT;
		}

		public int getSPNT() {
			return SPNT;
		}

		public void setSPNT(int sPNT) {
			SPNT = sPNT;
		}

		public int getNONT() {
			return NONT;
		}

		public void setNONT(int nONT) {
			NONT = nONT;
		}

		public int getSPT() {
			return SPT;
		}

		public void setSPT(int sPT) {
			SPT = sPT;
		}

		public int getTOB() {
			return TOB;
		}

		public void setTOB(int tOB) {
			TOB = tOB;
		}

		public int getWP() {
			return WP;
		}

		public void setWP(int wP) {
			WP = wP;
		}

		public int getDIIO() {
			return DIIO;
		}

		public void setDIIO(int dIIO) {
			DIIO = dIIO;
		}

		@Override
		public String toString() {
			return "RiskRatingBase [description=" + description + ", ruleName="
					+ ruleName + ", rollupComplete=" + rollupComplete
					+ ", riskSummaryType=" + riskSummaryType + ", UPNT=" + UPNT
					+ ", SPNT=" + SPNT + ", NONT=" + NONT + ", SPT=" + SPT
					+ ", TOB=" + TOB + ", WP=" + WP + ", DIIO=" + DIIO + "]";
		}	
		
}

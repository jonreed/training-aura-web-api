package com.massmutual.domain;

import java.util.ArrayList;
import java.util.List;

public class RiskRatingRollup extends RiskRatingBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private String riskType;
	
	// Part one risk ratings
	private List<RiskRating> riskRatings;
	private List<RiskRating> missing;
	private List<ValidationMessage> messages;
	
	public RiskRatingRollup() {
	}
	
	public String getRiskType() {
		return riskType;
	}

	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}

	public List<RiskRating> getRiskRatings() {
		return riskRatings;
	}

	public void setRiskRatings(List<RiskRating> riskRatings) {
		this.riskRatings = riskRatings;
	}

	public List<RiskRating> getMissing() {
		return missing;
	}

	public void setMissing(List<RiskRating> missing) {
		this.missing = missing;
	}

	public List<ValidationMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<ValidationMessage> messages) {
		this.messages = messages;
	}

	public void addMessage(ValidationMessage message) {
		if (messages == null) {
			messages = new ArrayList<ValidationMessage>();
		}
		messages.add(message);
	}
	
	// need to check that none of the input values are found in the list
	public boolean getExcludeAllMessages(String valueList) {
		
		if (messages != null && valueList != null) {
			
			for (String questionIdentifier : valueList.split(",")) {
				
				for (ValidationMessage message : messages) {
				
					if (questionIdentifier.equals(message.getQuestionIdentifier())){
						return false;
					}
				}
			}
		}
		return true;		
	}

	@Override
	public String toString() {
		return "RiskRatingRollup [riskType=" + riskType + ", riskRatings=" + riskRatings + ", missing=" + missing
				+ ", messages=" + messages + "], " + super.toString();
	}

}


package com.massmutual.domain;

public class PartOneRiskRating extends RiskRating implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private PartOneRiskType partOneRiskType;
	
	public PartOneRiskRating() {
		super();
	}
		
	public PartOneRiskRating(RiskRating rating) {

		super(rating);	
		this.partOneRiskType = PartOneRiskType.valueOf(rating.getRiskType());
	
	}
	
	public PartOneRiskType getPartOneRiskType() {
		return partOneRiskType;
	}

	public void setPartOneRiskType(PartOneRiskType partOneRiskType) {
		this.partOneRiskType = partOneRiskType;
		
		if (partOneRiskType != null){
			riskType = partOneRiskType.name();
		}
	}
	
	public RiskRating createRiskRating(){
		RiskRating rating = new RiskRating((RiskRating) this);
		rating.setRiskType(String.valueOf(partOneRiskType));
		return rating;
	}

	@Override
	public String toString() {
		return "PartOneRiskRating [partOneRiskType=" + partOneRiskType + "], " + super.toString();
	}

}

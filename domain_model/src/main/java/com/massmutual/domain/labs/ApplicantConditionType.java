package com.massmutual.domain.labs;

public enum ApplicantConditionType {
	CARDIOVASCULAR_HISTORY, ANTIHYPERTENSIVE_USE, NORMAL_EKG_2YEARS, EBCT_CREDIT_2YEARS, BNPT_BELOW_125;
}

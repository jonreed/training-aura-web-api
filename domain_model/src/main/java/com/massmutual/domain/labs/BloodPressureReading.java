package com.massmutual.domain.labs;

import java.io.Serializable;

public class BloodPressureReading implements Serializable {
	private static final long serialVersionUID = -3820110175365431886L;
	
	private Integer systolic;
	private Integer diastolic;
	
	public BloodPressureReading() {
	}

	public BloodPressureReading(Integer systolic, Integer diastolic) {
		super();
		this.systolic = systolic;
		this.diastolic = diastolic;
	}

	public Integer getSystolic() {
		return systolic;
	}

	public void setSystolic(Integer systolic) {
		this.systolic = systolic;
	}

	public Integer getDiastolic() {
		return diastolic;
	}

	public void setDiastolic(Integer diastolic) {
		this.diastolic = diastolic;
	}

	@Override
	public String toString() {
		return "BloodPressureReading [systolic=" + systolic + ", diastolic="
				+ diastolic + "]";
	}

}

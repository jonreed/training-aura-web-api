package com.massmutual.domain.labs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.blender.BlenderRiskRating;

public class LabRulesSummary implements Serializable {
	private static final long serialVersionUID = 3634487443757564902L;
	
	private List<String> missingLabs = new ArrayList<String>();
	private Boolean summaryComplete;
	private Integer totalFlagCount = new Integer(0);
	private Double totalScoreValue = new Double(9999999.0);  
	private List<LabResultFlag> flags = new ArrayList<LabResultFlag>();
	private List<BlenderRiskRating> riskRatings = new ArrayList<BlenderRiskRating>();
	
	public LabRulesSummary() {
	}

	public List<String> getMissingLabs() {
		return missingLabs;
	}

	public void setMissingLabs(List<String> missingLabs) {
		this.missingLabs = missingLabs;
	}

	public Boolean getSummaryComplete() {
		return summaryComplete;
	}

	public void setSummaryComplete(Boolean summaryComplete) {
		this.summaryComplete = summaryComplete;
	}

	public Integer getTotalFlagCount() {
		return totalFlagCount;
	}

	public void setTotalFlagCount(Integer totalFlagCount) {
		this.totalFlagCount = totalFlagCount;
	}

	public Double getTotalScoreValue() {
		return totalScoreValue;
	}

	public void setTotalScoreValue(Double totalScoreValue) {
		this.totalScoreValue = totalScoreValue;
	}

	public List<LabResultFlag> getFlags() {
		return flags;
	}

	public void setFlags(List<LabResultFlag> flags) {
		this.flags = flags;
	}

	public List<BlenderRiskRating> getRiskRatings() {
		return riskRatings;
	}

	public void setRiskRatings(List<BlenderRiskRating> riskRatings) {
		this.riskRatings = riskRatings;
	}

	public static Integer convertNumber(Number value)
	{
		if(value != null)
		{
			return Integer.valueOf(value.intValue());
		}
		return new Integer(0);
	}
	
	@Override
	public String toString() {
		return "LabRulesSummary [missingLabs=" + missingLabs
				+ ", summaryComplete=" + summaryComplete + ", totalFlagCount="
				+ totalFlagCount + ", flags=" + flags + ", riskRatings="
				+ riskRatings + "]";
	}
}

package com.massmutual.domain.labs;

import java.io.Serializable;
import java.util.List;
import java.util.Date;

import com.massmutual.domain.parttwo.MedicalConditionUtil;
import com.massmutual.domain.parttwo.PartTwoRiskBase;

//Someone extended a PartTwo object - should have never done this, but now it's there and is in the json
//so need to live with this, but do NOT use this as an example and if at all possible do not populate these lower level PartTwoRiskBase properties.
public class LabSlip extends PartTwoRiskBase implements Serializable {
	private static final long serialVersionUID = -517758464938344111L;
	
	/* NOTE LabSlip is a Data Entry System - whatever is filled out on the form is entered into the system
	 * not all response combinations will make sense, so all fields must be accounted for you can't assume
	 * that yes to tobacco means that they didn't also claim to never using a nicotine product.
	 * RESPONSES TO LABSLIP QUESTIONS KEY:  
	 * Y = XML VALUE OF "YES" OR "TRUE"				
	 * N = XML VALUE OF "NO" OR "FALSE"				
	 * NOT SUPPLIED = XML VALUE OF "NOT SUPPLIED" 	
	 * NULL = NO TAG - NOT INCLUDED IN THE XML OR 1.0 VERSION OF THE FILE */
	
	/* LabSlip Question:  "Do you use tobacco in any form?"
	 *Possible Responses:  Y/N/NOT SUPPLIED or NULL for old Lab File that does not include this information */
	private String tobaccoUse;	
	
	/* LabSlip Question:  "If YES, what type of product(s) have you used?" 
	 * Possible Responses:  Can include more than one TobaccoProductType from below:
	 * Cigarette Y/NOT SUPPLIED 
	 * Cigar Y/NOT SUPPLIED 
	 * Pipe Y/NOT SUPPLIED 
	 * Smokeless Y/NOT SUPPLIED 
	 * or NULL for old Lab File that does not include this information */
	private List<String> typeOfNicotineProducts;
	
	//Below is separated out because technically they could respond to both.
	/* LabSlip Question:  "If NO how long since you last used any form of tobacco or nicotine?" 
	 * Possible Choices:  Number of Months:
	 * Possible Responses:  Numeric (number of months) or NULL for new file that return NOT SUPPLIED or 
	 * for old Lab File that does not include this information */
	private Integer smokingDuration;	
			
	/* LabSlip Question:  "If NO how long since you last used any form of tobacco or nicotine?" 
	 * Possible Choices:  Never Used Tobacco:
	 * Possible Responses:  Y/NOT SUPPLIED
	 * or NULL for old Lab File that does not include this information */
	private String neverUsedNicotine;	
	
	/* LabSlip Question:  "Are you currently using any type of nicotine delivery system (gum, patch, nasal spray, etc)?"
	 *Possible Responses:  Y/N/NOT SUPPLIED or NULL for old Lab File that does not include this information */
	private String useNicotineDeliverySystem;	
	
	/* LabSlip Question:  "Personal History of Diabetes?"
	 *Possible Responses:  Y/N/NOT SUPPLIED or NULL for old Lab File that does not include this information */
	private String historyOfDiabetes;	
	
	/* LabSlip Question:  "Personal History of Heart Disease?"
	 *Possible Responses:  Y/N/NOT SUPPLIED or NULL for old Lab File that does not include this information */
	private String historyOfHeartDisease;	
	
	/* LabSlip Question:  "Personal History of High Blood Pressure?"
	 *Possible Responses:  Y/N/NOT SUPPLIED or NULL for old Lab File that does not include this information */
	private String historyOfHighBloodPressure;	
	
	/* LabSlip Question:  "Personal History of Cancer?"
	 *Possible Responses:  Y/N/NOT SUPPLIED or NULL for old Lab File that does not include this information */
	private String historyOfCancer;	
	
	/* LabSlip Question:  "Are you currently taking ANY prescription drug, vitamins, or over-the-counter medications?"
	 *Possible Responses:  Y/N/NOT SUPPLIED or NULL for old Lab File that does not include this information */
	private String takingPrescriptionDrugs;	
	
	/* LabSlip Question:  "In the past 5 years have you had more than 2 moving violations?"
	 *Possible Responses:  Y/N or NULL for old Lab File that does not include this information or new Lab File that does not include this information */
	private String movingViolations;	
	
	/* LabSlip Question:  "Currently menses?"
	 *Possible Responses:  Y/N business wants this defaulted to N (being handled by the front end), 
	 *						should never be NULL even for an old lab file
	 *Note:  Raw CRL actually returns True/False but front end convert for consistency */
	private String currentlyMenses;	
	
	//Note:  For LabSlip Question:  "Currently pregnant?" please see parent object Applicant, it was set at that level long before labslip was supported and set by another source, so it will remain at that level.
	
	//Note:  The below data was put in the labslip object but is actually part of physical measurements - part of the json used in rules can't be moved.
	private double heightFeet = 0.0;
	private double heightInches = 0.0;
	private double weightPounds = 0.0;
	
	//OBSOLETE, never used but in the json so can't remove
	private double policyAmount = 0.0;
	
	
	public String getTobaccoUse() {
		return tobaccoUse;
	}
	
	public void setTobaccoUse(String tobaccoUse) {
		this.tobaccoUse = tobaccoUse;
	}
	
	public List<String> getTypeOfNicotineProducts() {
		return typeOfNicotineProducts;
	}
	
	public void setTypeOfNicotineProducts(List<String> typeOfNicotineProducts) {
		this.typeOfNicotineProducts = typeOfNicotineProducts;
	}
	
	public Integer getSmokingDuration() {
		return smokingDuration;
	}
			
	public void setSmokingDuration(Integer smokingDuration) {
		this.smokingDuration = smokingDuration;
	}
		
	public String getNeverUsedNicotine() {
		return neverUsedNicotine;
	}

	public void setNeverUsedNicotine(String neverUsedNicotine) {
		this.neverUsedNicotine = neverUsedNicotine;
	}
	
	public String getUseNicotineDeliverySystem() {
		return useNicotineDeliverySystem;
	}
	
	public void setUseNicotineDeliverySystem(String useNicotineDeliverySystem) {
		this.useNicotineDeliverySystem = useNicotineDeliverySystem;
	}
	
	public String getHistoryOfDiabetes() {
		return historyOfDiabetes;
	}

	public void setHistoryOfDiabetes(String historyOfDiabetes) {
		this.historyOfDiabetes = historyOfDiabetes;
	}

	public String getHistoryOfHeartDisease() {
		return historyOfHeartDisease;
	}

	public void setHistoryOfHeartDisease(String historyOfHeartDisease) {
		this.historyOfHeartDisease = historyOfHeartDisease;
	}

	public String getHistoryOfHighBloodPressure() {
		return historyOfHighBloodPressure;
	}

	public void setHistoryOfHighBloodPressure(String historyOfHighBloodPressure) {
		this.historyOfHighBloodPressure = historyOfHighBloodPressure;
	}

	public String getHistoryOfCancer() {
		return historyOfCancer;
	}

	public void setHistoryOfCancer(String historyOfCancer) {
		this.historyOfCancer = historyOfCancer;
	}

	public String getMovingViolations() {
		return movingViolations;
	}
	
	public void setMovingViolations(String movingViolations) {
		this.movingViolations = movingViolations;
	}
	
	public String getTakingPrescriptionDrugs() {
		return takingPrescriptionDrugs;
	}

	public void setTakingPrescriptionDrugs(String takingPrescriptionDrugs) {
		this.takingPrescriptionDrugs = takingPrescriptionDrugs;
	}
	
	public String getCurrentlyMenses() {
		return currentlyMenses;
	}

	public void setCurrentlyMenses(String currentlyMenses) {
		this.currentlyMenses = currentlyMenses;
	}
	
	public double getHeightFeet() {
		return heightFeet;
	}

	public void setHeightFeet(double heightFeet) {
		this.heightFeet = heightFeet;
	}

	public double getHeightInches() {
		return heightInches;
	}

	public void setHeightInches(double heightInches) {
		this.heightInches = heightInches;
	}

	public double getWeightPounds() {
		return weightPounds;
	}

	public void setWeightPounds(double weightPounds) {
		this.weightPounds = weightPounds;
	}
	
	/*******************************************************************/
	/*                    BRMS HELPER METHODS                          */
	/*******************************************************************/
	
	// Will return total height in inches
	public double getHeight() {
		return heightFeet*12 + heightInches;
	}
		
	/**
	 * Dummy method to avoid gson/jackson issues, doesn't happen 100% of the time but better to prevent - 
	 * for every get method that is not a real property create a fake set method - do not remove or change.
	 */
	public void setHeight(double height){}

	/**
	 * Helper method used by the brms rules which calculates the BMI overloaded from regular BMI
	 * where it allows an additional value (weightPoundsLost) to be passed in as part of the equation.
	 * * Calculation:
	 * 	(weight + (weightPoundsLoss/2) / [height]2) x 703
	 * @param heightFeet
	 * @param heightInches
	 * @param weightPounds
	 * @param weightPoundsLost
	 * @return
	 */
	public double calculateBMIWeightLoss(double weightChangePounds)
	{
		return MedicalConditionUtil.calculateBMIWeightLost(heightFeet, heightInches, weightPounds, weightChangePounds);
	}
	
	@Override
	public String toString() {
		return "LabSlip [tobaccoUse=" + tobaccoUse
				+ ", typeOfNicotineProducts=" + typeOfNicotineProducts
				+ ", smokingDuration=" + smokingDuration
				+ ", neverUsedNicotine=" + neverUsedNicotine
				+ ", useNicotineDeliverySystem=" + useNicotineDeliverySystem
				+ ", historyOfDiabetes=" + historyOfDiabetes
				+ ", historyOfHeartDisease=" + historyOfHeartDisease
				+ ", historyOfHighBloodPressure=" + historyOfHighBloodPressure
				+ ", historyOfCancer=" + historyOfCancer
				+ ", movingViolations=" + movingViolations
				+ ", currentlyMenses=" + currentlyMenses
				+ ", takingPrescriptionDrugs=" + takingPrescriptionDrugs
				+ ", heightFeet=" + heightFeet + ", heightInches="
				+ heightInches + ", weightPounds=" + weightPounds
				+ ", policyAmount=" + policyAmount + "]";
	}

	/*******************************************************************************************************/
	/*                        OBSOLETE METHODS FOR OBSOLETE PROPERTIES THAT ARE NOT						   */
	/*                        BEING USED BUT CAN'T BE REMOVED AS THEY ARE IN THE JSON 					   */
	/*******************************************************************************************************/
	
	//OBSOLETE, never used but in the json so can't remove
	public double getPolicyAmount() {
		return policyAmount;
	}

	//OBSOLETE, never used but in the json so can't remove
	public void setPolicyAmount(double policyAmount) {
		this.policyAmount = policyAmount;
	}

}

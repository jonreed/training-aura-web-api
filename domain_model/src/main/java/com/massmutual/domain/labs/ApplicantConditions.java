package com.massmutual.domain.labs;

import java.io.Serializable;

public class ApplicantConditions implements Serializable {
	private static final long serialVersionUID = -1010068836385792561L;
	
	private Boolean cardiovascularParentsHistory;
	private Boolean antihypertensiveUse;
	private Boolean normalEKGLast2Years;
	private Boolean ebctCreditLast2Years;
	private Boolean bnptBelow125;
	
	public ApplicantConditions() {
	}

	public ApplicantConditions(Boolean cardiovascularParentsHistory,
			Boolean antihypertensiveUse, Boolean normalEKGLast2Years,
			Boolean ebctCreditLast2Years, Boolean bnptBelow125) {
		super();
		this.cardiovascularParentsHistory = cardiovascularParentsHistory;
		this.antihypertensiveUse = antihypertensiveUse;
		this.normalEKGLast2Years = normalEKGLast2Years;
		this.ebctCreditLast2Years = ebctCreditLast2Years;
		this.bnptBelow125 = bnptBelow125;
	}

	public Boolean getCardiovascularParentsHistory() {
		return cardiovascularParentsHistory;
	}

	public void setCardiovascularParentsHistory(Boolean cardiovascularParentsHistory) {
		this.cardiovascularParentsHistory = cardiovascularParentsHistory;
	}

	public Boolean getAntihypertensiveUse() {
		return antihypertensiveUse;
	}

	public void setAntihypertensiveUse(Boolean antihypertensiveUse) {
		this.antihypertensiveUse = antihypertensiveUse;
	}

	public Boolean getNormalEKGLast2Years() {
		return normalEKGLast2Years;
	}

	public void setNormalEKGLast2Years(Boolean normalEKGLast2Years) {
		this.normalEKGLast2Years = normalEKGLast2Years;
	}

	public Boolean getEbctCreditLast2Years() {
		return ebctCreditLast2Years;
	}

	public void setEbctCreditLast2Years(Boolean ebctCreditLast2Years) {
		this.ebctCreditLast2Years = ebctCreditLast2Years;
	}

	public Boolean getBnptBelow125() {
		return bnptBelow125;
	}

	public void setBnptBelow125(Boolean bnptBelow125) {
		this.bnptBelow125 = bnptBelow125;
	}

	@Override
	public String toString() {
		return "ApplicantConditions [cardiovascularParentsHistory="
				+ cardiovascularParentsHistory + ", antihypertensiveUse="
				+ antihypertensiveUse + ", normalEKGLast2Years="
				+ normalEKGLast2Years + ", ebctCreditLast2Years="
				+ ebctCreditLast2Years + ", bnptBelow125=" + bnptBelow125 + "]";
	}

}

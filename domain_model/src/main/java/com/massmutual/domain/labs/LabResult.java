package com.massmutual.domain.labs;

import java.io.Serializable;
import java.math.BigDecimal;

public class LabResult implements Serializable {
	private static final long serialVersionUID = -517758464938344111L;

	private LabResultType resultType;
	private String score;
	private String result;
	
	private final double NaN = -333.0;
	
	private BigDecimal scoreValue = BigDecimal.ZERO;
	private BigDecimal resultValue = BigDecimal.ZERO;

	public LabResult() {
	}

	public LabResult(LabResultType resultType, String score, String result) {
		super();
		this.resultType = resultType;
		this.score = score;
		this.result = result;
	}

	public LabResultType getResultType() {
		return resultType;
	}

	public void setResultType(LabResultType resultType) {
		this.resultType = resultType;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public Double getResultDoubleValue() {
		
		try {
			if (result != null && !"".equals(result)) {
				
				this.resultValue = new BigDecimal(result);
				return resultValue.doubleValue();
			}
		} catch (Exception swallow) {
			System.err.println("Invalid number for result["+resultType+"][" + result + "], Error: " + swallow.getMessage());
			//swallow.printStackTrace(System.out);
		}
		
		return NaN;
	}
	
	public String getResultIsANumber()  {
		
		if (getResultDoubleValue() == NaN){
			return "N";
		}
		
		return "Y";
	}

	public Double getScoreDoubleValue()  {
		try {
			if (score != null && !"".equals(score)) {
				this.scoreValue = new BigDecimal(score);
				return scoreValue.doubleValue();
			}
		} catch (Exception swallow) {
		}
		
		return NaN;
	}
	
	public String getScoreIsANumber()  {
		
		if (getScoreDoubleValue() == NaN){
			return "N";
		}
		
		return "Y";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((resultType == null) ? 0 : resultType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LabResult other = (LabResult) obj;
		if (resultType != other.resultType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LabResult [resultType=" + resultType + ", score=" + score
				+ ", result=" + result + "]";
	}

}

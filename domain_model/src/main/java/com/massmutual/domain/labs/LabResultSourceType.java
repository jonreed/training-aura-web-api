package com.massmutual.domain.labs;

//Represents the Lab File Types - what type of Lab Results the file contains.
public enum LabResultSourceType {
	BLOOD,
	BLOOD_URN,
	ORAL,
	PHYS,
	URINE;
}

package com.massmutual.domain.labs;

import java.io.Serializable;

public class LabResultFlag implements Serializable {
	private static final long serialVersionUID = -3674609122385437757L;

	private LabResultType resultType;
	private String description;
	private String ruleName;
	
	public LabResultFlag() {
	}

	public LabResultFlag(LabResultType resultType, String description,
			String ruleName) {
		super();
		this.resultType = resultType;
		this.description = description;
		this.ruleName = ruleName;
	}

	public LabResultType getResultType() {
		return resultType;
	}

	public void setResultType(LabResultType resultType) {
		this.resultType = resultType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	
	public void setStrResultType(String type) {
		this.resultType = LabResultType.valueOf(type);
	}

	@Override
	public String toString() {
		return "LabResultFlag [resultType=" + resultType + ", description="
				+ description + ", ruleName=" + ruleName + "]";
	}
	
}

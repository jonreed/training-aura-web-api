package com.massmutual.domain.labs;

import java.io.Serializable;

/**
 * Marker class for whether a particular Lab Type is required.  
 * These are currently generated inside the Rule Engine 
 * for labs, but could be refactored to come from other source
 * 
 * @author kspokas
 *
 */
public class LabRequirements implements Serializable {
	private static final long serialVersionUID = 8683473054955305363L;

	private LabResultType labType;
	private Boolean required;
	private Boolean scoreMustBeANumber;
	
	public LabRequirements() {
	}
	
	public LabRequirements(LabResultType labType, Boolean required) {
		super();
		this.labType = labType;
		this.required = required;
	}

	public LabResultType getLabType() {
		return labType;
	}

	public void setLabType(LabResultType labType) {
		this.labType = labType;
	}

	public Boolean getRequired() {
		return required;
	}

	public void setRequired(Boolean required) {
		this.required = required;
	}

	public Boolean getScoreMustBeANumber() {
		return scoreMustBeANumber;
	}

	public void setScoreMustBeANumber(Boolean scoreMustBeANumber) {
		this.scoreMustBeANumber = scoreMustBeANumber;
	}

	@Override
	public String toString() {
		return "LabRequirements [labType=" + labType + ", required=" + required + ", scoreMustBeANumber="
				+ scoreMustBeANumber + "]";
	}

	
	
}

package com.massmutual.domain.labs;

import java.util.ArrayList;
import java.util.List;

public enum TobaccoProductType {

	
	CIGARETTE,
	CIGAR,
	PIPE,
	SMOKELESS;
	
	
	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (TobaccoProductType t : values()) {
			
				names.add(t.name());
	    }
		
		return names;
	}
	
}

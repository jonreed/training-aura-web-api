package com.massmutual.domain.labs;

public enum LabResultType {
	ALBUMIN,
	ALKALINE_PHOSPHATASE,
	BLOOD_ALCOHOL,
	BLOOD_PRESSURE,
	BMI,
	BUN,
	CDT,
	CEA,
	CHOL_HDL_RATIO,
	CHOLESTEROL,
	COCAINE_METABOLITES,
	CREATININE,
	DEMOGRAPHIC_ADJUSTMENT,
	EGFR,
	FRUCTOSAMINE,
	GGT,
	GLOBULIN,
	HBA1C,
	HBV,
	HCV,
	HDL,
	HEIGHT,
	HEMOGLOBIN,
	HGBA1C,
	HIV,
	LABSLIP_MENSES,		//Do not remove - used for naming only - special lab rule on labslip data, value is not in json as labresult, it is on labslip object, but output if rule triggers will use this name
	METHAMPHETAMINE,
	MICROALBUMIN_CREATININE,
	NT_PROBNP,
	ORAL_SALIVA_COCAINE,
	ORAL_SALIVA_HIV,
	ORAL_SALIVA_NICOTINE,
	ORAL_SALIVA_QUALIFIER,
	PSA,
	PULSE_IRREGULAR_REST,
	PULSE_STANDARD_REST,
	RED_BLOOD_COUNT,
	SGOT_AST,
	SGPT_ALT,
	TOTAL_BILIRUBIN,
	TOTAL_SCORE,
	TRIGLYCERIDES,
	URINE_BLOOD,
	URINE_CREATININE,
	URN_PC_RATIO,
	URN_NICOTINE,
	WEIGHT;
}

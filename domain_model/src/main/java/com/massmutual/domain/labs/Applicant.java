package com.massmutual.domain.labs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.massmutual.domain.PolicyInformation;
import com.massmutual.domain.RiskBase;
import com.massmutual.domain.RulesUtil;

public class Applicant extends RiskBase implements Serializable {
	private static final long serialVersionUID = 8920302649226917900L;

	/* Represents the date the file was created by CRL (source system) and sent to us 
	 * may not be needed, will likely use version number but might be useful to save */
	//private Date fileCreationDate;
	
	/* Represents the date (yyyy-MM-dd) that the labs were performed and these questions were asked by an examiner */
	private Date examDate;
	
	private Long id;
	private String sex;
	//Primary Insured's Insurance Age (determined by the CalcInsuranceAge service), currently held in the following table:
	//UW_POL table UW_POL_INS_AGE column
	private Integer age;
	private Date birthDate;
	
	//Is already at this level as being retrieved from another source (CMI), we will now be getting this information at the LabSlip
	//level on the CRL file - however for old files it will continue to be retreived from CMI - rules are written against this level
	//and with it being set by multiple sources will leave it here rather than having it be marked obsolete and calling at the labslip level.
	//Possible values Y or N
	private String currentlyPregnant;
	
	private ApplicantConditions conditions;
	private List<LabResult> labResults = new ArrayList<LabResult>();
	private LabSlip labSlip;
	//type of labs collected, ex: Oral, Blood
	private LabResultSourceType labResultSourceType;
	
	//Add policyInformation and use that shared object to hold the base policy information that this object needs,
	//other properties that were added here prior to this shared objects creation will be marked obsolete so that all new code uses this
	//object/property - old code will remain for backward compatibility on old json for complex blades.
	private PolicyInformation policyInformation;
	
	//OBSOLETE, use PolicyInformation totalAmountAtRisk (TAR) in the future, can't remove from here however as included in json
	//base(face) + rtr + lisr + alir (does not include gir or pcr)
	private Double totalRiskAmount;
	
	//OBSOLETE, use PolicyInformation faceAmount in the future, can't remove from here however as included in json
	private Double faceAmount;

	public Applicant() {
	}

	public Applicant(Long id, String sex, Integer age) {
		super();
		this.id = id;
		this.sex = sex;
		this.age = age;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getCurrentlyPregnant() {
		return currentlyPregnant;
	}

	public void setCurrentlyPregnant(String currentlyPregnant) {
		this.currentlyPregnant = currentlyPregnant;
	}

	public ApplicantConditions getConditions() {
		return conditions;
	}

	public void setConditions(ApplicantConditions conditions) {
		this.conditions = conditions;
	}
	
	public List<LabResult> getLabResults() {
		return labResults;
	}

	public void setLabResults(List<LabResult> labResults) {
		this.labResults = labResults;
	}

	public LabSlip getLabSlip() {
		return labSlip;
	}

	public void setLabSlip(LabSlip labSlip) {
		this.labSlip = labSlip;
	}

	public LabResultSourceType getLabResultSourceType() {
		return labResultSourceType;
	}

	public void setLabResultSourceType(LabResultSourceType labResultSourceType) {
		this.labResultSourceType = labResultSourceType;
	}

	public PolicyInformation getPolicyInformation() {
		return policyInformation;
	}

	public void setPolicyInformation(PolicyInformation policyInformation) {
		this.policyInformation = policyInformation;
	}


	@Override
	public String toString() {
		return "Applicant [examDate=" + examDate + ", id=" + id + ", sex="
				+ sex + ", age=" + age + ", birthDate=" + birthDate
				+ ", currentlyPregnant=" + currentlyPregnant + ", conditions="
				+ conditions + ", labResults=" + labResults + ", labSlip="
				+ labSlip + ", labResultSourceType=" + labResultSourceType
				+ ", policyInformation=" + policyInformation
				+ ", totalRiskAmount=" + totalRiskAmount + ", faceAmount="
				+ faceAmount + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Applicant other = (Applicant) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	/*******************************************************************/
	/*                    BRMS HELPER METHODS                          */
	/*******************************************************************/

	/**
	 * Returns true only if labResultSourceType is NOT null and the value is ORAL, returns
	 * false at all other times.
	 * @return Boolean
	 */
	public Boolean getLabSourceIsOral()
	{
		if(labResultSourceType != null && labResultSourceType.equals(LabResultSourceType.ORAL))
		{
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Dummy method to avoid jackson issues, doesn't happen 100% of the time but better to prevent - 
	 * for every get method that is not a real property create a fake set method - do not remove or change.
	 */
	public void setLabSourceIsOral(Boolean labSource){}
	
	/**
	 * Returns true ONLY if faceAmount is not null and face amount plus RTR amount is less than 50,000,
	 * @return Boolean
	 */
	public Boolean getCaseIsLowTotalRiskAmt() {
		Double highRiskAmt = new Double(50000);
		Double faceAmt = getFaceAmount();
		
		//Intentionally calling getFaceAmount() as opposed to using the property directly or calling 
		//PolicyInformation.getFaceAmount for those cases that are in progress where the json has already
		//been created for the labs against old code and we need to continue to support that for the complex
		//blades that pull in that information (verify & blender/rlgo_blndr)
		if(faceAmt != null) {
			Double rtrAmt;
			if(policyInformation == null || policyInformation.getRenewableTermRiderAmount() == null) {
			    rtrAmt = new Double(0.0);
			}
			else {
			    rtrAmt = policyInformation.getRenewableTermRiderAmount();
			}

			if (Double.compare(faceAmt.doubleValue() + rtrAmt.doubleValue(), highRiskAmt) < 0) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Dummy method to avoid jackson issues, doesn't happen 100% of the time but better to prevent - 
	 * for every get method that is not a real property create a fake set method - do not remove or change.
	 */
	public void setCaseIsLowTotalRiskAmt(Boolean value){}

	/**
	 * cmiSigDate - examDate, difference in days is returned.
	 * 
	 * @param date - represents the CMI Signature Date
	 * @return Long, difference in days between the Applicant Lab Exam Date and the CMI Signature Date (positive or negative)
	 */
	public Long calculateDiffInDays(Date cmiSigDate)
	{	
		//below method order is startDate, endDate and calculation is endDate - startDate, 
		//examDate is startDate
		//cmiSignatureDate is endDate
		//if cmiSignatureDate is after examDate we will have a positive number returned.
		return RulesUtil.calculateDiffInDays(examDate, cmiSigDate);
	}
	
	/*******************************************************************************************************/
	/*                        OBSOLETE METHODS FOR OBSOLETE PROPERTIES THAT ARE NOT						   */
	/*                        BEING USED BUT CAN'T BE REMOVED AS THEY ARE IN THE JSON 					   */
	/*******************************************************************************************************/
	
	//OBSOLETE, call PolicyInformation totalAmountAtRisk instead, in the json so can't remove
	public Double getTotalRiskAmount() {
		//To support json generated by old code or if any brms code is missed that is still calling at this
		//level - check if we have a value here first, if we don't then call PolicyInformation and return that value.
		if(totalRiskAmount != null)
		{
			return totalRiskAmount;
		}
		else if(policyInformation != null)
		{
			return policyInformation.getTotalAmountAtRisk();
		}
		return null;
	}

	//OBSOLETE, call PolicyInformation totalAmountAtRisk instead, in the json so can't remove
	public void setTotalRiskAmount(Double totalRiskAmount) {
		this.totalRiskAmount = totalRiskAmount;
	}

	//OBSOLETE, call PolicyInformation faceAmount instead, in the json so can't remove
	public Double getFaceAmount() {
		//To support json generated by old code or if any brms code is missed that is still calling at this
		//level - check if we have a value here first, if we don't then call PolicyInformation and return that value.
		if(faceAmount != null)
		{
			return faceAmount;
		}
		else if(policyInformation != null)
		{
			return policyInformation.getFaceAmount();
		}
		return null;
	}

	//OBSOLETE, call PolicyInformation faceAmount instead, in the json so can't remove
	public void setFaceAmount(Double faceAmount) {
		this.faceAmount = faceAmount;
	}
}

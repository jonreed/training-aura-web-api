package com.massmutual.domain.blender;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.RiskRatingSummary;
import com.massmutual.domain.labs.Applicant;
import com.massmutual.domain.labs.LabResultFlag;
import com.massmutual.domain.m3s.M3SData;

public class FinalRollupInput implements Serializable {
	private static final long serialVersionUID = -887610372030900858L;
	
	private Applicant applicant;
	private M3SData m3sData;

	// Part I, Part II, MVR, MIB, RX, Verification
	private List<RiskRatingSummary> ratingSummaries = new ArrayList<RiskRatingSummary>();
	private List<BlenderRiskRating> labRatings = new ArrayList<BlenderRiskRating>();	// labs
	private List<LabResultFlag> labFlags = new ArrayList<LabResultFlag>();	// labs

	public FinalRollupInput() {
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	public M3SData getM3sData() {
		return m3sData;
	}

	public void setM3sData(M3SData m3sData) {
		this.m3sData = m3sData;
	}

	public List<RiskRatingSummary> getRatingSummaries() {
		return ratingSummaries;
	}

	public void setRatingSummaries(List<RiskRatingSummary> ratingSummaries) {
		this.ratingSummaries = ratingSummaries;
	}
	
	public void addRatingSummary(RiskRatingSummary summary) {
		
		ratingSummaries.add(summary);
	}

	public List<LabResultFlag> getLabFlags() {
		return labFlags;
	}

	public void setLabFlags(List<LabResultFlag> labFlags) {
		this.labFlags = labFlags;
	}

	public List<BlenderRiskRating> getLabRatings() {
		return labRatings;
	}

	public void setLabRatings(List<BlenderRiskRating> labRatings) {
		this.labRatings = labRatings;
	}
	
	@Override
	public String toString() {
		return "FinalRollupInput [applicant=" + applicant + ", m3sData=" + m3sData + ", ratingSummaries="
				+ ratingSummaries + ", labRatings=" + labRatings + ", labFlags=" + labFlags + "]";
	}	
}
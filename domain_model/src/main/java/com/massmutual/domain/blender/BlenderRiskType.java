package com.massmutual.domain.blender;

import com.massmutual.domain.RiskType;

public enum BlenderRiskType implements RiskType {

	AVIATION,
	AVOCATION,
	BNPT,
	CARDIOVASCULAR,
	DUI,
	EBCT,
	EKG,
	FINAL,
	LAB_FLAGS,
	LAB_POINTS,
	MOVING_VIOLATIONS,
	M3S,
	NICOTINE_USE,
	OCCUPATION,
	PREFERRED_POINTS,
	RX_USE;
	
}

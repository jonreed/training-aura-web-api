package com.massmutual.domain.blender;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FinalRollupResult implements Serializable {
	private static final long serialVersionUID = 2948777591217058564L;

	private List<String> missingConditions = new ArrayList<String>();
	private Boolean rollupComplete;
	private BlenderRiskRating preferredPointRating;
	private List<BlenderRiskRating> otherRatings = new ArrayList<BlenderRiskRating>();
	private BlenderRiskRating finalRating;
	/** This is the offer/recommendation based on the highest green(1) or yellow(2)*/
	private String finalResult;
	
	public FinalRollupResult() {
	}

	public List<String> getMissingConditions() {
		return missingConditions;
	}

	public void setMissingConditions(List<String> missingConditions) {
		this.missingConditions = missingConditions;
	}
	
	public Boolean getRollupComplete() {
		return rollupComplete;
	}

	public void setRollupComplete(Boolean rollupComplete) {
		this.rollupComplete = rollupComplete;
	}

	public BlenderRiskRating getPreferredPointRating() {
		return preferredPointRating;
	}

	public void setPreferredPointRating(BlenderRiskRating preferredPointRating) {
		this.preferredPointRating = preferredPointRating;
	}

	public List<BlenderRiskRating> getOtherRatings() {
		return otherRatings;
	}

	public void setOtherRatings(List<BlenderRiskRating> otherRatings) {
		this.otherRatings = otherRatings;
	}

	public BlenderRiskRating getFinalRating() {
		return finalRating;
	}

	public void setFinalRating(BlenderRiskRating finalRating) {
		this.finalRating = finalRating;
	}

	public String getFinalResult() {
		return finalResult;
	}

	public void setFinalResult(String finalResult) {
		this.finalResult = finalResult;
	}

	
	@Override
	public String toString() {
		return "FinalRollupResult [missingConditions=" + missingConditions + ", rollupComplete=" + rollupComplete
				+ ", preferredPointRating=" + preferredPointRating + ", otherRatings=" + otherRatings + ", finalRating="
				+ finalRating + ", finalResult=" + finalResult + "]";
	}
}
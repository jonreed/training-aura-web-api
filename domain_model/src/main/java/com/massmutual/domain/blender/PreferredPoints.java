package com.massmutual.domain.blender;

import java.io.Serializable;

public class PreferredPoints implements Serializable {
	private static final long serialVersionUID = -3338404303610940064L;
	private int pointCount = 0;
	
	public PreferredPoints() {
	}
	
	public PreferredPoints(int pointCount) {
		super();
		this.pointCount = pointCount;
	}

	public int getPointCount() {
		return pointCount;
	}

	public void setPointCount(int pointCount) {
		this.pointCount = pointCount;
	}

	@Override
	public String toString() {
		return "PreferredPoints [pointCount=" + pointCount + "]";
	}
}

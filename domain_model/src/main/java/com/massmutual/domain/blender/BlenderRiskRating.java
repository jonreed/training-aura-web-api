package com.massmutual.domain.blender;

import java.io.Serializable;

import com.massmutual.domain.RiskRating;
import com.massmutual.domain.RiskRatingBase;
import com.massmutual.domain.RiskType;

public class BlenderRiskRating extends RiskRating implements Serializable {
	private static final long serialVersionUID = -1906208153396014442L;

	private BlenderRiskType blenderRiskType;
	private Integer preferredPoints = 0;
	
	public Integer getPreferredPoints() {
		return preferredPoints;
	}

	public void setPreferredPoints(Integer preferredPoints) {
		this.preferredPoints = preferredPoints;
	}

	public BlenderRiskType getBlenderRiskType() {
		return blenderRiskType;
	}

	public void setBlenderRiskType(BlenderRiskType blenderRiskType) {
		this.blenderRiskType = blenderRiskType;
		if (blenderRiskType != null){
			riskType = blenderRiskType.name();
		}
		
	}

	@Override
	public String toString() {
		return "BlenderRiskRating [blenderRiskType=" + blenderRiskType + ", preferredPoints=" + preferredPoints + "], " + super.toString();
	}

}

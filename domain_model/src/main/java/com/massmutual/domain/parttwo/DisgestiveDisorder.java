package com.massmutual.domain.parttwo;

import com.massmutual.domain.RiskBase;

public class DisgestiveDisorder extends PartTwoRiskBase  implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "DisgestiveDisorder [" + super.toString() + "]";
	}
	
	
	
//	private String hadDDAnorectalFistula;
//	private String hadDDCeliacDisease;
//
//	private String hadDDColonPolyp;
//	private String hadDDChrons;
//	private String hadDDColitusNonUlcerative;
//	private String hadDDColitusUlcerative;
//	private String hadDDiverticulitis;
//	private String hadDDDLast6Months;
//	private String hadDDNAFLD;
//	private String hadDDLiverBiopsy;
//	private String hadDDGastritis;
//	private String hadDDHepititis;
//	private String hadDDPancreaticInflamation;
//	private String hadDDPancreatitis;
//	private String hadDDUlcer;
//	private String hadDDURecovered;
//	private String hadDDWeightLossSurgery;
//	private String hadDDBenign;
//	private String hadBDNotSure;
//	public String getHadDDAnorectalFistula() {
//		return hadDDAnorectalFistula;
//	}
//	public void setHadDDAnorectalFistula(String hadDDAnorectalFistula) {
//		this.hadDDAnorectalFistula = hadDDAnorectalFistula;
//	}
//	public String getHadDDCeliacDisease() {
//		return hadDDCeliacDisease;
//	}
//	public void setHadDDCeliacDisease(String hadDDCeliacDisease) {
//		this.hadDDCeliacDisease = hadDDCeliacDisease;
//	}
//	public String getHadDDColonPolyp() {
//		return hadDDColonPolyp;
//	}
//	public void setHadDDColonPolyp(String hadDDColonPolyp) {
//		this.hadDDColonPolyp = hadDDColonPolyp;
//	}
//	public String getHadDDChrons() {
//		return hadDDChrons;
//	}
//	public void setHadDDChrons(String hadDDChrons) {
//		this.hadDDChrons = hadDDChrons;
//	}
//	public String getHadDDColitusNonUlcerative() {
//		return hadDDColitusNonUlcerative;
//	}
//	public void setHadDDColitusNonUlcerative(String hadDDColitusNonUlcerative) {
//		this.hadDDColitusNonUlcerative = hadDDColitusNonUlcerative;
//	}
//	public String getHadDDColitusUlcerative() {
//		return hadDDColitusUlcerative;
//	}
//	public void setHadDDColitusUlcerative(String hadDDColitusUlcerative) {
//		this.hadDDColitusUlcerative = hadDDColitusUlcerative;
//	}
//	public String getHadDDiverticulitis() {
//		return hadDDiverticulitis;
//	}
//	public void setHadDDiverticulitis(String hadDDiverticulitis) {
//		this.hadDDiverticulitis = hadDDiverticulitis;
//	}
//	public String getHadDDDLast6Months() {
//		return hadDDDLast6Months;
//	}
//	public void setHadDDDLast6Months(String hadDDDLast6Months) {
//		this.hadDDDLast6Months = hadDDDLast6Months;
//	}
//	public String getHadDDNAFLD() {
//		return hadDDNAFLD;
//	}
//	public void setHadDDNAFLD(String hadDDNAFLD) {
//		this.hadDDNAFLD = hadDDNAFLD;
//	}
//	public String getHadDDLiverBiopsy() {
//		return hadDDLiverBiopsy;
//	}
//	public void setHadDDLiverBiopsy(String hadDDLiverBiopsy) {
//		this.hadDDLiverBiopsy = hadDDLiverBiopsy;
//	}
//	public String getHadDDGastritis() {
//		return hadDDGastritis;
//	}
//	public void setHadDDGastritis(String hadDDGastritis) {
//		this.hadDDGastritis = hadDDGastritis;
//	}
//	public String getHadDDHepititis() {
//		return hadDDHepititis;
//	}
//	public void setHadDDHepititis(String hadDDHepititis) {
//		this.hadDDHepititis = hadDDHepititis;
//	}
//	public String getHadDDPancreaticInflamation() {
//		return hadDDPancreaticInflamation;
//	}
//	public void setHadDDPancreaticInflamation(String hadDDPancreaticInflamation) {
//		this.hadDDPancreaticInflamation = hadDDPancreaticInflamation;
//	}
//	public String getHadDDPancreatitis() {
//		return hadDDPancreatitis;
//	}
//	public void setHadDDPancreatitis(String hadDDPancreatitis) {
//		this.hadDDPancreatitis = hadDDPancreatitis;
//	}
//	public String getHadDDUlcer() {
//		return hadDDUlcer;
//	}
//	public void setHadDDUlcer(String hadDDUlcer) {
//		this.hadDDUlcer = hadDDUlcer;
//	}
//	public String getHadDDURecovered() {
//		return hadDDURecovered;
//	}
//	public void setHadDDURecovered(String hadDDURecovered) {
//		this.hadDDURecovered = hadDDURecovered;
//	}
//	public String getHadDDWeightLossSurgery() {
//		return hadDDWeightLossSurgery;
//	}
//	public void setHadDDWeightLossSurgery(String hadDDWeightLossSurgery) {
//		this.hadDDWeightLossSurgery = hadDDWeightLossSurgery;
//	}
//	public String getHadDDBenign() {
//		return hadDDBenign;
//	}
//	public void setHadDDBenign(String hadDDBenign) {
//		this.hadDDBenign = hadDDBenign;
//	}
//	public String getHadBDNotSure() {
//		return hadBDNotSure;
//	}
//	public void setHadBDNotSure(String hadBDNotSure) {
//		this.hadBDNotSure = hadBDNotSure;
//	}
//	
	
}

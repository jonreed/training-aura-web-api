package com.massmutual.domain.parttwo;

import com.massmutual.domain.RiskBase;

public class MentalHealth extends PartTwoRiskBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "MentalHealth [" + super.toString() + "]";
	}
	
//	private String hadEOHospitalizedLast5Years;
//	private String hadEOAlcoholism;
//	private String hadEOADepression;
//	private String hadEOTreatment;
//	private String hadEODepressionSituationalOnly;
//	private String hadEOHeartPalpitations;
//
//	private String hadEOADHD;
//	private String hadEOADHDChildhood;
//	private String hadEO;
//	private String hadEODelusion;
//	private String hadEODrugCounseling;
//	private String hadEOEatingDisorder;
//	private String hadEOMajorDepression;
//	private String hadEOPanicAttack;
//	private String hadEOPAMedsLast2Years;
//	private String hadEOSuicideAttempt;
//	private String hadEOMinorDisorders;
//	private String hadEONotSure;
//	public String getHadEOHospitalizedLast5Years() {
//		return hadEOHospitalizedLast5Years;
//	}
//	public void setHadEOHospitalizedLast5Years(String hadEOHospitalizedLast5Years) {
//		this.hadEOHospitalizedLast5Years = hadEOHospitalizedLast5Years;
//	}
//	public String getHadEOAlcoholism() {
//		return hadEOAlcoholism;
//	}
//	public void setHadEOAlcoholism(String hadEOAlcoholism) {
//		this.hadEOAlcoholism = hadEOAlcoholism;
//	}
//	public String getHadEOADepression() {
//		return hadEOADepression;
//	}
//	public void setHadEOADepression(String hadEOADepression) {
//		this.hadEOADepression = hadEOADepression;
//	}
//	public String getHadEOTreatment() {
//		return hadEOTreatment;
//	}
//	public void setHadEOTreatment(String hadEOTreatment) {
//		this.hadEOTreatment = hadEOTreatment;
//	}
//	public String getHadEODepressionSituationalOnly() {
//		return hadEODepressionSituationalOnly;
//	}
//	public void setHadEODepressionSituationalOnly(String hadEODepressionSituationalOnly) {
//		this.hadEODepressionSituationalOnly = hadEODepressionSituationalOnly;
//	}
//	public String getHadEOHeartPalpitations() {
//		return hadEOHeartPalpitations;
//	}
//	public void setHadEOHeartPalpitations(String hadEOHeartPalpitations) {
//		this.hadEOHeartPalpitations = hadEOHeartPalpitations;
//	}
//	public String getHadEOADHD() {
//		return hadEOADHD;
//	}
//	public void setHadEOADHD(String hadEOADHD) {
//		this.hadEOADHD = hadEOADHD;
//	}
//	public String getHadEOADHDChildhood() {
//		return hadEOADHDChildhood;
//	}
//	public void setHadEOADHDChildhood(String hadEOADHDChildhood) {
//		this.hadEOADHDChildhood = hadEOADHDChildhood;
//	}
//	public String getHadEO() {
//		return hadEO;
//	}
//	public void setHadEO(String hadEO) {
//		this.hadEO = hadEO;
//	}
//	public String getHadEODelusion() {
//		return hadEODelusion;
//	}
//	public void setHadEODelusion(String hadEODelusion) {
//		this.hadEODelusion = hadEODelusion;
//	}
//	public String getHadEODrugCounseling() {
//		return hadEODrugCounseling;
//	}
//	public void setHadEODrugCounseling(String hadEODrugCounseling) {
//		this.hadEODrugCounseling = hadEODrugCounseling;
//	}
//	public String getHadEOEatingDisorder() {
//		return hadEOEatingDisorder;
//	}
//	public void setHadEOEatingDisorder(String hadEOEatingDisorder) {
//		this.hadEOEatingDisorder = hadEOEatingDisorder;
//	}
//	public String getHadEOMajorDepression() {
//		return hadEOMajorDepression;
//	}
//	public void setHadEOMajorDepression(String hadEOMajorDepression) {
//		this.hadEOMajorDepression = hadEOMajorDepression;
//	}
//	public String getHadEOPanicAttack() {
//		return hadEOPanicAttack;
//	}
//	public void setHadEOPanicAttack(String hadEOPanicAttack) {
//		this.hadEOPanicAttack = hadEOPanicAttack;
//	}
//	public String getHadEOPAMedsLast2Years() {
//		return hadEOPAMedsLast2Years;
//	}
//	public void setHadEOPAMedsLast2Years(String hadEOPAMedsLast2Years) {
//		this.hadEOPAMedsLast2Years = hadEOPAMedsLast2Years;
//	}
//	public String getHadEOSuicideAttempt() {
//		return hadEOSuicideAttempt;
//	}
//	public void setHadEOSuicideAttempt(String hadEOSuicideAttempt) {
//		this.hadEOSuicideAttempt = hadEOSuicideAttempt;
//	}
//	public String getHadEOMinorDisorders() {
//		return hadEOMinorDisorders;
//	}
//	public void setHadEOMinorDisorders(String hadEOMinorDisorders) {
//		this.hadEOMinorDisorders = hadEOMinorDisorders;
//	}
//	public String getHadEONotSure() {
//		return hadEONotSure;
//	}
//	public void setHadEONotSure(String hadEONotSure) {
//		this.hadEONotSure = hadEONotSure;
//	}
//	

}

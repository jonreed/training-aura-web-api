package com.massmutual.domain.parttwo;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.massmutual.domain.RiskBase;
import com.massmutual.domain.verification.MedicalHistory;

public class PartTwoMedicalHistory extends MedicalHistory implements Serializable{
	
	private static final long serialVersionUID = -705381172507124136L;

	@Override
	public String toString() {
		return "PartTwoMedicalHistory [" + super.toString() + "]";
	}
	
}

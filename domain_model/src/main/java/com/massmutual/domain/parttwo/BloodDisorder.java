package com.massmutual.domain.parttwo;

import com.massmutual.domain.RiskBase;

public class BloodDisorder extends PartTwoRiskBase  implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "BloodDisorder ["+ super.toString() + "]";
	}
	
	
	
//	private String hadBDAnemia;
//	private String hadBDAnemiaType;
//	private String hasBDAnemiaResolved;
//	private String hadBDAnemiaBleeding;
//	private String hasBDAnemiaControlled;
//	private String hasBDAnemiaSymptoms;
//	private String hadBDSpleen;
//	private String hadBDClotting;
//	private String hadBDHemochromatosis;
//	private String hadBDOther;
//	public String getHadBDAnemia() {
//		return hadBDAnemia;
//	}
//	public void setHadBDAnemia(String hadBDAnemia) {
//		this.hadBDAnemia = hadBDAnemia;
//	}
//	public String getHadBDAnemiaType() {
//		return hadBDAnemiaType;
//	}
//	public void setHadBDAnemiaType(String hadBDAnemiaType) {
//		this.hadBDAnemiaType = hadBDAnemiaType;
//	}
//	public String getHasBDAnemiaResolved() {
//		return hasBDAnemiaResolved;
//	}
//	public void setHasBDAnemiaResolved(String hasBDAnemiaResolved) {
//		this.hasBDAnemiaResolved = hasBDAnemiaResolved;
//	}
//	public String getHadBDAnemiaBleeding() {
//		return hadBDAnemiaBleeding;
//	}
//	public void setHadBDAnemiaBleeding(String hadBDAnemiaBleeding) {
//		this.hadBDAnemiaBleeding = hadBDAnemiaBleeding;
//	}
//	public String getHasBDAnemiaControlled() {
//		return hasBDAnemiaControlled;
//	}
//	public void setHasBDAnemiaControlled(String hasBDAnemiaControlled) {
//		this.hasBDAnemiaControlled = hasBDAnemiaControlled;
//	}
//	public String getHasBDAnemiaSymptoms() {
//		return hasBDAnemiaSymptoms;
//	}
//	public void setHasBDAnemiaSymptoms(String hasBDAnemiaSymptoms) {
//		this.hasBDAnemiaSymptoms = hasBDAnemiaSymptoms;
//	}
//	public String getHadBDSpleen() {
//		return hadBDSpleen;
//	}
//	public void setHadBDSpleen(String hadBDSpleen) {
//		this.hadBDSpleen = hadBDSpleen;
//	}
//	public String getHadBDClotting() {
//		return hadBDClotting;
//	}
//	public void setHadBDClotting(String hadBDClotting) {
//		this.hadBDClotting = hadBDClotting;
//	}
//	public String getHadBDHemochromatosis() {
//		return hadBDHemochromatosis;
//	}
//	public void setHadBDHemochromatosis(String hadBDHemochromatosis) {
//		this.hadBDHemochromatosis = hadBDHemochromatosis;
//	}
//	public String getHadBDOther() {
//		return hadBDOther;
//	}
//	public void setHadBDOther(String hadBDOther) {
//		this.hadBDOther = hadBDOther;
//	}


}

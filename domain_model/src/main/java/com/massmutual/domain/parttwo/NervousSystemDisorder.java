package com.massmutual.domain.parttwo;


public class NervousSystemDisorder extends PartTwoRiskBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "NervousSystemDisorder [" + super.toString() + "]";
	}
	
	
	
//	private String hadBDAneurysm;
//	private String hadDBTumor;
//	private String hadDBEncephalitits;
//	private String hasDBRecovered;
//	private String hasDBRecovered2Years;
//	private String hadDBSeizures;
//	private String medsDBSeizures;
//	private String hadDBSiezuresLast5Years;
//	private String hadDBMigranes;
//	private String hadDBMDisabilityPayments;
//	private String hadDBMS;
//	private String hadDBNeuropathy;
//	private String hadDBParkinsons;
//	private String hadDBSciatica;
//	private String hadDBStroke;
//	private String hadDBFainting;
//	private String hadDBFMultipleEpisodes;
//	private String hadDBFLastYear;
//	private String hadDBTIA;
//	private String hadDBHematoma;
//	private String hadDBTBI;
//	private String hadDBTBILast5Years;
//	private String hasTBISymptomsOrMeds;
//	private String hadTBTourettes;
//	private String hadTBNotSure;
//	public String getHadBDAneurysm() {
//		return hadBDAneurysm;
//	}
//	public void setHadBDAneurysm(String hadBDAneurysm) {
//		this.hadBDAneurysm = hadBDAneurysm;
//	}
//	public String getHadDBTumor() {
//		return hadDBTumor;
//	}
//	public void setHadDBTumor(String hadDBTumor) {
//		this.hadDBTumor = hadDBTumor;
//	}
//	public String getHadDBEncephalitits() {
//		return hadDBEncephalitits;
//	}
//	public void setHadDBEncephalitits(String hadDBEncephalitits) {
//		this.hadDBEncephalitits = hadDBEncephalitits;
//	}
//	public String getHasDBRecovered() {
//		return hasDBRecovered;
//	}
//	public void setHasDBRecovered(String hasDBRecovered) {
//		this.hasDBRecovered = hasDBRecovered;
//	}
//	public String getHasDBRecovered2Years() {
//		return hasDBRecovered2Years;
//	}
//	public void setHasDBRecovered2Years(String hasDBRecovered2Years) {
//		this.hasDBRecovered2Years = hasDBRecovered2Years;
//	}
//	public String getHadDBSeizures() {
//		return hadDBSeizures;
//	}
//	public void setHadDBSeizures(String hadDBSeizures) {
//		this.hadDBSeizures = hadDBSeizures;
//	}
//	public String getMedsDBSeizures() {
//		return medsDBSeizures;
//	}
//	public void setMedsDBSeizures(String medsDBSeizures) {
//		this.medsDBSeizures = medsDBSeizures;
//	}
//	public String getHadDBSiezuresLast5Years() {
//		return hadDBSiezuresLast5Years;
//	}
//	public void setHadDBSiezuresLast5Years(String hadDBSiezuresLast5Years) {
//		this.hadDBSiezuresLast5Years = hadDBSiezuresLast5Years;
//	}
//	public String getHadDBMigranes() {
//		return hadDBMigranes;
//	}
//	public void setHadDBMigranes(String hadDBMigranes) {
//		this.hadDBMigranes = hadDBMigranes;
//	}
//	public String getHadDBMDisabilityPayments() {
//		return hadDBMDisabilityPayments;
//	}
//	public void setHadDBMDisabilityPayments(String hadDBMDisabilityPayments) {
//		this.hadDBMDisabilityPayments = hadDBMDisabilityPayments;
//	}
//	public String getHadDBMS() {
//		return hadDBMS;
//	}
//	public void setHadDBMS(String hadDBMS) {
//		this.hadDBMS = hadDBMS;
//	}
//	public String getHadDBNeuropathy() {
//		return hadDBNeuropathy;
//	}
//	public void setHadDBNeuropathy(String hadDBNeuropathy) {
//		this.hadDBNeuropathy = hadDBNeuropathy;
//	}
//	public String getHadDBParkinsons() {
//		return hadDBParkinsons;
//	}
//	public void setHadDBParkinsons(String hadDBParkinsons) {
//		this.hadDBParkinsons = hadDBParkinsons;
//	}
//	public String getHadDBSciatica() {
//		return hadDBSciatica;
//	}
//	public void setHadDBSciatica(String hadDBSciatica) {
//		this.hadDBSciatica = hadDBSciatica;
//	}
//	public String getHadDBStroke() {
//		return hadDBStroke;
//	}
//	public void setHadDBStroke(String hadDBStroke) {
//		this.hadDBStroke = hadDBStroke;
//	}
//	public String getHadDBFainting() {
//		return hadDBFainting;
//	}
//	public void setHadDBFainting(String hadDBFainting) {
//		this.hadDBFainting = hadDBFainting;
//	}
//	public String getHadDBFMultipleEpisodes() {
//		return hadDBFMultipleEpisodes;
//	}
//	public void setHadDBFMultipleEpisodes(String hadDBFMultipleEpisodes) {
//		this.hadDBFMultipleEpisodes = hadDBFMultipleEpisodes;
//	}
//	public String getHadDBFLastYear() {
//		return hadDBFLastYear;
//	}
//	public void setHadDBFLastYear(String hadDBFLastYear) {
//		this.hadDBFLastYear = hadDBFLastYear;
//	}
//	public String getHadDBTIA() {
//		return hadDBTIA;
//	}
//	public void setHadDBTIA(String hadDBTIA) {
//		this.hadDBTIA = hadDBTIA;
//	}
//	public String getHadDBHematoma() {
//		return hadDBHematoma;
//	}
//	public void setHadDBHematoma(String hadDBHematoma) {
//		this.hadDBHematoma = hadDBHematoma;
//	}
//	public String getHadDBTBI() {
//		return hadDBTBI;
//	}
//	public void setHadDBTBI(String hadDBTBI) {
//		this.hadDBTBI = hadDBTBI;
//	}
//	public String getHadDBTBILast5Years() {
//		return hadDBTBILast5Years;
//	}
//	public void setHadDBTBILast5Years(String hadDBTBILast5Years) {
//		this.hadDBTBILast5Years = hadDBTBILast5Years;
//	}
//	public String getHasTBISymptomsOrMeds() {
//		return hasTBISymptomsOrMeds;
//	}
//	public void setHasTBISymptomsOrMeds(String hasTBISymptomsOrMeds) {
//		this.hasTBISymptomsOrMeds = hasTBISymptomsOrMeds;
//	}
//	public String getHadTBTourettes() {
//		return hadTBTourettes;
//	}
//	public void setHadTBTourettes(String hadTBTourettes) {
//		this.hadTBTourettes = hadTBTourettes;
//	}
//	public String getHadTBNotSure() {
//		return hadTBNotSure;
//	}
//	public void setHadTBNotSure(String hadTBNotSure) {
//		this.hadTBNotSure = hadTBNotSure;
//	}
//	
	

}

package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents the sub-condition, the type of disease associated with the primary condition (FamilyMemberConditionType) 
 */
public enum FamilyMemberDiseaseType {	
    BREAST_CANCER("Breast Cancer"),
    COLON_CANCER("Colon Cancer"),
    MELANOMA("Melanoma"),
    NOT_SURE("Not Sure"),
    OTHER_CANCER("Other Cancer"),
    OVARIAN_CANCER("Ovarian Cancer"),
    PROSTATE_CANCER("Prostate Cancer"),
    SKIN_CANCER_NON_MELANOMA("Skin Cancer (non-Melanoma)");
	
	private String diseaseValue;

	// Reverse-lookup map
    private static final Map<String, FamilyMemberDiseaseType> lookup = new HashMap<String, FamilyMemberDiseaseType>();		 
	static {
        for (FamilyMemberDiseaseType p : FamilyMemberDiseaseType.values()) {
            lookup.put(p.getDiseaseValue(), p);
        }
    }
	
    public static FamilyMemberDiseaseType get(String getDiseaseValue) {
       return lookup.get(getDiseaseValue);
    }
    
	private FamilyMemberDiseaseType(String getDiseaseValue) {
		this.diseaseValue = getDiseaseValue;
	}
	
	public String getDiseaseValue() {
		return diseaseValue;
	}

	public void setDiseaseValue(String diseaseValue) {
		this.diseaseValue = diseaseValue;
	}

	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (FamilyMemberDiseaseType d : values()) {
			
				names.add(d.name());
	    }
		
		return names;
	}
	
	
}

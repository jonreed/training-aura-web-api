package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents the primary medical condition, could be a cause of death or an existing condition. 
 */

public enum FamilyMemberConditionType { 
	BREAST_CANCER("Breast Cancer"),
	BREAST_OR_OVARIAN_CANCER("Breast or Ovarian Cancer"),
	CANCER("Cancer"),
	CARDIOMYOPATHY("Cardiomyopathy (Heart Muscle Disease)"),
	COLON_CANCER("Colon Cancer"),
	DIABETES("Diabetes"),
	HEART_DISEASE("Heart Disease"),
	HUNTINGTONS_DISEASE("Huntington's Disease"),
	KIDNEY_DISEASE("Kidney Disease"),
	LYNCH_SYNDROME("Lynch Syndrome"),
	MARFAN_DISEASE("Marfan Syndrome"),
	OTHER("Other"),
	OTHER_CANCER("Other Cancers"),
	OVARIAN_CANCER("Ovarian Cancer"),
	POLYCYSTIC_KIDNEY_DISEASE("Polycystic Kidney Disease"),
	PROSTATE_CANCER("Prostate Cancer"),
	SKIN_CANCER("Skin Cancer"),
	STROKE("Stroke");
	
	private String conditionValue;

	// Reverse-lookup map
    private static final Map<String, FamilyMemberConditionType> lookup = new HashMap<String, FamilyMemberConditionType>();		 
	static {
        for (FamilyMemberConditionType p : FamilyMemberConditionType.values()) {
            lookup.put(p.getConditionValue(), p);
        }
    }
	
	private FamilyMemberConditionType(String conditionValue) {
		this.conditionValue = conditionValue;
	}
	
	public static FamilyMemberConditionType get(String conditionValue) {
	       return lookup.get(conditionValue);
	    }
	
	public String getConditionValue() {
		return conditionValue;
	}

	public void setConditionValue(String conditionValue) {
		this.conditionValue = conditionValue;
	}

	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (FamilyMemberConditionType c : values()) {
			
				names.add(c.name());
	    }
		
		return names;
	}
	
    
	
}

package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents the Life status of the Family Member 
 */
public enum FamilyMemberLifeStatusType {
	ALIVE("alive"),
	DECEASED("deceased"),
	UNKNOWN("unknown"),			//For backward compatibility need to keep "unknown", starting with CMI 2.0 they will start sending "Not Sure"
	NS("Not Sure"),
	NA("not applicable");		//used in the case of no siblings
	
	private String lifeStatusValue;
	
	// Reverse-lookup map
    private static final Map<String, FamilyMemberLifeStatusType> lookup = new HashMap<String, FamilyMemberLifeStatusType>();		 
	static {
        for (FamilyMemberLifeStatusType p : FamilyMemberLifeStatusType.values()) {
            lookup.put(p.getLifeStatusValue(), p);
        }
    }
	
    public static FamilyMemberLifeStatusType get(String lifeStatusValue) {
       return lookup.get(lifeStatusValue);
    }	
    
	private FamilyMemberLifeStatusType(String lifeStatusValue) {
		this.lifeStatusValue = lifeStatusValue;
	}
	
	public String getLifeStatusValue() {
		return lifeStatusValue;
	}

	public void setLifeStatusValue(String lifeStatusValue) {
		this.lifeStatusValue = lifeStatusValue;
	}

	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (FamilyMemberLifeStatusType f : values()) {
			
				names.add(f.name());
	    }
		
		return names;
	}
	
}

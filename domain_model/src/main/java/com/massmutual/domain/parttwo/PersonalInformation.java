package com.massmutual.domain.parttwo;

public class PersonalInformation extends PartTwoRiskBase implements java.io.Serializable
{
	
	private static final long serialVersionUID = 4712314690185531216L;
	
	private double heightFeet = 0;
	private double heightInches = 0;
	private double weightPounds = 0;
	private double weightChangePounds = 0;
	private String weightLossReason;		//populate with enum value from WeightChangeType
	private String weightGainReason;		//populate with enum value from WeightChangeType
	
	
	// Will return total height in inches
	public double getHeight() {
		return heightFeet*12 + heightInches;
	}
		
	public double getHeightFeet() {
		return heightFeet;
	}
	public void setHeightFeet(double heightFeet) {
		this.heightFeet = heightFeet;
	}
	public double getHeightInches() {
		return heightInches;
	}
	public void setHeightInches(double heightInches) {
		this.heightInches = heightInches;
	}
	public double getWeightPounds() {
		return weightPounds;
	}
	public void setWeightPounds(double weightPounds) {
		this.weightPounds = weightPounds;
	}
	public double getWeightChangePounds() {
		return weightChangePounds;
	}
	public void setWeightChangePounds(double weightChangePounds) {
		this.weightChangePounds = weightChangePounds;
	}
	public String getWeightLossReason() {
		return weightLossReason;
	}
	public void setWeightLossReason(String weightLossReason) {
		this.weightLossReason = weightLossReason;
	}
	public String getWeightGainReason() {
		return weightGainReason;
	}
	public void setWeightGainReason(String weightGainReason) {
		this.weightGainReason = weightGainReason;
	}
	
	@Override
	public String toString() {
		return "PersonalInformation [heightFeet=" + heightFeet
				+ ", heightInches=" + heightInches + ", weightPounds="
				+ weightPounds + ", weightChangePounds=" + weightChangePounds
				+ ", weightLossReason=" + weightLossReason
				+ ", weightGainReason=" + weightGainReason + "]";
	}

	/**
	 * Helper method used by the brms rules which calculates the BMI
	 * Calculation:
	 * 	(weight / [height]2) x 703
	 * @param heightFeet
	 * @param heightInches
	 * @param weightPounds
	 * @return
	 */
	public double getCalculateBMIBasic()
	{
		return MedicalConditionUtil.calculateBMI(heightFeet, heightInches, weightPounds);
	}
	
	/**
	 * Helper method used by the brms rules which calculates the BMI overloaded from regular BMI
	 * where it allows an additional value (weightPoundsLost) to be passed in as part of the equation.
	 * * Calculation:
	 * 	(weight + (weightPoundsLoss/2) / [height]2) x 703
	 * @param heightFeet
	 * @param heightInches
	 * @param weightPounds
	 * @param weightPoundsLost
	 * @return
	 */
	public double getCalculateBMIWeightLoss()
	{
		return MedicalConditionUtil.calculateBMIWeightLost(heightFeet, heightInches, weightPounds, weightChangePounds);
	}

}

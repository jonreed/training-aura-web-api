package com.massmutual.domain.parttwo;

import com.massmutual.domain.RiskBase;

public class MusculoskeletalDisorder extends PartTwoRiskBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "MusculoskeletalDisorder [" + super.toString() + "]";
	}

	
	
//	private String hadBMDAmputationsFromTrauma;
//	private String hadBMDArthritis;
//	private String hadBMDAType;
//	private String hasBMDMeds;
//	private String hasBMDDisabilityBenefits;
//	private String hasBMDOsteoporosis;
//	private String hasBMDBackPain;
//	private String hasBMDBPMeds;
//	private String hasBMDPagets;
//	private String hasBMDBenign;
//	private String hasBMDOther;
//	private String diagosisBMDAOther;
//	public String getHadBMDAmputationsFromTrauma() {
//		return hadBMDAmputationsFromTrauma;
//	}
//	public void setHadBMDAmputationsFromTrauma(String hadBMDAmputationsFromTrauma) {
//		this.hadBMDAmputationsFromTrauma = hadBMDAmputationsFromTrauma;
//	}
//	public String getHadBMDArthritis() {
//		return hadBMDArthritis;
//	}
//	public void setHadBMDArthritis(String hadBMDArthritis) {
//		this.hadBMDArthritis = hadBMDArthritis;
//	}
//	public String getHadBMDAType() {
//		return hadBMDAType;
//	}
//	public void setHadBMDAType(String hadBMDAType) {
//		this.hadBMDAType = hadBMDAType;
//	}
//	public String getHasBMDMeds() {
//		return hasBMDMeds;
//	}
//	public void setHasBMDMeds(String hasBMDMeds) {
//		this.hasBMDMeds = hasBMDMeds;
//	}
//	public String getHasBMDDisabilityBenefits() {
//		return hasBMDDisabilityBenefits;
//	}
//	public void setHasBMDDisabilityBenefits(String hasBMDDisabilityBenefits) {
//		this.hasBMDDisabilityBenefits = hasBMDDisabilityBenefits;
//	}
//	public String getHasBMDOsteoporosis() {
//		return hasBMDOsteoporosis;
//	}
//	public void setHasBMDOsteoporosis(String hasBMDOsteoporosis) {
//		this.hasBMDOsteoporosis = hasBMDOsteoporosis;
//	}
//	public String getHasBMDBackPain() {
//		return hasBMDBackPain;
//	}
//	public void setHasBMDBackPain(String hasBMDBackPain) {
//		this.hasBMDBackPain = hasBMDBackPain;
//	}
//	public String getHasBMDBPMeds() {
//		return hasBMDBPMeds;
//	}
//	public void setHasBMDBPMeds(String hasBMDBPMeds) {
//		this.hasBMDBPMeds = hasBMDBPMeds;
//	}
//	public String getHasBMDPagets() {
//		return hasBMDPagets;
//	}
//	public void setHasBMDPagets(String hasBMDPagets) {
//		this.hasBMDPagets = hasBMDPagets;
//	}
//	public String getHasBMDBenign() {
//		return hasBMDBenign;
//	}
//	public void setHasBMDBenign(String hasBMDBenign) {
//		this.hasBMDBenign = hasBMDBenign;
//	}
//	public String getHasBMDOther() {
//		return hasBMDOther;
//	}
//	public void setHasBMDOther(String hasBMDOther) {
//		this.hasBMDOther = hasBMDOther;
//	}
//	public String getDiagosisBMDAOther() {
//		return diagosisBMDAOther;
//	}
//	public void setDiagosisBMDAOther(String diagosisBMDAOther) {
//		this.diagosisBMDAOther = diagosisBMDAOther;
//	}
//	
	
}

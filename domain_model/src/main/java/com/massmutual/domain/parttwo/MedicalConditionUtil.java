package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//import org.apache.log4j.Logger;

public class MedicalConditionUtil {

	//private static final Logger log = Logger.getLogger(MedicalConditionUtil.class);
	
	private static List<String> heartConditions =
			Arrays.asList(	" ",
							"hadHeartCondition",	// parent question
							"hadHCHypertension",	// parent question
							"hadHCAttack",
							"hadHCABFibrillation",
							"hadHCABFTreatment",
							"hadHCABFTFainted",
							"hadHCHMedication",
							"hadHCMurmu",			 				 
							"hadHCMAdviseECG",
							"hadHCMAdviseSurgery",
							"hadHCMAdviseMeds",
							"hadHCPericarditis",
							"hadHCPMultipleEpisodes", 
							"hadHCPFullyRecovered",
							"hadHCBenign",
							"hadHCOther", 	
							"hadHighCholesterol",
							"hadHCElevatedNow",
							"medsHCH",
							"hadVascularDisease"
							);
	
	private static List<String> bloodDisorders =
			Arrays.asList(	" ",
							"hadBDAnemia",
							"hasBDAnemiaSymptoms",
							"hadBDSpleen",
							"hadBDClotting",
							"hadBDThalassemia",
							"hadBDHemochromatosis",
							"hadBDOther",
							"hadBloodDisorder");
	
	private static List<String> nervousSystemDisorders =
			Arrays.asList(	" ",
							"hadBrainDisorder",
							"hadBDAneurysm",
							"hadDBEncephalitits",
							"hadDBFainting",
							"hadDBFLastYear", 
							"hadDBFMultipleEpisodes", 
							"hadDBHematoma",
							"hadDBMDisabilityPayments",
							"hadDBMigranes",
							"hadDBMigranes4InAMonth",
							"hadDBMigranesDailyMed",
							"hadDBMigranesMissedWork",
							"hadDBMigranesNarc",
							"hadDBMS",
							"hadDBNeuropathy", 
							"hadDBParkinsons", 
							"hadDBSciatica",
							"hadDBSeizures",
							"hadDBSiezuresLast5Years",
							"hadDBStroke",
							"hadDBTBI",
							"hadDBTBILast6Months", 
							"hadDBTBIMoreThan1BI",
							"hadTBNotSure",
							"hadDBTIA",
							"hadTBTourettes",
							"hadDBTumor",
							"hasDBRecovered",
							"hasDBRecovered2Years", 		
							"hasTBISymptomsOrMeds", 
							"medsDBSeizures"
							);
	
	private static List<String> diabetes =
			Arrays.asList(	" ",
							"hasDTDiabetes",
							"hadGestOver5YearsBefore",
							"hasDTProlactinoma",
							"hasDTThyroidDisease",
							"hasDTThyroidSurgeryBenign", 	
							"hasDTCystNoduleSurgery",
							"hasDTTImageBiopsy",
							"hasDTThyroidNotSerious",
							"hasDTHypothyroidism",
							"hasDTTStable",
							"hasDTHypothyroidismTreatment",
							"hasDTNotSure",
							"hasDTThyroidOveractiveTreatment",
							"hasDTThyroidOveractive"
							);
	
	private static List<String> disgestiveDisorders =
			Arrays.asList(	" ",
							"hadDDColitusUlcerative",
							"hadDDiverticulitis",
							"hadDDDLast6Months",
							"hadDDNAFLD",
							"hadDDLiverBiopsy", 				 
							"hadDDGastritis",
							"hadDDHepititis",
							"hadDDPancreaticInflamation",
							"hadDDPancreatitis",
							"hadDDUlcer", 				 
							"hadDDURecovered",
							"hadDDWeightLossSurgery",
							"hadDDBenign",
							"hadBDNotSure"
							);
	
	private static List<String> mentalHealth =
			Arrays.asList(" ",
							"hadEmotionalDisorder",
							"hadEO",
							"hadEOADepression",
							"hadEOADHDChildhood",
							"hadEOADHDDiagnosed",
							"hadEOAnxietyOrPanic",
							"hadEOEatingDisorder",
							"hadEODelusion",
							"hadEODepression",
							"hadEODepressionSituationalOnly",
							"hadEOHeartPalpitations",
							"hadEOHospitalizedLast10Years",
							"hadEOMajorDepression",
							"hadEOMajorDepressionSituationalOnly",
							"hadEOMajorDepressionTreatment",
							"hadEOMemoryImpairment",
							"hadEOMinorDisorders",
							"hadEONotSure",
							"hadEOPalpPanickAttach2Years",
							"hadEOPAMedsLast2Years",
							"hadEOPanicAttack",	
							"hadEOSuicideAttempt",
							"hadEOTreatment"
							);
	
	private static List<String> respiratoryDisorders =
			Arrays.asList(	" ",
							"hadRDAERLast1Year",
							"hadRDAHospitalizedLast1Year",
							"hadRDAsthma",
							"hadRDCOPD",
							"hadRDInfluenza",
							"hadRDOther",
							"hadRDPulmonaryNodule",
							"hadRDSarcoidosis",
							"hadRDTuberculosis",
							"hadRespiratoryDisorder",
							"hasRDAMeds",
							"hasRDApnea",
							"hasRDBRecovered",
							"hasRDBronchitis",
							"hasRDCPAP",
							"hasRDCPAPLast6Months",
							"hadRDPneumonia",
							"hasRDPRecovered",
							"hasRDTRecovered"
							);
							
		private static List<String> reproductiveDisorders =
			Arrays.asList(	" ",
							"hadIDPolycystic",
							"hadIDReproductive"
							);
							
	private static List<String> familyHistory =
			Arrays.asList(	" ",
							"hasConditions",
							"hadSeriousCondition"
					);
	
	private static List<String> otherConditions =
			Arrays.asList(	" ",
							"hadNoCondition",
							"hadRXOther"
					);
	
	private static List<String> muscularDisorders =
			Arrays.asList(	" ",
							"hadBMDAmputationsFromTrauma",
							"hadBMDArthritis",
							"hasBMDMeds"
							
					);
	
	private static List<String> cancer =
			Arrays.asList(	" ",
							"hadCancerMelanoma",
							"hadLeukemia",
							"hadLymphoma",
							"hadOtherCancer"								
					);
	
	private static List<String> labSlipConditions =
			Arrays.asList(	" ",
							"hasDTDiabetes",
							"hadHeartCondition",
							"hadCPHighBloodPressure",
							"hadCancer"								
					);
	
	//Represents all Prescription questions across all objects, special list for a custom method.
	private static List<String> prescriptionProperties =
			Arrays.asList(	"hadHCABFTreatment",          		//HeartCondition
							"hadHCHMedication",          		//HeartCondition
							"medsHCH",          				//HeartCondition
							"hadHCMAdviseMeds",          		//HeartCondition
							"medsDBSeizures",          			//NervousSystemDisorder
							"hasTBISymptomsOrMeds",          	//NervousSystemDisorder
							"hadDBMigranesDailyMed",          	//NervousSystemDisorder
							"hadDBMigranesNarc",          		//NervousSystemDisorder
							"hadDDLiverBiopsy",          		//DigestiveDisorder
							"hasBMDMeds",          				//MusculoskeletalDisorder
							"hasSDMeds",          				//SkinDisorder
							"hasBDAnemiaResolved",          	//BloodDisorder
							"hasBDAnemiaControlled",          	//BloodDisorder
							"hadEOPAMedsLast2Years",          	//MentalHealth
							"hasRDAMeds",          				//PulmonaryDisorder
							"hadRDAsthmaPrescribedLast12Months",//PulmonaryDisorder
							"hadTreatmentOther",          		//OtherDisorder
							"hadRXOther"          				//OtherDisorder
							);
	
	private static List<String> allConditions;
	
	static{
	
		allConditions = new ArrayList<String>(heartConditions);
		allConditions.addAll(bloodDisorders);
		allConditions.addAll(nervousSystemDisorders);
		allConditions.addAll(diabetes);
		allConditions.addAll(disgestiveDisorders);
		allConditions.addAll(mentalHealth);
		allConditions.addAll(respiratoryDisorders);
		allConditions.addAll(reproductiveDisorders);
		allConditions.addAll(familyHistory);
		allConditions.addAll(muscularDisorders);
		allConditions.addAll(cancer);
		allConditions.addAll(otherConditions);
		
	}
	
	
	public static List<String> getHeartConditions() {
	
		return heartConditions;
	}
	
	public static List<String> getBloodDisorders() {
	
		return bloodDisorders;
	}
	
	public static List<String> getCancer() {
		return cancer;
	}

	public static List<String> getNervousSystemDisorders() {
		return nervousSystemDisorders;
	}

	public static List<String> getDiabetes() {
		return diabetes;
	}

	public static List<String> getDisgestiveDisorders() {
		return disgestiveDisorders;
	}

	public static List<String> getMentalHealth() {
		return mentalHealth;
	}

	public static List<String> getRespiratoryDisorders() {
		return respiratoryDisorders;
	}
	
	public static List<String> getReproductiveDisorders() {
		return reproductiveDisorders;
	}
	
	public static List<String> getFamilyHistory() {
		return familyHistory;
	}

	public static List<String> getOtherConditions() {
		return otherConditions;
	}

	public static List<String> getMuscularDisorders() {
		return muscularDisorders;
	}

	public static List<String> getLabSlipConditions() {
		return labSlipConditions;
	}
	
	public static List<String> getPrescriptionProperties() {
		return prescriptionProperties;
	}

	public static List<String> getAllConditions() {
		
		return allConditions;				
	}
	
	/*								BRMS HELPER METHODS											*/
	
	/**
	 * Helper method used by the brms rules which calculates the BMI
	 * Calculation:
	 * 	(weight / [height]2) x 703
	 * @param heightFeet
	 * @param heightInches
	 * @param weightPounds
	 * @return
	 */
	public static double calculateBMI(double heightFeet, double heightInches, double weightPounds)
	{
		return calculateBMIWeightLost(heightFeet, heightInches, weightPounds, -1);
	}
	
	/**
	 * Helper method used by the brms rules which calculates the BMI overloaded from regular BMI
	 * where it allows an additional value (weightPoundsLost) to be passed in as part of the equation.
	 * * Calculation:
	 * 	(weight + (weightPoundsLoss/2) / [height]2) x 703
	 * @param heightFeet
	 * @param heightInches
	 * @param weightPounds
	 * @param weightPoundsLost
	 * @return
	 */
	public static double calculateBMIWeightLost(double heightFeet, double heightInches, double weightPounds, double weightPoundsLost)
	{
		try
		{
			double totalHeight = (heightFeet * 12) + heightInches;
			return calculateBMIWeightLost(totalHeight, weightPounds, weightPoundsLost);
		}
		catch(Exception e)
		{
			System.err.println("Exception raised in calculateBMI(...) - will be returning -1: " + e.getMessage());
		}
		return -1.0;
	}
	
	/**
	 * Helper method used by the brms rules which calculates the BMI overloaded from regular BMI
	 * where it allows an additional value (weightPoundsLost) to be passed in as part of the equation.
	 * * Calculation:
	 * 	(weight + (weightPoundsLoss/2) / [height]2) x 703
	 * @param heightFeet
	 * @param heightInches
	 * @param weightPounds
	 * @param weightPoundsLost
	 * @return
	 */
	public static double calculateBMIWeightLost(double heightInches, double weightPounds, double weightPoundsLost)
	{
		try{
			double totalHeight = 0.0;
			double totalWeight = 0.0;
			double bmi = -1.0;
			
			//Make sure the height is greater than 0, inches can be zero or more and weight greater than zero.
			if(heightInches > 0.0 && weightPounds > 0.0){

				totalHeight = Math.pow(heightInches, 2);
				//If a value was passed for weightPoundsLoss then take half of that value and add it into the original weightPounds
				//If the value passed in is zero then do not perform this additional step.
				totalWeight = weightPoundsLost > 0 ? weightPounds + (weightPoundsLost/2) : weightPounds;
				
				bmi = (totalWeight/totalHeight)*703;
				
			}
			
			System.out.println("Calculated BMI = " + bmi);
			return bmi;
		}
		catch(Exception e){
			System.err.println("Exception raised in calculateBMI(...) - will be returning -1: " + e.getMessage());
		}
		return -1.0;
	}
}

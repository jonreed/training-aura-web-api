package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum WeightChangeType {
	CHANGE_DIET_EXERCISE("Change to diet/exercise"),
	CHILD_BIRTH("Child-birth"),
	DIET_EXERCISE("Diet and/or exercise"),
	OTHER("Other"),
	PREGNANCY("Pregnancy"),
	WEIGHT_LOSS_SURGERY("Weight loss surgery");
	
	private String weightChangeValue;

	// Reverse-lookup map
    private static final Map<String, WeightChangeType> lookup = new HashMap<String, WeightChangeType>();		 
	static {
        for (WeightChangeType p : WeightChangeType.values()) {
            lookup.put(p.getWeightChangeValue(), p);
        }
    }
	
    public static WeightChangeType get(String weightChangeValue) {
       return lookup.get(weightChangeValue);
    }
    
    private WeightChangeType(String weightChangeValue) {
		this.weightChangeValue = weightChangeValue;
	}

	public String getWeightChangeValue() {
		return weightChangeValue;
	}

	public void setWeightChangeValue(String weightChangeValue) {
		this.weightChangeValue = weightChangeValue;
	}

	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (WeightChangeType f : values()) {
			
				names.add(f.name());
	    }
		
		return names;
	}
}

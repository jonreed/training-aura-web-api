package com.massmutual.domain.parttwo;

import com.massmutual.domain.RiskBase;

public class ChronicFatigue extends PartTwoRiskBase  implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "ChronicFatigue [" + super.toString() + "]";
	}
	
	

//	private String hadRDChronicFatigue;
//	private String hasRDCFFunctionNormal;
//	private String hadRDDepression;
//	private String hadRDEpsteinBarr;
//	private String hadRDEBOtherSymptoms;
//	private String hadRDLupus;
//	private String hadRDLymeDisease;
//	private String hadRDLDSymptoms;
//	private String hasRDLDTreatment;
//	private String hasRDOther;
//	public String getHadRDChronicFatigue() {
//		return hadRDChronicFatigue;
//	}
//	public void setHadRDChronicFatigue(String hadRDChronicFatigue) {
//		this.hadRDChronicFatigue = hadRDChronicFatigue;
//	}
//	public String getHasRDCFFunctionNormal() {
//		return hasRDCFFunctionNormal;
//	}
//	public void setHasRDCFFunctionNormal(String hasRDCFFunctionNormal) {
//		this.hasRDCFFunctionNormal = hasRDCFFunctionNormal;
//	}
//	public String getHadRDDepression() {
//		return hadRDDepression;
//	}
//	public void setHadRDDepression(String hadRDDepression) {
//		this.hadRDDepression = hadRDDepression;
//	}
//	public String getHadRDEpsteinBarr() {
//		return hadRDEpsteinBarr;
//	}
//	public void setHadRDEpsteinBarr(String hadRDEpsteinBarr) {
//		this.hadRDEpsteinBarr = hadRDEpsteinBarr;
//	}
//	public String getHadRDEBOtherSymptoms() {
//		return hadRDEBOtherSymptoms;
//	}
//	public void setHadRDEBOtherSymptoms(String hadRDEBOtherSymptoms) {
//		this.hadRDEBOtherSymptoms = hadRDEBOtherSymptoms;
//	}
//	public String getHadRDLupus() {
//		return hadRDLupus;
//	}
//	public void setHadRDLupus(String hadRDLupus) {
//		this.hadRDLupus = hadRDLupus;
//	}
//	public String getHadRDLymeDisease() {
//		return hadRDLymeDisease;
//	}
//	public void setHadRDLymeDisease(String hadRDLymeDisease) {
//		this.hadRDLymeDisease = hadRDLymeDisease;
//	}
//	public String getHadRDLDSymptoms() {
//		return hadRDLDSymptoms;
//	}
//	public void setHadRDLDSymptoms(String hadRDLDSymptoms) {
//		this.hadRDLDSymptoms = hadRDLDSymptoms;
//	}
//	public String getHasRDLDTreatment() {
//		return hasRDLDTreatment;
//	}
//	public void setHasRDLDTreatment(String hasRDLDTreatment) {
//		this.hasRDLDTreatment = hasRDLDTreatment;
//	}
//	public String getHasRDOther() {
//		return hasRDOther;
//	}
//	public void setHasRDOther(String hasRDOther) {
//		this.hasRDOther = hasRDOther;
//	}
	
	
}

package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents the Source of the information, for example a a unique identifier to a question or a group of questions. 
 */
public enum FamilyMemberInformationSource {
	BROTHER_DETAILS("brothersDetails"),
	BROTHER_STATUS("brothersStatus"),
	FATHER_DETAILS("fatherDetails"),
	FATHER_STATUS("fatherStatus"),
	MOTHER_DETAILS("motherDetails"),
	MOTHER_STATUS("motherStatus"),
	SISTER_DETAILS("sistersDetails"),
	SISTER_STATUS("sistersStatus"),
	FAMILY_CONDITIONS("familyConditions"),
	FAMILY_SERIOUS_CONDITIONS("familySeriousConditions");
	
	private String informationSourceValue;

	// Reverse-lookup map
    private static final Map<String, FamilyMemberInformationSource> lookup = new HashMap<String, FamilyMemberInformationSource>();		 
	static {
        for (FamilyMemberInformationSource p : FamilyMemberInformationSource.values()) {
            lookup.put(p.getInformationSourceValue(), p);
        }
    }
	
    public static FamilyMemberInformationSource get(String informationSourceValue) {
       return lookup.get(informationSourceValue);
    }
    
    private FamilyMemberInformationSource(String informationSourceValue) {
		this.informationSourceValue = informationSourceValue;
	}

	public String getInformationSourceValue() {
		return informationSourceValue;
	}

	public void setInformationSourceValue(String informationSourceValue) {
		this.informationSourceValue = informationSourceValue;
	}

	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (FamilyMemberInformationSource f : values()) {
			
				names.add(f.name());
	    }
		
		return names;
	}

}

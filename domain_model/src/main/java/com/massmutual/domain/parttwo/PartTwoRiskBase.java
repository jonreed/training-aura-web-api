package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.massmutual.domain.RiskBase;

public abstract class PartTwoRiskBase extends RiskBase {
	
	
	protected List<MedicalCondition> conditions;
	protected String state;
	
	public List<MedicalCondition> getConditions() {
		return conditions;
	}

	public void setConditions(List<MedicalCondition> conditions) {
		this.conditions = conditions;
	}
	
	private List<String> conditionNames;
	private List<String> conditionsFound;
	
	public List<String> getConditionNames(){
	
		List<String> names = null;
		
		if(conditionNames == null && conditions != null){
		
			names = new ArrayList<String>();
			for (MedicalCondition condition : conditions){
				
				names.add(condition.getName());
			}
			conditionNames = names;
		}
			
		return conditionNames;
	}
	
	public List<String> getConditionsFound(){

		List<String> names = null;
		
		if(conditionsFound == null && conditions != null){
			
			names = new ArrayList<String>();
			for (MedicalCondition condition : conditions){
				
				if ("Y".equals(condition.getFound())){
					names.add(condition.getName());
				}
			}
			conditionsFound = names;
		}
		
		return conditionsFound;
	}
	
	public List<String> getConditionsNotFound(){
		
		List<String> names = new ArrayList<String>();
		
		if(conditions != null){
			for (MedicalCondition condition : conditions){
			
				if ("N".equals(condition.getFound())){
					names.add(condition.getName());
				}
			}
		}
		
		return names;
	}
	
	public List<String> getConditionsNotSure(){
		
		List<String> names = new ArrayList<String>();
		
		if(conditions != null){
			for (MedicalCondition condition : conditions){
			
				if ("NS".equals(condition.getFound())){
					names.add(condition.getName());
				}
			}
		}
		
		return names;
	}


	/***** BRMS HELPER METHODS ******/
	
	/**
	 * Helper method that searches all conditions to see if the user has all conditions that they entered a custom description for (Other).
	 * 
	 * All searchConditions must be in the list!!
	 *  
	 * @param searchConditionName the rule to search for
	 * @param searchConditions 1 or more user entered values to search for
	 * @return true if they are all in the list
	 */
	public boolean hasUserEnteredValueAll(String searchConditionName, String... searchConditions) {
		try {
			// Loop through all the conditions looking for the one specified by searchConditionName
			if(conditions == null)
			{
				return false;
			}
			for(MedicalCondition condition : conditions) { 
				if(condition.getName().equalsIgnoreCase(searchConditionName)) {
					// Found it
					if(condition.getUserEnteredValues() == null || condition.getUserEnteredValues().isEmpty()) {
						// There are no user entered values
						return false;
					} 
					else {
						//System.out.println("There are user entered values");
						// There are user entered values
						// Are ALL of the user entered values in the list provided
						List<String> searchConditionsList = Arrays.asList(searchConditions);
						for(String userEnteredValue: condition.getUserEnteredValues()) {
							if(!searchConditionsList.contains(userEnteredValue.toUpperCase())) {
								// A value is missing
								return false;
							}
						}
						// All of the user entered values are in the searchConditions
						return true;
					}
				}
			}
		}
		catch(Exception e) {
			// ignore and continue
			e.printStackTrace();
		}
		
		return false;		
	}


	/**
	 * Helper method that searches all conditions to see if the user has any condition that they entered a custom description for (Other).
	 * 
	 * One or more searchConditions must be in the list!!
	 *  
	 * @param searchConditionName the rule to search for
	 * @param searchConditions one or more user entered values to search for
	 * @return true if atleast one is in the list
	 */
	public boolean hasUserEnteredValueAny(String searchConditionName, String... searchConditions) {
		try {
			// Loop through all the conditions looking for the one specified by searchConditionName
			if(conditions == null)
			{
				return false;
			}
			for(MedicalCondition condition : conditions) { 
				if(condition.getName().equalsIgnoreCase(searchConditionName)) {
					// Found it
					if(condition.getUserEnteredValues() == null || condition.getUserEnteredValues().isEmpty()) {
						// There are no user entered values
						return false;
					} 
					else {
						// There are user entered values
						// Are ANY of the user entered values are in the list provided
						List<String> searchConditionsList = Arrays.asList(searchConditions);
						for(String userEnteredValue: condition.getUserEnteredValues()) {
							if(searchConditionsList.contains(userEnteredValue.toUpperCase())) {
								// A value is in the list
								return true;
							}
						}
						// None of the user entered values are in the searchConditions
						return false;
					}
				}
			}
		}
		catch(Exception e) {
			// ignore and continue
			e.printStackTrace();
		}
		
		return false;		
	}	

	// check if any of the input conditions are found
	// to use pass a string with the following format:
	// "CANCER:Y,BLOOD:N,DIABETES:Y,MEDS:3"
	public boolean getIncludesAnyCondition(String conditionList) {
		
		if (conditions != null && conditionList != null) {
			
			for (String possibleCondition : conditionList.split(",")) {
				
				for (MedicalCondition condition : conditions){
					
					try{
						String[] attributes = possibleCondition.split(":");
						String name = attributes[0];
						String found = attributes[1];
						
						if (name.equals(condition.getName()) && found.equals(condition.getFound())){
							return true;
						}
					}
					catch(Exception e){return false;}
				}
				
			}
		}
		
		return false;		
	}
	
		// Return a List of type <String> of all conditions
		// In the following format:
		// "CANCER:Y", "BLOOD:N", "DIABETES:Y", "MEDS:3", "phyReasonLastSeen:ROUTINE"
		public List<String> getHasCondition() {
			
			List<String> includedConditions = new ArrayList<String>();
			
			if (conditions != null) {
							
				for (MedicalCondition condition : conditions){

					includedConditions.add(condition.getName() + ":" + condition.getFound());
			
				}
								
			}
			
			return includedConditions;		
		}
	
	// need to check that none of the input conditions are found in the list
	// to use pass a string with the following format:
	// "CANCER:Y,BLOOD:N,DIABETES:Y,MEDS:3"
	public boolean getExcludeAllConditions(String conditionList) {
	
		if (conditions != null && conditionList != null) {
			
			for (String possibleCondition : conditionList.split(",")) {
				
				for (MedicalCondition condition : conditions){
					
					try{
						String[] attributes = possibleCondition.split(":");
						String name = attributes[0];
						String found = attributes[1];
						
						if (name.equals(condition.getName()) && found.equals(condition.getFound())){
							return false;
						}
					}
					catch(Exception e){return false;}
				}
				
			}
			
		}
		
		return true;
	}

	//Check N# of properties/conditions and return true if all are any combination of 
	//missing, N or NS or any value other than Y.
	//Returns false ONLY in the case of at least one of the properties being passed in
	//having a value of Y.
	//Format for conditions:   "hadLeukemia,hadLymphoma,hadOtherCancer,hadCancerMelanoma"
	public boolean checkConditionsNoPositiveResponse(String checkConditionList)
	{	
		try
		{
			//Returns a list of conditions with a response of "Y"
			List<String> conditionsWithPositiveResponse = getConditionsFound();
			if(conditionsWithPositiveResponse != null && checkConditionList != null)
			{
				for (String checkCondition : checkConditionList.split(","))
				{
					if(checkCondition != null)
					{
						if(conditionsWithPositiveResponse.contains(checkCondition.trim()))
						{
							return false;
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			System.err.println("Exception raised in checkConditionsNoPositiveResponse(...) - will be returning false, values passed in:  " + checkConditionList + ", error message:  " + e.getMessage());
			return false;
		}
		return true;
	}
		
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "PartTwoRiskBase [conditions=" + conditions + ", state=" + state + "]";
	}
	

}

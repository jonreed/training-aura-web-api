package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum PhysicianVisitType {
	ACUTE("Acute limited illness (cough, cold, flu, sore throat, sinus infection, rashes, urinary infection)"),
	INJURY("Minor injury (sprain, strain, bruise, laceration, etc.)"),
	OTHER("Other"),
	ROUTINE("Routine physical,  immunizations, routine gynecological care, prenatal care");
	
	private String physicianVisitValue;

	// Reverse-lookup map
    private static final Map<String, PhysicianVisitType> lookup = new HashMap<String, PhysicianVisitType>();		 
	static {
        for (PhysicianVisitType p : PhysicianVisitType.values()) {
            lookup.put(p.getPhysicianVisitValue(), p);
        }
    }
	
    public static PhysicianVisitType get(String physicianVisitValue) {
       return lookup.get(physicianVisitValue);
    }
    
    private PhysicianVisitType(String physicianVisitValue) {
		this.physicianVisitValue = physicianVisitValue;
	}

	public String getPhysicianVisitValue() {
		return physicianVisitValue;
	}

	public void setPhysicianVisitValue(String physicianVisitValue) {
		this.physicianVisitValue = physicianVisitValue;
	}

	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (PhysicianVisitType f : values()) {
			
				names.add(f.name());
	    }
		
		return names;
	}

}

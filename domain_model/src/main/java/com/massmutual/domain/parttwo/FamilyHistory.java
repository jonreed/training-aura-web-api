package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.List;

public class FamilyHistory extends PartTwoRiskBase implements java.io.Serializable {

	private static final long serialVersionUID = -1549741162455192039L;
	
	private List<FamilyMember> motherDetails;
	private List<FamilyMember> fatherDetails;
	private List<FamilyMember> brothersDetails;
	private List<FamilyMember> sistersDetails;
	private List<FamilyMember> familyConditions;
	private List<FamilyMember> familySeriousConditions;
	
	private List<FamilyMember> allFamilyMemberLists;
	
	public List<FamilyMember> getAllFamilyMemberLists() {
		
		allFamilyMemberLists = new ArrayList<FamilyMember>();
		if(motherDetails != null && motherDetails.size() > 0)
		{
			allFamilyMemberLists.addAll(motherDetails);
		}
		if(fatherDetails != null && fatherDetails.size() > 0)
		{
			allFamilyMemberLists.addAll(fatherDetails);
		}
		
		if(brothersDetails != null && brothersDetails.size() > 0)
		{
			allFamilyMemberLists.addAll(brothersDetails);
		}
		
		if(sistersDetails != null && sistersDetails.size() > 0)
		{
			allFamilyMemberLists.addAll(sistersDetails);
		}
		
		if(familyConditions != null && familyConditions.size() > 0)
		{
			allFamilyMemberLists.addAll(familyConditions);
		}
		
		if(familySeriousConditions != null && familySeriousConditions.size() > 0)
		{
			allFamilyMemberLists.addAll(familySeriousConditions);
		}
		
		return allFamilyMemberLists;
		
	}
	
	
	public List<FamilyMember> getMotherDetails() {
		return motherDetails;
	}
	public void setMotherDetails(List<FamilyMember> motherDetails) {
		this.motherDetails = motherDetails;
	}
	public List<FamilyMember> getFatherDetails() {
		return fatherDetails;
	}
	public void setFatherDetails(List<FamilyMember> fatherDetails) {
		this.fatherDetails = fatherDetails;
	}
	public List<FamilyMember> getBrothersDetails() {
		return brothersDetails;
	}
	public void setBrothersDetails(List<FamilyMember> brothersDetails) {
		this.brothersDetails = brothersDetails;
	}
	public List<FamilyMember> getSistersDetails() {
		return sistersDetails;
	}
	public void setSistersDetails(List<FamilyMember> sistersDetails) {
		this.sistersDetails = sistersDetails;
	}
	public List<FamilyMember> getFamilyConditions() {
		return familyConditions;
	}
	public void setFamilyConditions(List<FamilyMember> familyConditions) {
		this.familyConditions = familyConditions;
	}
	public List<FamilyMember> getFamilySeriousConditions() {
		return familySeriousConditions;
	}
	public void setFamilySeriousConditions(
			List<FamilyMember> familySeriousConditions) {
		this.familySeriousConditions = familySeriousConditions;
	}


	@Override
	public String toString() {
		return "FamilyHistory [motherDetails=" + motherDetails
				+ ", fatherDetails=" + fatherDetails + ", brothersDetails="
				+ brothersDetails + ", sistersDetails=" + sistersDetails
				+ ", familyConditions=" + familyConditions
				+ ", familySeriousConditions=" + familySeriousConditions
				+ super.toString() + "]";
	}

	
}

package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents the type of Family Member 
 */
public enum FamilyMemberType {
	BROTHER("Brother"),
	FATHER("Father"),
	MOTHER("Mother"),
	SISTER("Sister");
	
	private String memberTypeValue;

	// Reverse-lookup map
    private static final Map<String, FamilyMemberType> lookup = new HashMap<String, FamilyMemberType>();		 
	static {
        for (FamilyMemberType p : FamilyMemberType.values()) {
            lookup.put(p.getMemberTypeValue(), p);
        }
    }
	
    public static FamilyMemberType get(String memberTypeValue) {
       return lookup.get(memberTypeValue);
    }
    
	private FamilyMemberType(String memberTypeValue) {
		this.memberTypeValue = memberTypeValue;
	}
	
	public String getMemberTypeValue() {
		return memberTypeValue;
	}

	public void setMemberTypeValue(String memberTypeValue) {
		this.memberTypeValue = memberTypeValue;
	}

	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		
		for (FamilyMemberType f : values()) {
			
				names.add(f.name());
	    }
		
		return names;
	}
	
}

package com.massmutual.domain.parttwo;

import com.massmutual.domain.RiskBase;

public class PulmonaryDisorder extends PartTwoRiskBase  implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "PulmonaryDisorder [" + super.toString() + "]";
	}
	
	
//	private String hadRDSarcoidosis;
//	private String hadRDCOPD;
//	private String hadRDEmphysema;
//	private String hadRDAsthma;
//	private String hadRDAHospitalizedLast1Year;
//	private String hadRDAERLast1Year;
//	private String hasRDAMeds;
//	private String hadRDPulmonaryNodule;
//	private String hasRDApnea;
//	private String hasRDCPAP;
//	private String hasRDCPAPLast6Months;
//	private String hasRDBronchitis;
//	private String hasRDBRecovered;
//	private String hadRDPneumonia;
//	private String hasRDPRecovered;
//	private String hadRDTuberculosis;
//	private String hasRDTRecovered;
//	private String hadRDInfluenza;
//	private String hadRDOther;
//	public String getHadRDSarcoidosis() {
//		return hadRDSarcoidosis;
//	}
//	public void setHadRDSarcoidosis(String hadRDSarcoidosis) {
//		this.hadRDSarcoidosis = hadRDSarcoidosis;
//	}
//	public String getHadRDCOPD() {
//		return hadRDCOPD;
//	}
//	public void setHadRDCOPD(String hadRDCOPD) {
//		this.hadRDCOPD = hadRDCOPD;
//	}
//	public String getHadRDEmphysema() {
//		return hadRDEmphysema;
//	}
//	public void setHadRDEmphysema(String hadRDEmphysema) {
//		this.hadRDEmphysema = hadRDEmphysema;
//	}
//	public String getHadRDAsthma() {
//		return hadRDAsthma;
//	}
//	public void setHadRDAsthma(String hadRDAsthma) {
//		this.hadRDAsthma = hadRDAsthma;
//	}
//	public String getHadRDAHospitalizedLast1Year() {
//		return hadRDAHospitalizedLast1Year;
//	}
//	public void setHadRDAHospitalizedLast1Year(String hadRDAHospitalizedLast1Year) {
//		this.hadRDAHospitalizedLast1Year = hadRDAHospitalizedLast1Year;
//	}
//	public String getHadRDAERLast1Year() {
//		return hadRDAERLast1Year;
//	}
//	public void setHadRDAERLast1Year(String hadRDAERLast1Year) {
//		this.hadRDAERLast1Year = hadRDAERLast1Year;
//	}
//	public String getHasRDAMeds() {
//		return hasRDAMeds;
//	}
//	public void setHasRDAMeds(String hasRDAMeds) {
//		this.hasRDAMeds = hasRDAMeds;
//	}
//	public String getHadRDPulmonaryNodule() {
//		return hadRDPulmonaryNodule;
//	}
//	public void setHadRDPulmonaryNodule(String hadRDPulmonaryNodule) {
//		this.hadRDPulmonaryNodule = hadRDPulmonaryNodule;
//	}
//	public String getHasRDApnea() {
//		return hasRDApnea;
//	}
//	public void setHasRDApnea(String hasRDApnea) {
//		this.hasRDApnea = hasRDApnea;
//	}
//	public String getHasRDCPAP() {
//		return hasRDCPAP;
//	}
//	public void setHasRDCPAP(String hasRDCPAP) {
//		this.hasRDCPAP = hasRDCPAP;
//	}
//	public String getHasRDCPAPLast6Months() {
//		return hasRDCPAPLast6Months;
//	}
//	public void setHasRDCPAPLast6Months(String hasRDCPAPLast6Months) {
//		this.hasRDCPAPLast6Months = hasRDCPAPLast6Months;
//	}
//	public String getHasRDBronchitis() {
//		return hasRDBronchitis;
//	}
//	public void setHasRDBronchitis(String hasRDBronchitis) {
//		this.hasRDBronchitis = hasRDBronchitis;
//	}
//	public String getHasRDBRecovered() {
//		return hasRDBRecovered;
//	}
//	public void setHasRDBRecovered(String hasRDBRecovered) {
//		this.hasRDBRecovered = hasRDBRecovered;
//	}
//	public String getHadRDPneumonia() {
//		return hadRDPneumonia;
//	}
//	public void setHadRDPneumonia(String hadRDPneumonia) {
//		this.hadRDPneumonia = hadRDPneumonia;
//	}
//	public String getHasRDPRecovered() {
//		return hasRDPRecovered;
//	}
//	public void setHasRDPRecovered(String hasRDPRecovered) {
//		this.hasRDPRecovered = hasRDPRecovered;
//	}
//	public String getHadRDTuberculosis() {
//		return hadRDTuberculosis;
//	}
//	public void setHadRDTuberculosis(String hadRDTuberculosis) {
//		this.hadRDTuberculosis = hadRDTuberculosis;
//	}
//	public String getHasRDTRecovered() {
//		return hasRDTRecovered;
//	}
//	public void setHasRDTRecovered(String hasRDTRecovered) {
//		this.hasRDTRecovered = hasRDTRecovered;
//	}
//	public String getHadRDInfluenza() {
//		return hadRDInfluenza;
//	}
//	public void setHadRDInfluenza(String hadRDInfluenza) {
//		this.hadRDInfluenza = hadRDInfluenza;
//	}
//	public String getHadRDOther() {
//		return hadRDOther;
//	}
//	public void setHadRDOther(String hadRDOther) {
//		this.hadRDOther = hadRDOther;
//	}
	
	

}

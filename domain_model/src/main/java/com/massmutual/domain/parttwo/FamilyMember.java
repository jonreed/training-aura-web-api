package com.massmutual.domain.parttwo;

public class FamilyMember implements java.io.Serializable {
	
	private static final long serialVersionUID = 2694855374494377142L;
	
	//Available options:  Father, Mother, Sister, Brother
	//Note you can have multiple FamilyMembers of the same type.
	private String member;	// use enum: FamilyMemberType, defining as String instead of Enum makes it more flexible in rules engine.
	
	//Examples:  Cancer, Heart Disease, Diabetes
	//Represents either condition died from (DiedOf) if lifeStatus is deceased or
	//Represents condition (condition) that occurred at a particular age (ageAtOnset) if lifeStatus is alive
	private String condition;	// using enum: FamilyMemberConditionType, defining as String instead of Enum makes it more flexible in rules engine.
	
	//Examples:  Breast Cancer, Skin Cancer, Other Cancers
	private String diseaseType;	// using enum: FamilyMemberDiseaseType, defining as String instead of Enum makes it more flexible in rules engine.
	
	//Represents either Age of Death (DiedAt) if lifeStatus is deceased or
	//Represents Age of Onset (ageAtOnset) if lifeStatus is alive or not specified (meaning null - life status was not asked/answered)
	private int age = -1;
	
	//Available options:  deceased, alive, unknown
	private String lifeStatus;	// using enum: FamilyMemberLifeStatusType, defining as String instead of Enum makes it more flexible in rules engine.

	//Represents the Source of the information, a unique identifier to a question or group of questions. 
	private String informationSource;	// using enum: FamilyMemberInformationSource, defining as String instead of Enum makes it more flexible in rules engine.
	
	
	public String getMember() {
		return member;
	}
	public void setMember(String member) {
		this.member = member;
	}

	public String getCondition() {
		return condition;
	}
	
	public void setCondition(String condition) {
		this.condition = condition;
	}
	
	public String getDiseaseType() {
		return diseaseType;
	}
	public void setDiseaseType(String diseaseType) {
		this.diseaseType = diseaseType;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getLifeStatus() {
		return lifeStatus;
	}
	
	public void setLifeStatus(String lifeStatus) {
		this.lifeStatus = lifeStatus;
	}
	
	public String getInformationSource() {
		return informationSource;
	}
	public void setInformationSource(String informationSource) {
		this.informationSource = informationSource;
	}
	
	@Override
	public String toString() {
		return "FamilyMember [member=" + member + ", condition=" + condition
				+ ", diseaseType=" + diseaseType + ", age=" + age
				+ ", lifeStatus=" + lifeStatus + ", informationSource="
				+ informationSource + "]";
	}
	
}

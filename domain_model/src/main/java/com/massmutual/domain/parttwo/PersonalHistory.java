package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.Insured;
import com.massmutual.domain.PartTwoRiskType;
import com.massmutual.domain.PolicyInformation;
import com.massmutual.domain.RiskInputBase;

public class PersonalHistory extends RiskInputBase implements java.io.Serializable {
	private static final long serialVersionUID = 8366633186855592902L;
	private CMIInformation cmiInformation;
	private PartTwoRiskType riskType;
	
	private VersionInformation versionInformation;
	private HeartCondition heartCondition;
	private Cancer cancer;
	private BloodDisorder bloodDisorder;
	
	private NervousSystemDisorder nervousSystemDisorder;
	private MentalHealth mentalHealth;
	private EarsNoseThroatDisorder earsNoseThroatDisorder;
	private PulmonaryDisorder pulmonaryDisorder;
	private DisgestiveDisorder disgestiveDisorder;
	private MusculoskeletalDisorder musculoskeletalDisorder;
	private ChronicFatigue chronicFatigue;
	private Diabetes diabetes;
	
	private UrinaryTrack urinaryTrack;
	private SkinDisorder skinDisorder;
	private ReproductiveDisorder reproductiveDisorder;
	private Pregnancy pregnancy;
	private PastTenYearWindow pastTenYearWindow;
	private PastFiveYearWindow pastFiveYearWindow;
	private PastThreeYearWindow pastThreeYearWindow;
	private PersonalInformation personalInformation;
	private PartTwoMedicalHistory medicalHistory;
	
	private OtherDisorder otherCondition;
	private FamilyHistory familyHistory;
	private Insured insured;
	private PolicyInformation policyInformation;


	public CMIInformation getCmiInformation() {
		return cmiInformation;
	}
	public void setCmiInformation(CMIInformation cmiInformation) {
		this.cmiInformation = cmiInformation;
	}
	public VersionInformation getVersionInformation() {
		return versionInformation;
	}
	public void setVersionInformation(VersionInformation versionInformation) {
		this.versionInformation = versionInformation;
	}
	public HeartCondition getHeartCondition() {
		return heartCondition;
	}
	public void setHeartCondition(HeartCondition heartCondition) {
		this.heartCondition = heartCondition;
	}
	public Cancer getCancer() {
		return cancer;
	}
	public void setCancer(Cancer cancer) {
		this.cancer = cancer;
	}
	public BloodDisorder getBloodDisorder() {
		return bloodDisorder;
	}
	public void setBloodDisorder(BloodDisorder bloodDisorder) {
		this.bloodDisorder = bloodDisorder;
	}
	public NervousSystemDisorder getNervousSystemDisorder() {
		return nervousSystemDisorder;
	}
	public void setNervousSystemDisorder(NervousSystemDisorder nervousSystemDisorder) {
		this.nervousSystemDisorder = nervousSystemDisorder;
	}
	public MentalHealth getMentalHealth() {
		return mentalHealth;
	}
	public void setMentalHealth(MentalHealth mentalHealth) {
		this.mentalHealth = mentalHealth;
	}
	public EarsNoseThroatDisorder getEarsNoseThroatDisorder() {
		return earsNoseThroatDisorder;
	}
	public void setEarsNoseThroatDisorder(EarsNoseThroatDisorder earsNoseThroatDisorder) {
		this.earsNoseThroatDisorder = earsNoseThroatDisorder;
	}
	public PulmonaryDisorder getPulmonaryDisorder() {
		return pulmonaryDisorder;
	}
	public void setPulmonaryDisorder(PulmonaryDisorder pulmonaryDisorder) {
		this.pulmonaryDisorder = pulmonaryDisorder;
	}
	public DisgestiveDisorder getDisgestiveDisorder() {
		return disgestiveDisorder;
	}
	public void setDisgestiveDisorder(DisgestiveDisorder disgestiveDisorder) {
		this.disgestiveDisorder = disgestiveDisorder;
	}
	public MusculoskeletalDisorder getMusculoskeletalDisorder() {
		return musculoskeletalDisorder;
	}
	public void setMusculoskeletalDisorder(MusculoskeletalDisorder musculoskeletalDisorder) {
		this.musculoskeletalDisorder = musculoskeletalDisorder;
	}
	public ChronicFatigue getChronicFatigue() {
		return chronicFatigue;
	}
	public void setChronicFatigue(ChronicFatigue chronicFatigue) {
		this.chronicFatigue = chronicFatigue;
	}
	public Diabetes getDiabetes() {
		return diabetes;
	}
	public void setDiabetes(Diabetes diabetes) {
		this.diabetes = diabetes;
	}
	public UrinaryTrack getUrinaryTrack() {
		return urinaryTrack;
	}
	public void setUrinaryTrack(UrinaryTrack urinaryTrack) {
		this.urinaryTrack = urinaryTrack;
	}
	public SkinDisorder getSkinDisorder() {
		return skinDisorder;
	}
	public void setSkinDisorder(SkinDisorder skinDisorder) {
		this.skinDisorder = skinDisorder;
	}
	public ReproductiveDisorder getReproductiveDisorder() {
		return reproductiveDisorder;
	}
	public void setReproductiveDisorder(ReproductiveDisorder reproductiveDisorder) {
		this.reproductiveDisorder = reproductiveDisorder;
	}
	public Pregnancy getPregnancy() {
		return pregnancy;
	}
	public void setPregnancy(Pregnancy pregnancy) {
		this.pregnancy = pregnancy;
	}
	public PastTenYearWindow getPastTenYearWindow() {
		return pastTenYearWindow;
	}
	public void setPastTenYearWindow(PastTenYearWindow pastTenYearWindow) {
		this.pastTenYearWindow = pastTenYearWindow;
	}
	public PastFiveYearWindow getPastFiveYearWindow() {
		return pastFiveYearWindow;
	}
	public void setPastFiveYearWindow(PastFiveYearWindow pastFiveYearWindow) {
		this.pastFiveYearWindow = pastFiveYearWindow;
	}
	public PastThreeYearWindow getPastThreeYearWindow() {
		return pastThreeYearWindow;
	}
	public void setPastThreeYearWindow(PastThreeYearWindow pastThreeYearWindow) {
		this.pastThreeYearWindow = pastThreeYearWindow;
	}
	public PersonalInformation getPersonalInformation() {
		return personalInformation;
	}
	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}
	public PartTwoRiskType getRiskType() {
		return riskType;
	}
	public void setRiskType(PartTwoRiskType riskType) {
		this.riskType = riskType;
	}
	public OtherDisorder getOtherCondition() {
		return otherCondition;
	}
	public void setOtherCondition(OtherDisorder otherCondition) {
		this.otherCondition = otherCondition;
	}
	
	public PartTwoMedicalHistory getMedicalHistory() {
		return medicalHistory;
	}
	public void setMedicalHistory(PartTwoMedicalHistory medicalHistory) {
		this.medicalHistory = medicalHistory;
	}
	
	public FamilyHistory getFamilyHistory() {
		return familyHistory;
	}
	public void setFamilyHistory(FamilyHistory familyHistory) {
		this.familyHistory = familyHistory;
	}
	public Insured getInsured() {
		return insured;
	}
	public void setInsured(Insured insured) {
		this.insured = insured;
	}

	public PolicyInformation getPolicyInformation() {
		return policyInformation;
	}
	public void setPolicyInformation(PolicyInformation policyInformation) {
		this.policyInformation = policyInformation;
	}
	@Override
	public String toString() {
		return "PersonalHistory [cmiInformation=" + cmiInformation + ", riskType=" + riskType + ", versionInformation="
				+ versionInformation + ", heartCondition=" + heartCondition + ", cancer=" + cancer + ", bloodDisorder="
				+ bloodDisorder + ", nervousSystemDisorder=" + nervousSystemDisorder + ", mentalHealth=" + mentalHealth
				+ ", earsNoseThroatDisorder=" + earsNoseThroatDisorder + ", pulmonaryDisorder=" + pulmonaryDisorder
				+ ", disgestiveDisorder=" + disgestiveDisorder + ", musculoskeletalDisorder=" + musculoskeletalDisorder
				+ ", chronicFatigue=" + chronicFatigue + ", diabetes=" + diabetes + ", urinaryTrack=" + urinaryTrack
				+ ", skinDisorder=" + skinDisorder + ", reproductiveDisorder=" + reproductiveDisorder + ", pregnancy="
				+ pregnancy + ", pastTenYearWindow=" + pastTenYearWindow + ", pastFiveYearWindow=" + pastFiveYearWindow
				+ ", pastThreeYearWindow=" + pastThreeYearWindow + ", personalInformation=" + personalInformation
				+ ", medicalHistory=" + medicalHistory + ", otherCondition=" + otherCondition + ", familyHistory="
				+ familyHistory + ", insured=" + insured + ", policyInformation=" + policyInformation + "]";
	}	
	
	/* 					BRMS HELPER METHODS 							*/
	
	/**
	 * There are 20 Prescriptions questions across numerous of the categories, this helper method will check
	 * all of them and if at least one of them responded with a "Y" then it will return false, ONLY in the case
	 * of all of them responding with "N", "NS" or No Response (missing tag) - basically anything but a "Y" that
	 * is when it will return a response of true.
	 * @return true ONLY if all 20 prescription questions were NOT answered with a Yes
	 */
	public boolean checkPrescriptionConditionsNoPositiveResponse()
	{
		try
		{
			List<String> prescriptionProperties = MedicalConditionUtil.getPrescriptionProperties();	
			
			//Not all categories have Prescription Questions - only those categories that include the 20 prescription
			//questions being interrogated are being checked.
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, bloodDisorder))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, disgestiveDisorder))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, heartCondition))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, mentalHealth))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, musculoskeletalDisorder))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, nervousSystemDisorder))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, otherCondition))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, pulmonaryDisorder))
			{
				return false;
			}
			if(!checkConditionsNoPositiveResponse(prescriptionProperties, skinDisorder))
			{
				return false;
			}
		}
		catch(Exception e)
		{
			System.err.println("Exception raised in PersonalHistory checkPrescriptionConditionsNoPositiveResponse() - will be returning false: " + e.getMessage());
			return false;
		}
		return true;
	}
	
	/*private helper method for the above public helper method */
	private boolean checkConditionsNoPositiveResponse(List<String> prescriptionProperties, PartTwoRiskBase parentCondition)
	{
		try
		{
			List<String> parentCondtionsFound = parentCondition != null ?  parentCondition.getConditionsFound() : null;
			for(String prescriptionProperty : prescriptionProperties)
			{
				if(parentCondtionsFound != null && parentCondtionsFound.contains(prescriptionProperty))
				{
					return false;
				}
			}
		}
		catch(Exception e)
		{
			System.err.println("Exception raised in PersonalHistory checkConditionsNoPositiveResponse() - will be returning false: " + e.getMessage());
			return false;
		}
		return true;
	}
}
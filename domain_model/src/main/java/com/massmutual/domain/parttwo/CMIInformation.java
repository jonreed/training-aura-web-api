package com.massmutual.domain.parttwo;

import java.util.Date;
import com.massmutual.domain.RiskBase;

public class CMIInformation extends RiskBase  implements java.io.Serializable {
	private static final long serialVersionUID = 7962087416225917897L;
	private Date cmiSignatureDate;

	public Date getCmiSignatureDate() {
		return cmiSignatureDate;
	}

	public void setCmiSignatureDate(Date cmiSignatureDate) {
		this.cmiSignatureDate = cmiSignatureDate;
	}

	@Override
	public String toString() {
		return "CMIInformation [cmiSignatureDate=" + cmiSignatureDate + "]";
	}
}
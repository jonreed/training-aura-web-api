package com.massmutual.domain.parttwo;

import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.RiskBase;

public class HeartCondition extends PartTwoRiskBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "HeartCondition [" + super.toString() + "]";
	}

	//private String hadHeartCondition;
	/*private String hadHCAttack;
	private String hadHCAbnormalBeat;
		
	private String hadHCABFibrillation;
	private String hadHCABFTreatment;
	private String hadHCABFTFainted;
	
	private String hadHCHypertension;
	private String hadHCHMedication;
	//private String medsHCH;

	private String hadHCMurmur;
	private String hadHCMAdviseECG;
	private String hadHCMAdviseSurgery;
	private String hadHCMAdviseMeds;
	
	private String hadHCPericarditis;
	private String hadHCPMultipleEpisodes;
	private String hadHCPFullyRecovered;
	
	private String hadHCBenign;
	private String hadHCOther;
	private String hadHighCholesterol;
	private String hadHCElevatedNow;*/

	/*public String getHadHCAttack() {
		return hadHCAttack;
	}

	public void setHadHCAttack(String hadHCAttack) {
		this.hadHCAttack = hadHCAttack;
	}

	public String getHadHCAbnormalBeat() {
		return hadHCAbnormalBeat;
	}

	public void setHadHCAbnormalBeat(String hadHCAbnormalBeat) {
		this.hadHCAbnormalBeat = hadHCAbnormalBeat;
	}

	public String getHadHCABFibrillation() {
		return hadHCABFibrillation;
	}

	public void setHadHCABFibrillation(String hadHCABFibrillation) {
		this.hadHCABFibrillation = hadHCABFibrillation;
	}

	public String getHadHCABFTreatment() {
		return hadHCABFTreatment;
	}

	public void setHadHCABFTreatment(String hadHCABFTreatment) {
		this.hadHCABFTreatment = hadHCABFTreatment;
	}

	public String getHadHCABFTFainted() {
		return hadHCABFTFainted;
	}

	public void setHadHCABFTFainted(String hadHCABFTFainted) {
		this.hadHCABFTFainted = hadHCABFTFainted;
	}

	public String getHadHCHypertension() {
		return hadHCHypertension;
	}

	public void setHadHCHypertension(String hadHCHypertension) {
		this.hadHCHypertension = hadHCHypertension;
	}

	public String getHadHCHMedication() {
		return hadHCHMedication;
	}

	public void setHadHCHMedication(String hadHCHMedication) {
		this.hadHCHMedication = hadHCHMedication;
	}

	public String getHadHCMurmur() {
		return hadHCMurmur;
	}

	public void setHadHCMurmur(String hadHCMurmur) {
		this.hadHCMurmur = hadHCMurmur;
	}

	public String getHadHCMAdviseECG() {
		return hadHCMAdviseECG;
	}

	public void setHadHCMAdviseECG(String hadHCMAdviseECG) {
		this.hadHCMAdviseECG = hadHCMAdviseECG;
	}

	public String getHadHCMAdviseSurgery() {
		return hadHCMAdviseSurgery;
	}

	public void setHadHCMAdviseSurgery(String hadHCMAdviseSurgery) {
		this.hadHCMAdviseSurgery = hadHCMAdviseSurgery;
	}

	public String getHadHCMAdviseMeds() {
		return hadHCMAdviseMeds;
	}

	public void setHadHCMAdviseMeds(String hadHCMAdviseMeds) {
		this.hadHCMAdviseMeds = hadHCMAdviseMeds;
	}

	public String getHadHCPericarditis() {
		return hadHCPericarditis;
	}

	public void setHadHCPericarditis(String hadHCPericarditis) {
		this.hadHCPericarditis = hadHCPericarditis;
	}

	public String getHadHCPMultipleEpisodes() {
		return hadHCPMultipleEpisodes;
	}

	public void setHadHCPMultipleEpisodes(String hadHCPMultipleEpisodes) {
		this.hadHCPMultipleEpisodes = hadHCPMultipleEpisodes;
	}

	public String getHadHCPFullyRecovered() {
		return hadHCPFullyRecovered;
	}

	public void setHadHCPFullyRecovered(String hadHCPFullyRecovered) {
		this.hadHCPFullyRecovered = hadHCPFullyRecovered;
	}

	public String getHadHCBenign() {
		return hadHCBenign;
	}

	public void setHadHCBenign(String hadHCBenign) {
		this.hadHCBenign = hadHCBenign;
	}

	public String getHadHCOther() {
		return hadHCOther;
	}

	public void setHadHCOther(String hadHCOther) {
		this.hadHCOther = hadHCOther;
	}

	public String getHadHighCholesterol() {
		return hadHighCholesterol;
	}

	public void setHadHighCholesterol(String hadHighCholesterol) {
		this.hadHighCholesterol = hadHighCholesterol;
	}

	public String getHadHCElevatedNow() {
		return hadHCElevatedNow;
	}

	public void setHadHCElevatedNow(String hadHCElevatedNow) {
		this.hadHCElevatedNow = hadHCElevatedNow;
	}

	@Override
	public String toString() {
		return "HeartCondition [ hadHCAttack=" + hadHCAttack
				+ ", hadHCAbnormalBeat=" + hadHCAbnormalBeat + ", hadHCABFibrillation=" + hadHCABFibrillation
				+ ", hadHCABFTreatment=" + hadHCABFTreatment + ", hadHCABFTFainted=" + hadHCABFTFainted
				+ ", hadHCHypertension=" + hadHCHypertension + ", hadHCHMedication=" + hadHCHMedication 
				+ ", hadHCMurmur=" + hadHCMurmur + ", hadHCMAdviseECG=" + hadHCMAdviseECG
				+ ", hadHCMAdviseSurgery=" + hadHCMAdviseSurgery + ", hadHCMAdviseMeds=" + hadHCMAdviseMeds
				+ ", hadHCPericarditis=" + hadHCPericarditis + ", hadHCPMultipleEpisodes=" + hadHCPMultipleEpisodes
				+ ", hadHCPFullyRecovered=" + hadHCPFullyRecovered + ", hadHCBenign=" + hadHCBenign + ", hadHCOther="
				+ hadHCOther + ", hadHighCholesterol=" + hadHighCholesterol + ", hadHCElevatedNow=" + hadHCElevatedNow
				+ "]";
	}*/

	
}

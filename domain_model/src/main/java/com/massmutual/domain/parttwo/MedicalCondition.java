package com.massmutual.domain.parttwo;

import java.util.List;

public class MedicalCondition implements java.io.Serializable {
	private static final long serialVersionUID = 7949155815121600065L;

	String name;
	String found;
	List<String> userEnteredValues;
		
	public MedicalCondition(){
		
	}

	public MedicalCondition(String name, String found) {
		
		this.name = name;
		this.found = found;
	}

	public List<String> getUserEnteredValues() {
		return userEnteredValues;
	}

	public void setUserEnteredValues(List<String> userEnteredValues) {
		this.userEnteredValues = userEnteredValues;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getFound() {
		return found;
	}

	public void setFound(String found) {
		this.found = found;
	}
	
	public Integer getFoundAsNumber() {
		try {
			if (found != null && !"".equals(found)) {
				Integer num = Integer.valueOf(found);
				return num;
			}
		} catch (Exception notANumber) {
		}
		return -1;
	}

	@Override
	public String toString() {
		return "MedicalCondition [ name=" + name + ", found=" + found + ", userEnteredValue=" + userEnteredValues + "]";
	}
}
package com.massmutual.domain.mib;

import java.io.Serializable;

import com.massmutual.domain.RiskRating;

public class MIBRiskRating extends RiskRating implements Serializable {
	private static final long serialVersionUID = -1296514145807036662L;

	private String responseData;
	
	
	public MIBRiskRating(RiskRating rating) {

		super(rating);	
		this.responseData = responseData;
	
	}
	public MIBRiskRating(){
		super();
		super.riskType = "MIB";
	}
	
	public MIBRiskRating(String responseData) {
		super();
		this.responseData = responseData;
	}
	
	public String getResponseData() {
		return responseData;
	}

	public void setResponseData(String responseData) {
		this.responseData = responseData;
	}
	
	public void setFormResponse(FormResponse response) {
		this.responseData = response.getResponseData();
		setRuleName(responseData);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MIBRiskRating other = (MIBRiskRating) obj;
		if (getUPNT() != other.getUPNT())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "MIBRiskRating [responseData=" + responseData + ", " + super.toString() + "]";
	}
	
	
	
}

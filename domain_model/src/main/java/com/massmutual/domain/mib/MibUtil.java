package com.massmutual.domain.mib;

import java.util.Arrays;
import java.util.List;

public class MibUtil {
	private static List<String> validModifierValues = 
			Arrays.asList(" ", "*", "G", "H", "U", "W", "J", "M", "#", "S", "K", "T", "Q");
	private static List<String> validSourceValues =
			Arrays.asList(" ", "*", "E", "Y", "R", "X", "V", "#", "Z");
	private static List<String> validTimeframeValues =
			Arrays.asList(" ", "*", "N", "B", "C", "D", "F", "L", "P", "#");
	
	public static List<String> getValidModifierValues() {
		return validModifierValues;
	}

	public static List<String> getValidSourceValues() {
		return validSourceValues;
	}

	public static List<String> getValidTimeframeValues() {
		return validTimeframeValues;
	}

	public static String convertDiagnosisCode(String responseData) {
		return (responseData != null && responseData.length() >= 3) 
				? responseData.substring(0,3)
				: null;		
	}
	
	public static String convertModifier(String responseData) {
		return (responseData != null && responseData.length() >= 4)
				? responseData.substring(3,4)
				: null;
	}
	
	public static String convertSource(String responseData) {
		return (responseData != null && responseData.length() >= 5) 
				? responseData.substring(4,5)
				: null;
	}
	
	public static String convertTimeframe(String responseData) {
		return (responseData != null && responseData.length() >= 6)
				? responseData.substring(5,6)
				: null;
	}
	
	public static String convertSiteCode(String responseData) {
		// ******(***)
		return (responseData != null && responseData.length() >= 9)
				? responseData.substring(7,10)
				: null;
	}
	
	public static String convertValueToResponseData(String value, int pos, int length, String responseData) {
		
		//System.out.println("Inside convertValueToResponseData value ["+value+"], responseData["+responseData+"]");
		
		if (value != null && value.length() == length){
			
			if (responseData == null || responseData.length() < pos){
				responseData = padRight(responseData, "*", 6);
			}
			
		
			char[] chars = responseData.toCharArray();
			int i = 0;
					
			for (i = 0; i < length; i++){
				
				chars[pos + i] = value.charAt(i);
				//System.out.println("char["+value.charAt(i)+"]");
			}
			
			return new String(chars);	//chars.toString(); <- this is not the same as -> new String(chars)
		}
		
		return null;
		
				
	}
	
	public static String padRight(String val, String p, int length) {
	      
		if(val == null) {
			val = "";
		}
		
		StringBuffer sb = new StringBuffer(val);
		int numChars = length - val.length();
     
		for(int i = 0; i < numChars; i++) {
			sb.append(p);
		}
      
		return sb.toString();
   }
}

package com.massmutual.domain.mib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.Insured;
import com.massmutual.domain.RiskInputBase;


public class MIBInput extends RiskInputBase implements Serializable {
	private static final long serialVersionUID = -8194465319447842337L;

	private List<FormResponse> responses = new ArrayList<FormResponse>();
	private IAIGroup iaiGroup;
	private Insured insured;
	

	public MIBInput() {
	}

	public List<FormResponse> getResponses() {
		return responses;
	}

	public void setResponses(List<FormResponse> responses) {
		this.responses = responses;
	}

	public IAIGroup getIaiGroup() {
		return iaiGroup;
	}

	public void setIaiGroup(IAIGroup iaiGroup) {
		this.iaiGroup = iaiGroup;
	}

	public Insured getInsured() {
		return insured;
	}

	public void setInsured(Insured insured) {
		this.insured = insured;
	}

	@Override
	public String toString() {
		return "MIBInput [responses=" + responses + ", iaiGroup=" + iaiGroup + ", insured=" + insured + "]";
	}	
}
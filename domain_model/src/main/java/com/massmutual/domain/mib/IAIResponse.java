package com.massmutual.domain.mib;

import java.io.Serializable;
import java.util.Date;


public class IAIResponse implements Serializable {
	private static final long serialVersionUID = 7966593382714553256L;

	private String applicationType; // "Life" or "Disability"
	private String carrierCode;
	private Date submitDate;

	public IAIResponse() {
	}

	public IAIResponse(String applicationType, Date submitDate) {
		this.applicationType = applicationType;
		this.submitDate = submitDate;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}


	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((submitDate == null) ? 0 : submitDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IAIResponse other = (IAIResponse) obj;
		if (submitDate == null) {
			if (other.submitDate != null)
				return false;
		} else if (!submitDate.equals(other.submitDate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "IAIResponse [applicationType=" + applicationType + ", carrierCode=" + carrierCode + ", submitDate="
				+ submitDate + "]";
	}
}

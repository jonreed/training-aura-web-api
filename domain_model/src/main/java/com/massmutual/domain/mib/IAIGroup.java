package com.massmutual.domain.mib;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class IAIGroup implements Serializable {
	private static final long serialVersionUID = -4379994034232452491L;

	private List<IAIResponse> iaiResponses = new ArrayList<IAIResponse>();
	Date latestSubmitDate;

	public IAIGroup() {
	}
	
	public List<IAIResponse> getIaiResponses() {
		return iaiResponses;
	}

	public void setIaiResponses(List<IAIResponse> iaiResponses) {
		this.iaiResponses = iaiResponses;
	}

	public void setLatestSubmitDate(Date latestSubmitDate) {
		this.latestSubmitDate = latestSubmitDate;
	}

	public long calculateDiffinDays(Date signatureDate){
		
		Date submitDate = getLatestSubmitDate();
		
		if( submitDate == null || signatureDate == null){
			return -1;
		}
		
		Calendar calEnd = Calendar.getInstance();
		calEnd.setTime(signatureDate);
		cleanDate(calEnd);
		
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(submitDate);
		cleanDate(calStart);
		
		long diff = calEnd.getTimeInMillis() - calStart.getTimeInMillis();
		
		long diffDays = diff / (1000 * 60 * 60 * 24);
		
		if(diffDays < 1){
			diffDays = diffDays * -1;
		}
				
		return diffDays;
	}
	
	private void cleanDate(Calendar date) {
		date.clear(Calendar.HOUR);
		date.clear(Calendar.MINUTE);
		date.clear(Calendar.SECOND);
		date.clear(Calendar.MILLISECOND);
	}

	public Date getLatestSubmitDate() {

		try
		{
		if (latestSubmitDate == null && iaiResponses != null && iaiResponses.size() > 0) {

			latestSubmitDate = iaiResponses.get(0).getSubmitDate();

			for (IAIResponse r : iaiResponses) {

				if (r.getSubmitDate().after(latestSubmitDate)) {

					latestSubmitDate = r.getSubmitDate();
				}
			}

			System.out.println("Found latestSubmitDate: " + latestSubmitDate);
		}

		return latestSubmitDate;
		}
		catch(Exception e)
		{
			return null;
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iaiResponses == null) ? 0 : iaiResponses.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "IAIGroup [iaiResponses=" + iaiResponses + "]";
	}

}

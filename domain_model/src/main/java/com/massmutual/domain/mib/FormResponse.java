package com.massmutual.domain.mib;
import java.io.Serializable;
import java.util.Date;


public class FormResponse implements Serializable {
	private static final long serialVersionUID = -4379994034232452491L;

								// attribute: Diagnosis Modifier Source Timeframe SiteCode
								// position:    0-2        3        4       5        (7-9)
	private String responseData = "******(***)";	
	private String diagnosis;
	private String modifier;
	private String source;
	private String timeframe;
	private String siteCode;
	private Date   submitDate;
	
	public FormResponse() {
	}
	
	public FormResponse(String responseData) {
		setResponseData(responseData);
	}

	public String getResponseData() {
		return responseData;
	}

	// attribute: Diagnosis Modifier Source Timeframe SiteCode
	// position:    0-2        3        4       5        (7-9)
	public void setResponseData(String responseData) {
		this.responseData = responseData;

		if (responseData.equals("HIT - NO DATA") || responseData.equals("NO MATCH"))
           this.diagnosis = responseData;
		else
		  {
		   this.diagnosis = MibUtil.convertDiagnosisCode(this.responseData);
		   this.modifier = MibUtil.convertModifier(this.responseData);
		   this.source = MibUtil.convertSource(this.responseData);
		   this.timeframe = MibUtil.convertTimeframe(this.responseData);
		   this.siteCode = MibUtil.convertSiteCode(this.responseData);
		  }
	}
	
	public String getDiagnosis() {
		return this.diagnosis;
	}
	
//	public void setDiagnosis(String diagnosis) {
//		this.diagnosis = diagnosis;
//		this.responseData = MibUtil.convertValueToResponseData(diagnosis, 0, 3, responseData);
//	}
	
	public String getModifier() {
		return this.modifier;
	}
	
//	public void setModifier(String modifier) {
//		this.modifier = modifier;
//		this.responseData = MibUtil.convertValueToResponseData(modifier, 3, 1, responseData);
//	}
	
	public String getSource() {
		return this.source;
	}
	
//	public void setSource(String source) {
//		this.source = source;
//		this.responseData = MibUtil.convertValueToResponseData(source, 4, 1, responseData);
//	}
	
	public String getTimeframe() {
		return this.timeframe;
	}
	
//	public void setTimeframe(String timeframe) {
//		this.timeframe = timeframe;
//		this.responseData = MibUtil.convertValueToResponseData(timeframe, 5, 1, responseData);
//	}
	
	public String getSiteCode() {
		return siteCode;
	}

//	public void setSiteCode(String siteCode) {
//		this.siteCode = siteCode;
//	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((responseData == null) ? 0 : responseData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FormResponse other = (FormResponse) obj;
		if (responseData == null) {
			if (other.responseData != null)
				return false;
		} else if (!responseData.equals(other.responseData))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FormResponse [responseData=" + responseData + ", diagnosis=" + diagnosis + ", modifier=" + modifier
				+ ", source=" + source + ", timeframe=" + timeframe + ", siteCode=" + siteCode + ", submitDate="
				+ submitDate + "]";
	}
	
}

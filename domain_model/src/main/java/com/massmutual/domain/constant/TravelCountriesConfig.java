package com.massmutual.domain.constant;

import java.util.ArrayList;
import java.util.List;

public class TravelCountriesConfig implements java.io.Serializable{
	
	static final long serialVersionUID = 1L;
	
	private String description;
	private List<String> safeCountries;
	private List<String> badCountries;
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<String> getSafeCountries() {
		return safeCountries;
	}
	public void setSafeCountries(List<String> safeCountries) {
		this.safeCountries = safeCountries;
	}
	
	public String getAddSafeCountry() {
		return null;
	}
	
	public void setAddSafeCountry(String country) {
		if (safeCountries == null) {
			safeCountries = new ArrayList<String>();
		}
		safeCountries.add(country);
	}
	
	public List<String> getBadCountries() {
		return badCountries;
	}
	
	public void setBadCountries(List<String> badCountries) {
		this.badCountries = badCountries;
	}
	
	public String getAddBadCountry() {
		return null;
	}
	
	public void setAddBadCountry(String country) {
		if (badCountries == null) {
			badCountries = new ArrayList<String>();
		}
		badCountries.add(country);
	}
	
	
	
	

}

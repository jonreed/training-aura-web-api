package com.massmutual.domain.constant;

import java.util.ArrayList;
import java.util.List;

public enum TravelCountryType {
	A_RATED,
	B_RATED;
	
	
	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		for (TravelCountryType s : values()) {
			
				names.add(s.name());
	    }
		
		return names;
	}

}

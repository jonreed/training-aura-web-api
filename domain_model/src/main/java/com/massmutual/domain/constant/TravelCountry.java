package com.massmutual.domain.constant;

import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.RiskSummaryType;

public enum TravelCountry {
	ALBANIA(TravelCountryType.A_RATED),
	AMERICAN_SAMOA(TravelCountryType.A_RATED),
	ANDORRA(TravelCountryType.A_RATED),
	ANGUILLA(TravelCountryType.A_RATED),
	ANTIGUA_BARBUDA(TravelCountryType.A_RATED),
	ARGENTINA(TravelCountryType.A_RATED),
	ARUBA(TravelCountryType.A_RATED),
	AUSTRALIA(TravelCountryType.A_RATED),
	AUSTRIA(TravelCountryType.A_RATED),
	AZORES(TravelCountryType.A_RATED),
	BARBADOS(TravelCountryType.A_RATED),
	BELGIUM(TravelCountryType.A_RATED),
	BERMUDA(TravelCountryType.A_RATED),
	BOSNIA_HERZEGOVINA(TravelCountryType.A_RATED),
	BRUNEI(TravelCountryType.A_RATED),
	BULGARIA(TravelCountryType.A_RATED),
	CANADA(TravelCountryType.A_RATED),
	CANARY_ISLANDS(TravelCountryType.A_RATED),
	CAYMAN_ISLANDS(TravelCountryType.A_RATED),
	CHILE(TravelCountryType.A_RATED),
	COOK_ISLANDS(TravelCountryType.A_RATED),
	CROATIA(TravelCountryType.A_RATED),
	CURACAO(TravelCountryType.A_RATED),
	CYPRUS(TravelCountryType.A_RATED),
	CZECH_REPUBLIC(TravelCountryType.A_RATED),
	DENMARK(TravelCountryType.A_RATED),
	DOMINICA(TravelCountryType.A_RATED),
	ESTONIA(TravelCountryType.A_RATED),
	FALKLAND_ISLANDS(TravelCountryType.A_RATED),
	FINLAND(TravelCountryType.A_RATED),
	FRANCE(TravelCountryType.A_RATED),
	FRENCH_POLYNESIA(TravelCountryType.A_RATED),
	GERMANY(TravelCountryType.A_RATED),
	GREECE(TravelCountryType.A_RATED),
	GREENLAND(TravelCountryType.A_RATED),
	GRENADA(TravelCountryType.A_RATED),
	GUADELOUPE(TravelCountryType.A_RATED),
	HOLLAND(TravelCountryType.A_RATED),
	HONG_KONG(TravelCountryType.A_RATED),
	HUNGARY(TravelCountryType.A_RATED),
	ICELAND(TravelCountryType.A_RATED),
	IRELAND(TravelCountryType.A_RATED),
	ISRAEL_EXCLUDING_GAZA_AND_WEST_BANK(TravelCountryType.A_RATED),
	ISRAEL_INCLUDING_GAZA_AND_WEST_BANK(TravelCountryType.B_RATED),
	ITALY(TravelCountryType.A_RATED),
	JAPAN(TravelCountryType.A_RATED),
	SOUTH_KOREA(TravelCountryType.A_RATED),
	KUWAIT(TravelCountryType.A_RATED),
	LATVIA(TravelCountryType.A_RATED),
	LIECHTENSTEIN(TravelCountryType.A_RATED),
	LITHUANIA(TravelCountryType.A_RATED),
	LUXEMBOURG(TravelCountryType.A_RATED),
	MACAU(TravelCountryType.A_RATED),
	MACEDONIA(TravelCountryType.A_RATED),
	MADEIRA(TravelCountryType.A_RATED),
	MALTA(TravelCountryType.A_RATED),
	MARSHALL_ISLANDS(TravelCountryType.A_RATED),
	MARTINIQUE(TravelCountryType.A_RATED),
	// Added Mexico Good country here as part of ALU-1896 to maintain alphabetical order
	MEXICO_AREAS_OF_ACAPULCO_CANCUN_COZUMEL_CABO_SAN_LUCAS_PLAYA_DEL_CARMEN_RIVIERA_MAYA_TULUM_MEXICO_CITY_YUCATAN(TravelCountryType.A_RATED),
	MONACO(TravelCountryType.A_RATED),
	MONTENEGRO(TravelCountryType.A_RATED),
	MONTSERRAT(TravelCountryType.A_RATED),
	NETHERLANDS(TravelCountryType.A_RATED),
	NETHERLANDS_ANTILLES(TravelCountryType.A_RATED),
	NEW_CALEDONIA(TravelCountryType.A_RATED),
	NEW_ZEALAND(TravelCountryType.A_RATED),
	NORTHERN_IRELAND(TravelCountryType.A_RATED),
	NORTHERN_MARIANA_ISLANDS(TravelCountryType.A_RATED),
	NORWAY(TravelCountryType.A_RATED),
	PALAU(TravelCountryType.A_RATED),
	POLAND(TravelCountryType.A_RATED),
	PORTUGAL(TravelCountryType.A_RATED),
	QATAR(TravelCountryType.A_RATED),
	ROMANIA(TravelCountryType.A_RATED),
	SAIPAN(TravelCountryType.A_RATED),
	SAN_MARINO(TravelCountryType.A_RATED),
	SERBIA(TravelCountryType.A_RATED),
	SINGAPORE(TravelCountryType.A_RATED),
	SLOVAKIA(TravelCountryType.A_RATED),
	SLOVENIA(TravelCountryType.A_RATED),
	SPAIN(TravelCountryType.A_RATED),
	ST_KITTS_NEVIS(TravelCountryType.A_RATED),
	ST_LUCIA(TravelCountryType.A_RATED),
	ST_MARTIN(TravelCountryType.A_RATED),
	ST_THOMAS(TravelCountryType.A_RATED),
	ST_VINCENT_GRENADINES(TravelCountryType.A_RATED),
	SWEDEN(TravelCountryType.A_RATED),
	SWITZERLAND(TravelCountryType.A_RATED),
	TAIWAN(TravelCountryType.A_RATED),
	TURKS_CAICOS(TravelCountryType.A_RATED),
	UNITED_ARAB_EMIRATES(TravelCountryType.A_RATED),
	UNITED_KINGDOM(TravelCountryType.A_RATED),
	URUGUAY(TravelCountryType.A_RATED),
	VIRGIN_ISLANDS(TravelCountryType.A_RATED),
	
	// JCR Moved other to "B" list 5/19/2016
	OTHER(TravelCountryType.B_RATED),
	
	ARMENIA(TravelCountryType.B_RATED),
	AZERBAIJAN(TravelCountryType.B_RATED),
	BAHAMAS(TravelCountryType.B_RATED),
	BELARUS(TravelCountryType.B_RATED),
	BHUTAN(TravelCountryType.B_RATED),
	BRAZIL(TravelCountryType.B_RATED),
	CAPE_VERDE(TravelCountryType.B_RATED),
	CHINA(TravelCountryType.B_RATED),
	COSTA_RICA(TravelCountryType.B_RATED),
	ECUADOR(TravelCountryType.B_RATED),
	FEDERATED_STATES_OF_MICRONESIA(TravelCountryType.B_RATED),
	FIJI(TravelCountryType.B_RATED),
	FRENCH_GUIANA(TravelCountryType.B_RATED),
	GEORGIA(TravelCountryType.B_RATED),
	JAMAICA(TravelCountryType.B_RATED),
	JORDAN(TravelCountryType.B_RATED),
	KAZAKHSTAN(TravelCountryType.B_RATED),
	KOSOVO(TravelCountryType.B_RATED),
	MALDIVES(TravelCountryType.B_RATED),
	MAURITIUS(TravelCountryType.B_RATED),
	MEXICO(TravelCountryType.B_RATED), 
// Added following entry for MEXICO bad country as part of ALU-1896	
	MEXICO_AREAS_OTHER_THAN_ACAPULCO_CANCUN_COZUMEL_CABO_SAN_LUCAS_PLAYA_DEL_CARMEN_RIVIERA_MAYA_TULUM_MEXICO_CITY_YUCATAN(TravelCountryType.B_RATED),	
	MICRONESIA(TravelCountryType.B_RATED),
	MOLDOVA(TravelCountryType.B_RATED),
	MONGOLIA(TravelCountryType.B_RATED),
	NIUE(TravelCountryType.B_RATED),
	OMAN(TravelCountryType.B_RATED),
	PANAMA(TravelCountryType.B_RATED),
	PARAGUAY(TravelCountryType.B_RATED),
	PERU(TravelCountryType.B_RATED),
	RUSSIA_EXCLUDING_CHECHNYA(TravelCountryType.B_RATED),
	SAMOA(TravelCountryType.B_RATED),
	SEYCHELLES(TravelCountryType.B_RATED),
	SURINAME(TravelCountryType.B_RATED),
	TONGA(TravelCountryType.B_RATED),
	TRINIDAD_TOBAGO(TravelCountryType.B_RATED),
	TURKEY(TravelCountryType.B_RATED),
	UKRAINE(TravelCountryType.B_RATED),
	YUGOSLAVIA(TravelCountryType.B_RATED);
	
	TravelCountryType type;
	
	TravelCountry(TravelCountryType type) {
        this.type = type;

    }
	
	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		for (TravelCountry s : values()) {
			
				names.add(s.name());
	    }
		
		return names;
	}
	
	public static List<String> getARatedCountries(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		for (TravelCountry c : values()) {
			
			if (c.type == TravelCountryType.A_RATED){
				names.add(c.name());
			}
				
	    }
		
		return names;
	}
	
	public static List<String> getBRatedCountries(){
		
		List<String> names = new ArrayList<String>();
		names.add(" ");
		for (TravelCountry c : values()) {
			
			if (c.type == TravelCountryType.B_RATED){
				names.add(c.name());
			}
				
	    }
		
		return names;
	}

}

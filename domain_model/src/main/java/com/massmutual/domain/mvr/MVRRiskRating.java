package com.massmutual.domain.mvr;

import java.io.Serializable;

import com.massmutual.domain.PartOneRiskType;
import com.massmutual.domain.RiskRating;
import com.massmutual.domain.RiskRatingBase;
import com.massmutual.domain.RiskType;

public class MVRRiskRating extends RiskRating implements Serializable {
	private static final long serialVersionUID = -1906208153396014442L;

	private MVRRiskType mvrRiskType;
	
	public MVRRiskRating() {
		super();
	}
	public MVRRiskRating(RiskRating rating) {

		super(rating);	
		this.mvrRiskType = mvrRiskType;
	
	}
	
	public MVRRiskType getMvrRiskType() {
		return mvrRiskType;
	}

	public void setMvrRiskType(MVRRiskType mvrRiskType) {
		this.mvrRiskType = mvrRiskType;
		
		if (mvrRiskType != null){
			riskType = mvrRiskType.name();
		}
	}

	@Override
	public String toString() {
		return "MVRRiskRating [mvrRiskType=" + mvrRiskType + "]";
	}

}

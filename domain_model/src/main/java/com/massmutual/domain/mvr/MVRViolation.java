package com.massmutual.domain.mvr;

import java.io.Serializable;
import java.util.Date;

import com.massmutual.domain.RiskBase;

// motor vehicle report 
public class MVRViolation extends RiskBase implements Serializable {
	
	private static final long serialVersionUID = -4379994034232452491L;
	
	private String violationCode;
	private Date violationDate;
	private int points;
	
	public String getViolationCode() {
		return violationCode;
	}
	public void setViolationCode(String violationCode) {
		this.violationCode = violationCode;
	}
	public Date getViolationDate() {
		return violationDate;
	}
	public void setViolationDate(Date violationDate) {
		this.violationDate = violationDate;
	}
	
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	
	/*******************************************************************/
	/*                    BRMS HELPER METHODS                          */
	/*******************************************************************/

	public double getDifferenceYears() {
	    
		double diffYears = 0.0;
		
		if (violationDate != null){
			long diff = new Date().getTime() - violationDate.getTime();
			
			double diffDays = diff / (24 * 60 * 60 * 1000);
			//System.out.println("diffDays: " + diffDays);
			
			// get years and round to 2 decimal point
			diffYears = (diffDays / 365)*100;	
			diffYears = (double) Math.round(diffYears);
			diffYears = diffYears/100;
			
		}
		
		//System.out.println("diffYears: " + diffYears);
		
		return diffYears;
	}
	
	/**
	 * Dummy method to avoid jackson issues, doesn't happen 100% of the time but better to prevent - 
	 * for every get method that is not a real property create a fake set method - do not remove or change.
	 */
	public void setDifferenceYears(double differenceYears){}
	
	/**
	 * Helper method used by the brms rules which assists with determining if the number of years
	 * between the application submit date and the violation date is less than or equal to the numYears passed in, if true then
	 * it will return true.
	 * 
	 * Processing:
	 * 		If violationDate is null return true - this is possible we have seen many violations where the date is missing, unable to verify conditions are not met must return true.
	 * 		If applicationSubmitDate is null return true - this should NEVER occur but will code for just in case - same reason as above unable to determine must assume the worst and have it reviewed.
	 * 		If violationDate is within numYears from applicationSubmitDate return true
	 *		Otherwise return false.
	 *
	 * @param applicationSubmitDate 
	 * @param numYears
	 * 
	 * @return boolean - see processing defined above
	 */
	public boolean violationDateIsWithinNumYearsSubmitDate(Date applicationSubmitDate, int numYears)
	{	
		try
		{
			//If either date is null return true, we can confirm that the date is not within num years,
			//yield on side of caution, assume the worst and it must be reviewed, let the rule trigger.
			if(violationDate == null || applicationSubmitDate == null)
			{
				return true;
			}
			
			System.out.println("violationDate = " + violationDate.toString());
			System.out.println("applicationSubmitDate = " + applicationSubmitDate.toString());
			
			//diffMillis will be positive regardless of which date comes before/after - additionally neither date has a timestamp portion, so no concerns
			//with going to that level, can do a simple rudimentary implementation like the below without concern as 5.1 is truly greater than 5
			long diffMillis =  Math.abs(applicationSubmitDate.getTime() - violationDate.getTime());
			long diffDays = diffMillis/86400000;	
			float diffYears = (float)diffDays/365;
			
			if(diffYears <= numYears)
			{
				System.out.println("diffYears is less than or equal to numYears, returning true " + diffYears + " <= " + numYears);
				return true;
			}
			System.out.println("diffYears is NOT less than or equal to numYears, returning false " + diffYears + " > " + numYears);
			return false;
		}
		catch(Exception e)
		{
			System.err.println("Exception raised in violationDateIsWithinNumYearsSubmitDate(...) - will be returning true, unable to verify conditions are not met so must respond with true so data is reviewed: " + e.getMessage());
			return true;
		}
	}
	
	@Override
	public String toString() {
		return "MVRViolation [violationCode=" + violationCode + ", violationDate=" + violationDate + ", differenceYears=" + getDifferenceYears() + ", points="
				+ points + "]";
	}

}

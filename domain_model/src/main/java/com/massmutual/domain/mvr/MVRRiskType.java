package com.massmutual.domain.mvr;

import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.RiskType;

public enum MVRRiskType implements RiskType {

	DEFAULT,
	MOVING_VIOLATIONS,
	NON_MOVING_VIOLATIONS,
	DUI;
	
	
	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		for (MVRRiskType v : values()) {
			
			names.add(v.name());		
	    }
		
		return names;
	}

}

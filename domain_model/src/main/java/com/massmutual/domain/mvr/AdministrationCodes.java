package com.massmutual.domain.mvr;

import java.util.ArrayList;
import java.util.List;
// Non Moving Violation codes regarded as Administration Codes
public class AdministrationCodes {
	
	private List<String> codes = new ArrayList<String>();
	private String description;

	public List<String> getCodes() {
		return codes;
	}

	public void setCodes(List<String> codes) {
		this.codes = codes;
	}
	
	// dummy method for rules only
	public String getAddCode() {
		return "";
	}
	public void setAddCode(String code) {
		this.codes.add(code);
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "AdministrationCodes [codes=" + codes + "]";
	}

}

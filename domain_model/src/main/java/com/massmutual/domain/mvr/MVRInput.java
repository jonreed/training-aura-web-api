package com.massmutual.domain.mvr;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.massmutual.domain.Insured;
import com.massmutual.domain.RiskInputBase;

// motor vehicle report 
public class MVRInput extends RiskInputBase implements Serializable {
	
	private static final long serialVersionUID = -4379994034232452491L;
	
	private Insured insured;
	//private MVRViolations violations;
	
	private List<MVRViolation> violationCodes;
	
	private Date applicationSubmitDate;
	
	public Insured getInsured() {
		return insured;
	}

	public void setInsured(Insured insured) {
		this.insured = insured;
	}

	public List<MVRViolation> getViolationCodes() {
		return violationCodes;
	}

	public void setViolationCodes(List<MVRViolation> violationCodes) {
		this.violationCodes = violationCodes;
	}

	public Date getApplicationSubmitDate() {
		return applicationSubmitDate;
	}

	public void setApplicationSubmitDate(Date applicationSubmitDate) {
		this.applicationSubmitDate = applicationSubmitDate;
	}

	@Override
	public String toString() {
		return "MVRInput [insured=" + insured + ", violationCodes="
				+ violationCodes + ", applicationSubmitDate="
				+ applicationSubmitDate + "]";
	}

}

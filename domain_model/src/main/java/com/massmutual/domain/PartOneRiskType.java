package com.massmutual.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public enum PartOneRiskType implements RiskType{

	AGE(RiskSummaryType.PART_I), 
	AGENT(RiskSummaryType.PART_I),
	ALIR(RiskSummaryType.PART_I), 
	ANNUAL_INCOME(RiskSummaryType.PART_I), 
	APS(RiskSummaryType.PART_I), 
	AVIATION(RiskSummaryType.PART_I), 
	AVOCATION(RiskSummaryType.PART_I), 
	BENE(RiskSummaryType.PART_I), 
	CAPTIVE_INSURANCE_CO(RiskSummaryType.PART_I),
	CITIZEN(RiskSummaryType.PART_I), 
	COLLATERAL_ASSIGNMENT(RiskSummaryType.PART_I), 
	DISABILITY(RiskSummaryType.PART_I), 
	DUI(RiskSummaryType.PART_I), 
	ECONOMIC_INCENTIVE(RiskSummaryType.PART_I), 
	FAMILY_INSURANCE(RiskSummaryType.PART_I), 
	FELONY(RiskSummaryType.PART_I), 
	FOREIGN_TRAVEL(RiskSummaryType.PART_I), 
	LAST_SEEN(RiskSummaryType.PART_I),
	LISR(RiskSummaryType.PART_I), 
	LONGTERM_CARE_RIDER(RiskSummaryType.PART_I), 
	//MAXIMUM_FACE_AMOUNT(RiskSummaryType.PART_I), 
	MILITARY(RiskSummaryType.PART_I), 
	MOVING_VIOLATIONS(RiskSummaryType.PART_I),
	MVR(RiskSummaryType.PART_I),
	OCCUPATION(RiskSummaryType.PART_I), 
	OWNER(RiskSummaryType.PART_I),
	PAYOR(RiskSummaryType.PART_I), 
	PREMIUM_SOURCE(RiskSummaryType.PART_I), 
	PREMIUM_TO_INCOME(RiskSummaryType.PART_I), 
	PURPOSE_OF_INSURANCE(RiskSummaryType.PART_I), 
	REPLACEMENT(RiskSummaryType.PART_I), 
	//RTR(RiskSummaryType.PART_I), 
	SMOKER(RiskSummaryType.PART_I), 
	SOURCE_OF_PREMIUM(RiskSummaryType.PART_I), 
	TERM(RiskSummaryType.PART_I), 
	TLIR(RiskSummaryType.PART_I), 
	TOTAL_AMOUNT_AT_RISK(RiskSummaryType.PART_I),
	TOTAL_AMOUNT(RiskSummaryType.PART_I), 
	UNEMPLOYED(RiskSummaryType.PART_I), 
	VIATICAL_CO(RiskSummaryType.PART_I), 
	WA_JUVI(RiskSummaryType.PART_I),
	ALL(RiskSummaryType.PART_I),
	EMPTY(RiskSummaryType.PART_I),
	HOUSE_HOLD_QUALIFICATION(RiskSummaryType.PART_I),
	UNEMPLOYMENT(RiskSummaryType.PART_I),
	AMOUNT_OF_RISK(RiskSummaryType.PART_I),
	WAIVER_OF_PREMIUM(RiskSummaryType.PART_I),
	FAMILY_LIMITS(RiskSummaryType.PART_I),
	LTC_RIDER(RiskSummaryType.PART_I),
	ADDITIONAL_LIFE_INSURANCE_RIDER(RiskSummaryType.PART_I),
	LIFE_INSURANCE_SUPPLEMENT_RIDER(RiskSummaryType.PART_I),
	//TOTAL_INSURED_AMOUNT(RiskSummaryType.PART_I),
	//TERM_RIDER(RiskSummaryType.PART_I),
	ASSIGNMENT_INTENDED(RiskSummaryType.PART_I),
	VIATICAL(RiskSummaryType.PART_I),
	CAPTIVE_OWNERSHIP(RiskSummaryType.PART_I),
	OWNERSHIP(RiskSummaryType.PART_I),
	BENEFICIARY(RiskSummaryType.PART_I),
	OTHER_LIFE_COVERAGE_REPLACEMENT(RiskSummaryType.PART_I),
	TEMPORARY_LIFE_INSURANCE_RECEIPT(RiskSummaryType.PART_I),
	PREMIUM_PAYER(RiskSummaryType.PART_I),
	EXAMINIATION_HISTORY(RiskSummaryType.PART_I),
	GIR(RiskSummaryType.PART_I),
	PCR(RiskSummaryType.PART_I);
		
	private RiskSummaryType riskSummaryType;
	
	PartOneRiskType(RiskSummaryType type) {
        this.riskSummaryType = type;

    }
	
	private RiskSummaryType getRiskType() { return riskSummaryType; }
	
	public List<String> getNames(RiskSummaryType type){
		
		List<String> names = new ArrayList<String>();
		for (PartOneRiskType r : values()) {
			
			if (r.getRiskType() == type){
				names.add(r.name());
			}
	    }
		
		return names;
	}

}

package com.massmutual.domain;

import com.massmutual.domain.mvr.MVRInput;
import com.massmutual.domain.partone.PrimaryInsured;
import com.massmutual.domain.parttwo.PersonalHistory;
import com.massmutual.domain.rx.RXInput;

public class ConsolidatedRiskInput implements java.io.Serializable {

	static final long serialVersionUID = 1L;
	
	PrimaryInsured primaryInsured;
	PersonalHistory personalHistory;
	MVRInput mvrInput;
	RXInput rxInput;
	
	public PrimaryInsured getPrimaryInsured() {
		return primaryInsured;
	}
	public void setPrimaryInsured(PrimaryInsured primaryInsured) {
		this.primaryInsured = primaryInsured;
	}
	public PersonalHistory getPersonalHistory() {
		return personalHistory;
	}
	public void setPersonalHistory(PersonalHistory personalHistory) {
		this.personalHistory = personalHistory;
	}
	public MVRInput getMvrInput() {
		return mvrInput;
	}
	public void setMvrInput(MVRInput mvrInput) {
		this.mvrInput = mvrInput;
	}
	public RXInput getRxInput() {
		return rxInput;
	}
	public void setRxInput(RXInput rxInput) {
		this.rxInput = rxInput;
	}
	

}

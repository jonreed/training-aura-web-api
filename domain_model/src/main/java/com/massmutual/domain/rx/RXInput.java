package com.massmutual.domain.rx;

import java.io.Serializable;
import java.util.List;

import com.massmutual.domain.Insured;
import com.massmutual.domain.RiskInputBase;

// motor vehicle report 
public class RXInput extends RiskInputBase implements Serializable {
	
	private static final long serialVersionUID = -4379994034232452491L;
	
	private Insured insured;
	private RXCodes codes;
	
	public Insured getInsured() {
		return insured;
	}

	public void setInsured(Insured insured) {
		this.insured = insured;
	}

	public RXCodes getCodes() {
		return codes;
	}

	public void setCodes(RXCodes codes) {
		this.codes = codes;
	}

	@Override
	public String toString() {
		return "RXInput [insured=" + insured + ", codes=" + codes + "]";
	}

}

package com.massmutual.domain.rx;

import java.io.Serializable;
import java.util.List;

import com.massmutual.domain.Insured;
import com.massmutual.domain.RiskBase;
import com.massmutual.domain.RiskInputBase;

// motor vehicle report 
public class RXCodes extends RiskBase implements Serializable {
	
	private static final long serialVersionUID = -4379994034232452491L;
	
	private List<String> codes;

	public List<String> getCodes() {
		return codes;
	}

	public void setCodes(List<String> codes) {
		this.codes = codes;
	}

	// check if any of the input values are found
	public boolean getIncludesAnyCodes(String codeList) {
			
		if (codes != null && codeList != null) {
			for (String code : codeList.split(",")) {
				if (codes.contains(code)){
					return true;
				}
			}
		}
		
		return false;		
	}
	

	@Override
	public String toString() {
		return "RXCodes [codes=" + codes + "]";
	}

}

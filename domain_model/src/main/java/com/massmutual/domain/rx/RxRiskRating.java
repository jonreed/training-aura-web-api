package com.massmutual.domain.rx;

import com.massmutual.domain.RiskRating;

public class RxRiskRating extends RiskRating implements java.io.Serializable {

	static final long serialVersionUID = 1L;
	
	public RxRiskRating() {
		super();
	}
		
	public RxRiskRating(RiskRating rating) {

		super(rating);	
	
	}

	@Override
	public String toString() {
		return "RxRiskRating [super=" + super.toString() + "]";
	}

}

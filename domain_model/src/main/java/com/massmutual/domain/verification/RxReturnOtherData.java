package com.massmutual.domain.verification;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.massmutual.domain.RiskBase;

public class RxReturnOtherData extends RiskBase implements Serializable {
	private static final long serialVersionUID = 1213146254331666266L;
	private Date rxLastFillDate;

	public Date getRxLastFillDate() {
		return rxLastFillDate;
	}

	public void setRxLastFillDate(Date rxLastFillDate) {
		this.rxLastFillDate = rxLastFillDate;
	}

	public Long calculateDiffinDays(Date date) {
	    System.out.println(rxLastFillDate);
	    System.out.println(date);
	    if(rxLastFillDate == null || date == null){
			return null;
		}
		
		Calendar calEnd = Calendar.getInstance();
		calEnd.setTime(rxLastFillDate);
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(date);

		if(calEnd == null || calStart == null){
			return null;
		}

		long diff = calEnd.getTimeInMillis() - calStart.getTimeInMillis();
		long diffDays = diff / (1000 * 60 * 60 * 24);
	    System.out.println(Long.valueOf(diffDays));

		return Long.valueOf(diffDays);
	}
	
	@Override
	public String toString() {
		return "RxReturnOtherData [rxLastFillDate=" + rxLastFillDate + "]";
	}
}
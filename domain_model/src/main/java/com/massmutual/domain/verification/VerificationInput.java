package com.massmutual.domain.verification;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.labs.Applicant;
import com.massmutual.domain.mib.MIBInput;
import com.massmutual.domain.partone.PrimaryInsured;
import com.massmutual.domain.parttwo.PersonalHistory;


public class VerificationInput implements Serializable {
	private static final long serialVersionUID = -2151358398338317705L;
	private List<RxReturn> rxReturns;		// RX - RuleHit
	private RxReturnOtherData rxReturnOtherData;
	
	private PrimaryInsured insured;			// Part I
	private PersonalHistory personalHistory;// Part II
	private MIBInput mibInput;				// MIB
	private Applicant applicant;			// Labs
	
	
	public List<RxReturn> getRxReturns() {
		return rxReturns;
	}

	public void setRxReturns(List<RxReturn> rxReturns) {
		this.rxReturns = rxReturns;
	}


	public void addRxRule(RxReturn rxReturn) 
	{
		if(rxReturn == null)
		{
			return;
		}
		
		if(rxReturns == null)
		{
			rxReturns = new ArrayList<RxReturn>();
		}
		rxReturns.add(rxReturn);
	}
	
	
	public RxReturnOtherData getRxReturnOtherData() {
		return rxReturnOtherData;
	}

	public void setRxReturnOtherData(RxReturnOtherData rxReturnOtherData) {
		this.rxReturnOtherData = rxReturnOtherData;
	}

	public PrimaryInsured getInsured() {
		return insured;
	}

	public void setInsured(PrimaryInsured insured) {
		this.insured = insured;
	}

	public PersonalHistory getPersonalHistory() {
		return personalHistory;
	}

	public void setPersonalHistory(PersonalHistory personalHistory) {
		this.personalHistory = personalHistory;
	}

	public MIBInput getMibInput() {
		return mibInput;
	}

	public void setMibInput(MIBInput mibInput) {
		this.mibInput = mibInput;
	}

	public Applicant getApplicant() {
		return applicant;
	}

	public void setApplicant(Applicant applicant) {
		this.applicant = applicant;
	}

	
	/**
	 * Helper method
	 * 
	 * @param rxList a list of RXs to check for
	 * @return
	 */
	public boolean missingRXs(String...rxList) {
		try {
			// Build a list of all the RXs on this case
			List<String> rxs = new ArrayList<String>();
			for(RxReturn rxReturn:rxReturns) {
				rxs.add(rxReturn.getRuleName());
			}

			// See if any of the rxs passed in are on this case 
			for( String rx: rxList) {
				if( rxs.contains(rx) ) {
					return false;
				}
			}
		}
		catch(Exception e) {
			System.err.println("Exception raised in missingRXs(...) - will be returning false: " + e.getMessage());
			return false;
		} 
		
		return true;
	}
	

	@Override
	public String toString() {
		return "VerificationInput [rxReturns=" + rxReturns + ", rxReturnOtherData=" + rxReturnOtherData + ", insured="
				+ insured + ", personalHistory=" + personalHistory + ", mibInput=" + mibInput + ", applicant="
				+ applicant + "]";
	}
}
package com.massmutual.domain.verification;

import java.util.ArrayList;
import java.util.List;

import com.massmutual.domain.RiskType;

public enum VerificationType implements RiskType {

	DEFAULT,
	RX_PART_I, 
	RX_PART_II, 
	MIB_PART_I, 
	MIB_PART_II,
	LABS_PART_I, 
	LABS_PART_II,
	LABS_PART_II_RX;
	
	public static List<String> getNames(){
		
		List<String> names = new ArrayList<String>();
		for (VerificationType v : values()) {
			
			names.add(v.name());		
	    }
		
		return names;
	}

}

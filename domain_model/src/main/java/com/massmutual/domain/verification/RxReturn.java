package com.massmutual.domain.verification;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import com.massmutual.domain.RiskBase;

public class RxReturn extends RiskBase implements Serializable // RuleHit
{
	private static final long serialVersionUID = 7918241855142568039L;
	
	//Moving forward with versioning for Rx that impacts Verification Layer, setting a default of 1.0
	//this value will be overridden by aura-rules when appropriate for a new Rx version of the file.
	//Currently rx files are good for 6 months so we will be dealing with multiple versions for a while.
	private static final String DEFAULT_SCHEMA_VERSION = "1.0";
	
	/**
	 * Default constructor will set default schema version.
	 */
	public RxReturn() {
		super();
		super.setSchemaVersion(DEFAULT_SCHEMA_VERSION);
	}
	
	/**
	 * Rule Identifier
	 * In Rx XML example:  <RuleIdent>10002</RuleIdent>
	 * In Part II json:		
	*/
	private String ruleIdentifier;
	
	private String ruleName;
	private String rulePrefix;
	private String ruleSuffix;
	
	public String getRuleIdentifier() {
		return ruleIdentifier;
	}
	public void setRuleIdentifier(String ruleIdentifier) {
		this.ruleIdentifier = ruleIdentifier;
	}
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	public String getRulePrefix() {
		return rulePrefix;
	}
	public void setRulePrefix(String rulePrefix) {
		this.rulePrefix = rulePrefix;
	}
	public String getRuleSuffix() {
		return ruleSuffix;
	}
	public void setRuleSuffix(String ruleSuffix) {
		this.ruleSuffix = ruleSuffix;
	}
	@Override
	public String toString() {
		return "RxReturn [ruleIdentifier=" + ruleIdentifier + ", ruleName="
				+ ruleName + ", rulePrefix=" + rulePrefix + ", ruleSuffix="
				+ ruleSuffix + ", toString()=" + super.toString() + "]";
	}


}

package com.massmutual.domain.verification;

import com.massmutual.domain.RiskRating;

public class VerificationRiskRating extends RiskRating implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private VerificationType verificationRiskType;
	
	public VerificationRiskRating() {
		super();
	}
		
	public VerificationRiskRating(RiskRating rating) {

		super(rating);	
		this.verificationRiskType = VerificationType.valueOf(rating.getRiskType());
	
	}
	
	public VerificationType getVerificationRiskType() {
		return verificationRiskType;
	}

	public void setVerificationRiskType(VerificationType verificationRiskType) {
		this.verificationRiskType = verificationRiskType;
		riskType = verificationRiskType.name();
	}
	
	public RiskRating createRiskRating(){
		RiskRating rating = new RiskRating((RiskRating) this);
		rating.setRiskType(String.valueOf(verificationRiskType));
		return rating;
	}

	@Override
	public String toString() {
		return "VerificationRiskRating [verificationRiskType=" + verificationRiskType + ", super="
				+ super.toString() + "]";
	}

}

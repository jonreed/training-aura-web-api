package com.massmutual.domain.verification;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.massmutual.domain.RiskBase;

public class MedicalHistory extends RiskBase {

	private String physicianLastSeen;
	
	public String getPhysicianLastSeen() {
		return physicianLastSeen;
	}

	public void setPhysicianLastSeen(String physicianLastSeen) {
		this.physicianLastSeen = physicianLastSeen;
	}

	/**
	 * Tries to compare the two dates passed in.
	 *
	 * The dates coming in are Strings in one of two formats: yyyy-MM-dd or MM-yyyy
	 * @param date
	 * @return
	 */
	public Long calculateDiffinDays(String date){
		if(physicianLastSeen == null || date == null){
			return null;
		}
		
		Calendar calEnd = convertDate(physicianLastSeen);
		Calendar calStart = convertDate(date);
		
		if(calEnd == null || calStart == null){
			return null;
		}

		long diff = calEnd.getTimeInMillis() - calStart.getTimeInMillis();
		long diffDays = diff / (1000 * 60 * 60 * 24);
				
		return Long.valueOf(diffDays);
	}

	
	/**
	 * Tries to compare the physician last seen date to the date passed in.
	 *
	 * @param date
	 * @return
	 */
	public Long calculateDiffinDays(Date date){
	    System.out.println(physicianLastSeen);
	    System.out.println(date);
	    if(physicianLastSeen == null || date == null){
			return null;
		}
		
		Calendar calEnd = convertDate(physicianLastSeen);
		Calendar calStart = Calendar.getInstance();
		calStart.setTime(date);

		if(calEnd == null || calStart == null){
			return null;
		}

		long diff = calEnd.getTimeInMillis() - calStart.getTimeInMillis();
		long diffDays = diff / (1000 * 60 * 60 * 24);
	    System.out.println(Long.valueOf(diffDays));

		return Long.valueOf(diffDays);
	}

    /**
     * Attempts to convert a String to a Calendar by parsing again common patterns
     *
	 * @param dateStr The String representation of a Date
	 * @return
     */
	private Calendar convertDate(String dateStr) {

		try {
    		SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd");
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(format.parse(dateStr));
			return calendar;
		} catch (ParseException e) {
			return null;
		}
	}

	@Override
	public String toString() {
		return "MedicalHistory [physicianLastSeen=" + physicianLastSeen + "]";
	}
}
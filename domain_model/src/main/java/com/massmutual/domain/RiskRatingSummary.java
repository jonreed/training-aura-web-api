package com.massmutual.domain;

import java.util.ArrayList;
import java.util.List;

public class RiskRatingSummary extends RiskRatingBase implements java.io.Serializable {

	static final long serialVersionUID = 1L;
	
	// Risk rating rollups
	private List<RiskRatingRollup> riskRatingGroups = new ArrayList<RiskRatingRollup>();
	
	public RiskRatingSummary() {
		
	}

	public List<RiskRatingRollup> getRiskRatingGroups() {
		return riskRatingGroups;
	}

	public void setRiskRatingGroups(List<RiskRatingRollup> riskRatingGroups) {
		this.riskRatingGroups = riskRatingGroups;
	}

	@Override
	public String toString() {
		return "RiskRatingSummary [riskRatingGroups=" + riskRatingGroups + "], " + super.toString();
	}

}

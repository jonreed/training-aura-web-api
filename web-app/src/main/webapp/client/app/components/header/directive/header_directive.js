angular.module('HeaderModule',[])
    .directive('initHeader', function($log, $location) {

    return {
        restrict: 'A',
        templateUrl: 'client/app/components/header/view/header_view.html',
        controller : ['$scope', '$location', function ($scope, $location) {

            $log.debug("Directive initHeader:controller method");

            $scope.goTo = function (pageView){

                $log.debug("Inside goTo, pageView["+pageView+"]");

                $location.url("/" + pageView);
            }

            $scope.showConfigurationSettings = function(){

                $log.debug("Inside showConfigurationSettings");
            }

        }],
        link: function($scope, $element, $attributes) {

            $log.debug("Directive initHeader:link method, $attributes: ", $attributes );

            $scope.pageView = $attributes.initHeader;
            $scope.headerLabel = $attributes.headerLabel;


        }


    };
})
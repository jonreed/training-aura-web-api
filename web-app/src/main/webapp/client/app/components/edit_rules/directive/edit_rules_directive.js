angular.module('RulesModule')
    .directive('fileModel', function($log, $parse) {

        return {
            restrict: 'A',

            link: function($scope, $element, $attrs) {
                var model = $parse($attrs.fileModel);
                var modelSetter = model.assign;

                $log.debug("found fileModel: ", $attrs.fileModel);

                $element.bind('change', function(){
                    $scope.$apply(function(){
                        modelSetter($scope, $element[0].files[0]);
                    });
                });
            }


        };
    })
    /*
    How to use:
    HTML:

     <input type="file" on-read-file="showContent($fileContent)" />

    CONTROLLER:

     myapp.controller('MainCtrl', function ($scope) {
        $scope.showContent = function($fileContent){
                                $scope.content = $fileContent;
                            };
     });

     */
    .directive('onReadFile', function ($log, $parse) {
        return {
            restrict: 'A',
            scope: false,
            link: function(scope, element, attrs) {

                var fn = $parse(attrs.onReadFile);

                $log.debug("found onReadFile: ", attrs.onReadFile);

                element.on('change', function(onChangeEvent) {
                    var reader = new FileReader();

                    reader.onload = function(onLoadEvent) {
                        scope.$apply(function() {
                            $log.debug("found content: ", onLoadEvent.target.result);

                            fn(scope, {fileContent:onLoadEvent.target.result});
                        });
                    };

                    reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
                });
            }
        };
    });

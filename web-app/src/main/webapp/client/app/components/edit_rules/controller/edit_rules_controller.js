angular.module('RulesModule', ['ngResource', 'cb.x2js'])
.controller('RulesController',
    function ($scope, $http, $location, $log, x2js){

        console.log("Inside RulesController");

        $scope.showMouseHourGlass = false;
        $scope.rulesFile = null;
        $scope.loadedRules = [];
        
        var dummyXML = "<data><list>            <value>            <valueNumeric class=\"int\">1</valueNumeric>            <valueString/>            <dataType>NUMERIC_INTEGER</dataType>            <isOtherwise>false</isOtherwise>            </value>            <value>            <valueString/>            <dataType>STRING</dataType>            <isOtherwise>false</isOtherwise>            </value>            <value>            <valueString>calculate-risk-rating</valueString>            <dataType>STRING</dataType>            <isOtherwise>false</isOtherwise>            </value>            <value>            <valueString/>            <dataType>NUMERIC_INTEGER</dataType>            <isOtherwise>false</isOtherwise>            </value>            <value>            <valueNumeric class=\"int\">16</valueNumeric>            <valueString/>            <dataType>NUMERIC_INTEGER</dataType>            <isOtherwise>false</isOtherwise>            </value>            <value>            <valueString>rule-age-1</valueString>            <dataType>STRING</dataType>            <isOtherwise>false</isOtherwise>            </value>            <value>            <valueString>If calculated issue Insurance age is less than 17 or greater than  40.</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>PartOneRiskType.AGE</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>3</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>3</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>2</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>3</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>2</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>1</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        <value>        <valueString>1</valueString>        <dataType>STRING</dataType>        <isOtherwise>false</isOtherwise>        </value>        </list></data>";


        $scope.showContent = function(fileContent){

            $log.debug("Inside showContent, fileContent: ", fileContent);
            $scope.content = fileContent;
        };

        $scope.uploadFile = function(){

            $log.debug("Inside uploadFile", $scope.rulesFile);

            $scope.showMouseHourGlass = true;

            //var f = document.getElementById('file').files[0];
            var reader = new FileReader();

            reader.onloadend = function(e){
                var data = e.target.result;
                //$log.debug("read data", data);

                $scope.convertToRules(data);
            }
            //reader.readAsBinaryString(f);
            reader.readAsText($scope.rulesFile);


            $scope.showMouseHourGlass = false;

        };

        $scope.convertToRules = function(xml){

            $log.debug("Inside convertToRules, data: ");    //), xml);

            var parser = new DOMParser();
            var xmlDom = parser.parseFromString(dummyXML,"text/xml");
            var rules = xmlDom.getElementsByTagName("data")[0];

            $log.debug("rules: ", rules.innerHTML);

            var rows = rules.getElementsByTagName("list");

            $log.debug("rows: ", rows);

            // these are the rows (<list>)
            for (var i = 0; i < rows.length; i++){

                $log.debug("row["+i+"] *******");
                var columns = rows[i].getElementsByTagName("value");;

                // these are the columns (<value>)
                for (var j = 0; j < columns.length; j++){

                    $log.debug("col["+j+"] *******", columns[j].innerHTML);

                    var attributes = columns[j].childNodes;

                    for (var k = 0; k < attributes.length; k++){

                        // ignore spaces
                        if (attributes[k].nodeName !== "#text" ) {
                            $log.debug("attribute name: " + attributes[k].nodeName + ", value: " + attributes[k].nodeValue);
                        }


                    // <value>
                    //     <valueNumeric class="int">1</valueNumeric>
                    //     <valueString/>
                    //     <dataType>NUMERIC_INTEGER</dataType>
                    //     <isOtherwise>false</isOtherwise>
                    //    </value>
                    }
                }
            }

            // https://github.com/abdmob/x2js and https://github.com/cesarbarone/angular-x2js
            var x2js = new X2JS();

            var jsonObject = x2js.xml_str2json( dummyXML );

            $log.debug("jsonObject: ", jsonObject);

            $log.debug("xml: ", x2js.json2xml_str(jsonObject));
        }

        $scope.resetForm = function (){
            $scope.mainForm.$setPristine();

        };

        function init(){
            console.log("Inside init");


        }

        init();


    });

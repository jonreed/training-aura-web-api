var module = angular.module('PolicyService', [ 'ngResource' ])
    .factory('sharedResultSet', function($http) {
			    return {
			        get: function(polnumb) {
			            return $http.get('../aura/srvc/part1resp/' + polnumb).then(function (response) {
			                return response.data;
			            }, function (err) {
			                throw {
			                    message: 'Repos error',
			                    status: err.status, 
			                    data: err.data
			                };
			            });
			        }
			    };
			});







	

var module = angular.module('VerificationModule')

    .factory('VerificationRating', function ($resource) {
        return $resource('/mm-rest-app/rest/verification/rating', {},
            {
                'rate': { method:'POST',
                          isArray:false,
                          headers : {
                            Accept : 'application/json'
                          }
                        }
            });

    })
    .factory('VerificationRollup', function ($resource) {
        return $resource('/mm-rest-app/rest/verification/rollup', {},
            {
                'rate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    })
    .factory('VerificationSummary', function ($resource) {
        return $resource('/mm-rest-app/rest/verification/summary', {},
            {
                'rate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    });







	

angular.module('VerificationModule', ['ngResource'])
.controller('VerificationController',
    function ($scope, $http, $location, $log, VerificationRating, VerificationRollup, VerificationSummary, sharedProperties){

        console.log("Inside VerificationController");

        $scope.showMouseHourGlass = false;

        $scope.verificationOutput = {};
        $scope.rxInputList =  [
            {ruleName:"447"},
            {ruleName:"447Prior1"},
            {ruleName:"447Prior2"},

            {ruleName:"438"},
            {ruleName:"399Prior1"},
            {ruleName:"105"},
            {ruleName:"319Prior2"},
            {ruleName:"41"}

        ];

        $scope.principalInsured = {id: "a"};
        $scope.principalInsured.riskSummaryType = "PART_I";
        $scope.principalInsured.smoking = {};
        $scope.principalInsured.travel = {};
        $scope.principalInsured.military = {};
        $scope.principalInsured.aviation = {};
        $scope.principalInsured.avocation = {};


        $scope.principalInsured.policyAppliedFor = {};
        $scope.principalInsured.citizenShip = {};
        $scope.principalInsured.disability = {};
        $scope.principalInsured.dui = {};
        $scope.principalInsured.medicalHistory = {};
        $scope.principalInsured.felony = {};

        $scope.principalInsured.purposeOfInsurance = {};
        $scope.principalInsured.ownerBeneficary = {};
        $scope.principalInsured.paymentInformation = {};
        $scope.principalInsured.financialHistory = {};
        $scope.principalInsured.temporaryLifeInsuranceReceipt = {};


        $scope.personalHistory = {};
        $scope.personalHistory.riskSummaryType = "PART_II";

        $scope.personalHistory.bloodDisorder = {};
        $scope.personalHistory.bloodDisorder.conditions = [
            {name:"hadBDAnemia"        , found:"N", question: "Anemia"},
            {name:"hadBDAnemiaType"      , found:"", question: "Which type of anemia?"},
            {name:"hasBDAnemiaResolved"  , found:"N", question: "Is your anemia currently stable with oral iron pills/supplements?"},
            {name:"hadBDAnemiaBleeding", found:"N", question: "Is your iron deficiency due to chronic bleeding other than menstrual bleeding?"},
            {name:"hasBDAnemiaControlled"  , found:"N", question: "Is your anemia stable with vitamin B12 supplements (oral or injected)?"},
            {name:"hasBDAnemiaSymptoms"   , found:"N", question: "Do you currently have symptoms related to your anemia?"},
            {name:"hadBDSpleen"        , found:"N", question: "Enlarged spleen (hypersplenism), low platelet count (thrombocytopenia) or ITP (idiopathic thrombocytopenic purpura)"},
            {name:"hadBDClotting"  , found:"N", question: "Problems with blood clotting, easy or excessive bleeding, or coagulation defects (hemophilia)"},
            {name:"hadBDHemochromatosis", found:"N", question: "Hemochromatosis Iron or Vitamin Deficiency (without Anemia) Thalassemia Trait or Sickle Cell Trait (without Anemia)"},
            {name:"hadBDOther"  , found:"N", question: "Other"}

        ];

        $scope.personalHistory.cancer = {};
        $scope.personalHistory.cancer.conditions = [
            {name:"hadLeukemia",found:"N", question: "Leukemia?"},
            {name:"hadLymphoma",found:"N", question: "Lymphoma?"},
            {name:"hadOtherCancer",found:"N", question: "Other Cancer?"},
            {name:"hadCancerMelanoma",found:"N", question: "Did you have melanoma?"}

        ];

        $scope.personalHistory.heartCondition = {};
        $scope.personalHistory.heartCondition.conditions = [
            {name:"hadHCAttack"        , found:"N", question: "Heart attack, Stroke, Myocardial Infarction, Transient Ischemic Attack, Cardiomyopathy (Heart Muscle Problem) or other Heart Failure"},
            //{name:"hadHCAbnormalBeat"  , found:"N", question: "Abnormal heartbeat / atrial fibrillation"},
            {name:"hadHCABFibrillation", found:"N", question: "Do you have a history of atrial fibrillation or atrial flutter?"},
            {name:"hadHCABFTreatment"  , found:"N", question: "Has your abnormal heart beat been treated with medication, surgery or electrical shock (cardioversion)?"},
            {name:"hadHCABFTFainted"   , found:"N", question: "Did your abnormal heart beat ever cause you to faint or lose consciousness?"},

            //{name:"hadHCHypertension"  , found:"N", question: "High blood pressure / hypertension"},
            {name:"hadHCHMedication"   , found:"N", question: "Are you taking medication to control your hypertension or high blood pressure?"},
            //{name:"hadHCMurmur"        , found:"N", question: "Heart Murmur or Heart Valve Problem (including Mitral Valve Prolapse)"},
            {name:"hadHCMAdviseECG"    , found:"N", question: "Have you been advised to have your heart periodically tested with echocardiogram?"},
            {name:"hadHCMAdviseSurgery", found:"N", question: "Have you been advised to have surgery for your heart murmur or heart valve problem?"},

            {name:"hadHCMAdviseMeds"   , found:"N", question: "Have you been advised to take medication or restrict your activity due to your heart murmur or heart valve problem?"},
            //{name:"hadHCPericarditis"  , found:"N", question: "Pericarditis"},
            {name:"hadHCPMultipleEpisodes", found:"N", question: "Have you had more than one episode?"},
            {name:"hadHCPFullyRecovered"  , found:"N", question: "Was the last episode more than 6 months ago?"},
            {name:"hadHCBenign"        , found:"N", question: "Varicose Veins, Venous Insufficiency"},

            {name:"hadHCOther"        , found:"N", question: "Other/ Not Sure"},
            {name:"??"  , found:"N", question: "Elevated cholesterol or triglyceride levels (hyperlipidemia)?"},
            {name:"hadHCElevatedNow", found:"N", question: "Do you currently have elevated cholesterol levels?"}

        ];

        $scope.personalHistory.mentalHealth = {};
        $scope.personalHistory.mentalHealth.conditions = [
            {name:"hadEOHospitalizedLast5Years"     , found:"N", question: "In the past 5 years, have you been hospitalized or been enrolled in a partial hospitalization due to any of these disorders? "},
            {name:"hadEOAlcoholism"                 , found:"N", question: "Alcoholism?"},
            {name:"hadEOADepression"                , found:"N", question: "Anxiety or Depression?"},
            {name:"hadEOTreatment"                  , found:"N", question: "Are you currently undergoing any form of treatment (medication or counseling)?"},
            {name:"hadEODepressionSituationalOnly"  , found:"N", question: "Is your anxiety or depression situational only?"},
            {name:"hadEOHeartPalpitations"          , found:"N", question: "Do you suffer from panic or stress induced chest pains or heart palpitations?"},
            {name:"hadEOPalpPanickAttach2Years"     , found:"N", question: "Have you received medical care or been prescribed medication for panic attacks within the past 2 years"},
            {name:"hadEOADHDChildhood"              , found:"N", question: "Did your ADHD Symptoms begin in childhood (before age 10)?"},
            {name:"hadEO"                   , found:"N", question: "Cognitive Impairment?"},
            {name:"hadEODelusion"           , found:"N", question: "Delusion?"},
            {name:"hadEODrugCounseling"     , found:"N", question: "Drug Counseling?"},
            {name:"hadEOEatingDisorder"     , found:"N", question: "Eating Disorder?"},
            {name:"hadEOMajorDepression"    , found:"N", question: " Depression"},
            {name:"hadEOPanicAttack"        , found:"N", question: "Panic Attack?"},
            {name:"hadEOPAMedsLast2Years"   , found:"N", question: "Have you received medical care or been prescribed medication for panic attacks within the past 2 years?"},

            {name:"hadEOMajorDepressionTreatment", found:"N", question: "Any major depression treatment?"},
            {name:"hadEOSuicideAttempt"     , found:"N", question: "Suicide Attempt?"},
            {name:"hadEOMinorDisorders"     , found:"N", question: "Brief Stress or Grief Reaction  Performance Anxiety (public speaking, etc.)  Seasonal Affective Disorder  Social Phobia  Simple Phobia (fear of snakes, etc.)?"},
            {name:"hadEONotSure"            , found:"N", question: "Other?"}

        ];

        $scope.personalHistory.familyHistory = {};
        $scope.personalHistory.familyHistory.conditions = [
            {
                name: "hasConditions",
                found: "Y",
                question: "Has any conditions"
            }, {
                name: "hadSeriousCondition",
                found: "Y",
                question: "Has any Serious conditions"
            }

        ];

        $scope.personalHistory.familyHistory.motherDetails = [{
            member: "MOTHER",
            condition: "BREAST_OR_OVARIAN_CANCER",
            age: 48,
            lifeStatus: "DECEASED",
            informationSource: "MOTHER_DETAILS"
        }];

        $scope.personalHistory.familyHistory.fatherDetails = [{
            member: "FATHER",
            condition: "HEART_DISEASE",
            age: 49,
            lifeStatus: "DECEASED",
            informationSource: "FATHER_DETAILS" ,
            include: true
        }];

        $scope.momAndDad = [];

        // add mom and dad to the momAndDad list
        Array.prototype.push.apply($scope.momAndDad,
                                   $scope.personalHistory.familyHistory.motherDetails,
                                   $scope.personalHistory.familyHistory.fatherDetails);


        $scope.personalHistory.familyHistory.brothersDetails = [
            {
                member: "BROTHER",
                age: -1,
                lifeStatus: "DECEASED",
                informationSource: "BROTHER_STATUS",
                include: true
            }, {
                member: "BROTHER",
                condition: "BREAST_CANCER",
                age: 45,
                lifeStatus: "DECEASED",
                informationSource: "BROTHER_DETAILS",
                include: true
            }, {
                member: "BROTHER",
                condition: "OTHER_CANCER",
                age: 43,
                lifeStatus: "DECEASED",
                informationSource: "BROTHER_DETAILS",
                include: true
            }
        ];

        $scope.personalHistory.familyHistory.sistersDetails = [
            {
                member: "SISTER",
                age: -1,
                lifeStatus: "DECEASED",
                informationSource: "SISTER_STATUS",
                include: true
            }, {
                member: "SISTER",
                condition: "BREAST_CANCER",
                age: 47,
                lifeStatus: "DECEASED",
                informationSource: "SISTER_DETAILS"
            }, {
                member: "SISTER",
                condition: "SKIN_CANCER",
                age: 46,
                lifeStatus: "DECEASED",
                informationSource: "SISTER_DETAILS"
            }
        ];

        $scope.personalHistory.familyHistory.familyConditions = [
            {
                member: "FATHER",
                condition: "CANCER",
                diseaseType: "BREAST_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS",
                include: true
            }, {
                member: "FATHER",
                condition: "CANCER",
                diseaseType: "COLON_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS",
                include: true
            }, {
                member: "FATHER",
                condition: "HEART_DISEASE",
                age: 40,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "MOTHER",
                condition: "CANCER",
                diseaseType: "BREAST_CANCER",
                age: 39,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "SISTER",
                condition: "CANCER",
                diseaseType: "OVARIAN_CANCER",
                age: 41,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "SISTER",
                condition: "CANCER",
                diseaseType: "COLON_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "BROTHER",
                condition: "CANCER",
                diseaseType: "COLON_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "BROTHER",
                condition: "CANCER",
                diseaseType: "BREAST_CANCER",
                age: 41,
                informationSource: "FAMILY_CONDITIONS"
            }
        ];

        $scope.personalHistory.familyHistory.familySeriousConditions = [
            {
                member: "SISTER",
                condition: "HUNTINGTONS_DISEASE",
                age: 42,
                informationSource: "FAMILY_SERIOUS_CONDITIONS",
                include: true
            }, {
                member: "BROTHER",
                condition: "CARDIOMYOPATHY",
                age: 43,
                informationSource: "FAMILY_SERIOUS_CONDITIONS"
            }
        ];

        $scope.personalHistory.otherCondition = {};
        $scope.personalHistory.otherCondition.conditions = [
            {name:"hadTreatmentAdviseNotComp"   , found:"N", question: "Been advised by a member of the medical profession to have surgery, medical treatment, or diagnostic testing excluding HIV testing, that has not been completed. "},
            {name:"hadSurgeryNotDisclosed"      , found:"N", question: "Had surgery or been a patient overnight in a hospital, clinic or other medical or mental health facility for a condition not previously disclosed on this application?"},
            {name:"surgeriesRoutine"            , found:"N", question: "Were all such surgeries or overnight stays related to the following: sinus or ear problems, tonsillectomy, hernia surgery, gall bladder inflammation or stones, kidney stones, appendicitis, orthopedic repairs of injury, skin or plastic surgery?"},
            {name:"hadTreatmentOther"           , found:"N", question: "Are you currently under treatment by a member of the medical profession for anything not previously stated on this application?"},
            {name:"hadNoCondition"              , found:"N", question: "Do you have a Clean Sheet?"},
            {name:"hadImmunoDeficiency"         , found:"N", question: "A diagnosis of Human Immunodeficiency Virus (HIV) infection or Acquired Immune Deficiency Syndrome (AIDS)?"}


        ];

        $scope.personalHistory.personalInformation = {};

        $scope.personalHistory.pulmonaryDisorder = {};
        $scope.personalHistory.pulmonaryDisorder.conditions = [
            {name:"hadRDSarcoidosis"            , found:"N", question: "Sarcoidosis"},
            {name:"hadRDCOPD"                   , found:"N", question: "Chronic Obstructive Pulmonary Disease (COPD)"},
            {name:"hadRDEmphysema"              , found:"N", question: "Emphysema?"},
            {name:"hadRDAsthma"                 , found:"N", question: "Asthma"},
            {name:"hadRDAHospitalizedLast1Year" , found:"N", question: "Have you been hospitalized for asthma (kept overnight in the hospital) in the past year?"},
            {name:"hadRDAERLast1Year"   , found:"N", question: "Have you visited the emergency department in the past year for your asthma?"},
            {name:"hasRDAMeds"          , found:"N", question: "Are you currently taking daily steroid pills, such as prednisone, for your asthma?"},
            {name:"hadRDPulmonaryNodule", found:"N", question: "Pulmonary Nodule or spot on the lung?"},
            {name:"hasRDApnea"          , found:"N", question: "Sleep Apnea"},
            {name:"hasRDCPAP"           , found:"N", question: "Are you currently being treated with CPAP?"},
            {name:"hasRDCPAPLast6Months", found:"N", question: "Have you been using CPAP for more than 6 months?"},
            {name:"hasRDBronchitis"     , found:"N", question: "Bronchitis?"},
            {name:"hasRDBRecovered"     , found:"N", question: "Have you fully recovered?"},
            {name:"hadRDPneumonia"      , found:"N", question: "Pneumonia or Legionnaire's disease"},
            {name:"hasRDPRecovered"     , found:"N", question: "Did you fully recover without complications?"},
            {name:"hadRDTuberculosis"   , found:"N", question: "Tuberculosis"},
            {name:"hasRDTRecovered"     , found:"N", question: "Have you fully recovered without complications and are you no longer taking medications for tuberculosis?"},
            {name:"hadRDInfluenza"      , found:"N", question: "Influenza,  Intermittent Cough/Cold,  Seasonal or Perennial Allergies"},
            {name:"hadRDOther"          , found:"N", question: "Other"}

        ];


        $scope.personalHistory.medicalHistory = {};

        $scope.applicant = {age: 25,
                            sex: "M",
                            birthDate: "1995-02-05",
                            labSlip: {tobaccoUse: "Y",
                                smokingDuration: "5",
                                useNicotineDeliverySystem: "Y",
                                movingViolations: "Y",
                                conditions: [
                                    {name:"hasDTDiabetes"    , found:"N", question: "Personal history of diabetes?"},
                                    {name:"hadHeartCondition", found:"Y", question: "Heart disease?"}

                                ]}

        };


        $scope.mibInput = {riskSummaryType: "MIB"};
        $scope.mibInput.responses =  [
            {diagnosis:"181", modifier:"*", source:"*", timeframe: "N", siteCode: ""},
            {diagnosis:"985", modifier:"M", source:"*", timeframe: "N"}


        ];

        $scope.updateResponseData = function(response){

            $log.debug("inside updateResponseData, response: ", response);
            response.responseData = response.diagnosis + response.modifier + response.source + response.timeframe;

            if (response.siteCode != undefined && response.siteCode.length > 2){
                response.responseData += "(" + response.siteCode + ")";
            }

            return response.responseData;
        };

        function initScopeVars (){

            console.log("Inside initScopeVars");

            $scope.errorQuoteMessages = [];
            $scope.warningQuoteMessages = [];
            $scope.infoQuoteMessages = [];

            $scope.QuoteStatus  = {
                "FORM_INCOMPLETE": "FORM_INCOMPLETE",
                "NEED_VALIDATION_CHECK": "NEED_VALIDATION_CHECK",
                "ELIBIBILITY_COMPLETE": "ELIBIBILITY_COMPLETE"

            };

            for (index in $scope.mibInput.responses){

                $scope.updateResponseData($scope.mibInput.responses[index]);

            }

        }

        initScopeVars();

        function doQuoteMessages (){

            var haveErrors = false;
            var messages = $scope.wrapper.quoteMessages;
            $scope.errorQuoteMessages = [];
            $scope.infoQuoteMessages = [];
            $scope.warningQuoteMessages = [];

            if (messages && messages.length > 0){

                for (var i = 0; i < messages.length; i++) {
                    var msg = messages[i];
                    if ("ERROR" == msg.messageStatus) {
                        $scope.errorQuoteMessages.push(msg);
                        haveErrors = true;
                    }
                    if ("INFO" == msg.messageStatus) {
                        $scope.infoQuoteMessages.push(msg);
                    }
                    if ("WARNING" == msg.messageStatus) {
                        $scope.warningQuoteMessages.push(msg);
                    }

                }
            }


            return haveErrors;

        }

        function updateQuoteStatus (){
            $log.debug("Inside updateQuoteStatus, before: " + $scope.mainForm.$valid);

            if ($scope.mainForm.$valid) {

                $scope.quoteStatus = $scope.QuoteStatus.NEED_ELIGIBILITY_CHECK;
            }
            else {

                $scope.quoteStatus = $scope.QuoteStatus.FORM_INCOMPLETE;
            }

            //console.log("status after: " + $scope.quoteStatus, $scope.mainForm.$error);

            if ($scope.mainForm.$error && $scope.mainForm.$error.required){

                $scope.requiredFieldList = "The following fields are required:\n";

                for(index in $scope.mainForm.$error.required){
                    console.log("required field: " + $scope.mainForm.$error.required[index].$name);
                    $scope.requiredFieldList += $scope.mainForm.$error.required[index].$name + "\n";
                }
            }
        }

        function processUserInput (riskType){

            $log.debug("Inside processUserInput, riskType["+riskType+"]");

            if ($scope.principalInsured.smoking.usedPast12Months == "Y"){
                $scope.principalInsured.smoking.smokingDuration = 13;
            }
            else if ($scope.principalInsured.smoking.usedPast24Months == "Y"){
                $scope.principalInsured.smoking.smokingDuration = 25;
            }
            else if ($scope.principalInsured.smoking.usedPast12Months == "N" &&
                     $scope.principalInsured.smoking.usedPast24Months == "N"){

                $scope.principalInsured.smoking.smokingDuration = 0;
            }

            $scope.principalInsured.riskType = riskType;

        }

        function replacer(key, value) {

            return "null";
        }

        $scope.calculateVerificationSummary = function (riskType){

            $log.debug("Inside calculateRiskSummary Rx, principalInsured: ", JSON.stringify($scope.principalInsured, null, 4));

            $scope.showMouseHourGlass = true;
            processUserInput(riskType);

            var verificationInput = {};

            verificationInput.rxReturns = $scope.rxInputList;
            verificationInput.insured = $scope.principalInsured;
            verificationInput.personalHistory = $scope.personalHistory;
            verificationInput.applicant = $scope.applicant;
            verificationInput.mibInput = $scope.mibInput;

            $log.debug("verificationInput: ", JSON.stringify(verificationInput, null, 4));

            VerificationSummary.rate(verificationInput,
                function (response){

                    $log.debug("calculateRiskSummary received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;
                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskSummary:received an error: ', err);
                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });

        };

        $scope.calculateVerificationRollup = function (riskType){

            $log.debug("Inside calculateVerificationRollup");

            $scope.showMouseHourGlass = true;

            processUserInput(riskType);

            var verificationInput = {};

            verificationInput.rxReturns = $scope.rxInputList;
            verificationInput.insured = $scope.principalInsured;
            verificationInput.personalHistory = $scope.personalHistory;
            verificationInput.applicant = $scope.applicant;
            verificationInput.mibInput = $scope.mibInput;

            $log.debug("verificationInput: ", JSON.stringify(verificationInput, null, 4));

            VerificationRollup.rate(verificationInput,
                function (response){

                    $log.debug("calculateVerificationRollup received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;
                    $location.url("/results");


                },
                function (err){
                    console.error('calculateRiskRollup:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateVerificationRating = function (riskType){

            $log.debug("Inside calculateVerificationRating");

            $scope.showMouseHourGlass = true;

            processUserInput(riskType);

            var verificationInput = {};

            verificationInput.rxReturns = $scope.rxInputList;
            verificationInput.insured = $scope.principalInsured;
            verificationInput.personalHistory = $scope.personalHistory;
            verificationInput.applicant = $scope.applicant;
            verificationInput.mibInput = $scope.mibInput;

            $log.debug("verificationInput: ", JSON.stringify(verificationInput, null, 4));

            VerificationRating.rate(verificationInput,
                function (response){

                    $log.debug("calculateVerificationRating received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;
                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskRating:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };




        $scope.resetQuote = function (){
            $scope.mainForm.$setPristine();

            initQuote();

            updateQuoteStatus();

        };

        function initQuote(){
            console.log("Inside initQuote");

        }

        initQuote();

    });

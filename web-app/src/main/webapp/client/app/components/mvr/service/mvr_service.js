var module = angular.module('MVRModule')
    .factory('RiskRatingMVR', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rating_mvr', {},
            {
                'rate': { method:'POST',
                          isArray:false,
                          headers : {
                            Accept : 'application/json'
                          }
                        }
            });

    })
    .factory('RiskRollupMVR', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rollup_mvr', {},
            {
                'rate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    })
    .factory('RiskSummaryMVR', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/summary_mvr', {},
            {
                'rate': { method:'POST',
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    });









	

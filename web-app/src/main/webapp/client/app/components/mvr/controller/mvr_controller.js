angular.module('MVRModule', ['ngResource'])
.controller('MVRController',
    function ($scope, $http, $location, $log, RiskRatingMVR, RiskRollupMVR, RiskSummaryMVR){

        console.log("Inside MVRController");

        $scope.showMouseHourGlass = false;
        $scope.inputCodeArray = [{include: true, violationCode: "01110", violationDate: "2015-03-15"},
                                {include: true, violationCode: "01120", violationDate: "2015-03-15"},
                                {include: true, violationCode: "01130", violationDate: "2015-03-15"},
                                {include: false, violationCode: "09110", violationDate: "2015-03-15"},
                                {include: false, violationCode: "16700", violationDate: "2015-03-15"},
                                {include: false, violationCode: "16710", violationDate: "2015-03-15"},
                                {include: false, violationCode: "", violationDate: ""},
                                {include: false, violationCode: "", violationDate: ""},
                                {include: false, violationCode: "", violationDate: ""},
                                {include: false, violationCode: "", violationDate: ""}];

        $scope.mvrInput = {insured: {}, violations:{}};
        $scope.mvrInput.insured = {licenseAvailable: "Y"};
        $scope.mvrInput.violationCodes =  [];
        $scope.results = {};

        $scope.resetViolationCodes = function (){
            $scope.mainForm.$setPristine();

        };

        function copyViolationCodes(){

            $log.debug("inside copyViolationCodes, $scope.inputCodeArray: ", $scope.inputCodeArray);

            $scope.mvrInput.violationCodes =  [];

            for (index in $scope.inputCodeArray){

                if ($scope.inputCodeArray[index].include === true){
                    $scope.mvrInput.violationCodes.push($scope.inputCodeArray[index]);
                }

            }

            $log.debug("mvrInput: ", JSON.stringify($scope.mvrInput, null, 4));

        };

        $scope.getRiskClass = function(riskClassValue){

            var riskClass = "column-risk-yellow";

            if (riskClassValue == 3){
                riskClass = "column-risk-red";
            }
            else if (riskClassValue == 2){
                riskClass = "column-risk-orange";
            }
            else if (riskClassValue == 1){
                riskClass = "column-risk-green";
            }


            return riskClass;
        };

        $scope.calculateRiskSummary = function (){

            $log.debug("Inside calculateRiskSummary MVR");

            $scope.showMouseHourGlass = true;
            $scope.results = null;

            updateQuoteStatus();

            copyViolationCodes();

            RiskSummaryMVR.rate($scope.mvrInput,
                function (response){

                    $log.debug("calculateRiskSummary received data: ", response);

                    $scope.results = response;
                    $scope.results.riskRatings = [];
                    $scope.results.riskRollups = [];

                    var rollups = [];

                    if (response.summary[0] != undefined){
                        rollups = response.summary[0].riskRatingGroups;
                    }

                    //$log.debug("rollups: ", rollups);

                    for (index in rollups) {
                        $log.debug("found rollup: " , rollups[index]);

                        if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                            $log.debug("found rating list: " , rollups[index].riskRatings);
                            // append to results.riskRatings array
                            $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                            $log.debug("rating list after adding: " , $scope.results.riskRatings);
                        }
                    }

                    $scope.results.riskRollups = rollups;

                    $scope.showMouseHourGlass = false;

                },
                function (err){
                    console.error('calculateRiskSummary:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskRollup = function (){

            $log.debug("Inside calculateRiskRollup MVR");

            $scope.showMouseHourGlass = true;
            $scope.results = null;

            updateQuoteStatus();

            copyViolationCodes();

            RiskRollupMVR.rate($scope.mvrInput,
                function (response){

                    $log.debug("calculateRiskRollup received data: ", response);


                    $scope.results = response;
                    $scope.results.riskRatings = [];
                    var rollups = response.riskRollups; //riskRatings

                    for (index in rollups) {
                        $log.debug("found rollup: " , rollups[index]);

                        if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                            $log.debug("found rating list: " , rollups[index].riskRatings);
                            $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                            $log.debug("rating list after adding: " , $scope.results.riskRatings);
                        }
                    }

                    $scope.showMouseHourGlass = false;

                },
                function (err){
                    console.error('calculateRiskRollup:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskResults = function (){

            $log.debug("Inside calculateRiskResults MVR");

            $scope.showMouseHourGlass = true;
            $scope.results = null;

            updateQuoteStatus();

            copyViolationCodes();

            RiskRatingMVR.rate($scope.mvrInput,
                function (response){

                    $log.debug("calculateRiskResults received data: ", response);

                    $scope.results = response;

                    $scope.showMouseHourGlass = false;

                },
                function (err){
                    console.error('calculateRiskResults:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        function updateQuoteStatus (){

            $log.debug("Inside updateQuoteStatus, before: " + $scope.mainForm);

            if ($scope.mainForm == undefined) {

                $log.debug("no form to check yet");
                return;
            }

            if ($scope.mainForm.$valid) {

                $log.debug("All required fields are filled in");
            }

            if ($scope.mainForm.$error && $scope.mainForm.$error.required){

                $scope.requiredFieldList = "The following fields are required:\n";

                for(index in $scope.mainForm.$error.required){
                    console.log("required field: " + $scope.mainForm.$error.required[index].$name);
                    $scope.requiredFieldList += $scope.mainForm.$error.required[index].$name + "\n";
                }
            }
        }

        function init(){
            console.log("Inside init");

            updateQuoteStatus();


        }

        init();


    });

var module = angular.module('MIBModule')
    .factory('RiskRatingMIB', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rating_mib', {},
            {
                'rate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    })
    .factory('RiskRollupMIB', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rollup_mib', {},
            {
                'rate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    })
    .factory('RiskSummaryMIB', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/summary_mib', {},
            {
                'rate': { method:'POST',
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    });









	

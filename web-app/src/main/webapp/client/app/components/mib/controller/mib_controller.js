angular.module('MIBModule', ['ngResource'])
.controller('MIBController',
    function ($scope, $http, $location, $log, RiskRatingMIB, RiskRollupMIB, RiskSummaryMIB, sharedProperties){

        console.log("Inside MIBController");

        $scope.showMouseHourGlass = false;
        $scope.mibInput = {};

        // attribute: Diagnosis Modifier Source Timeframe SiteCode
        // position:    0-2        3        4       5        (7-9)
        $scope.mibInput.responses =  [
            {diagnosis:"011", modifier:"*", source:"*", timeframe: "*", siteCode: "*"},
            {diagnosis:"057", modifier:"*", source:"*", timeframe: "N"},
            {diagnosis:"057", modifier:"*", source:"*", timeframe: "B"},
            {diagnosis:"110", modifier:"W", source:"*", timeframe: "*"},
            {diagnosis:"140", modifier:"T", source:"*", timeframe: "*"},
            {diagnosis:"203", modifier:"*", source:"*", timeframe: "N"},
            {diagnosis:"300", modifier:"*", source:"*", timeframe: "P"}

        ];

        $scope.resetDiagnosis = function (){
            $scope.mainForm.$setPristine();

        };

        $scope.updateResponseData = function(response){

            $log.debug("inside updateResponseData, response: ", response);
            response.responseData = response.diagnosis + response.modifier + response.source + response.timeframe;

            if (response.siteCode != undefined && response.siteCode.length > 2){
                response.responseData += "(" + response.siteCode + ")";
            }

            return response.responseData;
        };

        $scope.getRiskClass = function(riskClassValue){

            var riskClass = "column-risk-yellow";

            if (riskClassValue == 3){
                riskClass = "column-risk-red";
            }
            else if (riskClassValue == 2){
                riskClass = "column-risk-orange";
            }
            else if (riskClassValue == 1){
                riskClass = "column-risk-green";
            }


            return riskClass;
        };

        $scope.calculateRiskSummary = function (){

            $log.debug("Inside calculateRiskSummary MIB", JSON.stringify($scope.mibInput, null, 4));

            $scope.showMouseHourGlass = true;

            updateQuoteStatus();

            RiskSummaryMIB.rate($scope.mibInput,
                function (response){

                    $log.debug("calculateRiskSummary received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType("ALL");    // this filters the risk for a given type

                    $scope.showMouseHourGlass = false;

                    $location.url("/results");


                },
                function (err){
                    console.error('calculateRiskSummary:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskRollup = function (){

            $log.debug("Inside calculateRiskRollup MIB");

            $scope.showMouseHourGlass = true;

            updateQuoteStatus();

            RiskRollupMIB.rate($scope.mibInput,
                function (response){

                    $log.debug("calculateRiskRollup received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType("ALL");

                    $scope.showMouseHourGlass = false;

                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskRollup:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskResults = function (){

            $log.debug("Inside calculateRiskResults MIB");

            $scope.showMouseHourGlass = true;

            updateQuoteStatus();

            RiskRatingMIB.rate($scope.mibInput,
                function (response){

                    $log.debug("calculateRiskResults received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType("ALL");

                    $scope.showMouseHourGlass = false;

                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskResults:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        function updateQuoteStatus (){

            $log.debug("Inside updateQuoteStatus, before: " + $scope.mainForm);

            if ($scope.mainForm == undefined) {

                $log.debug("no form to check yet");
                return;
            }

            if ($scope.mainForm.$valid) {

                $log.debug("All required fields are filled in");
            }

            if ($scope.mainForm.$error && $scope.mainForm.$error.required){

                $scope.requiredFieldList = "The following fields are required:\n";

                for(index in $scope.mainForm.$error.required){
                    console.log("required field: " + $scope.mainForm.$error.required[index].$name);
                    $scope.requiredFieldList += $scope.mainForm.$error.required[index].$name + "\n";
                }
            }
        }

        function init(){
            console.log("Inside init");

            updateQuoteStatus();

            for (index in $scope.mibInput.responses){

                $scope.updateResponseData($scope.mibInput.responses[index]);

            }

        }

        init();


    });

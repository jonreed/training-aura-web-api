angular.module('RatingResultsController', ['Part1Module'])
.controller('ResultsController',
    function ($scope, $http, $location, $log, sharedProperties){

        $log.debug("Inside ResultsController");

        $scope.results = sharedProperties.getRatingResults();
        var filterByRiskType = sharedProperties.getRiskType();
        $log.debug("filterByRiskType["+filterByRiskType+"], riskRatings: ", $scope.results);

        processInputResuts();

        function processInputResuts(){

            $log.debug("Inside processInputResuts");

            if ($scope.results.riskRollups != undefined && $scope.results.riskRollups.length > 0){

                $log.debug("Have a list of rollups");
                $scope.results.riskRatings = [];
                var rollups = $scope.results.riskRollups; //riskRatings

                for (index in rollups) {
                    //$log.debug("found rollup: " , rollups[index]);

                    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                        //$log.debug("found rating list: " , rollups[index].riskRatings);
                        $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                        //$log.debug("rating list after adding: " , results.riskRatings);
                    }
                }
            }
            else if ($scope.results.summary != undefined && $scope.results.summary.length > 0){

                $log.debug("Have a summary so pull rollups and risk ratings");
                $scope.results.riskRatings = [];
                $scope.results.riskRollups = [];

                var rollups = [];

                rollups = $scope.results.summary[0].riskRatingGroups;

                for (index in rollups) {
                    //$log.debug("found rollup: " , rollups[index]);

                    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                        //$log.debug("found rating list: " , rollups[index].riskRatings);
                        // append to results.riskRatings array
                        $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                        //$log.debug("rating list after adding: " , results.riskRatings);
                    }
                }

                $scope.results.riskRollups = rollups;
            }

        }

        $scope.filterByRiskType = function (rating){

            //$log.debug("Inside filterByRiskType, rating.riskType["+rating.riskType+"]");

            if (filterByRiskType == "ALL"){
                return true;
            }
            else if (filterByRiskType == rating.riskType){
                return true;
            }

            return false;
        };

        $scope.getRiskClass = function(riskClassValue){

            var riskClass = "column-risk-yellow";

            if (riskClassValue == 3){
                riskClass = "column-risk-red";
            }
            else if (riskClassValue == 2){
                riskClass = "column-risk-orange";
            }
            else if (riskClassValue == 1){
                riskClass = "column-risk-green";
            }


            return riskClass;
        }

        $scope.returnToquestions = function(){
            //$location.url("/part1");
            window.history.back();
        }




    });

var module = angular.module('Part2Module')
    .factory('RiskRatingP2', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rating_p2', {},
            {
                'rate': { method:'POST',
                          isArray:false,
                          headers : {
                            Accept : 'application/json'
                          }
                        }
            });

    })  //http://localhost:8080
    .factory('RiskRollupP2', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rollup_p2', {},
            {
                'rate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    })
    .factory('RiskSummaryP2', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/summary_p2', {},
            {
                'rate': { method:'POST',
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    });









	

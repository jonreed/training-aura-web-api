angular.module('Part2Module', ['ngResource'])
.controller('Part2Controller',
    function ($scope, $http, $location, $log, RiskRatingP2, RiskRollupP2, RiskSummaryP2, sharedProperties){

        console.log("Inside Part2Controller");

        $scope.showMouseHourGlass = false;
        $scope.personalHistory = {};
        $scope.personalHistory.heartCondition = {};
        $scope.personalHistory.heartCondition.conditions = [
            {name:"hadHCAttack"        , found:"N", question: "Heart attack, Stroke, Myocardial Infarction, Transient Ischemic Attack, Cardiomyopathy (Heart Muscle Problem) or other Heart Failure"},
           //{name:"hadHCAbnormalBeat"  , found:"N", question: "Abnormal heartbeat / atrial fibrillation"},
            {name:"hadHCABFibrillation", found:"N", question: "Do you have a history of atrial fibrillation or atrial flutter?"},
            {name:"hadHCABFTreatment"  , found:"N", question: "Has your abnormal heart beat been treated with medication, surgery or electrical shock (cardioversion)?"},
            {name:"hadHCABFTFainted"   , found:"N", question: "Did your abnormal heart beat ever cause you to faint or lose consciousness?"},
            //{name:"hadHCHypertension"  , found:"N", question: "High blood pressure / hypertension"},
            {name:"hadHCHMedication"   , found:"N", question: "Are you taking medication to control your hypertension or high blood pressure?"},
            {name:"medsHCH"           , found:"2", question: "Tell us which medication(s):"},
            {name:"hadHCMurmu"        , found:"N", question: "Heart Murmur or Heart Valve Problem (including Mitral Valve Prolapse)"},
            {name:"hadHCMAdviseECG"    , found:"N", question: "Have you been advised to have your heart periodically tested with echocardiogram?"},
            {name:"hadHCMAdviseSurgery", found:"N", question: "Have you been advised to have surgery for your heart murmur or heart valve problem?"},
            {name:"hadHCMAdviseMeds"   , found:"N", question: "Have you been advised to take medication or restrict your activity due to your heart murmur or heart valve problem?"},
            {name:"hadHCPericarditis"  , found:"N", question: "Pericarditis"},
            {name:"hadHCPMultipleEpisodes", found:"N", question: "Have you had more than one episode?"},
            {name:"hadHCPFullyRecovered"  , found:"N", question: "Was the last episode more than 6 months ago?"},
            {name:"hadHCBenign"        , found:"N", question: "Varicose Veins, Venous Insufficiency"},
            {name:"hadHCOther"        , found:"N", question: "Other/ Not Sure"},
            {name:"hadHighCholesterol"  , found:"N", question: "Elevated cholesterol or triglyceride levels (hyperlipidemia)?"},
            {name:"hadHCElevatedNow", found:"N", question: "Do you currently have elevated cholesterol levels?"}

        ];

        $scope.personalHistory.cancer = {};
        $scope.personalHistory.cancer.conditions = [
            {name:"hadLeukemia",found:"N", question: "Leukemia?"},
						{name:"hadLymphoma",found:"N", question: "Lymphoma?"},
						{name:"hadOtherCancer",found:"N", question: "Other Cancer?"},
                        {name:"hadCancerMelanoma",found:"N", question: "Did you have melanoma?"}

        ];

        $scope.personalHistory.bloodDisorder = {};
        $scope.personalHistory.bloodDisorder.conditions = [
            {name:"hadBDAnemia"        , found:"N", question: "Anemia"},
            {name:"hadBDAnemiaType"      , found:"", question: "Which type of anemia?"},
            {name:"hasBDAnemiaResolved"  , found:"N", question: "Is your anemia currently stable with oral iron pills/supplements?"},
            {name:"hadBDAnemiaBleeding", found:"N", question: "Is your iron deficiency due to chronic bleeding other than menstrual bleeding?"},
            {name:"hasBDAnemiaControlled"  , found:"N", question: "Is your anemia stable with vitamin B12 supplements (oral or injected)?"},
            {name:"hasBDAnemiaSymptoms"   , found:"N", question: "Do you currently have symptoms related to your anemia?"},
            {name:"hadBDSpleen"        , found:"N", question: "Enlarged spleen (hypersplenism), low platelet count (thrombocytopenia) or ITP (idiopathic thrombocytopenic purpura)"},
            {name:"hadBDClotting"  , found:"N", question: "Problems with blood clotting, easy or excessive bleeding, or coagulation defects (hemophilia)"},
            {name:"hadBDHemochromatosis", found:"N", question: "Hemochromatosis Iron or Vitamin Deficiency (without Anemia) Thalassemia Trait or Sickle Cell Trait (without Anemia)"},
            {name:"hadBDOther"  , found:"N", question: "Other"}

        ];

        $scope.personalHistory.chronicFatigue = {};
        $scope.personalHistory.chronicFatigue.conditions = [
            {name:"hadRDChronicFatigue"        , found:"N", question: "Chronic fatigue syndrome or fibromyalgia"},
            {name:"hasRDCFFunctionNormal"  , found:"N", question: "Are you able to function (work, school, etc.) normally?"},
            {name:"hadRDDepression", found:"N", question: "Do you suffer from depression?"},
            //{name:"hadRDEpsteinBarr"  , found:"N", question: "Epstein-Barr virus"},
            {name:"hadRDEBOtherSymptoms"   , found:"N", question: "Do you currently suffer from fatigue or other symptoms related to your virus?"},
            {name:"hadRDLupus"        , found:"N", question: "Systemic Lupus Erythematosus (SLE)"},
            {name:"hadRDLymeDisease"  , found:"N", question: "Lyme disease"},
            {name:"hadRDLDSymptoms", found:"N", question: "Do you currently have fatigue or any other symptoms associated with your Lyme disease?"},
            {name:"hasRDLDTreatment"  , found:"N", question: "Are you currently receiving treatment related to Lyme disease?"},
            {name:"hasRDOther"   , found:"N", question: "Other"}

        ];

        $scope.personalHistory.diabetes = {};
        $scope.personalHistory.diabetes.conditions = [
            {name:"hasDTDiabetes"        , found:"N", question: "Diabetes?"},
            {name:"hasDTDType"        , found:"", question: "What type of diabetes do/did you have?"},
            {name:"hadGestOver5YearsBefore"  , found:"N", question: "Has it been more than 5 years since your last pregnancy with gestational diabetes?"},
            {name:"hasDTProlactinoma", found:"N", question: "Prolactinoma?"},
            {name:"hasDTThyroidDisease"  , found:"N", question: "Thyroid cysts, nodules, or thyroid removal surgery"},
            {name:"hasDTThyroidSurgeryBenign"   , found:"N", question: "Have you had surgery or biopsy which demonstrated your cysts or nodules to be benign?"},
            {name:"hasDTTImageBiopsy"        , found:"N", question: "Have you been advised to have periodic imaging or biopsy of a thyroid cyst, nodule or tumor?"},
            {name:"hasDTThyroidNotSerious"  , found:"N", question: "Under- or overactive thyroid (hypo- or hyperthyroidism), Grave's or Hashimoto's disease?"},
            {name:"hasDTTStable", found:"N", question: "Is your condition treated with medication and stable?"},
            {name:"hasDTNotSure"   , found:"N", question: "Other"}

        ];

        $scope.personalHistory.disgestiveDisorder = {};
        $scope.personalHistory.disgestiveDisorder.conditions = [
            {name:"hadBDAnorectal"        , found:"N", question: "Anorectal fistula or sinus"},
            {name:"hadDDCeliacDisease"  , found:"N", question: "Celiac Disease?"},
            {name:"hadDDCeliacDietControlled", found:"N", question: "Are your celiac diease symptoms controlled with dietary restrictions (no medications)?"},
            {name:"hadDDColonPolyp"  , found:"N", question: "Colon polyp?"},
            {name:"hadDDChrons"   , found:"N", question: "Crohn's disease?"},
            {name:"hadDDColitusNonUlcerative"        , found:"N", question: "Colitis (non ulcerative)"},
            {name:"hadDDColitusUlcerative"  , found:"N", question: "Colitis (ulcerative)"},
            {name:"hadDDiverticulitis", found:"N", question: "Diverticulitis?"},
            {name:"hadDDDLast6Months"  , found:"N", question: "Have you had an episode in the past 6 months?"},
            {name:"hadDDNAFLD"   , found:"N", question: "Non-alcoholic fatty liver disease (NAFLD) or nonalcoholic steatohepatitis (NASH)"},
            {name:"hadDDLiverBiopsy"            , found:"N", question: "Have you ever been advised to have a biopsy of your liver, reduce alcohol or take medication for this condition?"},
            {name:"hadDDGastritis"              , found:"N", question: "Gastritis?"},
            {name:"hadDDHepititis"              , found:"N" , question: "Hepatitis B, C, or D?"},
            {name:"hadDDPancreaticInflamation"  , found:"N", question: "Pancreatic abscess or cysts"},
            {name:"hadDDPancreatitis"           , found:"N", question: "Pancreatitis (pancreatic inflammation)"},
            {name:"hadDDUlcer"                  , found:"N", question: "Ulcer"},
            {name:"hadDDURecovered"             , found:"N", question: "Is/are your ulcer(s) completely healed?"},
            {name:"hadDDWeightLossSurgery"      , found:"N", question: "Weight Loss Surgery?"},
            {name:"hadDDBenign"                 , found:"N", question: "Anorectal Abscess or Cyst  Appendicitis  Diverticulosis  Food (Lactose, Gluten, Etc.) Intolerance, Food Allergy  Gallbladder Pain  Gallstones  Gastroesophageal Reflux Disease (GERD)  Hemorrhoids  Hepatitis A or E, fully recovered  Hiatal Hernia  High Levels of Bilirubin (Hyperbilirubinemia)  Inguinal (Groin) Hernia  Intermittent or Chronic Constipation  Intermittent Diarrhea  Irritable Bowel Syndrome or Irritable Bowel Disease (IBS)  Rectal Prolapse"},
            {name:"hadBDNotSure"                , found:"N", question: "Other"}

        ];

        $scope.personalHistory.earsNoseThroatDisorder = {};
        $scope.personalHistory.earsNoseThroatDisorder.conditions = [
            {name:"hadEENTAcousicNeuroma" , found:"N", question: "Acoustic Neuroma?"},
            {name:"hadEENTLeukoplakia"    , found:"N", question: "Leukoplakia?"},
            {name:"hadEENTOpticNeuritis"  , found:"N", question: "Optic Neuritis?"},
            {name:"hadEENTNotSure"        , found:"N", question: "Other"}

        ];

        $scope.personalHistory.mentalHealth = {};
        $scope.personalHistory.mentalHealth.conditions = [
            {name:"hadEOHospitalizedLast5Years"     , found:"N", question: "In the past 5 years, have you been hospitalized or been enrolled in a partial hospitalization due to any of these disorders? "},
            {name:"hadEOAlcoholism"                 , found:"N", question: "Alcoholism?"},
            {name:"hadEOADepression"                , found:"N", question: "Anxiety or Depression?"},
            {name:"hadEOTreatment"                  , found:"N", question: "Are you currently undergoing any form of treatment (medication or counseling)?"},
            {name:"hadEODepressionSituationalOnly"  , found:"N", question: "Is your anxiety or depression situational only?"},
            {name:"hadEOHeartPalpitations"          , found:"N", question: "Do you suffer from panic or stress induced chest pains or heart palpitations?"},
            {name:"hadEOPalpPanickAttach2Years"     , found:"N", question: "Have you received medical care or been prescribed medication for panic attacks within the past 2 years"},
            {name:"hadEOADHDChildhood"              , found:"N", question: "Did your ADHD Symptoms begin in childhood (before age 10)?"},
            {name:"hadEO"                   , found:"N", question: "Cognitive Impairment?"},
            {name:"hadEODelusion"           , found:"N", question: "Delusion?"},
            {name:"hadEODrugCounseling"     , found:"N", question: "Drug Counseling?"},
            {name:"hadEOEatingDisorder"     , found:"N", question: "Eating Disorder?"},
            {name:"hadEOMajorDepression"    , found:"N", question: " Depression"},
            {name:"hadEOPanicAttack"        , found:"N", question: "Panic Attack?"},
            {name:"hadEOPAMedsLast2Years"   , found:"N", question: "Have you received medical care or been prescribed medication for panic attacks within the past 2 years?"},
            {name:"hadEOSuicideAttempt"     , found:"N", question: "Suicide Attempt?"},
            {name:"hadEOMinorDisorders"     , found:"N", question: "Brief Stress or Grief Reaction  Performance Anxiety (public speaking, etc.)  Seasonal Affective Disorder  Social Phobia  Simple Phobia (fear of snakes, etc.)?"},
            {name:"hadEONotSure"            , found:"N", question: "Other?"}


        ];

        $scope.personalHistory.musculoskeletalDisorder = {};
        $scope.personalHistory.musculoskeletalDisorder.conditions = [
            {name:"hadBMDAmputationsFromTrauma" , found:"N", question: "Amputations (due to a trauma only)"},
            {name:"hadBMDArthritis"             , found:"N", question: "Arthritis (including rheumatoid arthritis, osteoarthritis, and polyarthritis)"},
            {name:"hadBMDAType"                 , found:"", question: "What type of arthritis do you have?"},
            {name:"hasBMDMeds"                  , found:"N", question: "Do you take any prescription medication for your arthritis?"},
            {name:"hasBMDDisabilityBenefits"    , found:"N", question: "Do you currently receive disability benefits for your arthritis?"},
            {name:"hasBMDOsteoporosis"          , found:"N", question: "Osteoporosis or osteopenia"},
            {name:"hasBMDBackPain"              , found:"N", question: "Back pain"},
            {name:"hasBMDBPMeds"    , found:"N", question: "Are you currently taking prescription medication for your back pain?"},
            {name:"hasBMDPagets"    , found:"N", question: "Paget's disease of the bone"},
            {name:"hasBMDBenign"    , found:"N", question: "Bone Fractures Due to Trauma  Bursitis  Gout  Joint Laxity/Hypermobility  Overuse Injury (Stress Fracture, Shin Splints, Etc.)  Scoliosis  Tendonitis/Tendonsosis  Tennis/Golfer's Elbow (Epicondylitis)  Rotator Cuff Problems  Patellofemoral Pain/Chrondromalacia Patella"},
            {name:"hasBMDOther"     , found:"N", question: "Other"}

        ];

        $scope.personalHistory.nervousSystemDisorder = {};
        $scope.personalHistory.nervousSystemDisorder.conditions = [
            {name:"hadBDAneurysm"       , found:"N", question: "Brain aneurysm or bleeding"},
            {name:"hadDBTumor"          , found:"N", question: "Brain tumor?"},
            {name:"hadDBEncephalitits"  , found:"N", question: "Encephalitits or meningitis?"},
            {name:"hasDBRecovered"      , found:"N", question: "Did you fully recover?"},
            {name:"hasDBRecovered2Years", found:"N", question: "Did you fully recover from this episode more than 2 years ago?"},
            {name:"hadDBSeizures"       , found:"N", question: "Epilepsy or seizures?"},
            {name:"medsDBSeizures"    , found:"", question: "How many medications do you currently take for your seizures?"},
            {name:"hadDBSiezuresLast5Years" , found:"N", question: "Was your last seizure more than 5 years ago?"},
            {name:"hadDBMDisabilityPayments", found:"N", question: "Have you received disability payments within the past 12 months for your headaches or migraines?"},
            {name:"hadDBMS"        , found:"N", question: "Multiple Sclerosis?"},
            {name:"hadDBNeuropathy", found:"N", question: "Neuropathy (Chronic or Inflammatory)?"},
            {name:"hadDBParkinsons", found:"N", question: "Parkinson's Disease?"},
            {name:"hadDBSciatica"  , found:"N", question: "Sciatica?"},
            {name:"hadDBStroke"    , found:"N", question: "Stroke?"},
            {name:"hadDBFainting"  , found:"N", question: "Fainting (Syncope)"},
            {name:"hadDBFMultipleEpisodes"  , found:"N", question: "Have you had more than one episode of fainting?"},
            {name:"hadDBFLastYear"          , found:"N", question: "Was your most recent episode of fainting more than one year ago?"},
            {name:"hadDBTIA"                , found:"N", question: "Transient Ischemic Attack (TIA)"},
            {name:"hadDBHematoma"           , found:"N", question: "Subdural Hematoma / Hematoma in the brain"},
            {name:"hadDBTBI"                , found:"N", question: "Traumatic brain injury?"},
            {name:"hadDBTBILast5Years"      , found:"N", question: "Did the injury occur more than 5 years ago?"},
            {name:"hasTBISymptomsOrMeds"    , found:"N", question: "Do you currently have any symptoms or take any medication related to this injury?"},
            {name:"hadTBTourettes"          , found:"N", question: "Tourette's syndrome?"},
            {name:"hadTBNotSure"            , found:"N", question: "Other"}

        ];

        $scope.personalHistory.pastTenYearWindow = {};
        $scope.personalHistory.pastTenYearWindow.conditions = [
            {name:"hadDrugUse"              , found:"N", question: "Used cocaine, barbituates, amphetamines, heroin, narcotics, stimulants, hallucinogens or other controlled substances or habit forming drugs not prescribed by a physician?"},
            {name:"hadAlcoholAbuse"         , found:"N", question: "Received medical treatment, attended a program or been counseled for alcohol or drug abuse or been advised by a member of the medical profession to reduce the use of alcohol?"}

        ];

        $scope.personalHistory.pastFiveYearWindow = {};
        $scope.personalHistory.pastFiveYearWindow.conditions = [
            {name:"hadInsuranceRestricted"  , found:"N", question: "Had an application for life, disability or health insurance declined, postponed, rated or restricted?"},
            {name:"hadDisabilityClaim"      , found:"N", question: "Had a sickness or injury for which a disability claim was made or payments, benefits or pension benefits were received"},
            {name:"hadNoECGDL"              , found:"N", question: "None of the Above"}

        ];

        $scope.personalHistory.pastThreeYearWindow = {};
        $scope.personalHistory.pastThreeYearWindow.conditions = [
            {name:"hadExamOther"        , found:"N", question: "Had a physical exam, check-up, or evaluation by a member of the medical profession regarding a condition not previously stated on this application?"},
            {name:"hadEORoutine"  , found:"N", question: "Were all these exams, check-ups and/or evaluations performed for routine medical care (routine physicals, immunizations, routine gynecological care, etc.), acute limited illness (cough, cold, flu, sore throat, sinus infection, rashes, urinary infection, etc.), or minor musculoskeletal injury/pain (abrasions, contusions, lacerations, back/neck or joint injury, sprains, strains, etc.?"}

        ];


        $scope.personalHistory.personalInformation = {heightFeet: 5,
                                                      heightInches: 9,
                                                      weightPounds: 180,
                                                      weightChangePounds: 10,
                                                      weightLossReason: "WEIGHT_LOSS_SURGERY",		//populate with enum value from WeightChangeType
                                                      weightGainReason: ""};


        $scope.personalHistory.pregnancy = {};
        $scope.personalHistory.pregnancy.conditions = [
            {name:"hadCPBenign"                     , found:"N", question: "Multiple Miscarriages, Placental Problems, Bleeding or Labor/Delivery Issues"},
            {name:"hadCPHighBloodPressure"          , found:"N", question: "High Blood Pressure"},
            {name:"hadCPHeartLiverKidneyProblems"   , found:"N", question: "Heart, Liver or Kidney Problems"},
            {name:"hadCPSeizures"  , found:"N"      , question: "Seizures"},
            {name:"hadCPGestationalDiabities"   , found:"N", question: "Gestational Diabetes"},
            {name:"hadCPGestOver5YearsBefore"   , found:"N", question: "Has it been more than 5 years since your last pregnancy with gestational diabetes?"},
            {name:"hadCPNotSure"                , found:"N", question: "Other"},
            {name:"hadNoCondition"              , found:"N", question: "None of the Above"}

        ];

        $scope.personalHistory.pulmonaryDisorder = {};
        $scope.personalHistory.pulmonaryDisorder.conditions = [
            {name:"hadRDSarcoidosis"            , found:"N", question: "Sarcoidosis"},
            {name:"hadRDCOPD"                   , found:"N", question: "Chronic Obstructive Pulmonary Disease (COPD)"},
            {name:"hadRDEmphysema"              , found:"N", question: "Emphysema?"},
            {name:"hadRDAsthma"                 , found:"N", question: "Asthma"},
            {name:"hadRDAHospitalizedLast1Year" , found:"N", question: "Have you been hospitalized for asthma (kept overnight in the hospital) in the past year?"},
            {name:"hadRDAERLast1Year"   , found:"N", question: "Have you visited the emergency department in the past year for your asthma?"},
            {name:"hasRDAMeds"          , found:"N", question: "Are you currently taking daily steroid pills, such as prednisone, for your asthma?"},
            {name:"hadRDPulmonaryNodule", found:"N", question: "Pulmonary Nodule or spot on the lung?"},
            {name:"hasRDApnea"          , found:"N", question: "Sleep Apnea"},
            {name:"hasRDCPAP"           , found:"N", question: "Are you currently being treated with CPAP?"},
            {name:"hasRDCPAPLast6Months", found:"N", question: "Have you been using CPAP for more than 6 months?"},
            {name:"hasRDBronchitis"     , found:"N", question: "Bronchitis?"},
            {name:"hasRDBRecovered"     , found:"N", question: "Have you fully recovered?"},
            {name:"hadRDPneumonia"      , found:"N", question: "Pneumonia or Legionnaire's disease"},
            {name:"hasRDPRecovered"     , found:"N", question: "Did you fully recover without complications?"},
            {name:"hadRDTuberculosis"   , found:"N", question: "Tuberculosis"},
            {name:"hasRDTRecovered"     , found:"N", question: "Have you fully recovered without complications and are you no longer taking medications for tuberculosis?"},
            {name:"hadRDInfluenza"      , found:"N", question: "Influenza,  Intermittent Cough/Cold,  Seasonal or Perennial Allergies"},
            {name:"hadRDOther"          , found:"N", question: "Other"}

        ];



        $scope.personalHistory.reproductiveDisorder = {};
        $scope.personalHistory.reproductiveDisorder.conditions = [
            {name:"hadIDCervicitis" , found:"N", question: "Cervicitis, endometriosis, or pelvic inflammatory disease"},
            {name:"hadIDFibrocystic", found:"N", question: "Fibrocystic disease of the breast"},
            {name:"hadIDOvarianCyst", found:"N", question: "Ovarian cyst"},
            {name:"hadIDOBenign"    , found:"N", question: "Is it benign?"},
            {name:"hadIDPolycystic" , found:"N", question: "Polycystic ovarian syndrome"},
            {name:"hadIDCongenitalMalform", found:"N", question: "Congenital malformation of the genital tract  Ovarian Torsion  Ectopic Pregnancy  Papilloma of the Female Genital Tract  Sexually Transmitted Disease (other than HIV or Syphillis)  Uterine Fibroid  Uterine Prolapse"},
            {name:"hadIDNotSure"          , found:"N", question: "Other"}

        ];

        $scope.personalHistory.skinDisorder = {};
        $scope.personalHistory.skinDisorder.conditions = [
            {name:"hadSkinDisorder"        , found:"N", question: "A disorder of the skin including eczema or psoriasis?"},
            {name:"hadSDPsoriasis"        , found:"N", question: "Psoriasis"},
            {name:"hadSDArthritis"  , found:"N", question: "Do you have arthritis or psoratic arthritis?"},
            {name:"hasSDMeds", found:"N", question: "Are you undergoing any treatment other than the use of topical medications?"},
            {name:"hasSDOther"   , found:"N", question: "Other"}

        ];

        $scope.personalHistory.urinaryTrack = {};
        $scope.personalHistory.urinaryTrack.conditions = [
            {name:"hadUTDBladderStones"         , found:"N", question: "Bladder stones (calculi)"},
            {name:"hadUTDGlomerulonephiritis"   , found:"N", question: "Glomerulonephritis or Nephropathy"},
            {name:"hadUTDKidneyStones"      , found:"N", question: "Kidney stones"},
            {name:"hasUTDNormalKidney"      , found:"N", question: "Do you currently have normal kidney function without any obstrution or infection?"},
            {name:"hadUTDRenalFailure"      , found:"N", question: "Kidney failure / renal insufficiency"},
            {name:"hadUTDDialysis"          , found:"N", question: "Dialysis"},
            {name:"hadUTDPyelonephiritis"   , found:"N", question: "Kidney infection (pyelonephritis)"},
            {name:"hadUTStructural"         , found:"N", question: "Do you have any known issues with your urological anatomy (for example: strictures or an abnormal ureter)?"},
            {name:"hadUTDKidneyRecovered3Months"    , found:"N", question: "Was your kidney infection treated and cured more than 3 months ago?"},
            {name:"hadUTDProteinuria"   , found:"N", question: "Proteinuria (protein in the urine on more than one occasion)"},
            {name:"hadUTDRenalCyst"     , found:"N", question: "Renal cysts"},
            {name:"hadUTDRCMultiple"   , found:"", question: "Do you have a single cyst or multiple cysts?"},
            {name:"hadUTDBenign"        , found:"N", question: "Childhood kidney reflux (vesicoureteral reflux)  Congential solitary kidney, horseshoe kidney  Enlarged Prostate Gland/Prostatic Hyperplasia  Incontinence  Prostatitis  Urinary Tract Infection or Cystitis"},
            {name:"hadUTDNotSure"       , found:"N", question: "Other"}

        ];

        $scope.personalHistory.otherCondition = {};
        $scope.personalHistory.otherCondition.conditions = [
            {name:"hadTreatmentAdviseNotComp"   , found:"N", question: "Been advised by a member of the medical profession to have surgery, medical treatment, or diagnostic testing excluding HIV testing, that has not been completed. "},
            {name:"hadSurgeryNotDisclosed"      , found:"N", question: "Had surgery or been a patient overnight in a hospital, clinic or other medical or mental health facility for a condition not previously disclosed on this application?"},
            {name:"surgeriesRoutine"            , found:"N", question: "Were all such surgeries or overnight stays related to the following: sinus or ear problems, tonsillectomy, hernia surgery, gall bladder inflammation or stones, kidney stones, appendicitis, orthopedic repairs of injury, skin or plastic surgery?"},
            {name:"hadTreatmentOther"           , found:"N", question: "Are you currently under treatment by a member of the medical profession for anything not previously stated on this application?"},
            {name:"hadRXOther"                  , found:"N", question: "Are you currently taking any prescription medication not previously stated on this application (excluding contraceptives)?"},
            {name:"hadImmunoDeficiency"         , found:"N", question: "A diagnosis of Human Immunodeficiency Virus (HIV) infection or Acquired Immune Deficiency Syndrome (AIDS)?"}


        ];

        $scope.personalHistory.familyHistory = {};
        $scope.personalHistory.familyHistory.conditions = [
            {
                name: "hasConditions",
                found: "Y",
                question: "Has any conditions"
            }, {
                name: "hadSeriousCondition",
                found: "Y",
                question: "Has any Serious conditions"
            }

        ];

        $scope.personalHistory.familyHistory.motherDetails = [{
            member: "MOTHER",
            condition: "BREAST_OR_OVARIAN_CANCER",
            age: 48,
            lifeStatus: "DECEASED",
            informationSource: "MOTHER_DETAILS"
        }];

        $scope.personalHistory.familyHistory.fatherDetails = [{
            member: "FATHER",
            condition: "HEART_DISEASE",
            age: 49,
            lifeStatus: "DECEASED",
            informationSource: "FATHER_DETAILS" ,
            include: true
        }];

        $scope.momAndDad = [];

        // add mom and dad to the momAndDad list
        Array.prototype.push.apply($scope.momAndDad,
                                    $scope.personalHistory.familyHistory.motherDetails,
                                    $scope.personalHistory.familyHistory.fatherDetails);

        $scope.personalHistory.familyHistory.brothersDetails = [
            {
                member: "BROTHER",
                age: -1,
                lifeStatus: "DECEASED",
                informationSource: "BROTHER_STATUS",
                include: true
            }, {
                member: "BROTHER",
                condition: "BREAST_CANCER",
                age: 45,
                lifeStatus: "DECEASED",
                informationSource: "BROTHER_DETAILS",
                include: true
            }, {
                member: "BROTHER",
                condition: "OTHER_CANCER",
                age: 43,
                lifeStatus: "DECEASED",
                informationSource: "BROTHER_DETAILS",
                include: true
            }
        ];

        $scope.personalHistory.familyHistory.sistersDetails = [
            {
                member: "SISTER",
                age: -1,
                lifeStatus: "DECEASED",
                informationSource: "SISTER_STATUS",
                include: true
            }, {
                member: "SISTER",
                condition: "BREAST_CANCER",
                age: 47,
                lifeStatus: "DECEASED",
                informationSource: "SISTER_DETAILS"
            }, {
                member: "SISTER",
                condition: "SKIN_CANCER",
                age: 46,
                lifeStatus: "DECEASED",
                informationSource: "SISTER_DETAILS"
            }
        ];

        $scope.personalHistory.familyHistory.familyConditions = [
            {
                member: "FATHER",
                condition: "CANCER",
                diseaseType: "BREAST_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS",
                include: true
            }, {
                member: "FATHER",
                condition: "CANCER",
                diseaseType: "COLON_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS",
                include: true
            }, {
                member: "FATHER",
                condition: "HEART_DISEASE",
                age: 40,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "MOTHER",
                condition: "CANCER",
                diseaseType: "BREAST_CANCER",
                age: 39,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "SISTER",
                condition: "CANCER",
                diseaseType: "OVARIAN_CANCER",
                age: 41,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "SISTER",
                condition: "CANCER",
                diseaseType: "COLON_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "BROTHER",
                condition: "CANCER",
                diseaseType: "COLON_CANCER",
                age: 38,
                informationSource: "FAMILY_CONDITIONS"
            }, {
                member: "BROTHER",
                condition: "CANCER",
                diseaseType: "BREAST_CANCER",
                age: 41,
                informationSource: "FAMILY_CONDITIONS"
            }
        ];

        $scope.personalHistory.familyHistory.familySeriousConditions = [
            {
                member: "SISTER",
                condition: "HUNTINGTONS_DISEASE",
                age: 42,
                informationSource: "FAMILY_SERIOUS_CONDITIONS",
                include: true
            }, {
                member: "BROTHER",
                condition: "CARDIOMYOPATHY",
                age: 43,
                informationSource: "FAMILY_SERIOUS_CONDITIONS"
            }
        ];


        function initScopeVars (){

            console.log("Inside initScopeVars");

            $scope.errorQuoteMessages = [];
            $scope.warningQuoteMessages = [];
            $scope.infoQuoteMessages = [];

            $scope.QuoteStatus  = {
                "FORM_INCOMPLETE": "FORM_INCOMPLETE",
                "NEED_VALIDATION_CHECK": "NEED_VALIDATION_CHECK",
                "ELIBIBILITY_COMPLETE": "ELIBIBILITY_COMPLETE"

            };

        }

        initScopeVars();

        function doQuoteMessages (){

            var haveErrors = false;
            var messages = $scope.wrapper.quoteMessages;
            $scope.errorQuoteMessages = [];
            $scope.infoQuoteMessages = [];
            $scope.warningQuoteMessages = [];

            if (messages && messages.length > 0){

                for (var i = 0; i < messages.length; i++) {
                    var msg = messages[i];
                    if ("ERROR" == msg.messageStatus) {
                        $scope.errorQuoteMessages.push(msg);
                        haveErrors = true;
                    }
                    if ("INFO" == msg.messageStatus) {
                        $scope.infoQuoteMessages.push(msg);
                    }
                    if ("WARNING" == msg.messageStatus) {
                        $scope.warningQuoteMessages.push(msg);
                    }

                }
            }

            return haveErrors;

        }

        function updateQuoteStatus (){
            $log.debug("Inside updateQuoteStatus, before: " + $scope.mainForm.$valid);

            if ($scope.mainForm.$valid) {

                $scope.quoteStatus = $scope.QuoteStatus.NEED_ELIGIBILITY_CHECK;
            }
            else {

                $scope.quoteStatus = $scope.QuoteStatus.FORM_INCOMPLETE;
            }

            //console.log("status after: " + $scope.quoteStatus, $scope.mainForm.$error);

            if ($scope.mainForm.$error && $scope.mainForm.$error.required){

                $scope.requiredFieldList = "The following fields are required:\n";

                for(index in $scope.mainForm.$error.required){
                    console.log("required field: " + $scope.mainForm.$error.required[index].$name);
                    $scope.requiredFieldList += $scope.mainForm.$error.required[index].$name + "\n";
                }
            }
        }

        function processUserInput (riskType){

            $log.debug("Inside processUserInput, riskType["+riskType+"]");

            $scope.personalHistory.riskType = riskType;

            if ($scope.personalHistory.familyHistory != undefined) {


                $scope.personalHistory.familyHistory.motherDetails =
                   filterIncludedFamilyMembers($scope.momAndDad, "MOTHER");

                $scope.personalHistory.familyHistory.fatherDetails =
                   filterIncludedFamilyMembers($scope.momAndDad, "FATHER");

                $scope.personalHistory.familyHistory.brothersDetails =
                   filterIncludedFamilyMembers($scope.personalHistory.familyHistory.brothersDetails);

                $scope.personalHistory.familyHistory.sistersDetails =
                   filterIncludedFamilyMembers($scope.personalHistory.familyHistory.sistersDetails);

                $scope.personalHistory.familyHistory.familyConditions =
                   filterIncludedFamilyMembers($scope.personalHistory.familyHistory.familyConditions);
            }

        }

        function filterIncludedFamilyMembers (memberList, filterByMemberType){

            $log.debug("Inside filterIncludedFamilyMembers, filterByMemberType["+filterByMemberType+"]");

            if (memberList != undefined){

                var includeMemberList = [];

                memberList.forEach(function(member){

                    if (member.include == true && (filterByMemberType == undefined || filterByMemberType == member.member)){

                        $log.debug("include member", member);
                        includeMemberList.push(member);
                    }
                });

                // only return one
                if (filterByMemberType != undefined && includeMemberList.length == 0){
                    return undefined;
                }
                else if (filterByMemberType != undefined && includeMemberList.length == 1){
                    return includeMemberList[0];
                }

                // return list
                return includeMemberList;
            }

            return memberList;

        }

        // to be used by JSON.stringify when needed
        function replacer(key, value) {

            return "null";
        }

        $scope.calculateRiskSummary = function (riskType){

            $log.debug("Inside calculateRiskSummary P2, personalHistory: ", JSON.stringify($scope.personalHistory, null, 4));

            $scope.showMouseHourGlass = true;
            processUserInput(riskType);

            RiskSummaryP2.rate($scope.personalHistory,
                function (response){

                    $log.debug("calculateRiskSummary received data: ", response);

                    //var results = response;
                    //results.riskRatings = [];
                    //results.riskRollups = [];
                    //
                    ////var rollups = response.riskRollups;
                    //var rollups = [];
                    //
                    //if (response.summary[0] != undefined){
                    //    rollups = response.summary[0].riskRatingGroups;
                    //}
                    //
                    //for (index in rollups) {
                    //    $log.debug("found rollup: " , rollups[index]);
                    //
                    //    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){
                    //
                    //        $log.debug("found rating list: " , rollups[index].riskRatings);
                    //        // append to results.riskRatings array
                    //        results.riskRatings.push.apply(results.riskRatings, rollups[index].riskRatings);
                    //        $log.debug("rating list after adding: " , results.riskRatings);
                    //    }
                    //}
                    //
                    //results.riskRollups = rollups;

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;

                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskSummary:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        }

        $scope.calculateRiskRollup = function (riskType){

            $log.debug("Inside calculateRiskRollup P2 personalHistory: ", JSON.stringify($scope.personalHistory, null, 4));

            $scope.showMouseHourGlass = true;
            processUserInput(riskType);

            $log.debug("personalHistory after filter: ", JSON.stringify($scope.personalHistory, null, 4));

            RiskRollupP2.rate($scope.personalHistory,
                function (response){

                    $log.debug("calculateRiskRollup received data: ", response);


                    //var results = response;
                    //results.riskRatings = [];
                    //var rollups = response.riskRollups; //riskRatings
                    //
                    //for (index in rollups) {
                    //    $log.debug("found rollup: " , rollups[index]);
                    //
                    //    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){
                    //
                    //        $log.debug("found rating list: " , rollups[index].riskRatings);
                    //        results.riskRatings.push.apply(results.riskRatings, rollups[index].riskRatings);
                    //        $log.debug("rating list after adding: " , results.riskRatings);
                    //    }
                    //}

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;

                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskRollup:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskRating = function (riskType){
            console.log("Inside calculateRiskRating P2 personalHistory: ", JSON.stringify($scope.personalHistory, null, 4));


            $scope.showMouseHourGlass = true;
            processUserInput(riskType);

            RiskRatingP2.rate($scope.personalHistory,
                function (response){

                    $log.debug("calculateRiskRating received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;

                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskRating:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };




        $scope.resetQuote = function (){
            $scope.mainForm.$setPristine();

            initQuote();

            updateQuoteStatus();

        };

        function initQuote(){
            console.log("Inside initQuote");

        }

        initQuote();

    });

angular.module('BlenderModule', [])
.controller('BlenderController',
    function ($scope, $http, $location, $log, BlenderFinal, LabResults, BlenderGUIConstants){

        $log.debug("Inside BlenderController");

        function initScopeVars (){

            console.log("Inside initScopeVars");

            $scope.showMouseHourGlass = false;
            $scope.blenderStatus = "SUCCESS";
            $scope.errorMessages = [];
            $scope.missingConditions = [];
            $scope.totalScore = 0;

            $scope.applicant = {age: 25,
                sex: "M",
                conditions: {cardiovascularParentsHistory: "false",
                             antihypertensiveUse: "false",
                             normalEKGLast2Years: "false",
                             ebctCreditLast2Years: "false",
                             bnptBelow125: "false"},
                labResults: []
            };

            $scope.applicableSummaries = {PART_I: true,
                                          PART_II: true,
                                          RX_RULES: true,
                                          MVR: true};

            $scope.applicableLabs = {LAB_POINTS: true,
                                     LAB_FLAGS: true};

            $scope.applicableInputLabs = {ALBUMIN:true,
                                        ALKALINE_PHOSPHATASE:false,
                                        BLOOD_ALCOHOL:false,
                                        BLOOD_PRESSURE:true,
                                        BMI:true,
                                        //BNPT:true,
                                        CDT:false,
                                        CEA:false,
                                        CHOL_HDL_RATIO:true,
                                        CHOLESTEROL:true,
                                        COCAINE_METABOLITES:true,
                                        DEMOGRAPHIC_ADJUSTMENT:false,
                                        EGFR:false,
                                        FRUCTOSAMINE:false,
                                        GGT:false,
                                        GLOBULIN:false,
                                        HBA1C:false,
                                        HBV:false,
                                        HCV:false,
                                        HDL:true,
                                        HEMOGLOBIN:false,
                                        HIV:true,
                                        METHAMPHETAMINE:false,
                                        NT_PROBNP:false,
                                        PSA:false,
                                        PULSE_IRREGULAR_REST:false,
                                        PULSE_STANDARD_REST:false,
                                        RED_BLOOD_COUNT:false,
                                        SGOT_AST:false,
                                        SGPT_ALT:false,
                                        TOTAL_BILIRUBIN:false,
                                        URINE_BLOOD:false,
                                        URINE_CREATININE:false,
                                        URN_PC_RATIO:false,
                                        URN_NICOTINE: false,
                                        TOTAL_SCORE:true};



            // rating summarries
            $scope.ratingSummaries = [
                {riskSummaryType: "PART_I",
                description : "Part I",
                upnt : 1,
                spnt : 1,
                nont : 1,
                spt : 1,
                tob : 1,
                wp : 1,
                diio : 1,
                rollupComplete: true,
                // part one rollups
                riskRatingGroups : [
                    {
                    riskSummaryType: "PART_I",
                    riskType: "AVOCATION",
                    description : "Avocation - Rollup",
                    upnt : 1,
                    spnt : 1,
                    nont : 1,
                    spt : 1,
                    tob : 1,
                    wp : 1,
                    diio : 1,
                    // part one risk ratings - AVOCATION
                    riskRatings : [{
                        riskSummaryType: "PART_I",
                        riskType: "AVOCATION",
                        description : "Avocation Risk",
                        upnt : 1,
                        spnt : 1,
                        nont : 1,
                        spt : 1,
                        tob : 1,
                        wp : 1,
                        diio : 1
                    }
                ]
                },
                // part one risk ratings rollup - SMOKER
                {
                    riskSummaryType: "PART_I",
                    riskType: "SMOKER",
                    description : "Nicotine use - Rollup",
                    upnt : 1,
                    spnt : 1,
                    nont : 1,
                    spt : 1,
                    tob : 1,
                    wp : 1,
                    diio : 1,
                    // part one risk ratings - SMOKER
                    riskRatings : [{
                        riskSummaryType: "PART_I",
                        riskType: "SMOKER",
                        description : "Nicotine use - Risk",
                        ruleName : "rule-smoker-8",
                        upnt : 1,
                        spnt : 1,
                        nont : 1,
                        spt : 1,
                        tob : 1,
                        wp : 1,
                        diio : 1
                    }
                    ]
                },
                // part one risk ratings rollup - DUI
                {
                    riskSummaryType: "PART_I",
                    riskType: "DUI",
                    description : "DUI - Rollup",
                    upnt : 1,
                    spnt : 1,
                    nont : 1,
                    spt : 1,
                    tob : 1,
                    wp : 1,
                    diio : 1,
                    // part one risk ratings - DUI
                    riskRatings : [{
                            riskSummaryType: "PART_I",
                            riskType: "DUI",
                            description : "DUI risk",
                            ruleName : "rule-dui-2",
                            upnt : 1,
                            spnt : 1,
                            nont : 1,
                            spt : 1,
                            tob : 1,
                            wp : 1,
                            diio : 1
                        }
                        ]
                    },
                     // part one risk ratings rollup - MOVING_VIOLATIONS
                    {
                        riskSummaryType: "PART_I",
                        riskType: "MOVING_VIOLATIONS",
                        description : "Moving Violation - Rollup",
                        upnt : 1,
                        spnt : 1,
                        nont : 1,
                        spt : 1,
                        tob : 1,
                        wp : 1,
                        diio : 1,
                        // part one risk ratings - MOVING_VIOLATIONS
                        riskRatings : [{
                            riskSummaryType: "PART_I",
                            riskType: "MOVING_VIOLATIONS",
                            description : "Moving Violation risk",
                            ruleName : "rule-moving violations-2",
                            upnt : 1,
                            spnt : 1,
                            nont : 1,
                            spt : 1,
                            tob : 1,
                            wp : 1,
                            diio : 1
                        }
                        ]
                    }
                ]
            },
            // part two summary
            {riskSummaryType: "PART_II",
                description : "Part II",
                upnt : 1,
                spnt : 1,
                nont : 1,
                spt : 1,
                tob : 1,
                wp : 1,
                diio : 1},
            // rx summary
            {riskSummaryType: "RX_RULES",
                description : "RX Rules",
                upnt : 1,
                spnt : 1,
                nont : 1,
                spt : 1,
                tob : 1,
                wp : 1,
                diio : 1,
                rollupComplete: true,
                // rx rollup
                riskRatingGroups : [
                    {
                        riskSummaryType: "RX_RULES",
                        //riskType: "RX_RULES", // there are no groups for RX
                        description : "Rx - Rollup",
                        upnt : 1,
                        spnt : 1,
                        nont : 1,
                        spt : 1,
                        tob : 1,
                        wp : 1,
                        diio : 1,
                        // RX risk ratings - RX_RULES
                        riskRatings : [{
                            riskSummaryType: "RX_RULES",
                            riskType: "RX_RULES", // there are no groups for RX, just use it here to help with the GUI dropdown
                            ruleName : "rule-rx-default",
                            description : "Rx - Risk",
                            upnt : 1,
                            spnt : 1,
                            nont : 1,
                            spt : 1,
                            tob : 1,
                            wp : 1,
                            diio : 1
                        }
                        ]
                    }
                    ]
            },
            // mvr summary
            {riskSummaryType: "MVR",
                description : "MVR",
                upnt : 1,
                spnt : 1,
                nont : 1,
                spt : 1,
                tob : 1,
                wp : 1,
                diio : 1
            },
            // mib summary
            {riskSummaryType: "MIB",
                description : "MIB",
                upnt : 1,
                spnt : 1,
                nont : 1,
                spt : 1,
                tob : 1,
                wp : 1,
                diio : 1,
                //MIB rollup
                riskRatingGroups : [
                    {
                        riskSummaryType: "MIB",
                        //riskType: "MIB", // there are no groups for MIB
                        description : "MIB - Rollup",
                        upnt : 1,
                        spnt : 1,
                        nont : 1,
                        spt : 1,
                        tob : 1,
                        wp : 1,
                        diio : 1,
                        // MIB risk ratings - RX_RULES
                        riskRatings : [{
                            riskSummaryType: "MIB",
                            riskType: "MIB", // there are no groups for MIB, just use it here to help with the GUI dropdown
                            responseData: "181  N ",
                            ruleName : "rule-mib-default",
                            description : "MIB - Risk",
                            upnt : 1,
                            spnt : 1,
                            nont : 1,
                            spt : 1,
                            tob : 1,
                            wp : 1,
                            diio : 1
                        },
                        {
                            riskSummaryType: "MIB",
                            riskType: "MIB", // there are no groups for MIB, just use it here to help with the GUI dropdown
                            responseData: "200 BN ",
                            ruleName : "rule-mib-default",
                            description : "MIB - Risk",
                            upnt : 1,
                            spnt : 1,
                            nont : 1,
                            spt : 1,
                            tob : 1,
                            wp : 1,
                            diio : 1
                        }
                        ]
                    }
                ]
            },
            // verification summary
            {riskSummaryType: "VERIFICATION",
                description : "VERIFICATION",
                upnt : 1,
                spnt : 1,
                nont : 1,
                spt : 1,
                tob : 1,
                wp : 1,
                diio : 1
            }
            ];

            // now extract the mib ratings so that we can display them on the screen
            $scope.mibRatings = [];
            for (var i = 0; i < $scope.ratingSummaries.length; i++){

                // if we have an MIB summary
                if ($scope.ratingSummaries[i].riskSummaryType === "MIB"){

                    $log.debug("Found MIB summary");
                    // now pull all the ratings out of the rollup
                    for (var j = 0; j < $scope.ratingSummaries[i].riskRatingGroups[0].riskRatings.length; j++){

                        $log.debug("Add MIB risk: ", $scope.ratingSummaries[i].riskRatingGroups[0].riskRatings[j]);
                        $scope.mibRatings.push($scope.ratingSummaries[i].riskRatingGroups[0].riskRatings[j]);
                    }
                    break;  // do nothing else to do
                }

            }

            $scope.labTab = {
                results: {active: true},
                details: {active: false}
            };

            // Blender risk rating
            $scope.labRatings = [{riskType: "LAB_POINTS",
                preferredPoints : 0,
                description : "Lab Points",
                upnt : 1,
                spnt : 1,
                nont : 1,
                spt : 1,
                tob : 1,
                wp : 1,
                diio : 1},
                {riskType: "LAB_FLAGS",
                    preferredPoints : 0,
                    description : "Lab Flags",
                    upnt : 1,
                    spnt : 1,
                    nont : 1,
                    spt : 1,
                    tob : 1,
                    wp : 1,
                    diio : 1}
            ];

            $scope.applicant.labResults = [{
                                               resultType: "EGFR",
                                               score: "0",
                                               result:"112.55",
                                               flag: 0,
                                               flag_details: ""
                                           },
                                           {
                                               resultType: "NT_PROBNP",
                                               score: "0",
                                               result:"100",
                                               flag: 0,
                                               flag_details: ""
                                           },
                                           {
                                               resultType: "BMI",
                                               score: "0",
                                               result:"25",
                                               flag: 0,
                                               flag_details: ""
                                           },
                                           {
                                               resultType: "HIV",
                                               score: "0",
                                               result:"NEG",
                                               flag: 0,
                                               flag_details: ""
                                           },
                                            {
                                                resultType: "HEMOGLOBIN",
                                                score: "0",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "FRUCTOSAMINE",
                                                score: "",
                                                result:"1.6",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "HBA1C",
                                                score: "0",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "BLOOD_PRESSURE",
                                                score: "0",
                                                result:"135/85",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "RED_BLOOD_COUNT",
                                                score: "0",
                                                result:"0",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "CEA",
                                                score: "0",
                                                result:"0",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "PSA",
                                                score: "0",
                                                result:"0",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "HBV",
                                                score: "0",
                                                result:"NEG",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "HCV",
                                                score: "0",
                                                result:"NEG",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "URINE_CREATININE",
                                                score: "0",
                                                result:"62",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "URINE_BLOOD",
                                                score: "0",
                                                result:"NEG",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "URN_PC_RATIO",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "URN_NICOTINE",
                                                score: "",
                                                result:"0.2",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "PULSE_STANDARD_REST",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "PULSE_IRREGULAR_REST",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "CDT",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "ALKALINE_PHOSPHATASE",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "TOTAL_BILIRUBIN",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "SGOT_AST",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "SGPT_ALT",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "GGT",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },{
                                                resultType: "ALBUMIN",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "GLOBULIN",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "CHOLESTEROL",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "HDL",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },{
                                                resultType: "CHOL_HDL_RATIO",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "COCAINE_METABOLITES",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "BLOOD_ALCOHOL",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "METHAMPHETAMINE",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "DEMOGRAPHIC_ADJUSTMENT",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            },
                                            {
                                                resultType: "TOTAL_SCORE",
                                                score: "",
                                                result:"",
                                                flag: 0,
                                                flag_details: ""
                                            }
            ];


            $scope.blenderOutput = {otherRatings: [],
                                    preferredPointRating: {},
                                    finalRating: {}
            };

            $scope.preferredPointRating = {};
            $scope.finalRating = {};

            $scope.riskLevels = BlenderGUIConstants.getRiskLevel();

            $scope.ruleNames = [ {value: "rule-smoker-8", label: "rule-smoker-8", type:"SMOKER"}
                                ,{value: "rule-smoker-5-1", label: "rule-smoker-5-1", type:"SMOKER"}
                                ,{value: "rule-smoker-5", label: "rule-smoker-5", type:"SMOKER"}
                                ,{value: "rule-smoker-4", label: "rule-smoker-4", type:"SMOKER"}
                                ,{value: "rule-moving violations-1", label: "more than 2 moving violations", type:"MOVING_VIOLATIONS"}
                                ,{value: "rule-moving violations-2", label: "less than 2 moving violations", type:"MOVING_VIOLATIONS"}
                                ,{value: "rule-moving violations-3", label: "rule-moving violations-3", type:"MOVING_VIOLATIONS"}
                                ,{value: "rule-moving violations-4", label: "rule-moving violations-4", type:"MOVING_VIOLATIONS"}
                                ,{value: "rule-dui-1", label: "Any DUI in the past 5 years", type:"DUI"}
                                ,{value: "rule-dui-2", label: "No DUIs", type:"DUI"}
                                ,{value: "rule-rx-default", label: "Rx Default", type:"RX_RULES"}
                                ,{value: "rule-rx-code-532", label: "Smoking Rx Code 532", type:"RX_RULES"}

            ];

        }

        initScopeVars();

        $scope.getRiskClass = function(riskClassValue){

            var riskClass = "column-risk-yellow";

            if (riskClassValue == 3){
                riskClass = "column-risk-red";
            }
            else if (riskClassValue == 2){
                riskClass = "column-risk-orange";
            }
            else if (riskClassValue == 1){
                riskClass = "column-risk-green";
            }


            return riskClass;
        };

        $scope.iterateRiskLevel = function(rating, name, level){

            $log.debug("Inside iterateRiskLevel["+level+"]");
            if (level < 3){
                level = level + 1;
            }
            else{
                level = 1;
            }

            rating[name] = level;

        };

        function updateQuoteStatus (){
            $log.debug("Inside updateQuoteStatus, before: " + $scope.mainForm.$valid);

            $scope.errorMessages = [];
            $scope.missingConditions = [];

            if ($scope.mainForm.$valid) {

                $scope.blenderStatus = "SUCCESS";
            }
            else {

                $scope.blenderStatus = "FAIL";
            }

            //console.log("status after: " + $scope.quoteStatus, $scope.mainForm.$error);

            if ($scope.mainForm.$error && $scope.mainForm.$error.required){

                $scope.requiredFieldList = "The following fields are required:\n";

                for(index in $scope.mainForm.$error.required){
                    console.log("required field: " + $scope.mainForm.$error.required[index].$name);
                    $scope.requiredFieldList += $scope.mainForm.$error.required[index].$name + "\n";
                }
            }
        }

        $scope.updateTotalScore = function(score){
            //$log.debug("Inside updateTotalScore, score["+score+"], totalScore["+$scope.totalScore+"]");

            if (isNaN(score) == false) {

                $scope.totalScore += Number(score);

                $scope.applicant.labResults[33].score = $scope.totalScore;
            }

        };

        $scope.updateSummaryRatings = function(summary, rollup, risk){
            $log.debug("Inside updateSummaryRatings, riskType["+risk.riskType+"], ruleName["+risk.ruleName+"], summary: ", summary);

            if (risk.riskType == "MOVING_VIOLATIONS" && risk.ruleName === "rule-moving violations-1"){
                risk.upnt = 3;
                risk.spnt = 3;
                risk.nont = 2;
                risk.spt = 3;
                risk.tob = 2;
                risk.wp = 3;
                risk.diio = 3;
            }
            else if (risk.riskType == "MOVING_VIOLATIONS" && risk.ruleName === "rule-moving violations-2"){
                risk.upnt = 1;
                risk.spnt = 1;
                risk.nont = 1;
                risk.spt = 1;
                risk.tob = 1;
                risk.wp = 1;
                risk.diio = 1;
            }
            else if (risk.riskType == "DUI" && risk.ruleName === "rule-dui-1"){
                risk.upnt = 3;
                risk.spnt = 3;
                risk.nont = 3;
                risk.spt = 3;
                risk.tob = 3;
                risk.wp = 3;
                risk.diio = 3;
            }
            else if (risk.riskType == "DUI" && risk.ruleName === "rule-dui-2"){
                risk.upnt = 1;
                risk.spnt = 1;
                risk.nont = 1;
                risk.spt = 1;
                risk.tob = 1;
                risk.wp = 1;
                risk.diio = 1;
            }
            else if (risk.riskType == "RX_RULES" && risk.ruleName === "rule-rx-code-532"){
                risk.upnt = 3;
                risk.spnt = 3;
                risk.nont = 3;
                risk.spt = 3;
                risk.tob = 3;
                risk.wp = 3;
                risk.diio = 3;
            }
            else if (risk.riskType == "RX_RULES" && risk.ruleName === "rule-rx-default"){
                risk.upnt = 1;
                risk.spnt = 1;
                risk.nont = 1;
                risk.spt = 1;
                risk.tob = 1;
                risk.wp = 1;
                risk.diio = 1;
            }

            var riskClasses = ["upnt", "spnt", "nont", "spt", "tob", "wp", "diio"];
            var rClass;

            for (var r in riskClasses){

                rClass = riskClasses[r];
                // now do a rollup and update the summary
                if (risk[rClass] > summary[rClass]){
                    rollup[rClass] = risk[rClass];
                    summary[rClass] = risk[rClass];
                }
                else{
                    rollup[rClass] = 1;
                    summary[rClass] = 1;
                }

                $log.debug("summary[r]: " + summary[r]);
            }


        };

        $scope.calculateLabResults = function (){

            $log.debug("Inside calculateLabResults");

            $scope.missingConditions = [];
            var applicableLabs = [];
            var applicant = angular.copy($scope.applicant);
            $scope.totalScore = 0;

            // check for applicable labs
            for (var i = 0; i < $scope.applicant.labResults.length; i++){

                if ($scope.applicableInputLabs[$scope.applicant.labResults[i].resultType] == true) {

                    $log.debug("Add lab[" + $scope.applicant.labResults[i].resultType + "]", $scope.applicant.labResults[i]);

                    applicableLabs.push($scope.applicant.labResults[i]);
                }
            }

            $log.debug("applicant: ", JSON.stringify(applicant, null, 4));

            applicant.labResults = applicableLabs;

            LabResults.calculate( applicant,
                function (response){

                    $log.debug("calculateLabResults received data: ", response);

                    //reset flag data
                    for (var i = 0; i < $scope.applicant.labResults.length; i++) {

                        // reset values
                        $scope.applicant.labResults[i].flag = 0;
                        $scope.applicant.labResults[i].flag_details = "";
                    }

                    $scope.missingConditions = response.missingLabs;
                    var flags = response.flags;

                    if (flags != undefined && flags.length > 0){

                        // now indicate which labs got a flag
                        for (var i = 0; i < $scope.applicant.labResults.length; i++){

                            for (var j = 0; j < flags.length; j++){

                                if ($scope.applicant.labResults[i].resultType == flags[j].resultType){

                                    $log.debug("Set flag for lab["+$scope.applicant.labResults[i].resultType+"]");
                                    $scope.applicant.labResults[i].flag = 1;
                                    $scope.applicant.labResults[i].flag_details = flags[j].ruleName + "/" + flags[j].description;
                                }

                            }
                        }
                    }

                    if ($scope.missingConditions && $scope.missingConditions.length > 0){

                        for (var i = 0; i < $scope.applicant.labResults.length; i++){
                            for (var j = 0; j < $scope.missingConditions.length; j++){

                                if ($scope.missingConditions[j].startsWith($scope.applicant.labResults[i].resultType)){

                                    $log.debug("Indicate required condition for lab["+$scope.applicant.labResults[i].resultType+"]");

                                    if ($scope.missingConditions[j] == $scope.applicant.labResults[i].resultType){
                                        $scope.applicant.labResults[i].flag_details = "is Required!!";
                                    }
                                    else{
                                        $scope.applicant.labResults[i].flag_details += " " + $scope.missingConditions[j];
                                    }

                                }

                            }

                        }
                    }


                    // update Blender riskrating

                    $scope.labRatings = response.riskRatings;
                    $scope.labResultFlag = response.flags;
                    $scope.labTab.results.active = true;

                },
                function (err){
                    console.error('calculateFinalResults:received an error: ', err);
                    if (err.data) {

                        if (err.status == 400){

                            //alert("Internal error: " + err.data.message);
                            $scope.errorMessages = [{msg: err.data, status: err.statusText}];
                        }
                        else{
                            //alert("Internal error: " + err.data.message);
                            $scope.errorMessages = [{msg: err.data.message, status: err.statusText}];
                        }

                    }
                    else {
                        $scope.errorMessages = [{msg: 'Unknown  server error'}];
                    }

                });
        }

        $scope.calculateFinalResults = function (){

            $log.debug("Inside calculateFinalResults");

            $scope.showMouseHourGlass = true;

            updateQuoteStatus();
            var applicableSummaries = [];
            var applicableLabs = [];

            // check for applicable summaries
            for (var i = 0; i < $scope.ratingSummaries.length; i++){

                if ($scope.applicableSummaries[$scope.ratingSummaries[i].riskSummaryType] == true) {

                    $log.debug("Add summary[" + $scope.ratingSummaries[i].riskSummaryType + "]", $scope.ratingSummaries[i]);

                    applicableSummaries.push($scope.ratingSummaries[i]);
                }
            }

            // check for applicable labs
            for (var i = 0; i < $scope.labRatings.length; i++){

                if ($scope.applicableLabs[$scope.labRatings[i].riskType] == true) {

                    $log.debug("Add lab[" + $scope.labRatings[i].riskType + "]", $scope.labRatings[i]);

                    applicableLabs.push($scope.labRatings[i]);
                }
            }

            var finalInput = {applicant: $scope.applicant,
                              ratingSummaries : applicableSummaries,
                              labRatings: applicableLabs, // was generated via lab rating
                              labFlags: $scope.labResultFlag} // was generated via lab rating

            $log.debug("finalInput: ", JSON.stringify(finalInput, null, 4));

            BlenderFinal.blend(finalInput,
                function (response){

                    $log.debug("calculateFinalResults received data: ", response);

                    $scope.blenderOutput = response;
                    $scope.missingConditions = response.missingConditions;
                    $scope.preferredPointRating = response.preferredPointRating;
                    $scope.finalRating = response.finalRating;

                    $scope.showMouseHourGlass = false;

                },
                function (err){
                    console.error('calculateFinalResults:received an error: ', err);
                    $scope.showMouseHourGlass = false;
                    if (err.data) {

                        if (err.status == 400){

                            //alert("Internal error: " + err.data.message);
                            $scope.errorMessages = [{msg: err.data, status: err.statusText}];
                        }
                        else{
                            //alert("Internal error: " + err.data.message);
                            $scope.errorMessages = [{msg: err.data.message, status: err.statusText}];
                        }

                    }
                    else {
                        $scope.errorMessages = [{msg: 'Unknown  server error'}];
                    }

                });
        };

        $scope.resetInput = function (){

            $log.debug("Inside resetInput");
            initScopeVars();

            $scope.mainForm.$setPristine();

        };

    });

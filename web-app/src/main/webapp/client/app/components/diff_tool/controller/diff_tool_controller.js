angular.module('DiffToolModule', [])
.controller('DiffToolController',
    function ($scope, $http, $location, $log, sharedProperties){

        $log.debug("Inside DiffToolController");

        $scope.results = sharedProperties.getRatingResults();
        $scope.futureResults = sharedProperties.getFutureRatingResults();
        $scope.newRiskRatings = [];
        $scope.newRiskRollups = [];
        $scope.newRiskSummary = [];

        var filterByRiskType = sharedProperties.getRiskType();


        $log.debug("filterByRiskType["+filterByRiskType+"]");

        processInputResuts();

        compareResults("riskRatings");

        if ($scope.results.riskRollups != undefined && $scope.results.riskRollups.length > 0){
            compareResults("riskRollups");
        }

        if ($scope.results.summary != undefined && $scope.results.summary.length > 0){
            compareResults("summary");
        }


        //$log.debug("results: ", JSON.stringify($scope.results, null, 4));

        function processInputResuts(){

            $log.debug("Inside processInputResuts");

            if ($scope.results.riskRollups != undefined && $scope.results.riskRollups.length > 0){

                $log.debug("Have a list of rollups");
                $scope.results.riskRatings = [];
                var rollups = $scope.results.riskRollups; //riskRatings

                for (index in rollups) {
                    //$log.debug("found rollup: " , rollups[index]);

                    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                        //$log.debug("found rating list: " , rollups[index].riskRatings);
                        $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                        //$log.debug("rating list after adding: " , results.riskRatings);
                    }
                }
            }
            else if ($scope.results.summary != undefined && $scope.results.summary.length > 0){

                $log.debug("Have a summary so pull rollups and risk ratings");
                $scope.results.riskRatings = [];
                $scope.results.riskRollups = [];

                var rollups = [];

                rollups = $scope.results.summary[0].riskRatingGroups;

                for (index in rollups) {
                    //$log.debug("found rollup: " , rollups[index]);

                    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                        //$log.debug("found rating list: " , rollups[index].riskRatings);
                        // append to results.riskRatings array
                        $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                        //$log.debug("rating list after adding: " , results.riskRatings);
                    }
                }

                $scope.results.riskRollups = rollups;
            }

        }



        function compareResults(objName){

            $log.debug("Inside compareResults, objName["+objName+"]");

            var type = (objName === "summary") ? "riskSummaryType" : "riskType";

            // check for new risk ratings
            for (var i in $scope.futureResults[objName]){

                $log.debug("check for new risk type["+$scope.futureResults[objName][i][type]+"]");

                if (foundMatch($scope.results, objName, $scope.futureResults[objName][i]) === null){


                    if (objName === "riskRatings"){

                        $log.debug("Added a new Future RiskRating: ", $scope.futureResults[objName][i]);
                        $scope.newRiskRatings.push($scope.futureResults[objName][i]);

                    }
                    else{
                        $log.debug("Added a new Future RiskRollup: ", $scope.futureResults[objName][i]);
                        $scope.newRiskRollups.push($scope.futureResults[objName][i]);

                    }

                }

            }

            $log.debug("check for removed and edited");
            var ignoredFieldNames = ["riskRollups", "riskRatings", "missing", "messages"];

            // check for removed ratings
            for (var i in $scope.results[objName]){

                $log.debug("check for removed risktype["+$scope.results[objName][i][type]+"]");

                var found = foundMatch($scope.futureResults, objName, $scope.results[objName][i]);
                if (found === null){

                    $log.debug("Removed a RiskRating: ", $scope.results[objName][i]);
                    $scope.results[objName][i].removed = true;

                }
                else{

                    $log.debug("Edited RiskRating: ", $scope.results[objName][i]);

                    for (fieldName in $scope.results[objName][i]){

                        if (ignoredFieldNames.indexOf(fieldName) > -1){
                            $log.debug("skip fieldName["+fieldName+"]");
                            continue;
                        }

                        if(found[fieldName] === $scope.results[objName][i][fieldName]) {
                            $log.debug("Is the same");
                        }
                        else{
                            $log.debug("Is the same risk type and rule name, different value for fieldName["+fieldName+"]");

                            $scope.results[objName][i].future = found;
                            $scope.results[objName][i].edited = true;
                        }

                    }

                }

            }
        }

        // the unique key is the risk type and rule name
        function foundMatch(compareTo, objName, obj){

            //$log.debug("Inside foundMatch, obj: ", obj);
            var type = (objName === "summary") ? "riskSummaryType" : "riskType";

            for (var i in compareTo[objName]) {

                //$log.debug("compare object["+i+"]: ", compareTo[objName][i]);

                //$scope.futureResults[objName][i].hasOwnProperty(fieldName) &&
                if (obj[type] === compareTo[objName][i][type]) {
                    //$log.debug("Have the same risk type");

                    if (obj.ruleName == undefined || obj.ruleName === compareTo[objName][i].ruleName) {
                        //$log.debug("Have the same rule name");

                        return compareTo[objName][i];

                    }

                }
            }

            return null;

        }

        $scope.filterRatings = function (rating){

            //$log.debug("Inside filterByRiskType, rating.riskType["+rating.riskType+"]");

            if (rating.edited === true || rating.removed === true){

                if (filterByRiskType == "ALL"){
                    return true;
                }
                else if (filterByRiskType == rating.riskType){
                    return true;
                }
            }


            return false;
        };

        $scope.getRiskClass = function(riskClassValue, previousValue, rating){

            var riskClass = "column-risk-yellow";

            if (riskClassValue == 3){
                riskClass = "column-risk-red";
            }
            else if (riskClassValue == 2){
                riskClass = "column-risk-orange";
            }
            else if (riskClassValue == 1){
                riskClass = "column-risk-green";
            }

            if (previousValue != undefined ){

                if (riskClassValue != previousValue){

                    riskClass += " flash_background";
                }
                else{
                    riskClass += " risk_edited";
                }
            }

            if (rating != undefined && rating.edited == true){

                riskClass += " risk_edited";
                
            }


            return riskClass;
        };

        $scope.returnToquestions = function(){
            //$location.url("/part1");
            window.history.back();
        }



    });

var module = angular.module('DiffToolModule')
    .factory('BlenderGUIConstants', function() {

        var ConstantFields = {};
        ConstantFields.riskLevel = [ {
            "name" : "1",
            "value" : "1"
        }, {
            "name" : "2",
            "value" : "2"
        }, {
            "name" : "3",
            "value" : "3"
        }];

	    return {
            "getRiskLevel": function (){

                return ConstantFields.riskLevel;
            }

        };
	
    })
    .factory('BlenderFinal', function ($resource) {
        return $resource('/mm-rest-app/rest/blender/final', {},
            {
                'blend': { method:'POST',
                          isArray:false,
                          headers : {
                            Accept : 'application/json'
                          }
                        }
            });

    })
    .factory('LabResults', function ($resource) {
        return $resource('/mm-rest-app/rest/blender/labs', {},
            {
                'calculate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    });







	

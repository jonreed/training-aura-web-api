angular.module('Part1Module', ['ngResource'])
.controller('Part1Controller',
    function ($scope, $http, $location, $log, RiskRatingP1, RiskRollupP1, RiskSummaryP1, sharedProperties, QuoteGUIConstants){

        console.log("Inside QuoteEntryController");

        $scope.showMouseHourGlass = false;

        $scope.principalInsured = {id: "a"};
        $scope.principalInsured.insured = {};
        $scope.principalInsured.smoking = {};
        $scope.principalInsured.travel = {};
        $scope.principalInsured.military = {};
        $scope.principalInsured.aviation = {};
        $scope.principalInsured.avocation = {};


        $scope.principalInsured.policyAppliedFor = {};
        $scope.principalInsured.citizenShip = {};
        $scope.principalInsured.disability = {};
        $scope.principalInsured.dui = {};
        $scope.principalInsured.medicalHistory = {};
        $scope.principalInsured.felony = {};

        $scope.principalInsured.purposeOfInsurance = {};
        $scope.principalInsured.ownerBeneficary = {};
        $scope.principalInsured.paymentInformation = {};
        $scope.principalInsured.financialHistory = {};
        $scope.principalInsured.temporaryLifeInsuranceReceipt = {};


        function initScopeVars (){

            console.log("Inside initScopeVars");

            $scope.errorQuoteMessages = [];
            $scope.warningQuoteMessages = [];
            $scope.infoQuoteMessages = [];

            $scope.QuoteStatus  = {
                "FORM_INCOMPLETE": "FORM_INCOMPLETE",
                "NEED_VALIDATION_CHECK": "NEED_VALIDATION_CHECK",
                "ELIBIBILITY_COMPLETE": "ELIBIBILITY_COMPLETE"

            };

        }

        initScopeVars();

        function doQuoteMessages (){

            var haveErrors = false;
            var messages = $scope.wrapper.quoteMessages;
            $scope.errorQuoteMessages = [];
            $scope.infoQuoteMessages = [];
            $scope.warningQuoteMessages = [];

            if (messages && messages.length > 0){

                for (var i = 0; i < messages.length; i++) {
                    var msg = messages[i];
                    if ("ERROR" == msg.messageStatus) {
                        $scope.errorQuoteMessages.push(msg);
                        haveErrors = true;
                    }
                    if ("INFO" == msg.messageStatus) {
                        $scope.infoQuoteMessages.push(msg);
                    }
                    if ("WARNING" == msg.messageStatus) {
                        $scope.warningQuoteMessages.push(msg);
                    }

                }
            }


            return haveErrors;

        }

        $scope.stateList = QuoteGUIConstants.getStates();

        function updateQuoteStatus (){
            $log.debug("Inside updateQuoteStatus, before: " + $scope.mainForm.$valid);

            if ($scope.mainForm.$valid) {

                $scope.quoteStatus = $scope.QuoteStatus.NEED_ELIGIBILITY_CHECK;
            }
            else {

                $scope.quoteStatus = $scope.QuoteStatus.FORM_INCOMPLETE;
            }

            //console.log("status after: " + $scope.quoteStatus, $scope.mainForm.$error);

            if ($scope.mainForm.$error && $scope.mainForm.$error.required){

                $scope.requiredFieldList = "The following fields are required:\n";

                for(index in $scope.mainForm.$error.required){
                    console.log("required field: " + $scope.mainForm.$error.required[index].$name);
                    $scope.requiredFieldList += $scope.mainForm.$error.required[index].$name + "\n";
                }
            }
        }

        function processUserInput (riskType){

            $log.debug("Inside processUserInput, riskType["+riskType+"]");

            if ($scope.principalInsured.smoking.usedPast12Months == "Y"){
                $scope.principalInsured.smoking.smokingDuration = 13;
            }
            else if ($scope.principalInsured.smoking.usedPast24Months == "Y"){
                $scope.principalInsured.smoking.smokingDuration = 25;
            }
            else if ($scope.principalInsured.smoking.usedPast12Months == "N" &&
                     $scope.principalInsured.smoking.usedPast24Months == "N"){

                $scope.principalInsured.smoking.smokingDuration = 0;
            }

            $scope.principalInsured.riskType = riskType;

        }

        function replacer(key, value) {

            return "null";
        }

        $scope.calculateRiskSummary = function (riskType, cb){

            $log.debug("Inside calculateRiskSummary P1, principalInsured: ", JSON.stringify($scope.principalInsured, null, 4));

            $scope.showMouseHourGlass = true;
            processUserInput(riskType);

            RiskSummaryP1.rate($scope.principalInsured,
                function (response){

                    $log.debug("calculateRiskSummary received data: ", response);

                    //var results = response;
                    //results.riskRatings = [];
                    //results.riskRollups = [];
                    //
                    //var rollups = [];
                    //
                    //if (response.summary[0] != undefined){
                    //    rollups = response.summary[0].riskRatingGroups;
                    //}
                    //
                    ////$log.debug("rollups: ", rollups);
                    //
                    //for (index in rollups) {
                    //    //$log.debug("found rollup: " , rollups[index]);
                    //
                    //    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){
                    //
                    //        //$log.debug("found rating list: " , rollups[index].riskRatings);
                    //        // append to results.riskRatings array
                    //        results.riskRatings.push.apply(results.riskRatings, rollups[index].riskRatings);
                    //        //$log.debug("rating list after adding: " , results.riskRatings);
                    //    }
                    //}
                    //
                    //results.riskRollups = rollups;

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);
                    $scope.showMouseHourGlass = false;

                    cb();

                },
                function (err){
                    console.error('calculateRiskSummary:received an error: ', err);
                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                    cb(err);

                });

        };

        $scope.calculateRiskSummaryDiff = function (riskType){

            $log.debug("Inside calculateRiskSummaryDiff P1");

            $scope.calculateRiskSummary(riskType, function(err){

                if(err){

                    return alert("Internal error: " + $scope.errorMessages);
                }
                else{

                    var futureResults = {
                        "riskRollups": [
                            {
                                "description": "TEMPORARY_LIFE_INSURANCE_RECEIPT group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                "riskRatings": [
                                    {
                                        "description": "The policy owner  is not planning to use a Loan/Premium Financing arrangement.",
                                        "ruleName": "rule-TLIR-8",
                                        "rollupComplete": false,
                                        "riskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                        "partOneRiskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "ANNUAL_INCOME group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "ANNUAL_INCOME",
                                "riskRatings": [
                                    {
                                        "description": "For income amounts less than $31,000, is the proposed plan premium less than or equal to 5% of income?",
                                        "ruleName": "rule-premium to income-2",
                                        "rollupComplete": false,
                                        "riskType": "ANNUAL_INCOME",
                                        "partOneRiskType": "ANNUAL_INCOME",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "RTR group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "RTR",
                                "riskRatings": [
                                    {
                                        "description": "Is the total of the RTR Face amount and Base Face Amount applied for less than  or euqal to $1,000,000? ",
                                        "ruleName": "rule-rtr-2",
                                        "rollupComplete": false,
                                        "riskType": "RTR",
                                        "partOneRiskType": "RTR",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "SOURCE_OF_PREMIUM group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "SOURCE_OF_PREMIUM",
                                "riskRatings": [
                                    {
                                        "description": "Is the source of premium payments from a source other than Earned Income, Investment Income, Investment/Savings, or Gifts/Inheritence?",
                                        "ruleName": "rule-source of premium-2",
                                        "rollupComplete": false,
                                        "riskType": "SOURCE_OF_PREMIUM",
                                        "partOneRiskType": "SOURCE_OF_PREMIUM",
                                        "upnt": 3,
                                        "spnt": 3,
                                        "nont": 3,
                                        "spt": 3,
                                        "tob": 3,
                                        "wp": 3,
                                        "diio": 3
                                    }
                                ],
                                "upnt": 3,
                                "spnt": 3,
                                "nont": 3,
                                "spt": 3,
                                "tob": 3,
                                "wp": 3,
                                "diio": 3
                            },
                            {
                                "description": "TERM_RIDER group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "TERM_RIDER",
                                "riskRatings": [
                                    {
                                        "description": "Is the total face amount of PCR inforce policies less than or equal to  $1,000,000?",
                                        "ruleName": "rule-term-4",
                                        "rollupComplete": false,
                                        "riskType": "TERM_RIDER",
                                        "partOneRiskType": "TERM_RIDER",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Is requested term face amount applied for less than or equal to $1,000,000?",
                                        "ruleName": "rule-term-2",
                                        "rollupComplete": false,
                                        "riskType": "TERM_RIDER",
                                        "partOneRiskType": "TERM_RIDER",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "OTHER_LIFE_COVERAGE_REPLACEMENT group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                "riskRatings": [
                                    {
                                        "description": "Is the anticipated value of the exchanged policiesequal to or less than $1,000,000?",
                                        "ruleName": "rule-replacement-6",
                                        "rollupComplete": false,
                                        "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                        "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Is the total amount of life insurance currently applied for on the proposed insured equal to or less than $1,000,000?",
                                        "ruleName": "rule-replacement-4",
                                        "rollupComplete": false,
                                        "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                        "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Is the total amount of life insurance already in force on the proposed insured equal to or less  than $1,000,000? ",
                                        "ruleName": "rule-replacement-2",
                                        "rollupComplete": false,
                                        "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                        "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "WAIVER_OF_PREMIUM group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "WAIVER_OF_PREMIUM",
                                "riskRatings": [
                                    {
                                        "description": "rule-total amount at risk - WP - 2",
                                        "ruleName": "rule-total amount at risk - WP - 2",
                                        "rollupComplete": false,
                                        "riskType": "WAIVER_OF_PREMIUM",
                                        "partOneRiskType": "WAIVER_OF_PREMIUM",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "MAXIMUM_FACE_AMOUNT group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                "riskRatings": [
                                    {
                                        "description": "If an Alternate policy has been requested, is the greater of the Base Face Amount and the Alternate Policy Face Amount plus the current in-force amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount plus the face amount for any additional policy applied for less than or equal to $1,000,000?",
                                        "ruleName": "rule-maximum face amount- 10",
                                        "rollupComplete": false,
                                        "riskType": "MAXIMUM_FACE_AMOUNT",
                                        "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Is the current in-force amount plus the base policy amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount plus the face amount for any additional policy applied for less than or equal to $1,000,000?",
                                        "ruleName": "rule-maximum face amount- 8",
                                        "rollupComplete": false,
                                        "riskType": "MAXIMUM_FACE_AMOUNT",
                                        "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Is the current in-force amount plus the base policy amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount less than or equal to $1,000,000?",
                                        "ruleName": "rule-maximum face amount- 6",
                                        "rollupComplete": false,
                                        "riskType": "MAXIMUM_FACE_AMOUNT",
                                        "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Is the base policy amount being applied for greater than $1,000,000?",
                                        "ruleName": "rule-maximum face amount- 4",
                                        "rollupComplete": false,
                                        "riskType": "MAXIMUM_FACE_AMOUNT",
                                        "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Is the current in-force amount plus the base policy amount being applied for less than or equal to $1,000,000?",
                                        "ruleName": "rule-maximum face amount- 2",
                                        "rollupComplete": false,
                                        "riskType": "MAXIMUM_FACE_AMOUNT",
                                        "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "ALIR group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "ALIR",
                                "riskRatings": [
                                    {
                                        "description": "Unscheduled ALIR Lump Sum premium entered is less than or equal to $10,000.00",
                                        "ruleName": "rule-ALIR-3",
                                        "rollupComplete": false,
                                        "riskType": "ALIR",
                                        "partOneRiskType": "ALIR",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "Dollar amount entered in C12b is less than or equal to $10,000 AND ALIR Amount At Risk) plus the amount in C3 does not exceed $1,000,000",
                                        "ruleName": "rule-ALIR-5",
                                        "rollupComplete": false,
                                        "riskType": "ALIR",
                                        "partOneRiskType": "ALIR",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "TOTAL_INSURED_AMOUNT group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "TOTAL_INSURED_AMOUNT",
                                "riskRatings": [
                                    {
                                        "description": "Is the total face amount applied for less than or equal to $1,000,000?",
                                        "ruleName": "rule-total amount-2",
                                        "rollupComplete": false,
                                        "riskType": "TOTAL_INSURED_AMOUNT",
                                        "partOneRiskType": "TOTAL_INSURED_AMOUNT",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "TOTAL_AMOUNT_AT_RISK group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "TOTAL_AMOUNT_AT_RISK",
                                "riskRatings": [
                                    {
                                        "description": "If rule-at risk amt-1 is less than or equal to $1,000,000",
                                        "ruleName": "rule-at risk amt-2",
                                        "rollupComplete": false,
                                        "riskType": "TOTAL_AMOUNT_AT_RISK",
                                        "partOneRiskType": "TOTAL_AMOUNT_AT_RISK",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "SMOKER group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "SMOKER",
                                "riskRatings": [
                                    {
                                        "description": "12 months since Proposed Insured used tobacco products other than cigars",
                                        "ruleName": "rule-smoker-tobacco_products12",
                                        "rollupComplete": false,
                                        "riskType": "SMOKER",
                                        "partOneRiskType": "SMOKER",
                                        "upnt": 3,
                                        "spnt": 3,
                                        "nont": 3,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 3,
                                "spnt": 3,
                                "nont": 3,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "LISR group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "LISR",
                                "riskRatings": [
                                    {
                                        "description": "Is the total of the LISR Face amount and Base Face Amount applied for less than or equal to $1,000,000?",
                                        "ruleName": "rule-LISR-2",
                                        "rollupComplete": false,
                                        "riskType": "LISR",
                                        "partOneRiskType": "LISR",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "PREMIUM_PAYER group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "PREMIUM_PAYER",
                                "riskRatings": [
                                    {
                                        "description": "Is the Premium Payor the proposed insured or proposed owner?",
                                        "ruleName": "rule-premium payor-1",
                                        "rollupComplete": false,
                                        "riskType": "PREMIUM_PAYER",
                                        "partOneRiskType": "PREMIUM_PAYER",
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "AGE group rollup",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskType": "AGE",
                                "riskRatings": [
                                    {
                                        "description": "If calculated issue Insurance age is less than 17 or greater than  40.",
                                        "ruleName": "rule-age-1",
                                        "rollupComplete": false,
                                        "riskType": "AGE",
                                        "partOneRiskType": "AGE",
                                        "upnt": 3,
                                        "spnt": 3,
                                        "nont": 2,
                                        "spt": 3,
                                        "tob": 2,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 3,
                                "spnt": 3,
                                "nont": 2,
                                "spt": 3,
                                "tob": 2,
                                "wp": 1,
                                "diio": 1
                            }
                        ],
                        "messages": [
                            {
                                "questionIdentifier": "flying",
                                "riskType": "AVIATION",
                                "message": "Aviation flying is required",
                                "type": "REQUIRED"
                            },
                            {
                                "questionIdentifier": "memberOfArmedForces",
                                "riskType": "MILITARY",
                                "message": "Member Of Armed Forces is required",
                                "type": "REQUIRED"
                            }
                        ],
                        "summary": [
                            {
                                "description": "PART_I rollup summary",
                                "rollupComplete": true,
                                "riskSummaryType": "PART_I",
                                "riskRatingGroups": [
                                    {
                                        "description": "TEMPORARY_LIFE_INSURANCE_RECEIPT group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                        "riskRatings": [
                                            {
                                                "description": "The policy owner  is not planning to use a Loan/Premium Financing arrangement.",
                                                "ruleName": "rule-TLIR-8",
                                                "rollupComplete": false,
                                                "riskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                                "partOneRiskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "ANNUAL_INCOME group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "ANNUAL_INCOME",
                                        "riskRatings": [
                                            {
                                                "description": "For income amounts less than $31,000, is the proposed plan premium less than or equal to 5% of income?",
                                                "ruleName": "rule-premium to income-2",
                                                "rollupComplete": false,
                                                "riskType": "ANNUAL_INCOME",
                                                "partOneRiskType": "ANNUAL_INCOME",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "RTR group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "RTR",
                                        "riskRatings": [
                                            {
                                                "description": "Is the total of the RTR Face amount and Base Face Amount applied for less than  or euqal to $1,000,000? ",
                                                "ruleName": "rule-rtr-2",
                                                "rollupComplete": false,
                                                "riskType": "RTR",
                                                "partOneRiskType": "RTR",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "SOURCE_OF_PREMIUM group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "SOURCE_OF_PREMIUM",
                                        "riskRatings": [
                                            {
                                                "description": "Is the source of premium payments from a source other than Earned Income, Investment Income, Investment/Savings, or Gifts/Inheritence?",
                                                "ruleName": "rule-source of premium-2",
                                                "rollupComplete": false,
                                                "riskType": "SOURCE_OF_PREMIUM",
                                                "partOneRiskType": "SOURCE_OF_PREMIUM",
                                                "upnt": 3,
                                                "spnt": 3,
                                                "nont": 3,
                                                "spt": 3,
                                                "tob": 3,
                                                "wp": 3,
                                                "diio": 3
                                            }
                                        ],
                                        "upnt": 3,
                                        "spnt": 3,
                                        "nont": 3,
                                        "spt": 3,
                                        "tob": 3,
                                        "wp": 3,
                                        "diio": 3
                                    },
                                    {
                                        "description": "TERM_RIDER group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "TERM_RIDER",
                                        "riskRatings": [
                                            {
                                                "description": "Is the total face amount of PCR inforce policies less than or equal to  $1,000,000?",
                                                "ruleName": "rule-term-4",
                                                "rollupComplete": false,
                                                "riskType": "TERM_RIDER",
                                                "partOneRiskType": "TERM_RIDER",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Is requested term face amount applied for less than or equal to $1,000,000?",
                                                "ruleName": "rule-term-2",
                                                "rollupComplete": false,
                                                "riskType": "TERM_RIDER",
                                                "partOneRiskType": "TERM_RIDER",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "OTHER_LIFE_COVERAGE_REPLACEMENT group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                        "riskRatings": [
                                            {
                                                "description": "Is the anticipated value of the exchanged policiesequal to or less than $1,000,000?",
                                                "ruleName": "rule-replacement-6",
                                                "rollupComplete": false,
                                                "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                                "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Is the total amount of life insurance currently applied for on the proposed insured equal to or less than $1,000,000?",
                                                "ruleName": "rule-replacement-4",
                                                "rollupComplete": false,
                                                "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                                "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Is the total amount of life insurance already in force on the proposed insured equal to or less  than $1,000,000? ",
                                                "ruleName": "rule-replacement-2",
                                                "rollupComplete": false,
                                                "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                                "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "WAIVER_OF_PREMIUM group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "WAIVER_OF_PREMIUM",
                                        "riskRatings": [
                                            {
                                                "description": "rule-total amount at risk - WP - 2",
                                                "ruleName": "rule-total amount at risk - WP - 2",
                                                "rollupComplete": false,
                                                "riskType": "WAIVER_OF_PREMIUM",
                                                "partOneRiskType": "WAIVER_OF_PREMIUM",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "MAXIMUM_FACE_AMOUNT group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "MAXIMUM_FACE_AMOUNT",
                                        "riskRatings": [
                                            {
                                                "description": "If an Alternate policy has been requested, is the greater of the Base Face Amount and the Alternate Policy Face Amount plus the current in-force amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount plus the face amount for any additional policy applied for less than or equal to $1,000,000?",
                                                "ruleName": "rule-maximum face amount- 10",
                                                "rollupComplete": false,
                                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Is the current in-force amount plus the base policy amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount plus the face amount for any additional policy applied for less than or equal to $1,000,000?",
                                                "ruleName": "rule-maximum face amount- 8",
                                                "rollupComplete": false,
                                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Is the current in-force amount plus the base policy amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount less than or equal to $1,000,000?",
                                                "ruleName": "rule-maximum face amount- 6",
                                                "rollupComplete": false,
                                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Is the base policy amount being applied for greater than $1,000,000?",
                                                "ruleName": "rule-maximum face amount- 4",
                                                "rollupComplete": false,
                                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Is the current in-force amount plus the base policy amount being applied for less than or equal to $1,000,000?",
                                                "ruleName": "rule-maximum face amount- 2",
                                                "rollupComplete": false,
                                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "ALIR group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "ALIR",
                                        "riskRatings": [
                                            {
                                                "description": "Unscheduled ALIR Lump Sum premium entered is less than or equal to $10,000.00",
                                                "ruleName": "rule-ALIR-3",
                                                "rollupComplete": false,
                                                "riskType": "ALIR",
                                                "partOneRiskType": "ALIR",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            },
                                            {
                                                "description": "Dollar amount entered in C12b is less than or equal to $10,000 AND ALIR Amount At Risk) plus the amount in C3 does not exceed $1,000,000",
                                                "ruleName": "rule-ALIR-5",
                                                "rollupComplete": false,
                                                "riskType": "ALIR",
                                                "partOneRiskType": "ALIR",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "TOTAL_INSURED_AMOUNT group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "TOTAL_INSURED_AMOUNT",
                                        "riskRatings": [
                                            {
                                                "description": "Is the total face amount applied for less than or equal to $1,000,000?",
                                                "ruleName": "rule-total amount-2",
                                                "rollupComplete": false,
                                                "riskType": "TOTAL_INSURED_AMOUNT",
                                                "partOneRiskType": "TOTAL_INSURED_AMOUNT",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "TOTAL_AMOUNT_AT_RISK group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "TOTAL_AMOUNT_AT_RISK",
                                        "riskRatings": [
                                            {
                                                "description": "If rule-at risk amt-1 is less than or equal to $1,000,000",
                                                "ruleName": "rule-at risk amt-2",
                                                "rollupComplete": false,
                                                "riskType": "TOTAL_AMOUNT_AT_RISK",
                                                "partOneRiskType": "TOTAL_AMOUNT_AT_RISK",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "SMOKER group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "SMOKER",
                                        "riskRatings": [
                                            {
                                                "description": "12 months since Proposed Insured used tobacco products other than cigars",
                                                "ruleName": "rule-smoker-tobacco_products12",
                                                "rollupComplete": false,
                                                "riskType": "SMOKER",
                                                "partOneRiskType": "SMOKER",
                                                "upnt": 3,
                                                "spnt": 3,
                                                "nont": 3,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 3,
                                        "spnt": 3,
                                        "nont": 3,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "LISR group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "LISR",
                                        "riskRatings": [
                                            {
                                                "description": "Is the total of the LISR Face amount and Base Face Amount applied for less than or equal to $1,000,000?",
                                                "ruleName": "rule-LISR-2",
                                                "rollupComplete": false,
                                                "riskType": "LISR",
                                                "partOneRiskType": "LISR",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "PREMIUM_PAYER group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "PREMIUM_PAYER",
                                        "riskRatings": [
                                            {
                                                "description": "Is the Premium Payor the proposed insured or proposed owner?",
                                                "ruleName": "rule-premium payor-1",
                                                "rollupComplete": false,
                                                "riskType": "PREMIUM_PAYER",
                                                "partOneRiskType": "PREMIUM_PAYER",
                                                "upnt": 1,
                                                "spnt": 1,
                                                "nont": 1,
                                                "spt": 1,
                                                "tob": 1,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 1,
                                        "spnt": 1,
                                        "nont": 1,
                                        "spt": 1,
                                        "tob": 1,
                                        "wp": 1,
                                        "diio": 1
                                    },
                                    {
                                        "description": "AGE group rollup",
                                        "rollupComplete": true,
                                        "riskSummaryType": "PART_I",
                                        "riskType": "AGE",
                                        "riskRatings": [
                                            {
                                                "description": "If calculated issue Insurance age is less than 17 or greater than  40.",
                                                "ruleName": "rule-age-1",
                                                "rollupComplete": false,
                                                "riskType": "AGE",
                                                "partOneRiskType": "AGE",
                                                "upnt": 3,
                                                "spnt": 3,
                                                "nont": 2,
                                                "spt": 3,
                                                "tob": 2,
                                                "wp": 1,
                                                "diio": 1
                                            }
                                        ],
                                        "upnt": 3,
                                        "spnt": 3,
                                        "nont": 2,
                                        "spt": 3,
                                        "tob": 2,
                                        "wp": 1,
                                        "diio": 1
                                    }
                                ],
                                "upnt": 3,
                                "spnt": 3,
                                "nont": 3,
                                "spt": 3,
                                "tob": 3,
                                "wp": 3,
                                "diio": 3
                            }
                        ],
                        "riskRatings": [
                            {
                                "description": "The policy owner  is not planning to use a Loan/Premium Financing arrangement.",
                                "ruleName": "rule-TLIR-8",
                                "rollupComplete": false,
                                "riskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                "partOneRiskType": "TEMPORARY_LIFE_INSURANCE_RECEIPT",
                                "upnt": 2,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 3,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "For income amounts less than $31,000, is the proposed plan premium less than or equal to 5% of income?",
                                "ruleName": "rule-premium to income-2",
                                "rollupComplete": false,
                                "riskType": "ANNUAL_INCOME",
                                "partOneRiskType": "ANNUAL_INCOME",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the total of the RTR Face amount and Base Face Amount applied for less than  or euqal to $1,000,000? ",
                                "ruleName": "rule-rtr-2",
                                "rollupComplete": false,
                                "riskType": "RTR",
                                "partOneRiskType": "RTR",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the source of premium payments from a source other than Earned Income, Investment Income, Investment/Savings, or Gifts/Inheritence?",
                                "ruleName": "rule-source of premium-2",
                                "rollupComplete": false,
                                "riskType": "SOURCE_OF_PREMIUM",
                                "partOneRiskType": "SOURCE_OF_PREMIUM",
                                "upnt": 3,
                                "spnt": 3,
                                "nont": 3,
                                "spt": 3,
                                "tob": 3,
                                "wp": 3,
                                "diio": 3
                            },
                            {
                                "description": "Is the total face amount of PCR inforce policies less than or equal to  $1,000,000?",
                                "ruleName": "rule-term-4",
                                "rollupComplete": false,
                                "riskType": "TERM_RIDER",
                                "partOneRiskType": "TERM_RIDER",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is requested term face amount applied for less than or equal to $1,000,000?",
                                "ruleName": "rule-term-2",
                                "rollupComplete": false,
                                "riskType": "TERM_RIDER",
                                "partOneRiskType": "TERM_RIDER",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the anticipated value of the exchanged policiesequal to or less than $1,000,000?",
                                "ruleName": "rule-replacement-6",
                                "rollupComplete": false,
                                "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the total amount of life insurance currently applied for on the proposed insured equal to or less than $1,000,000?",
                                "ruleName": "rule-replacement-4",
                                "rollupComplete": false,
                                "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the total amount of life insurance already in force on the proposed insured equal to or less  than $1,000,000? ",
                                "ruleName": "rule-replacement-2",
                                "rollupComplete": false,
                                "riskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                "partOneRiskType": "OTHER_LIFE_COVERAGE_REPLACEMENT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "rule-total amount at risk - WP - 2",
                                "ruleName": "rule-total amount at risk - WP - 2",
                                "rollupComplete": false,
                                "riskType": "WAIVER_OF_PREMIUM",
                                "partOneRiskType": "WAIVER_OF_PREMIUM",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "If an Alternate policy has been requested, is the greater of the Base Face Amount and the Alternate Policy Face Amount plus the current in-force amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount plus the face amount for any additional policy applied for less than or equal to $1,000,000?",
                                "ruleName": "rule-maximum face amount- 10",
                                "rollupComplete": false,
                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the current in-force amount plus the base policy amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount plus the face amount for any additional policy applied for less than or equal to $1,000,000?",
                                "ruleName": "rule-maximum face amount- 8",
                                "rollupComplete": false,
                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the current in-force amount plus the base policy amount plus the ALIR At-Risk Amount plus  LISR face amount plus RTR Face Amount less than or equal to $1,000,000?",
                                "ruleName": "rule-maximum face amount- 6",
                                "rollupComplete": false,
                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the base policy amount being applied for greater than $1,000,000?",
                                "ruleName": "rule-maximum face amount- 4",
                                "rollupComplete": false,
                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the current in-force amount plus the base policy amount being applied for less than or equal to $1,000,000?",
                                "ruleName": "rule-maximum face amount- 2",
                                "rollupComplete": false,
                                "riskType": "MAXIMUM_FACE_AMOUNT",
                                "partOneRiskType": "MAXIMUM_FACE_AMOUNT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Unscheduled ALIR Lump Sum premium entered is less than or equal to $10,000.00",
                                "ruleName": "rule-ALIR-3",
                                "rollupComplete": false,
                                "riskType": "ALIR",
                                "partOneRiskType": "ALIR",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Dollar amount entered in C12b is less than or equal to $10,000 AND ALIR Amount At Risk) plus the amount in C3 does not exceed $1,000,000",
                                "ruleName": "rule-ALIR-5",
                                "rollupComplete": false,
                                "riskType": "ALIR",
                                "partOneRiskType": "ALIR",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the total face amount applied for less than or equal to $1,000,000?",
                                "ruleName": "rule-total amount-2",
                                "rollupComplete": false,
                                "riskType": "TOTAL_INSURED_AMOUNT",
                                "partOneRiskType": "TOTAL_INSURED_AMOUNT",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "12 months since Proposed Insured used tobacco products other than cigars",
                                "ruleName": "rule-smoker-tobacco_products12",
                                "rollupComplete": false,
                                "riskType": "SMOKER",
                                "partOneRiskType": "SMOKER",
                                "upnt": 3,
                                "spnt": 3,
                                "nont": 3,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the total of the LISR Face amount and Base Face Amount applied for less than or equal to $1,000,000?",
                                "ruleName": "rule-LISR-2",
                                "rollupComplete": false,
                                "riskType": "LISR",
                                "partOneRiskType": "LISR",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "Is the Premium Payor the proposed insured or proposed owner?",
                                "ruleName": "rule-premium payor-1",
                                "rollupComplete": false,
                                "riskType": "PREMIUM_PAYER",
                                "partOneRiskType": "PREMIUM_PAYER",
                                "upnt": 1,
                                "spnt": 1,
                                "nont": 1,
                                "spt": 1,
                                "tob": 1,
                                "wp": 1,
                                "diio": 1
                            },
                            {
                                "description": "If calculated issue Insurance age is less than 17 or greater than  40.",
                                "ruleName": "rule-age-1",
                                "rollupComplete": false,
                                "riskType": "AGE",
                                "partOneRiskType": "AGE",
                                "upnt": 3,
                                "spnt": 3,
                                "nont": 2,
                                "spt": 3,
                                "tob": 2,
                                "wp": 1,
                                "diio": 1
                            }
                        ]
                    };
                    sharedProperties.setFutureRatingResults(futureResults);

                    $location.url("/diff");
                }
            });
        };

        $scope.calculateRiskSummaryResults = function (riskType){

            $log.debug("Inside calculateRiskSummaryResults P1");

            $scope.calculateRiskSummary(riskType, function(err){

                if(err){

                    alert("Internal error: " + $scope.errorMessages);
                }
                else{

                    $location.url("/results");
                }
            });

        };

        $scope.calculateRiskRollup = function (riskType){

            $log.debug("Inside calculateRiskRollup P1, principalInsured: ", JSON.stringify($scope.principalInsured, null, 4));

            $scope.showMouseHourGlass = true;

            processUserInput(riskType);

            RiskRollupP1.rate($scope.principalInsured,
                function (response){

                    $log.debug("calculateRiskRollup received data: ", response);


                    //var results = response;
                    //results.riskRatings = [];
                    //var rollups = response.riskRollups; //riskRatings
                    //
                    //for (index in rollups) {
                    //    //$log.debug("found rollup: " , rollups[index]);
                    //
                    //    if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){
                    //
                    //        //$log.debug("found rating list: " , rollups[index].riskRatings);
                    //        results.riskRatings.push.apply(results.riskRatings, rollups[index].riskRatings);
                    //        //$log.debug("rating list after adding: " , results.riskRatings);
                    //    }
                    //}

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;

                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskRollup:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskRating = function (riskType){

            $log.debug("Inside calculateRiskRating P1, principalInsured: ", JSON.stringify($scope.principalInsured, null, 4));

            $scope.showMouseHourGlass = true;

            processUserInput(riskType);

            RiskRatingP1.rate($scope.principalInsured,
                function (response){

                    $log.debug("calculateRiskRating received data: ", response);

                    sharedProperties.setRatingResults(response);
                    sharedProperties.setRiskType(riskType);

                    $scope.showMouseHourGlass = false;
                    $location.url("/results");

                },
                function (err){
                    console.error('calculateRiskRating:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };




        $scope.resetQuote = function (){
            $scope.mainForm.$setPristine();

            initQuote();

            updateQuoteStatus();

        };

        function initQuote(){
            console.log("Inside initQuote");

        }

        initQuote();

    });

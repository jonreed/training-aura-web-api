angular.module('RXModule', ['ngResource'])
.controller('RXController',
    function ($scope, $http, $location, $log, RiskRatingRX, RiskRollupRX, RiskSummaryRX){

        console.log("Inside RXController");

        $scope.showMouseHourGlass = false;
        $scope.inputCodeArray = [{include: true, code: "159"},
                                {include: true, code: "172"},
                                {include: true, code: "175"},
                                {include: false, code: ""},
                                {include: false, code: "100"},
                                {include: false, code: "200"},
                                {include: false, code: ""},
                                {include: false, code: ""},
                                {include: false, code: ""},
                                {include: false, code: ""}];

        $scope.rxInput = {insured: {}, codes:{}};
        $scope.rxInput.insured = {licenseAvailable: "Y"};
        $scope.rxInput.codes.codes =  [];
        $scope.results = {};

        $scope.resetRXCodes = function (){
            $scope.mainForm.$setPristine();

        };

        function copyRxCodes(){

            $log.debug("inside copyRxCodes, $scope.inputCodeArray: ", $scope.inputCodeArray);

            $scope.rxInput.codes.codes =  [];

            for (index in $scope.inputCodeArray){

                if ($scope.inputCodeArray[index].include === true){
                    $scope.rxInput.codes.codes.push($scope.inputCodeArray[index].code);
                }

            }

            $log.debug("rxInput: ", JSON.stringify($scope.rxInput, null, 4));

        };

        $scope.getRiskClass = function(riskClassValue){

            var riskClass = "column-risk-yellow";

            if (riskClassValue == 3){
                riskClass = "column-risk-red";
            }
            else if (riskClassValue == 2){
                riskClass = "column-risk-orange";
            }
            else if (riskClassValue == 1){
                riskClass = "column-risk-green";
            }


            return riskClass;
        };

        $scope.calculateRiskSummary = function (){

            $log.debug("Inside calculateRiskSummary MVR");

            $scope.showMouseHourGlass = true;

            updateQuoteStatus();

            copyRxCodes();

            RiskSummaryRX.rate($scope.rxInput,
                function (response){

                    $log.debug("calculateRiskSummary received data: ", response);

                    $scope.results = response;
                    $scope.results.riskRatings = [];
                    $scope.results.riskRollups = [];

                    var rollups = [];

                    if (response.summary[0] != undefined){
                        rollups = response.summary[0].riskRatingGroups;
                    }

                    //$log.debug("rollups: ", rollups);

                    for (index in rollups) {
                        $log.debug("found rollup: " , rollups[index]);

                        if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                            $log.debug("found rating list: " , rollups[index].riskRatings);
                            // append to results.riskRatings array
                            $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                            $log.debug("rating list after adding: " , $scope.results.riskRatings);
                        }
                    }

                    $scope.results.riskRollups = rollups;

                    $scope.showMouseHourGlass = false;

                },
                function (err){
                    console.error('calculateRiskSummary:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskRollup = function (){

            $log.debug("Inside calculateRiskRollup RX");

            $scope.showMouseHourGlass = true;

            updateQuoteStatus();

            copyRxCodes();

            RiskRollupRX.rate($scope.rxInput,
                function (response){

                    $log.debug("calculateRiskRollup received data: ", response);


                    $scope.results = response;
                    $scope.results.riskRatings = [];
                    var rollups = response.riskRollups; //riskRatings

                    for (index in rollups) {
                        $log.debug("found rollup: " , rollups[index]);

                        if (rollups[index].riskRatings && rollups[index].riskRatings.length > 0){

                            $log.debug("found rating list: " , rollups[index].riskRatings);
                            $scope.results.riskRatings.push.apply($scope.results.riskRatings, rollups[index].riskRatings);
                            $log.debug("rating list after adding: " , $scope.results.riskRatings);
                        }
                    }

                    $scope.showMouseHourGlass = false;

                },
                function (err){
                    console.error('calculateRiskRollup:received an error: ', err);

                    $scope.showMouseHourGlass = false;

                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        $scope.calculateRiskResults = function (){

            $log.debug("Inside calculateRiskResults MVR");

            $scope.showMouseHourGlass = true;

            updateQuoteStatus();

            copyRxCodes();

            RiskRatingRX.rate($scope.rxInput,
                function (response){

                    $log.debug("calculateRiskResults received data: ", response);

                    $scope.results = response;

                    $scope.showMouseHourGlass = false;

                },
                function (err){
                    console.error('calculateRiskResults:received an error: ', err);

                    $scope.showMouseHourGlass = false;
                    if (err.data) {
                        //alert("Internal error: " + err.data.message);
                        $scope.errorMessages = err.data.message;
                    }
                    else {
                        $scope.errorMessages = ['Unknown  server error'];
                    }

                });
        };

        function updateQuoteStatus (){

            $log.debug("Inside updateQuoteStatus, before: " + $scope.mainForm);

            if ($scope.mainForm == undefined) {

                $log.debug("no form to check yet");
                return;
            }

            if ($scope.mainForm.$valid) {

                $log.debug("All required fields are filled in");
            }

            if ($scope.mainForm.$error && $scope.mainForm.$error.required){

                $scope.requiredFieldList = "The following fields are required:\n";

                for(index in $scope.mainForm.$error.required){
                    console.log("required field: " + $scope.mainForm.$error.required[index].$name);
                    $scope.requiredFieldList += $scope.mainForm.$error.required[index].$name + "\n";
                }
            }
        }

        function init(){
            console.log("Inside init");

            updateQuoteStatus();


        }

        init();


    });

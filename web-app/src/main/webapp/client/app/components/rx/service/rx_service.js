var module = angular.module('RXModule')
    .factory('RiskRatingRX', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rating_rx', {},
            {
                'rate': { method:'POST',
                          isArray:false,
                          headers : {
                            Accept : 'application/json'
                          }
                        }
            });

    })
    .factory('RiskRollupRX', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/rollup_rx', {},
            {
                'rate': { method:'POST',
                    isArray:false,
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    })
    .factory('RiskSummaryRX', function ($resource) {
        return $resource('/mm-rest-app/rest/risk/summary_rx', {},
            {
                'rate': { method:'POST',
                    headers : {
                        Accept : 'application/json'
                    }
                }
            });

    });









	

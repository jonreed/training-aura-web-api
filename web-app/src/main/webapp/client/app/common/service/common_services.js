var module = angular.module('CommonModule', [])
    .factory('sharedProperties', function ($rootScope, $log){

        var ratingResults = {};
        var futureRatingResults = {};
        var riskType = "";

        return {

            setRatingResults: function (results){

                ratingResults = results;
            },
            getRatingResults: function (){

                return ratingResults;
            },
            setFutureRatingResults: function (results){

                futureRatingResults = results;
            },
            getFutureRatingResults: function (){

                return futureRatingResults;
            },
            setRiskType: function (type){

                riskType = type;
            },
            getRiskType: function (){

                return riskType;
            }

        }
    })
    .factory('sharedResultSet', function($http) {
        return {
            get: function() {
                return $http.get('/client/app/assets/results/results.csv').then(function (response) {
                    return response.data;
                }, function (err) {
                    throw {
                        message: 'Repos error',
                        status: err.status,
                        data: err.data
                    };
                });
            }
        };
    });







	


// Define any routes for the app
// Note that this app is a single page app, and each partial is routed to using the URL fragment.
angular.module('insurance-demo', ['ngRoute','CommonModule', 'HeaderModule', 'Part1Module', 'Part2Module', 'MIBModule','MVRModule', 'VerificationModule', 'RXModule', 'BlenderModule', 'RulesModule', 'SwaggerModule', 'DiffToolModule', 'RatingResultsController', 'ui.bootstrap', 'ui.mask', 'PolicyService', 'RatingResultSetController'])
    .config([ '$httpProvider', '$routeProvider', function($httpProvider, $routeProvider) {

            $routeProvider.
            when('/results',{
                templateUrl : 'client/app/components/rating_results/view/rating_results_view.html',
                controller : 'ResultsController'
            })
            .when('/part1',{
            	templateUrl : 'client/app/components/part1/view/part1_view.html',
            	controller : 'Part1Controller'
            })
            .when('/part2',{
                templateUrl : 'client/app/components/part2/view/part2_view.html',
                controller : 'Part2Controller'
            })
            .when('/mib',{
                templateUrl : 'client/app/components/mib/view/mib_view.html',
                controller : 'MIBController'
            })
            .when('/mvr',{
                templateUrl : 'client/app/components/mvr/view/mvr_view.html',
                controller : 'MVRController'
            })
            .when('/rx',{
                templateUrl : 'client/app/components/rx/view/rx_view.html',
                controller : 'RXController'
            })
            .when('/verification',{
                templateUrl : 'client/app/components/verification/view/verification_view.html',
                controller : 'VerificationController'
            })
            .when('/blender',{
                templateUrl : 'client/app/components/blender/view/blender_view.html',
                controller : 'BlenderController'
            })
            .when('/rules',{
                templateUrl : 'client/app/components/edit_rules/view/edit_rules_view.html',
                controller : 'RulesController'
            })
            .when('/swagger',{
                templateUrl : 'client/app/components/swagger/view/swagger_view.html',
                controller : 'SwaggerController'
            })
            .when('/diff',{
                templateUrl : 'client/app/components/diff_tool/view/diff_tool_view.html',
                controller : 'DiffToolController'
            })
            .when('/resultset',{
                templateUrl : 'client/app/components/policy_rating_results/view/policy_rating_results_view.html',
                controller : 'ResultSetController'
            })
            .otherwise({
                redirectTo : '/part1'
            });
    }]);
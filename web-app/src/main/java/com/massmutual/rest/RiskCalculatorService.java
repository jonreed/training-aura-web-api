package com.massmutual.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//import org.jboss.logging.Logger;
import org.apache.log4j.Logger;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.domain.ConsolidatedRiskInput;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.mib.MIBInput;
import com.massmutual.domain.mvr.MVRInput;
import com.massmutual.domain.partone.PrimaryInsured;
import com.massmutual.domain.parttwo.PersonalHistory;
import com.massmutual.domain.rx.RXInput;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javassist.NotFoundException;

@Path("/risk")
@Api(value = "/risk")	//http://localhost:8080/mm-rest-app/rest/swagger.json or http://localhost:8080/mm-rest-app/rest/swagger.yaml
public class RiskCalculatorService {
	private static final Logger log = Logger.getLogger(RiskCalculatorService.class);
	
	@Inject
	private RuleProcessor rulesProsessor;

	@POST
	@Path("/rating_p1")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of PART I risk ratings given a PrimaryInsured", 
				  notes = "Returns a list of Risk Ratings",
				  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
		      				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingsP1") })
	public Response calculateRiskRatingsP1(PrimaryInsured insured) {
		log.info("inside calculateRiskRatingsP1, insured: " + insured);
			
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (insured == null) {
				throw new NotFoundException("Invalid input for insured");
			}
				
			insured.setRiskSummaryType(RiskSummaryType.PART_I);
			inputs.add(insured);
					
			response = rulesProsessor.calculateRiskRatings(inputs);
	
			log.info("retured risk rating list: " + response.getRiskRatings());
					
		} catch (Exception e) {
			log.error("calculateRiskRatingsP1, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingsP1, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
			
	}
	
	@POST
	@Path("/rating_p2")
	@ApiOperation(value = "Get a list of PART II risk ratings given a PrimaryInsured", 
	  notes = "Returns a list of Risk Ratings",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingsP2") })
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response calculateRiskRatingsP2(PersonalHistory medicalHistory) {
		log.info("inside calculateRiskRatingsP2, medicalHistory: " + medicalHistory);
			
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (medicalHistory == null) {
				throw new Exception("Invalid input for medicalHistory");
			}
				
			medicalHistory.setRiskSummaryType(RiskSummaryType.PART_II);
			inputs.add(medicalHistory);
					
			response = rulesProsessor.calculateRiskRatings(inputs);
	
			log.info("retured risk rating list: " + response.getRiskRatings());
					
		} catch (Exception e) {
			log.error("calculateRiskRatingsP2, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingsP2, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
			
	}
	
	@POST
	@Path("/rating_mvr")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of MVR risk ratings given a set of MVR codes", 
				  notes = "Returns a list of Risk Ratings",
				  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for MVR"),
		      				@ApiResponse(code = 500, message = "Exception in calculateMVRRiskRatings") })
	public Response calculateMVRRiskRatings(MVRInput input) {
		log.info("inside calculateMVRRiskRatings, insured: " + input);
			
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new NotFoundException("Invalid input for MVR");
			}
				
			input.setRiskSummaryType(RiskSummaryType.MVR);
			inputs.add(input);
					
			response = rulesProsessor.calculateRiskRatings(inputs);
	
			log.info("retured risk rating list: " + response.getRiskRatings());
					
		} catch (Exception e) {
			log.error("calculateMVRRiskRatings, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateMVRRiskRatings, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
			
	}
	
	@POST
	@Path("/rating_mib")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of MIB risk ratings given a set of MIB codes", 
				  notes = "Returns a list of Risk Ratings",
				  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for MIB"),
		      				@ApiResponse(code = 500, message = "Exception in calculateMIBRiskRatings") })
	public Response calculateMIBRiskRatings(MIBInput input) {
		log.info("inside calculateMIBRiskRatings, insured: " + input);
			
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new NotFoundException("Invalid input for MIB");
			}
				
			input.setRiskSummaryType(RiskSummaryType.MIB);
			inputs.add(input);
					
			response = rulesProsessor.calculateRiskRatings(inputs);
	
			log.info("retured risk rating list: " + response.getRiskRatings());
					
		} catch (Exception e) {
			log.error("calculateMIBRiskRatings, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateMIBRiskRatings, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
			
	}
	
	@POST
	@Path("/rating_rx")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of RX risk ratings given a set of RX codes", 
				  notes = "Returns a list of Risk Ratings",
				  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for RX"),
		      				@ApiResponse(code = 500, message = "Exception in calculateRXRiskRatings") })
	public Response calculateRXRiskRatings(RXInput input) {
		log.info("inside calculateRXRiskRatings, insured: " + input);
			
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new NotFoundException("Invalid input for RX");
			}
				
			input.setRiskSummaryType(RiskSummaryType.RX_RULES);
			inputs.add(input);
					
			response = rulesProsessor.calculateRiskRatings(inputs);
	
			log.info("retured risk rating list: " + response.getRiskRatings());
					
		} catch (Exception e) {
			log.error("calculateRXRiskRatings, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRXRiskRatings, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
			
	}

	// org.codehaus.jackson.map.JsonMapping jackson 1.9 - included in resteasy-jaxrs-3.0.9.Final.jar
	@POST
	@Path("/rollup_p1")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of PART I risk rating rollups given a PrimaryInsured", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupP1") })
	public Response calculateRiskRatingRollupP1(PrimaryInsured insured) {

		log.info("inside calculateRiskRatingRollupP1");
		
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (insured == null) {
				throw new Exception("Invalid input for insured");
			}
				
			insured.setRiskSummaryType(RiskSummaryType.PART_I);
			inputs.add(insured);
			response = rulesProsessor.calculateRiskRatingRollup(inputs);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupP1, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	@POST
	@Path("/rollup_p1rr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of PART I risk rating rollups given a PrimaryInsured", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupP1rr") })
	public Response calculateRiskRatingRollupP1rr(RiskResponse rr) {

		log.info("inside calculateRiskRatingRollupP1rr");
		
		RiskResponse response = null;
		
		try{
			
			if (rr == null) {
				throw new Exception("Invalid input for insured");
			}
				
			response = rulesProsessor.calculateRiskRatingRolluprrforp1(rr);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupP1rr, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@POST
	@Path("/rollup_p2rr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of PART I risk rating rollups given a PrimaryInsured", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupP2rr") })
	public Response calculateRiskRatingRollupP2rr(RiskResponse rr) {

		log.info("inside calculateRiskRatingRollupP2rr");
		
		RiskResponse response = null;
		
		try{
			
			if (rr == null) {
				throw new Exception("Invalid input for insured");
			}
				
			response = rulesProsessor.calculateRiskRatingRolluprrforp2(rr);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupP2rr, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@POST
	@Path("/rollup_mvrrr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of PART I risk rating rollups given a PrimaryInsured", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupmvrrr") })
	public Response calculateRiskRatingRollupmvrrr(RiskResponse rr) {

		log.info("inside calculateRiskRatingRollupmvrrr");
		
		RiskResponse response = null;
		
		try{
			
			if (rr == null) {
				throw new Exception("Invalid input for insured");
			}
				
			response = rulesProsessor.calculateRiskRatingRolluprrformvr(rr);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupmvrrr, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@POST
	@Path("/rollup_mibrr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of PART I risk rating rollups given a PrimaryInsured", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupmibrr") })
	public Response calculateRiskRatingRollupmibrr(RiskResponse rr) {

		log.info("inside calculateRiskRatingRollupmibrr");
		
		RiskResponse response = null;
		
		try{
			
			if (rr == null) {
				throw new Exception("Invalid input for insured");
			}
				
			response = rulesProsessor.calculateRiskRatingRolluprrformib(rr);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupmibrr, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	
	@POST
	@Path("/rollup_p2")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of PART II risk rating rollups given a PrimaryInsured", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupP2") })
	public Response calculateRiskRatingRollupP2(PersonalHistory medicalHistory) {

		log.info("inside calculateRiskRatingRollupP2");
		
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (medicalHistory == null) {
				throw new Exception("Invalid input for medicalHistory");
			}
				
			medicalHistory.setRiskSummaryType(RiskSummaryType.PART_II);
			inputs.add(medicalHistory);
			response = rulesProsessor.calculateRiskRatingRollup(inputs);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("calculateRiskRatingRollupP2, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupP2, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}

	@POST
	@Path("/rollup_mvr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of MVR risk rating rollups given a PrimaryInsured", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupMVR") })
	public Response calculateRiskRatingRollupMVR(MVRInput input) {

		log.info("inside calculateRiskRatingRollupMVR");
		
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for MVR");
			}
				
			input.setRiskSummaryType(RiskSummaryType.MVR);
			inputs.add(input);
			response = rulesProsessor.calculateRiskRatingRollup(inputs);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("calculateRiskRatingRollupMVR, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupMVR, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@POST
	@Path("/rollup_mib")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of MIB risk rating rollups given a MIB Input", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for MIB"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupMIB") })
	public Response calculateRiskRatingRollupMIB(MIBInput input) {

		log.info("inside calculateRiskRatingRollupMVR");
		
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for MIB");
			}
				
			input.setRiskSummaryType(RiskSummaryType.MIB);
			inputs.add(input);
			response = rulesProsessor.calculateRiskRatingRollup(inputs);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("calculateRiskRatingRollupMIB, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupMIB, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@POST
	@Path("/rollup_rx")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of RX risk rating rollups given a RXInput", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingRollupRX") })
	public Response calculateRiskRatingRollupRX(RXInput input) {

		log.info("inside calculateRiskRatingRollupRX");
		
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for RX");
			}
				
			input.setRiskSummaryType(RiskSummaryType.RX_RULES);
			inputs.add(input);
			response = rulesProsessor.calculateRiskRatingRollup(inputs);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("calculateRiskRatingRollupRX, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingRollupRX, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@Path("/summary_p1")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a PART I risk summary, which includes rollups and ratings for given a PrimaryInsured", 
	  notes = "Returns a Risk Summary",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingSummaryP1") })
	public Response calculateRiskRatingSummaryP1(PrimaryInsured insured) {
		
		log.info("inside calculateRiskRatingSummaryP1");	
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (insured == null) {
				throw new Exception("Invalid input for insured");
			}
				
			insured.setRiskSummaryType(RiskSummaryType.PART_I);
			inputs.add(insured);
			
			response = rulesProsessor.calculateRiskRatingSummary(inputs);
	
			if(log.isDebugEnabled()){
				log.info("retured riskRating Summary" + response.getSummary());
			}
			
					
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingSummaryP1, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@Path("/summary_p2")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a PART II risk summary, which includes rollups and ratings for given a PrimaryInsured", 
	  notes = "Returns a Risk Summary",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingSummaryP2") })
	public Response calculateRiskRatingSummaryP2(PersonalHistory medicalHistory) {
		
		log.info("inside calculateRiskRatingSummaryP2");	
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (medicalHistory == null) {
				throw new Exception("Invalid input for medicalHistory");
			}
				
			medicalHistory.setRiskSummaryType(RiskSummaryType.PART_II);
			inputs.add(medicalHistory);
			
			response = rulesProsessor.calculateRiskRatingSummary(inputs);
	
			if(log.isDebugEnabled()){
				log.info("retured riskRating Summary" + response.getSummary());
			}
			
					
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingSummaryP2, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@Path("/summary_mvr")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get an MVR risk summary, which includes rollups and ratings for given a PrimaryInsured", 
	  notes = "Returns a Risk Summary",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingSummaryMVR") })
	public Response calculateRiskRatingSummaryMVR(MVRInput input) {
		
		log.info("inside calculateRiskRatingSummaryMVR");	
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for MVR");
			}
				
			input.setRiskSummaryType(RiskSummaryType.MVR);
			inputs.add(input);
			
			response = rulesProsessor.calculateRiskRatingSummary(inputs);
	
			if(log.isDebugEnabled()){
				log.info("retured riskRating Summary" + response.getSummary());
			}
			
					
		} catch (Exception e) {
			log.error("calculateRiskRatingSummaryMVR, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingSummaryMVR, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@Path("/summary_mib")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get an MIB risk summary, which includes rollups and ratings for given a MIB input", 
	  notes = "Returns a Risk Summary",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for MIB"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingSummaryMIB") })
	public Response calculateRiskRatingSummaryMIB(MIBInput input) {
		
		log.info("inside calculateRiskRatingSummaryMIB");	
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for MIB");
			}
				
			input.setRiskSummaryType(RiskSummaryType.MIB);
			inputs.add(input);
			
			response = rulesProsessor.calculateRiskRatingSummary(inputs);
	
			if(log.isDebugEnabled()){
				log.info("retured riskRating Summary" + response.getSummary());
			}
			
					
		} catch (Exception e) {
			log.error("calculateRiskRatingSummaryMIB, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingSummaryMIB, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@Path("/summary_rx")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get an MVR risk summary, which includes rollups and ratings for given a PrimaryInsured", 
	  notes = "Returns a Risk Summary",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingSummaryRX") })
	public Response calculateRiskRatingSummaryRX(RXInput input) {
		
		log.info("inside calculateRiskRatingSummaryRX");	
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for RX");
			}
				
			input.setRiskSummaryType(RiskSummaryType.RX_RULES);
			inputs.add(input);
			
			response = rulesProsessor.calculateRiskRatingSummary(inputs);
	
			if(log.isDebugEnabled()){
				log.info("retured riskRating Summary" + response.getSummary());
			}
			
					
		} catch (Exception e) {
			log.error("calculateRiskRatingSummaryRX, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingSummaryRX, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@Path("/summary_all")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of risk summaries, which includes rollups and ratings for given a PrimaryInsured", 
	  notes = "Returns a list of Risk Summaries",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for ConsolidatedRiskInput"),
				@ApiResponse(code = 500, message = "Exception in calculateRiskRatingSummaryAll") })
	public Response calculateRiskRatingSummaryAll(ConsolidatedRiskInput input) {
		
		log.info("inside calculateRiskRatingSummaryAll");	
		RiskResponse response = null;
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for summary");
			}
				
			PrimaryInsured part1 = input.getPrimaryInsured();		
			part1.setRiskSummaryType(RiskSummaryType.PART_I);
			inputs.add(part1);
			
			PersonalHistory part2 = input.getPersonalHistory();		
			part2.setRiskSummaryType(RiskSummaryType.PART_II);
			inputs.add(part2);
			
			MVRInput mvr = input.getMvrInput();	
			part2.setRiskSummaryType(RiskSummaryType.MVR);
			inputs.add(mvr);
			
			RXInput rx = input.getRxInput();	
			rx.setRiskSummaryType(RiskSummaryType.MVR);
			inputs.add(rx);
			
			response = rulesProsessor.calculateRiskRatingSummary(inputs);
	
			if(log.isDebugEnabled()){
				log.info("retured riskRating Summary" + response.getSummary());
			}
			
					
		} catch (Exception e) {
			log.error("calculateRiskRatingSummaryAll, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateRiskRatingSummaryAll, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	

	@Path("/devSettings")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDevelopmentSettings() {

		String appEnvironment = System.getProperty("insurance.appEnvironment");
		Map<String, String> devSettings = new HashMap<String, String>();
		devSettings.put("appEnvironment", appEnvironment);
		return sendResponse(200, devSettings);	
		
	}
	
	@Path("/rules_deploy")
	@GET
	@ApiOperation(value = "Set the deploy environment for the brms rules", 
	  notes = "Sets environment variables",
	  response = String.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid environment varaible"),
	@ApiResponse(code = 500, message = "Exception in setDeploySettings") })
	public Response setDeploySettings(@DefaultValue("unknown") @QueryParam("env") String env) {
		
		log.info("inside setDeploySettings, env: " + env);	
		
		Properties props = System.getProperties();
		props.setProperty("rules.env", env);
		
		System.getProperties().list(System.out);

		return sendResponse(200, props.getProperty("rules.env"));
		
	}
	
	private Response sendResponse(int status, Object result){
	
		return Response.status(status).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209601").entity(result).build();
	}

}

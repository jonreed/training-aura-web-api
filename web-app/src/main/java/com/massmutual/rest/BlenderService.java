package com.massmutual.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

//import com.google.gson.Gson;
import com.massmutual.brms.BlenderRuleProcessor;
import com.massmutual.domain.blender.FinalRollupInput;
import com.massmutual.domain.blender.FinalRollupResult;
import com.massmutual.domain.labs.Applicant;
import com.massmutual.domain.labs.LabRulesSummary;

import io.swagger.annotations.Api;

@Path("/blender")
@Api(value = "/blender")
public class BlenderService {
	private static final Logger log = Logger.getLogger(BlenderService.class);
	
	@Inject
	private BlenderRuleProcessor rulesProcessor;
	
	@POST
	@Path("/final")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response finalRollup(FinalRollupInput input) {
		
		if (log.isInfoEnabled()) { log.info("Enter finalRollup: " + input); }
		try{
			
			
//			ObjectMapper mapper = new ObjectMapper();
//			//Object to JSON in String
//			String jsonInString = mapper.writeValueAsString(user);
//			FinalRollupInput obj = mapper.readValue(jsonInString, FinalRollupInput.class);
			
//			Gson prettyGson = gsonBuilder.setPrettyPrinting().create();
//			Gson gson = new Gson();
//			
//			String json = gson.toJson(input);
//			
//			log.info("generated json: " + json);
//			
//			FinalRollupInput obj = gson.fromJson(json, FinalRollupInput.class);
//			
//			log.info("converted POJO: " + obj);
			
			FinalRollupResult result = rulesProcessor.performFinalRollup(input);
			
			if (log.isInfoEnabled()) { log.info("returned: " + result); }
			
			return sendResponse(200, result);
					
		} catch (Exception e) {
			log.error("finalRollup, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in finalRollup, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
		}
	}
	
	@POST
	@Path("/labs")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response labSummary(Applicant applicant) {
		if (log.isInfoEnabled()) { log.info("Enter labSummary: " + applicant); }
		try{
			LabRulesSummary summary = rulesProcessor.performLabSummary(applicant);
			
			if (log.isInfoEnabled()) { log.info("returned: " + summary); }
			
			return sendResponse(200, summary);
					
		} catch (Exception e) {
			log.error("labSummary, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in labSummary, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
		}
	}
	
	private Response sendResponse(int status, Object result){
		return Response.status(status).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209601").entity(result).build();
	}
}

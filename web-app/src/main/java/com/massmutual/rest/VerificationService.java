package com.massmutual.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.massmutual.brms.VerificationRuleProcessor;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.partone.PrimaryInsured;
import com.massmutual.domain.verification.VerificationInput;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/verification")
@Api(value = "/verification")	//http://localhost:8080/mm-rest-app/rest/swagger.json or http://localhost:8080/mm-rest-app/rest/swagger.yaml
public class VerificationService 
{
	private static final Logger log = Logger.getLogger(VerificationService.class);
	
	@Inject
	private VerificationRuleProcessor rulesProsessor;

	@POST
	@Path("/rating")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of verification ratings given the VerificationInput with a list of RxRules", 
				  notes = "Returns a RiskResponse with list of RiskRatings",
				  response = RiskResponse.class)
	public Response calculateVerificationRatings(VerificationInput input) 
	{
		log.info("inside calculateVerificationRatings, input: " + input);
			
		RiskResponse response = null;
			
		try
		{
			
			if (input == null) {
				throw new Exception("Invalid input for calculateVerificationRatings, received NULL object");
			}
								
			response = rulesProsessor.calculateVerificationRatings(input);
	
			log.info("retured response: " + response);
					
		} 
		catch (Exception e) 
		{
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateVerificationRatings, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
		}

		return sendResponse(200, response);
			
	}
	
	// org.codehaus.jackson.map.JsonMapping jackson 1.9 - included in resteasy-jaxrs-3.0.9.Final.jar
	@POST
	@Path("/rollup")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of VERIFICATION rollups given a VerificationInput", 
	  notes = "Returns a list of Risk Rating Rollups",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for Verification"),
				@ApiResponse(code = 500, message = "Exception in calculateVerificationRollup") })
	public Response calculateVerificationRollup(VerificationInput input) {

		log.info("inside calculateVerificationRollup");
		
		RiskResponse response = null;
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for calculateVerificationRollup, received NULL object");
			}
				
			response = rulesProsessor.calculateVerificationRollup(input);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("updateQuote, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateVerificationRollup, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	
	@POST
	@Path("/summary")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get an Rx risk rating rollup and summary given a VerificationInput", 
	  notes = "Returns a list of Risk Ratings, one Rollup and one summary",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateVerificationSummary") })
	public Response calculateVerificationSummary(VerificationInput input) {

		log.info("inside calculateVerificationSummary");
		
		RiskResponse response = null;
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for calculateVerificationSummary, received NULL object");
			}
				
			response = rulesProsessor.calculateVerificationSummary(input);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("calculateRiskRatingRollupRx, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateVerificationSummary, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	@POST
	@Path("/summaryrr")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get an Rx risk rating rollup and summary given a VerificationInput", 
	  notes = "Returns a list of Risk Ratings, one Rollup and one summary",
	  response = RiskResponse.class)
	@ApiResponses(value = { @ApiResponse(code = 500, message = "Invalid input for insured"),
				@ApiResponse(code = 500, message = "Exception in calculateVerificationSummaryrr") })
	public Response calculateVerificationSummaryrr(RiskResponse input) {

		log.info("inside calculateVerificationSummaryrr");
		
		RiskResponse response = null;
		
		try{
			
			if (input == null) {
				throw new Exception("Invalid input for calculateVerificationSummary, received NULL object");
			}
				
			response = rulesProsessor.calculateVerificationSummaryrr(input);
	
			if(log.isDebugEnabled()){
				log.debug("retured riskRatingRollups" + response.getRiskRollups());
			}
							
		} catch (Exception e) {
			log.error("calculateRiskRatingRollupRx, exception",e);
			return Response.serverError().entity(new ErrorResponse("Exception in calculateVerificationSummaryrr, error: " + e.getMessage() + "\n" + e.getStackTrace())).build();
			
		}

		return sendResponse(200, response);
		
	}
	/**
	 * 
	 * @param status
	 * @param result
	 * @return
	 */
	private Response sendResponse(int status, Object result)
	{
		return Response.status(status).header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Headers", "origin, content-type, accept, authorization")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Max-Age", "1209601").entity(result).build();
	}
}

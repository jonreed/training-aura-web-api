/*
 * Copyright 2015 Vizuri, a business division of AEM Corporation
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.massmutual.rest.json;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.apache.log4j.Logger;

import com.massmutual.rest.BlenderService;
import com.massmutual.rest.RiskCalculatorService;
import com.massmutual.rest.VerificationService;

import io.swagger.jaxrs.config.BeanConfig;

@ApplicationPath("/rest")
public class JaxRsActivator extends Application {
	
	private static final Logger log = Logger.getLogger(JaxRsActivator.class);
	
	public JaxRsActivator() {
        
		log.info("Starting the rest interface for Massmutual");
		BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("0.9-SNAPSHOT");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:8080");
        beanConfig.setBasePath("/mm-rest-app/rest");	//http://localhost:8080/mm-rest-app/rest/swagger.json
        beanConfig.setResourcePackage("com.massmutual.rest");
        beanConfig.setScan(true);
        
        //ResteasyProviderFactory.getInstance().registerProvider(Class) or registerProviderInstance(Object)
    }
	
	//@SuppressWarnings("unchecked")
	public Set<Class<?>> getClasses() {
        //return new HashSet<Class<?>>(Arrays.asList(RiskCalculatorService.class, BlenderService.class, MIBService.class, VerificationService.class, JacksonConfig.class, io.swagger.jaxrs.listing.ApiListingResource.class, io.swagger.jaxrs.listing.SwaggerSerializers.class));
    
        Set<Class<?>> resources = new HashSet<>();
        
        resources.add(RiskCalculatorService.class);
        resources.add(BlenderService.class);
        resources.add(VerificationService.class);
        resources.add(JacksonConfig.class);
        resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        
        return resources;
	
	}
	
	
//	public Set<Object> getSingletons() {
//        Set<Object> singletons = new HashSet<Object>();
//        singletons.add(JacksonConfig.class); 
//        return singletons;
//    }
	
}

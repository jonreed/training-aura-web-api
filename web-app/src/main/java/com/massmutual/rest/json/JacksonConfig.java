package com.massmutual.rest.json;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.DeserializationConfig;
// codehaus is version 1.2 and up and fastxml is version 2.0 and up
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@Provider
@Produces(MediaType.APPLICATION_JSON)	//converts json to java
public class JacksonConfig implements ContextResolver<ObjectMapper> {
	
	private final static transient Logger log = Logger.getLogger(JacksonConfig.class);
    private final ObjectMapper objectMapper;  

    public JacksonConfig() throws Exception  {  

    	// version 1
    	log.info("Inside Custom Jackson JSON Provider Config (JacksonConfig) constructor");
        objectMapper = new ObjectMapper();
        
        //03.11.2015
        //objectMapper.setDateFormat(new SimpleDateFormat("dd.MM.yyyy"));
        // 6/6/2017
        //objectMapper.setDateFormat(new SimpleDateFormat("MM/dd/yyyy"));
        
        // Serialize - getter
        //objectMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
        
        // do not send fields back with null values
        objectMapper.setSerializationInclusion(Inclusion.NON_NULL);
        //objectMapper.getSerializationConfig().setSerializationInclusion(Inclusion.NON_NULL);
        
        // Deserialize - setter
        objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        
        // getters
        //objectMapper.getSerializationConfig().addMixInAnnotations(LabResult.class, ExcludeFieldsMixIn.class);
        //objectMapper.getSerializationConfig().addMixInAnnotations(CoreProperty.class, ExcludeFieldsMixIn.class);
        
        // setters
        //objectMapper.getDeserializationConfig().addMixInAnnotations(SourceProperty.class, ExcludeFieldsMixIn.class);
         
    }

    @Override
    public ObjectMapper getContext(Class<?> arg0) {
        return objectMapper;
    }  
}

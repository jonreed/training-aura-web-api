package com.massmutual.brms;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.log4j.Logger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.massmutual.domain.Insured;
import com.massmutual.domain.PartOneRiskRating;
import com.massmutual.domain.RiskBase;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskRating;
import com.massmutual.domain.RiskRatingBase;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskRatingSummary;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.ValidationMessage;
import com.massmutual.domain.mib.FormResponse;
import com.massmutual.domain.mib.IAIResponse;
import com.massmutual.domain.mib.MIBInput;
import com.massmutual.domain.mvr.MVRInput;
import com.massmutual.domain.mvr.MVRViolation;
import com.massmutual.domain.partone.PrimaryInsured;
import com.massmutual.domain.parttwo.FamilyHistory;
import com.massmutual.domain.parttwo.FamilyMember;
import com.massmutual.domain.parttwo.MedicalCondition;
import com.massmutual.domain.parttwo.PartTwoRiskBase;
import com.massmutual.domain.parttwo.PersonalHistory;
import com.massmutual.domain.rx.RXInput;
import com.massmutual.domain.verification.MedicalHistory;
import com.massmutual.domain.PartTwoRiskRating;
import com.massmutual.domain.mvr.MVRRiskRating;
import com.massmutual.domain.mib.MIBRiskRating;
import com.massmutual.domain.rx.RxRiskRating;

@Singleton
@Named("ruleProcessor")
public class RuleProcessor {

	private KieContainer kContainer = RuleFactory.get();
	private static final Logger log = Logger.getLogger(RuleProcessor.class);

	public RuleProcessor() {
	}

	public static final String AGENDA_CHECK_REQUIRED = "check_required";
	public static final String AGENDA_CALCULATE_DEPENDENCIES = "calculate-dependencies";
	public static final String AGENDA_CALCULATE_RISK_RATING = "calculate-risk-rating"; // construction
	public static final String AGENDA_CALCULATE_ROLLUP_RISK_RATING = "rollup-risk-ratings";
	public static final String AGENDA_CALCULATE_ROLLUP_GROUPS = "rollup-risk-rating-groups";

	AgendaListener agendaListener = new AgendaListener();
	RuleListener ruleListener = new RuleListener();
	
	private Set<Method> getterMethodsForPrimaryInsuredRiskBase = new HashSet<Method>();
	private Set<Method> getterMethodsForPersonalHistoryOfRiskBase = new HashSet<Method>();
	private Set<Method> getterMethodsForMVRInputOfRiskBase = new HashSet<Method>();
	private Set<Method> getterMethodsForRXInputOfRiskBase = new HashSet<Method>();
	

	private KieSession createNewRiskSession(boolean addListeners) {
		KieSession riskSession = kContainer.newKieSession();

		if (addListeners) {
			riskSession.addEventListener(agendaListener);
			riskSession.addEventListener(ruleListener);
		}

		return riskSession;
	}

	@PostConstruct
	public void init() {
		findAllRiskFields(RiskSummaryType.PART_I);
		findAllRiskFields(RiskSummaryType.PART_II);
		findAllRiskFields(RiskSummaryType.MVR);
		findAllRiskFields(RiskSummaryType.RX_RULES);
	}
		
	public void findAllRiskFields(RiskSummaryType type) {

		log.info("Inside findAllRiskFields for type: " + type);
		
		Method[] methods = null;
		Set<Method> methodList  = new HashSet<Method>(); 
		
		if (type == RiskSummaryType.PART_I){
			 
			methods = PrimaryInsured.class.getMethods();
			getterMethodsForPrimaryInsuredRiskBase = methodList;
		}
		else if (type == RiskSummaryType.PART_II){
		
			methods = PersonalHistory.class.getMethods();
			getterMethodsForPersonalHistoryOfRiskBase = methodList;
		}
		else if (type == RiskSummaryType.MVR){
			
			methods = MVRInput.class.getMethods();
			getterMethodsForMVRInputOfRiskBase = methodList;
		}
		else if (type == RiskSummaryType.RX_RULES){
			
			methods = RXInput.class.getMethods();
			getterMethodsForRXInputOfRiskBase = methodList;
		}
		
		for (Method method : methods) {
			Class<?> retType = method.getReturnType();
			
			Class<?> superClass = retType.getSuperclass();
			
			//log.info("add fact for superClass: " + superClass);
			
			if(superClass != null && ( superClass.equals(RiskBase.class) || 
									   superClass.equals(PartTwoRiskBase.class) ||
									   superClass.equals(MedicalHistory.class))) {
				
				//log.info("add fact: " + method.getName());
				methodList.add(method);
			}
			
		}
		
	}
	
	private void insertAllFacts(RiskInputBase input, KieSession riskSession) {
		
		log.info("Inside insertAllFacts");
		
		Set<Method> methodList  = new HashSet<Method>();
		//String filterByType = null;
		
		if (input.getRiskSummaryType() == RiskSummaryType.PART_I){
			 
			PrimaryInsured insured = (PrimaryInsured) input;
			
			if (insured != null){
				
				log.info("insert RiskType: " + insured.getRiskType());
				//filterByType = insured.getRiskType().name();

				methodList = getterMethodsForPrimaryInsuredRiskBase;
			}
		}
		else if (input.getRiskSummaryType() == RiskSummaryType.PART_II){
			
			PersonalHistory medicalHistory = (PersonalHistory) input;
			
			if (medicalHistory != null) {
				
				log.info("have medical conditions");
									
				methodList = getterMethodsForPersonalHistoryOfRiskBase;
				
			}
		}
		else if (input.getRiskSummaryType() == RiskSummaryType.MVR){
			
			MVRInput mvrInput = (MVRInput) input;
			
			if (mvrInput != null) {
				
				log.info("have MVR violations");
				//Add the root class so we have access to root level properties and if any helper methods are added.					
				riskSession.insert(mvrInput);	
				
				riskSession.insert(mvrInput.getInsured());	
				
				//methodList = getterMethodsForMVRInputOfRiskBase;
				
				if (mvrInput.getViolationCodes() != null){
					
					for (MVRViolation v :mvrInput.getViolationCodes()){
						log.info("add violation code: " + v);
						riskSession.insert(v);
					}
				}
				
			}
			
			return;	// nothing more to do
			
		}
		else if (input.getRiskSummaryType() == RiskSummaryType.MIB){
			
			MIBInput mibInput = (MIBInput) input;
			
			if (mibInput != null) {
				riskSession.insert(mibInput);	
				riskSession.insert(mibInput.getInsured());	
				
				if (mibInput.getResponses() != null){
					
					log.info("have MIB codes");
					
					// Insert facts
					for (FormResponse responseData : mibInput.getResponses()) {
						riskSession.insert(responseData);
					}
	
				}
				
				if (mibInput.getIaiGroup() != null){
					
					log.info("have MIB IAI responses");
					
					riskSession.insert(mibInput.getIaiGroup());
					
					// Insert facts
					for (IAIResponse r : mibInput.getIaiGroup().getIaiResponses()) {
						riskSession.insert(r);
					}
				}
				
							
			}
			
			return;	// nothing more to do
			
		}
		else if (input.getRiskSummaryType() == RiskSummaryType.RX_RULES){
			
			RXInput rxInput = (RXInput) input;
			
			if (rxInput != null) {
				
				log.info("have RX codes");
									
				//riskSession.insert(rxInput);	
				
				methodList = getterMethodsForRXInputOfRiskBase;
				
			}
			
		}
		
		for(Method method : methodList){
			
			RiskBase riskBase = null;
			Object obj = null;
			
			log.info("get fact: " + method.getName());
			
			try {
				obj = method.invoke(input);				
				log.info("get risk: " + obj);
				
			} catch (IllegalAccessException e) {
				log.error("Skip adding non-riskBase object for type[" + input.getRiskSummaryType() + "]" + obj, e);
				continue;
				
			} catch (IllegalArgumentException e) {
				log.error("Skip adding non-riskBase object for type[" + input.getRiskSummaryType() + "]" + obj, e);
				continue;
			} catch (InvocationTargetException e) {
				log.error("Skip adding non-riskBase object for type[" + input.getRiskSummaryType() + "]" + obj, e);
				continue;
			}
			
			riskBase = (RiskBase) obj;
			
			//log.info("riskBase :" + riskBase);
			
			if (riskBase != null) {
					
				//Special case for FamilyHistory - it's a more complex object with multiple lists of familymembers and
				//also derives from the base so has a list of medicalconditions as well.
				//For the rules to work correctly you want to pass in each individual FamilyMember object to be interrorgated
				//and also pass in the list of medicalconditions
				if ("getFamilyHistory".equals(method.getName())){	// PART II
	
					FamilyHistory h = (FamilyHistory) obj;
										
					FamilyHistory smallerFamilyRiskBase = new FamilyHistory();
					smallerFamilyRiskBase.setConditions(h.getConditions());
					log.info("add familyHistory object with list of medicalconditions:  " + smallerFamilyRiskBase);
					riskSession.insert(smallerFamilyRiskBase);
					
					log.info("add each Family member");
					for (FamilyMember f :h.getAllFamilyMemberLists()){
						log.info("add Familymember object: " + f);
						riskSession.insert(f);
					}
					
				}
				else{
					log.info("add riskBase:  " + riskBase);
					riskSession.insert(riskBase);
				}
			}
			
			// now add individual Medical conditions
			
//			if (input.getRiskSummaryType() == RiskSummaryType.PART_II){
//				PartTwoRiskBase condition = (PartTwoRiskBase) obj;
//				
//				log.info("add medical conditions");
//				
//				for(MedicalCondition mc : condition.getConditions()){
//				
//					riskSession.insert(mc);
//				}
//				
//			}
		
		}
		
	}

	private void insertFacts(List<RiskInputBase> inputs, KieSession riskSession) throws Exception {
		
		log.info("Inside insertFacts, inputs: " + inputs);
		
		for (RiskInputBase input : inputs){
		
			log.info("InsertFacts for type: " + input.getRiskSummaryType());
			insertAllFacts(input, riskSession);
					
		}	
				
	}

	private RiskResponse fireRiskRatingRules(KieSession riskSession) {
		
		log.info("Inside fireRiskRatingRules");
		
		RiskResponse response = new RiskResponse();
		List<ValidationMessage> messages = new ArrayList<ValidationMessage>();
		
		log.info("Fire AGENDA_CHECK_REQUIRED");
		riskSession.getAgenda().getAgendaGroup(AGENDA_CHECK_REQUIRED)
				.setFocus();
		riskSession.fireAllRules();

		QueryResults queryResults = riskSession
				.getQueryResults("getValidationMessages");

		for (QueryResultsRow row : queryResults) {
			messages.add((ValidationMessage) row.get("$error"));
		}
		response.setMessages(messages);

		log.info("Fire AGENDA_CALCULATE_DEPENDENCIES");
		riskSession.getAgenda()
				.getAgendaGroup(AGENDA_CALCULATE_DEPENDENCIES).setFocus();
		riskSession.fireAllRules();
		
		
		log.info("Fire AGENDA_CALCULATE_RISK_RATING");
		riskSession.getAgenda()
				.getAgendaGroup(AGENDA_CALCULATE_RISK_RATING).setFocus();
		riskSession.fireAllRules();

		return response;
	}
	
	// for part 1 & 2
	public RiskResponse calculateRiskRatings(List<RiskInputBase> inputs) throws Exception {

		log.info("Inside calculateRiskRatings");
		RiskResponse response = null;
		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingBase> riskRatings = new ArrayList<RiskRatingBase>();

		try {

			// set true if you need to see rule activation logging
			riskSession = createNewRiskSession(false);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}
			
			insertFacts(inputs, riskSession);		
			response = fireRiskRatingRules(riskSession);
			
			for (RiskInputBase input : inputs){
			
				if (input.getRiskSummaryType() == RiskSummaryType.PART_I){
					
					queryResults = riskSession.getQueryResults("getPartOneRiskRatings");		
				}			
				else if (input.getRiskSummaryType() == RiskSummaryType.PART_II){
					
					queryResults = riskSession.getQueryResults("getPartTwoRiskRatings");				
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.MVR){
					
					queryResults = riskSession.getQueryResults("getMVRRiskRatings");				
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.MIB){
					
					queryResults = riskSession.getQueryResults("getMIBRiskRatings");				
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.RX_RULES){
					
					queryResults = riskSession.getQueryResults("getRxRiskRatings");				
				}
				
				for (QueryResultsRow row : queryResults) {
					riskRatings.add((RiskRatingBase) row.get("$rating"));
				}
			}
			
			response.setRiskRatings(riskRatings);		
			
			return response;

		} catch (Exception e) {
			log.error("Exception in calculateRiskRatings", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}

	public RiskResponse calculateRiskRatingRollup(List<RiskInputBase> inputs)
			throws Exception {

		log.info("Inside calculateRiskRatingRollup");

		RiskResponse response = new RiskResponse();
		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		

		try {

			riskSession = createNewRiskSession(false);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			insertFacts(inputs, riskSession);
			response = fireRiskRatingRules(riskSession);

			log.info("Fire AGENDA_CALCULATE_ROLLUP_RISK_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_RISK_RATING).setFocus();
			riskSession.fireAllRules();
			
			for (RiskInputBase input : inputs){
				
				if (input.getRiskSummaryType() == RiskSummaryType.PART_I){
					
					queryResults = riskSession.getQueryResults("getPartOneRiskRatingRollup");				
				}				
				else if (input.getRiskSummaryType() == RiskSummaryType.PART_II){
					
					queryResults = riskSession.getQueryResults("getPartTwoRiskRatingRollup");
				}				
				else if (input.getRiskSummaryType() == RiskSummaryType.MVR){
					
					queryResults = riskSession.getQueryResults("getMVRRiskRatingRollup");
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.MIB){
					
					queryResults = riskSession.getQueryResults("getMIBRiskRatingRollup");
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.RX_RULES){
					
					queryResults = riskSession.getQueryResults("getRxRiskRatingRollup");
				}
				
				for (QueryResultsRow row : queryResults) {
					riskRatingRollups.add((RiskRatingRollup) row.get("$rollup"));
				}
			}
			
			response.setRiskRollups(riskRatingRollups);

			return response;

		} catch (Exception e) {
			log.error("Exception in calculateRiskRatingRollup", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}
	
	

	public RiskResponse calculateRiskRatingRolluprrforp1(RiskResponse rr) throws Exception {

		log.info("Inside calculateRiskRatingRolluprrforp1");

		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		List<RiskRatingSummary> summary = new ArrayList<RiskRatingSummary>();
		
		try {

			riskSession = createNewRiskSession(false);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			rr.setRiskRollups(null);
			
			RiskRatingSummary r = rr.getSummary().get(0);
			RiskRating riskrating1 = new RiskRating();
			PartOneRiskRating pr = new PartOneRiskRating();
			
			
   		    List<RiskRatingRollup> RiskRatingRollup = r.getRiskRatingGroups();
			System.out.println("RiskRatingRollup size>>:"+ RiskRatingRollup.size());
			for (int i=0; i< RiskRatingRollup.size();i++){
				List<RiskRating> riskRating = RiskRatingRollup.get(i).getRiskRatings();
				for (int j=0; j<riskRating.size();j++){
					riskrating1 = riskRating.get(j);
					pr = new PartOneRiskRating(riskRating.get(j));
					riskSession.insert(riskrating1);
					riskSession.insert(pr);
				}
//				riskrating1 = riskRating.get(0);
////				System.out.println("RR1: "+ riskrating1);
//				pr = new PartOneRiskRating(riskRating.get(0));
////				System.out.println("PR1: "+ pr);
//				riskSession.insert(riskrating1);
//				riskSession.insert(pr);				
			}
			


			log.info("AGENDA_CALCULATE_ROLLUP_RISK_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_RISK_RATING).setFocus();
			riskSession.fireAllRules();

			log.info("Fire AGENDA_CALCULATE_ROLLUP_GROUPS");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_GROUPS).setFocus();
			riskSession.fireAllRules();
			
					
		queryResults = riskSession.getQueryResults("getPartOneRiskRatingSummary");
			
		for (QueryResultsRow row : queryResults) {
					summary.add((RiskRatingSummary) row.get("$summary"));
					System.out.println("RR Output Added: " + rr );
				}
			
			rr.setSummary(summary);
			System.out.println("RR Output: " + rr);

			return rr;

		} catch (Exception e) {
			log.error("Exception in calculateRiskRatingRolluprrforp1", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}	
	
	public RiskResponse calculateRiskRatingRolluprrforp2(RiskResponse rr) throws Exception {

		log.info("Inside calculateRiskRatingRolluprrforp2");

		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		List<RiskRatingSummary> summary = new ArrayList<RiskRatingSummary>();
		
		try {

			riskSession = createNewRiskSession(false);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			rr.setRiskRollups(null);
			
			RiskRatingSummary r = rr.getSummary().get(0);
			RiskRating riskrating1 = new RiskRating();
			PartTwoRiskRating pr = new PartTwoRiskRating();
   		    List<RiskRatingRollup> RiskRatingRollup = r.getRiskRatingGroups();
			System.out.println("RiskRatingRollup size>>:"+ RiskRatingRollup.size());
			for (int i=0; i< RiskRatingRollup.size();i++){
				List<RiskRating> riskRating = RiskRatingRollup.get(i).getRiskRatings();
				for (int j=0; j<riskRating.size();j++){
					riskrating1 = riskRating.get(j);
					pr = new PartTwoRiskRating(riskRating.get(j));
					riskSession.insert(riskrating1);
					riskSession.insert(pr);
				}
//				riskrating1 = riskRating.get(0);
//				pr = new PartTwoRiskRating(riskRating.get(0));
//				riskSession.insert(riskrating1);
//				riskSession.insert(pr);				
			}
			


			log.info("AGENDA_CALCULATE_ROLLUP_RISK_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_RISK_RATING).setFocus();
			riskSession.fireAllRules();

			log.info("Fire AGENDA_CALCULATE_ROLLUP_GROUPS");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_GROUPS).setFocus();
			riskSession.fireAllRules();
			
					
		queryResults = riskSession.getQueryResults("getPartTwoRiskRatingSummary");
			
		for (QueryResultsRow row : queryResults) {
					summary.add((RiskRatingSummary) row.get("$summary"));
					System.out.println("RR Output Added: " + rr );
				}
			
			rr.setSummary(summary);
			System.out.println("RR Output: " + rr);

			return rr;

		} catch (Exception e) {
			log.error("Exception in calculateRiskRatingRolluprrforp2", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}	

	public RiskResponse calculateRiskRatingRolluprrformvr(RiskResponse rr) throws Exception {

		log.info("Inside calculateRiskRatingRolluprrformvr");

		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		List<RiskRatingSummary> summary = new ArrayList<RiskRatingSummary>();
		
		try {

			riskSession = createNewRiskSession(false);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			rr.setRiskRollups(null);
			
			RiskRatingSummary r = rr.getSummary().get(0);
			RiskRating riskrating1 = new RiskRating();
			MVRRiskRating pr = new MVRRiskRating();
   		    List<RiskRatingRollup> RiskRatingRollup = r.getRiskRatingGroups();
			System.out.println("RiskRatingRollup size>>:"+ RiskRatingRollup.size());
			for (int i=0; i< RiskRatingRollup.size();i++){
				List<RiskRating> riskRating = RiskRatingRollup.get(i).getRiskRatings();
				for (int j=0; j<riskRating.size();j++){
					riskrating1 = riskRating.get(j);
					pr = new MVRRiskRating(riskRating.get(j));
					riskSession.insert(riskrating1);
					riskSession.insert(pr);
				}
//				riskrating1 = riskRating.get(0);
//				
//				pr = new MVRRiskRating(riskRating.get(0));
//
//
//				riskSession.insert(riskrating1);
//				riskSession.insert(pr);				
			}
			


			log.info("AGENDA_CALCULATE_ROLLUP_RISK_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_RISK_RATING).setFocus();
			riskSession.fireAllRules();

			log.info("Fire AGENDA_CALCULATE_ROLLUP_GROUPS");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_GROUPS).setFocus();
			riskSession.fireAllRules();
			
					
		queryResults = riskSession.getQueryResults("getMVRRiskRatingSummary");
			
		for (QueryResultsRow row : queryResults) {
					summary.add((RiskRatingSummary) row.get("$summary"));
					System.out.println("RR Output Added: " + rr );
				}
			
			rr.setSummary(summary);
			System.out.println("RR Output: " + rr);

			return rr;

		} catch (Exception e) {
			log.error("Exception in calculateRiskRatingRolluprrformvr", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}
	
	public RiskResponse calculateRiskRatingRolluprrformib(RiskResponse rr) throws Exception {

		log.info("Inside calculateRiskRatingRolluprrformvr");

		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		List<RiskRatingSummary> summary = new ArrayList<RiskRatingSummary>();
		
		try {

			riskSession = createNewRiskSession(false);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			rr.setRiskRollups(null);
			
			RiskRatingSummary r = rr.getSummary().get(0);
			RiskRating riskrating1 = new RiskRating();
			MIBRiskRating pr = new MIBRiskRating();
   		    List<RiskRatingRollup> RiskRatingRollup = r.getRiskRatingGroups();
			System.out.println("RiskRatingRollup size>>:"+ RiskRatingRollup.size());
			for (int i=0; i< RiskRatingRollup.size();i++){
				List<RiskRating> riskRating = RiskRatingRollup.get(i).getRiskRatings();
				for (int j=0; j<riskRating.size();j++){
					riskrating1 = riskRating.get(j);
					pr = new MIBRiskRating(riskRating.get(j));
					riskSession.insert(riskrating1);
					riskSession.insert(pr);
				}
//				riskrating1 = riskRating.get(0);
//				
//				pr = new MIBRiskRating(riskRating.get(0));
//
//
//				riskSession.insert(riskrating1);
//				riskSession.insert(pr);				
			}
			


			log.info("AGENDA_CALCULATE_ROLLUP_RISK_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_RISK_RATING).setFocus();
			riskSession.fireAllRules();

			log.info("Fire AGENDA_CALCULATE_ROLLUP_GROUPS");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_GROUPS).setFocus();
			riskSession.fireAllRules();
			
					
		queryResults = riskSession.getQueryResults("getMIBRiskRatingSummary");
			
		for (QueryResultsRow row : queryResults) {
					summary.add((RiskRatingSummary) row.get("$summary"));
					System.out.println("RR Output Added: " + rr );
				}
			
			rr.setSummary(summary);
			System.out.println("RR Output: " + rr);

			return rr;

		} catch (Exception e) {
			log.error("Exception in calculateRiskRatingRolluprrformib", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}
	
	
	
	public RiskResponse calculateRiskRatingSummary(List<RiskInputBase> inputs)
			throws Exception {

		log.info("Inside calculateRiskRatingSummary");

		RiskResponse response = null;
		KieSession riskSession = null;
		//QueryResults queryRollupResults = null;
		QueryResults querySummaryResults = null;
		List<RiskRatingSummary> summary = new ArrayList<RiskRatingSummary>();
		//List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		

		try {

			riskSession = createNewRiskSession(false);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			insertFacts(inputs, riskSession);
			response = fireRiskRatingRules(riskSession);

			log.info("Fire AGENDA_CALCULATE_ROLLUP_RISK_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_RISK_RATING).setFocus();
			riskSession.fireAllRules();

			log.info("Fire AGENDA_CALCULATE_ROLLUP_GROUPS");
			riskSession.getAgenda().getAgendaGroup(AGENDA_CALCULATE_ROLLUP_GROUPS).setFocus();
			riskSession.fireAllRules();
			
			for (RiskInputBase input : inputs){
				
				if (input.getRiskSummaryType() == RiskSummaryType.PART_I){
					
					querySummaryResults = riskSession.getQueryResults("getPartOneRiskRatingSummary");				
				}			
				else if (input.getRiskSummaryType() == RiskSummaryType.PART_II){
									
					querySummaryResults = riskSession.getQueryResults("getPartTwoRiskRatingSummary");			
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.MVR){
						
					querySummaryResults = riskSession.getQueryResults("getMVRRiskRatingSummary");			
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.MIB){
					
					querySummaryResults = riskSession.getQueryResults("getMIBRiskRatingSummary");			
				}
				else if (input.getRiskSummaryType() == RiskSummaryType.RX_RULES){
					
					querySummaryResults = riskSession.getQueryResults("getRxRiskRatingSummary");			
				}
				
				//for (QueryResultsRow row : queryRollupResults) {
				//	riskRatingRollups.add((RiskRatingRollup) row.get("$rollup"));
				//}
				
				for (QueryResultsRow row : querySummaryResults) {
					summary.add((RiskRatingSummary) row.get("$summary"));
				}
			}

			//response.setRiskRollups(riskRatingRollups);
			response.setSummary(summary);

			return response;

		} catch (Exception e) {
			log.error("Exception in calculateRiskRatingSummary", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}
	
	

}

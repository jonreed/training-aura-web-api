package com.massmutual.brms;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.log4j.Logger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.massmutual.domain.RiskBase;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskRatingBase;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskRatingSummary;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.mib.FormResponse;
import com.massmutual.domain.mib.IAIResponse;
import com.massmutual.domain.mib.MIBInput;
import com.massmutual.domain.partone.PrimaryInsured;
import com.massmutual.domain.parttwo.PartTwoRiskBase;
import com.massmutual.domain.parttwo.PersonalHistory;
import com.massmutual.domain.verification.MedicalHistory;
import com.massmutual.domain.verification.RxReturn;
import com.massmutual.domain.verification.VerificationInput;
import com.massmutual.domain.verification.VerificationRiskRating;
import com.massmutual.domain.RiskRating;

@Singleton
@Named("verificationRuleProcessor")
public class VerificationRuleProcessor {
	// TO DO For now assuming a single agenda-group, update if appropriate.
	public static final String AGENDA_VERIFICATION_RATING = "risk_verification";
	public static final String AGENDA_VERIFICATION_ROLLUP_RATING = "verification-rollup-ratings";
	public static final String AGENDA_VERIFICATION_ROLLUP_GROUPS = "verification-rollup-groups";

	private KieContainer kContainer = RuleFactory.get();
	private static final Logger log = Logger.getLogger(VerificationRuleProcessor.class);
	private AgendaListener agendaListener = new AgendaListener();
	private RuleListener ruleListener = new RuleListener();

	private Set<Method> getterMethodsForPrimaryInsuredRiskBase = new HashSet<Method>();
	private Set<Method> getterMethodsForPersonalHistoryOfRiskBase = new HashSet<Method>();

	@PostConstruct
	public void init() {
		findAllRiskFields(RiskSummaryType.PART_I);
		findAllRiskFields(RiskSummaryType.PART_II);

	}

	public void findAllRiskFields(RiskSummaryType type) {

		Method[] methods = null;
		Set<Method> methodList = new HashSet<Method>();

		if (type == RiskSummaryType.PART_I) {

			methods = PrimaryInsured.class.getMethods();
			getterMethodsForPrimaryInsuredRiskBase = methodList;
		} else if (type == RiskSummaryType.PART_II) {

			methods = PersonalHistory.class.getMethods();
			getterMethodsForPersonalHistoryOfRiskBase = methodList;
		}

		for (Method method : methods) {
			Class<?> retType = method.getReturnType();

			Class<?> superClass = retType.getSuperclass();

			// log.info("add fact for superClass: " + superClass);

			//TODO: Need to add family history if we have any verification rules
			if (superClass != null && (superClass.equals(RiskBase.class) || 
									   superClass.equals(PartTwoRiskBase.class) || 
									   superClass.equals(MedicalHistory.class))) {

				// log.info("add fact: " + method.getName());
				methodList.add(method);
			}
		}
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public RiskResponse calculateVerificationRatings(VerificationInput input) throws Exception {

		log.info("inside calculateVerificationRatings");

		RiskResponse response = new RiskResponse();
		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingBase> riskRatings = new ArrayList<RiskRatingBase>();

		try {
			if (input == null) {
				throw new IllegalArgumentException("Expected VerificationInput received NULL object");
			}

			riskSession = createNewVerificationSession(true);

			insertFacts(input, riskSession);

			log.info("Fire AGENDA_VERIFICATION_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_RATING).setFocus();
			riskSession.fireAllRules();

			queryResults = riskSession.getQueryResults("getVerificationRiskRatings");

			for (QueryResultsRow row : queryResults) {
				riskRatings.add((RiskRatingBase) row.get("$rating"));
			}

			response.setRiskRatings(riskRatings);

			return response;

		} catch (Exception e) {
			log.error("Exception in calculateVerificationRatings", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}
	
	/**
	 * 
	 * @param input
	 * @return
	 */
	public RiskResponse calculateVerificationRollup(VerificationInput input) throws Exception {

		log.info("inside calculateVerificationRollup");

		RiskResponse response = new RiskResponse();
		KieSession riskSession = null;
		QueryResults queryResults = null;
		List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();

		try {
			if (input == null) {
				throw new IllegalArgumentException("Expected VerificationInput received NULL object");
			}

			riskSession = createNewVerificationSession(true);

			insertFacts(input, riskSession);

			log.info("Fire AGENDA_VERIFICATION_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_RATING).setFocus();
			riskSession.fireAllRules();
			
			log.info("Fire AGENDA_VERIFICATION_ROLLUP_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_ROLLUP_RATING).setFocus();
			riskSession.fireAllRules();

			queryResults = riskSession.getQueryResults("getVerificationRatingRollup");

			for (QueryResultsRow row : queryResults) {
				riskRatingRollups.add((RiskRatingRollup) row.get("$rollup"));
			}

			response.setRiskRollups(riskRatingRollups);

			return response;

		} catch (Exception e) {
			log.error("Exception in calculateVerificationRollup", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}
	
	public RiskResponse calculateVerificationSummary(VerificationInput input)
			throws Exception {

		log.info("Inside calculateVerificationSummary");

		RiskResponse response = new RiskResponse();
		KieSession riskSession = null;
		//QueryResults queryResults = null;
		//List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		QueryResults querySummaryResults = null;
		List<RiskRatingSummary> summary = new ArrayList<RiskRatingSummary>();
		

		try {

			riskSession = createNewVerificationSession(true);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			insertFacts(input, riskSession);
			
			log.info("Fire AGENDA_VERIFICATION_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_RATING).setFocus();
			riskSession.fireAllRules();

			log.info("Fire AGENDA_VERIFICATION_ROLLUP_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_ROLLUP_RATING).setFocus();
			riskSession.fireAllRules();		
			
			log.info("Fire AGENDA_VERIFICATION_ROLLUP_GROUPS");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_ROLLUP_GROUPS).setFocus();
			riskSession.fireAllRules();
			
			querySummaryResults = riskSession.getQueryResults("getVerificationRollupSummary");
			
			for (QueryResultsRow row : querySummaryResults) {
				summary.add((RiskRatingSummary) row.get("$summary"));
			}
			
			response.setSummary(summary);
						
			return response;

		} catch (Exception e) {
			log.error("Exception in calculateVerificationSummary", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}
	
	public RiskResponse calculateVerificationSummaryrr(RiskResponse rr)
			throws Exception {

		log.info("Inside calculateVerificationSummary");

		RiskResponse response = new RiskResponse();
		KieSession riskSession = null;
		//QueryResults queryResults = null;
		//List<RiskRatingRollup> riskRatingRollups = new ArrayList<RiskRatingRollup>();
		QueryResults querySummaryResults = null;
		List<RiskRatingSummary> summary = new ArrayList<RiskRatingSummary>();
		

		try {

			riskSession = createNewVerificationSession(true);

			if (riskSession == null) {
				throw new Exception("Invalid kie session");
			}

			rr.setRiskRollups(null);
			
			RiskRatingSummary r = rr.getSummary().get(0);
			RiskRating riskrating1 = new RiskRating();
			
			VerificationRiskRating pr = new VerificationRiskRating();
			
   		    List<RiskRatingRollup> RiskRatingRollup = r.getRiskRatingGroups();
			System.out.println("RiskRatingRollup size>>:"+ RiskRatingRollup.size());
			for (int i=0; i< RiskRatingRollup.size();i++){
				List<RiskRating> riskRating = RiskRatingRollup.get(i).getRiskRatings();
				for (int j=0; j<riskRating.size();j++){
					riskrating1 = riskRating.get(j);
					pr = new VerificationRiskRating(riskRating.get(j));
					riskSession.insert(riskrating1);
					riskSession.insert(pr);
				}
//				riskrating1 = riskRating.get(0);
//
//				pr = new VerificationRiskRating(riskRating.get(0));
//
//				riskSession.insert(riskrating1);
//				riskSession.insert(pr);				
			}
			
			log.info("Fire AGENDA_VERIFICATION_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_RATING).setFocus();
			riskSession.fireAllRules();

			log.info("Fire AGENDA_VERIFICATION_ROLLUP_RATING");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_ROLLUP_RATING).setFocus();
			riskSession.fireAllRules();		
			
			log.info("Fire AGENDA_VERIFICATION_ROLLUP_GROUPS");
			riskSession.getAgenda().getAgendaGroup(AGENDA_VERIFICATION_ROLLUP_GROUPS).setFocus();
			riskSession.fireAllRules();
			
			querySummaryResults = riskSession.getQueryResults("getVerificationRollupSummary");
			
			for (QueryResultsRow row : querySummaryResults) {
				summary.add((RiskRatingSummary) row.get("$summary"));
			}
			
			response.setSummary(summary);
						
			return response;

		} catch (Exception e) {
			log.error("Exception in calculateVerificationSummaryrr", e);
			throw e;
		} finally {
			if (riskSession != null) {
				riskSession.dispose();
			}
		}

	}

	/**
	 * 
	 * @param addListeners
	 * @return
	 */
	private KieSession createNewVerificationSession(boolean addListeners) {
		KieSession riskSession = kContainer.newKieSession();

		if (addListeners) {
			riskSession.addEventListener(agendaListener);
			riskSession.addEventListener(ruleListener);
		}
		return riskSession;
	}

	private void insertAllFacts(RiskInputBase input, KieSession riskSession) {

		log.info("Inside insertAllFacts");

		Set<Method> methodList = new HashSet<Method>();

		if (input.getRiskSummaryType() == RiskSummaryType.PART_I) {

			PrimaryInsured insured = (PrimaryInsured) input;

			if (insured != null) {

				log.info("insert RiskType: " + insured.getRiskType());

				methodList = getterMethodsForPrimaryInsuredRiskBase;
			}
		} else if (input.getRiskSummaryType() == RiskSummaryType.PART_II) {

			PersonalHistory medicalHistory = (PersonalHistory) input;

			if (medicalHistory != null) {

				log.info("have medical conditions");

				methodList = getterMethodsForPersonalHistoryOfRiskBase;
				//Add parent PersonalHistory as well.
				riskSession.insert(medicalHistory);
			}
		} else if (input.getRiskSummaryType() == RiskSummaryType.MIB) {

			MIBInput mibInput = (MIBInput) input;

			if (mibInput != null) {

				log.info("have MIB response codes");

				if (mibInput.getResponses() != null){
							
					log.info("have MIB response data");
					for (FormResponse responseData : mibInput.getResponses()) {
						riskSession.insert(responseData);
					}
				}
				
				if (mibInput.getIaiGroup() != null){
					
					log.info("have MIB IAI responses");
					
					riskSession.insert(mibInput.getIaiGroup());
					
					// Insert facts
//					for (IAIResponse r : mibInput.getIaiResponses()) {
//						riskSession.insert(r);
//					}
	
				}

			}

			return; // nothing further to do for MIB
		}

		for (Method method : methodList) {

			RiskBase riskBase = null;
			Object obj = null;

			// log.info("get fact: " + method.getName());

			try {
				obj = method.invoke(input);
				// log.info("get risk: " + obj);

			} catch (IllegalAccessException e) {
				log.error("Skip adding non-riskBase object for type[" + input.getRiskSummaryType() + "]" + obj);
				continue;

			} catch (IllegalArgumentException e) {
				log.error("Skip adding non-riskBase object for type[" + input.getRiskSummaryType() + "]" + obj);
				continue;
			} catch (InvocationTargetException e) {
				log.error("Skip adding non-riskBase object for type[" + input.getRiskSummaryType() + "]" + obj);
				continue;
			}

			riskBase = (RiskBase) obj;

			// log.info("riskBase :" + riskBase);

			if (riskBase != null) {
				riskSession.insert(riskBase);
			}

		}

	}

	private void insertFacts(VerificationInput input, KieSession riskSession) throws Exception {

		log.info("Inside insertFacts, input: " + input);

		if (input.getRxReturns() == null) {
			throw new Exception("Invalid rx rule input for cross Check Verification");
		}
		
		// Insert facts for VerificationInput
		if (input != null) {
			riskSession.insert(input);
		}
	
		// Insert facts for RxRules
		riskSession.insert(input.getRxReturnOtherData());
		for (RxReturn rxRule : input.getRxReturns()) {
			riskSession.insert(rxRule);
		}

		// Insert facts for Part I
		if (input.getInsured() != null) {
			insertAllFacts(input.getInsured(), riskSession);
		}

		// Insert facts for Part II
		if (input.getPersonalHistory() != null) {
			insertAllFacts(input.getPersonalHistory(), riskSession);
		}
		
		// Insert facts for Labs
		if (input.getApplicant() != null) {
			
			riskSession.insert(input.getApplicant());
			riskSession.insert(input.getApplicant().getLabSlip());
			
		}

		// Insert facts for MIB
		if (input.getMibInput() != null) {
			insertAllFacts(input.getMibInput(), riskSession);
		}

	}

}

package com.massmutual.brms;

import org.apache.log4j.Logger;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;

public class RuleFactory {
	private final static transient Logger log = Logger.getLogger(RuleFactory.class);
	private static KieContainer kContainer;
	static {
		try {

			// if the local maven repository is not in the default
			// ${user.home}/.m2
			// need to provide the custom settings.xml
			// pass property value
			// -Dkie.maven.settings.custom={custom.settings.location.full.path}

			KieServices kServices = KieServices.Factory.get();

			//ReleaseId releaseId = kServices.newReleaseId("com.massmutual","mm-risk-rules", "1.0");

			// kContainer = kServices.newKieContainer(releaseId,
			// Factory.class.getClassLoader());

			//kContainer = kServices.newKieContainer(releaseId);
			kContainer = kServices.newKieClasspathContainer();
			// KieScanner kScanner = kServices.newKieScanner(kContainer);
			// Start the KieScanner polling the maven repository every 10
			// seconds

			// kScanner.start(10000L);

		} catch (Exception e) {

			log.error("Exception trying to initialize the Kie Container", e);
		}
	}

	public static KieContainer get() {
		return kContainer;
	}
}

package com.massmutual.brms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import org.apache.log4j.Logger;
import org.drools.core.ClassObjectFilter;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.massmutual.domain.PartOneRiskRating;
import com.massmutual.domain.PartTwoRiskRating;
import com.massmutual.domain.RiskRating;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskRatingSummary;
import com.massmutual.domain.blender.BlenderRiskRating;
import com.massmutual.domain.blender.BlenderRiskType;
import com.massmutual.domain.blender.FinalRollupInput;
import com.massmutual.domain.blender.FinalRollupResult;
import com.massmutual.domain.labs.Applicant;
import com.massmutual.domain.labs.LabResult;
import com.massmutual.domain.labs.LabResultFlag;
import com.massmutual.domain.labs.LabRulesSummary;

@Singleton
@Named("blenderRuleProcessor")
public class BlenderRuleProcessor {
	private KieContainer kContainer = RuleFactory.get();
	private static final Logger log = Logger.getLogger(BlenderRuleProcessor.class);
	
	public static final String AGENDA_BLENDER_VALIDATION = "blender-validation";
	public static final String AGENDA_BLENDER_RISK_RATING_CALCULATION = "blender-risk-rating-calculation";
	public static final String AGENDA_BLENDER_ROLLUP_RISK_RATINGS = "blender-rollup-risk-ratings";
	public static final String AGENDA_BLENDER_POINTS_CALCULATION = "blender-point-calculation";
	public static final String AGENDA_BLENDER_POINTS_ROLLUP = "blender-point-rollup";
	public static final String AGENDA_BLENDER_SUMMARY_ROLLUP = "blender-summary-rollup";
	public static final String AGENDA_BLENDER_FINAL_ROLLUP = "blender-final-rollup";
	public static final String AGENDA_BLENDER_CLEANUP = "blender-cleanup";
	
	public static final String AGENDA_LAB_SETUP = "lab-setup";
	public static final String AGENDA_LAB_VALIDATE = "lab-validate";
	public static final String AGENDA_LAB_CALCULATE = "lab-calculate";
	public static final String AGENDA_LAB_ACCUMULATE = "lab-accumulate";
	public static final String AGENDA_LAB_SUMMARIZE = "lab-summarize";
	
	private AgendaListener agendaListener = new AgendaListener();
	private RuleListener ruleListener = new RuleListener();
	
	private KieSession createNewBlenderSession(boolean addListeners) {
		KieSession riskSession = kContainer.newKieSession();

		if (addListeners) {
			riskSession.addEventListener(agendaListener);
			riskSession.addEventListener(ruleListener);
		}

		return riskSession;
	}
	
	private FinalRollupResult insertBenderInput(KieSession session, FinalRollupInput input){
	
		log.info("inside insertBenderInput");
		
		FinalRollupResult result = new FinalRollupResult();
		List<String> missingConditions = new ArrayList<String>();
		
		result.setMissingConditions(missingConditions);
		result.setRollupComplete(false);
		
		// Insert facts
		
		
		if (input.getApplicant() != null){
			
			session.insert(input.getApplicant());
			session.insert(input.getApplicant().getConditions());
		}

		if (input.getM3sData() != null){
			
			session.insert(input.getM3sData());
		}

		// Insert Lab Result flags
		for (LabResultFlag labflag : input.getLabFlags()) {
			
			session.insert(labflag);
		}
		
		// insert Lab ratings
		for (BlenderRiskRating labRisk : input.getLabRatings()) {
			session.insert(labRisk);
		}
			
		// Insert Summaries, rollups and ris ratings
		for (RiskRatingSummary summary : input.getRatingSummaries()) {
			session.insert(summary);
			
			for (RiskRatingRollup rollup : summary.getRiskRatingGroups()) {
				
				log.info("insert rollup: " + rollup);
				session.insert(rollup);
							
				if (rollup.getRiskRatings() != null) {
					
					for (RiskRating risk : rollup.getRiskRatings()) {
						
						if (risk.getRiskSummaryType() == null) {
							throw new IllegalArgumentException("Expected a riskSummaryType for risk rating: " + risk);
						}
						
						switch(risk.getRiskSummaryType()){
						case PART_I:
							session.insert(new PartOneRiskRating(risk));
							break;
						case PART_II:
							session.insert(new PartTwoRiskRating(risk));
							break;
						default:	// MVR, MIB, RX rules
							session.insert(risk);
						}
					}
				}
				
			}
		}
		
		session.setGlobal("finalRollupResult", result);
		session.setGlobal("missingConditions", missingConditions);
		
		return result;
	}
	
	
	public FinalRollupResult performFinalRollup(FinalRollupInput input) {
		log.info("inside performFinalRollup");
		
		FinalRollupResult result;
		KieSession session = null;
		boolean caseIsLowTotalRiskAmt = false;
		if(input != null && input.getApplicant() != null)
		{
			caseIsLowTotalRiskAmt = input.getApplicant().getCaseIsLowTotalRiskAmt();
			//log.info(input.getApplicant().getId() + " getCaseIsLowTotalRiskAmt() returned " + caseIsLowTotalRiskAmt);
		}
		
		try {
			if (input == null) {
				throw new IllegalArgumentException("Expected FinalRollupInput");
			}
			
			session = createNewBlenderSession(true);
			
			// Insert facts
			result = insertBenderInput(session, input);
			
			log.info("Fire AGENDA_BLENDER_VALIDATION");
			session.getAgenda().getAgendaGroup(AGENDA_BLENDER_VALIDATION).setFocus();
			session.fireAllRules();
			
			log.info("Fire AGENDA_BLENDER_RISK_RATING_CALCULATION");
			session.getAgenda().getAgendaGroup(AGENDA_BLENDER_RISK_RATING_CALCULATION).setFocus();
			session.fireAllRules();
 			
			log.info("Fire AGENDA_BLENDER_ROLLUP_RISK_RATINGS");
			session.getAgenda().getAgendaGroup(AGENDA_BLENDER_ROLLUP_RISK_RATINGS).setFocus();
			session.fireAllRules();

			log.info("Fire AGENDA_BLENDER_SUMMARY_ROLLUP");
			session.getAgenda().getAgendaGroup(AGENDA_BLENDER_SUMMARY_ROLLUP).setFocus();
			session.fireAllRules();
			
			log.info("Fire AGENDA_BLENDER_FINAL_ROLLUP");
			session.getAgenda().getAgendaGroup(AGENDA_BLENDER_FINAL_ROLLUP).setFocus();
			session.fireAllRules();
			
			log.info("Fire AGENDA_BLENDER_CLEANUP");
			session.getAgenda().getAgendaGroup(AGENDA_BLENDER_CLEANUP).setFocus();
			session.fireAllRules();
			
			Collection<?> generated = session.getObjects(new ClassObjectFilter(BlenderRiskRating.class));
			
			for (Object obj : generated) {
				BlenderRiskRating rating = (BlenderRiskRating)obj;
				if (rating.getBlenderRiskType() == BlenderRiskType.FINAL) {
					log.info("Found blender riskType: " + rating.getRiskType());
					result.setFinalRating(rating);
					result.setFinalResult(calculateFinalResult(rating));
				} else if(rating.getBlenderRiskType() == BlenderRiskType.PREFERRED_POINTS) {
					log.info("Found blender riskType: " + rating.getRiskType());
					result.setPreferredPointRating(rating);
				} else {
					result.getOtherRatings().add(rating);
				}
			}
			
			if (result.getMissingConditions().isEmpty()) {
				result.setRollupComplete(true);
			}
			
			log.info("Done with blender");
			
		} catch (Exception e) {
			log.error("Exception in performFinalRollup", e);
			throw e;
		} finally {
			if (session != null) {
				session.dispose();
			}
		}
		
		return result;
	}
	
	public LabRulesSummary performLabSummary(Applicant applicant) {
		
		LabRulesSummary summary = new LabRulesSummary();
		List<String> missingLabs = new ArrayList<String>();
		
		summary.setMissingLabs(missingLabs);
		summary.setSummaryComplete(false);
		
		KieSession session = null;
		
		try {
			if (applicant == null) {
				throw new IllegalArgumentException("Expected Applicant");
			}
			session = createNewBlenderSession(false);
			
			// Insert facts
			session.insert(summary);
			session.insert(applicant);
			session.insert(applicant.getConditions());
			session.insert(applicant.getLabSlip());
			for (LabResult labResult : applicant.getLabResults()) {
				session.insert(labResult);
			}
			session.insert(applicant.getPolicyInformation());
			
			session.setGlobal("missingLabs", missingLabs);
			
			log.info("Fire AGENDA_LAB_SETUP");
			session.getAgenda().getAgendaGroup(AGENDA_LAB_SETUP).setFocus();
			session.fireAllRules();
			
			log.info("Fire AGENDA_LAB_VALIDATE");
			session.getAgenda().getAgendaGroup(AGENDA_LAB_VALIDATE).setFocus();
			session.fireAllRules();
			
			log.info("Fire AGENDA_LAB_CALCULATE");
			session.getAgenda().getAgendaGroup(AGENDA_LAB_CALCULATE).setFocus();
			session.fireAllRules();
			
			log.info("Fire AGENDA_LAB_ACCUMULATE");
			session.getAgenda().getAgendaGroup(AGENDA_LAB_ACCUMULATE).setFocus();
			session.fireAllRules();
			
			log.info("Fire AGENDA_LAB_SUMMARIZE");
			session.getAgenda().getAgendaGroup(AGENDA_LAB_SUMMARIZE).setFocus();
			session.fireAllRules();
			
			Collection<?> generated = session.getObjects(new ClassObjectFilter(LabResultFlag.class));
			for (Object obj : generated) {
				summary.getFlags().add((LabResultFlag)obj);
			}
			summary.setTotalFlagCount(summary.getFlags().size());
			
			generated = session.getObjects(new ClassObjectFilter(BlenderRiskRating.class));
			
			for (Object obj : generated) {
				summary.getRiskRatings().add((BlenderRiskRating)obj);
			}
			
			if (summary.getMissingLabs().isEmpty()) {
				summary.setSummaryComplete(true);
			}
			
		} catch (Exception e) {
			log.error("Exception in calculateRiskRating", e);
			throw e;
		} finally {
			if (session != null) {
				session.dispose();
			}
		}
		
		return summary;
	}


	/**
	 * Determines the highest Green (1) rating in the rollup for an offer
	 * If no Green, determine highest Yellow (2) rating in the rollup for a refer
	 * If no Yellow, Refer for possible None
	 * 
	 * @return
	 */
	private String calculateFinalResult(BlenderRiskRating finalRatings) {
		try {
			String highestGreen = "";
			if(finalRatings.getTOB()  == 1) {highestGreen = "TOB"; }
			if(finalRatings.getSPT()  == 1) {highestGreen = "SPT"; }
			if(finalRatings.getNONT() == 1) {highestGreen = "NT";  }
			if(finalRatings.getSPNT() == 1) {highestGreen = "SPNT";}
			if(finalRatings.getUPNT() == 1) {highestGreen = "UPNT";}

			String highestYellowOrRed = "";
			if(finalRatings.getTOB()  == 2 || finalRatings.getTOB()  == 3) {highestYellowOrRed = "TOB"; }
			if(finalRatings.getSPT()  == 2 || finalRatings.getSPT()  == 3) {highestYellowOrRed = "SPT"; }
			if(finalRatings.getNONT() == 2 || finalRatings.getNONT() == 3) {highestYellowOrRed = "NT";  }
			if(finalRatings.getSPNT() == 2 || finalRatings.getSPNT() == 3) {highestYellowOrRed = "SPNT";}
			if(finalRatings.getUPNT() == 2 || finalRatings.getUPNT() == 3) {highestYellowOrRed = "UPNT";}

			if(highestGreen.equals("") && highestYellowOrRed.equals("")) {
				// Default when there's no green, yellow or red
				return "Refer for possible Offer";
			} 
			else if(highestGreen.equals("")) {
			    //  There's no green but there is a yellow or red so refer the highest yellow or red
				return "Refer for possible " + highestYellowOrRed;
			}
			else {
			    //  There's green and yellow or red
			    if(referGreaterThanOffer(highestYellowOrRed, highestGreen)) {
			         // The highest yellow or red is greater than the highest green so refer the highest yellow
    				return "Refer for possible " + highestYellowOrRed;
			    }
			    else {
			         // The highest green is greater than or equal to the highest yellow or red so offer the highest green
    				return "Offer " + highestGreen;
			    }
			}
		} catch (Exception e) {
			log.error("Exception in calculateFinalResult(): ", e);
			return "";
		}
	}


	/**
	 * Compare two ratings
	 * 
	 * @param refer
	 * @param offer
	 * @return
	 */
	private static boolean referGreaterThanOffer(String refer, String offer) {
	    List<String> ratings = Arrays.asList("TOB", "SPT", "NT", "SPNT", "UPNT");
	    if( ratings.indexOf(refer) > ratings.indexOf(offer)) {
	    	return true;
	    }
	    
	    return false;
	}
}
package com.massmutual.brms.rules.parttwo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.JsonMappingException;
import org.junit.Test;
import org.kie.api.runtime.KieSession;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.PartTwoRiskType;
import com.massmutual.domain.parttwo.MedicalCondition;
import com.massmutual.domain.parttwo.NervousSystemDisorder;

@SuppressWarnings({ "unchecked", "rawtypes", "serial" })
public class NervousSystemDisorderTests extends BaseTest {
	private static final Logger log = Logger.getLogger(NervousSystemDisorderTests.class);
	
	

	@Test
	public void testStart() {
		this.drlNames = new String []{"Nervous.drl"};
		
		KieSession kieSession = getKieSessionClassPath();
		
	
		NervousSystemDisorder disorder = new NervousSystemDisorder(){{
			
			setConditions(new ArrayList<MedicalCondition>());
		}};
		//String jsonStr = readTextFromFile(this.getClass().getResourceAsStream("/nervous.json"));
		Map<String, String> ruleToRiskMap = new LinkedHashMap<String, String>();
		try {
			Map jsonMap = (Map) getMapFromJson(this.getClass().getResourceAsStream("/nervous.json"));
			
			List<Map> listOfStuffs = (List<Map>)jsonMap.get("list");
			log.info("listOfStuffs ; "+listOfStuffs);
			
			for (Map map : listOfStuffs) {
				MedicalCondition condition = new MedicalCondition((String) map.get("name"), "Y");
				disorder.getConditions().add(condition);
				ruleToRiskMap.put((String)map.get("rule"), (String)map.get("risks"));
			}
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
		kieSession.insert(disorder);
		kieSession.getAgenda()
				.getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING)
				.setFocus();
		kieSession.fireAllRules();
		// upnt, spnt nont spt tob wp diio

		assertMyPartTwoRiskType(ruleToRiskMap,
				PartTwoRiskType.NERVOUS_SYSTEM_DISORDER ,
				getPartTwoRiskRatings(kieSession));

		// System.out.println("list "+list.size());
	}

	@Test
	public void testToken() {
		makeMedicalCondition("hadBDAneurysm,Y,hadDBTumor,N");
	}

}

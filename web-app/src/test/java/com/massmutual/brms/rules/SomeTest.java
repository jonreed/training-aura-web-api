package com.massmutual.brms.rules;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.massmutual.brms.AgendaListener;
import com.massmutual.brms.RuleListener;
import com.massmutual.domain.Insured;
import com.massmutual.domain.PartOneRiskRating;
import com.massmutual.domain.RiskRating;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.partone.FinancialHistory;
import com.massmutual.domain.partone.PolicyAppliedFor;

public class SomeTest {
	@Test
	public void testSome(){
		final RiskRatingRollup roll =new RiskRatingRollup();
		roll.setDescription("hello");
		
		//final RiskRatingBase rat = new PartOneRiskRating();
		List<RiskRating> list = new ArrayList<RiskRating>(Arrays.asList(new PartOneRiskRating()));
		
		roll.setRiskRatings(list);
		for(int i =0;i<10000;i++){  //original value of 10000000 requires larger heap
			RiskRating bse = new PartOneRiskRating();
			bse.setDescription("i: "+i);
			roll.getRiskRatings().add(bse);
		}
		
		
		RiskResponse res = new RiskResponse(){{
			setRiskRollups(Arrays.asList(roll,roll,roll));
			
		}};
		
		String str = String.valueOf(res);
		//System.out.println("rool : "+String.valueOf(res.getRiskRollups()) );
	}
	

	KieServices kieServices = KieServices.Factory.get();
	@Test
	public void testStart(){
		try {
			testSomeDrl();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testSomeDrl(){
		KieRepository kr = kieServices.getRepository();
		KieFileSystem fileSystem = kieServices.newKieFileSystem();
		Resource resource = kieServices.getResources().newFileSystemResource(new File("src/test/resources/truthy.drl"));
		fileSystem.write(resource);
		
		KieBuilder kb = kieServices.newKieBuilder(fileSystem);
		
		kb.buildAll();
		if (kb.getResults().hasMessages(org.kie.api.builder.Message.Level.ERROR)) {
			throw new RuntimeException("Build errors:\n" + kb.getResults().toString());
		}
		
		KieContainer kContainer = kieServices.newKieContainer(kr.getDefaultReleaseId());
		KieSession kieSession = kContainer.newKieSession();
		
		//PrimaryInsured insured = new PrimaryInsured();
		//insured.setInsuredAge(37);

		FinancialHistory history = new FinancialHistory();
		history.setEarnedIncomeCurrentYear(10000d);
		history.setUnearnedIncomeCurrentYear(20000d);
		
		PolicyAppliedFor appliedFor = new PolicyAppliedFor();
		appliedFor.setProposedPlanPremium(1501d);
		
		Insured insured = new Insured();
		insured.setInsuredAge(37);
		
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		
		kieSession.insert(insured);
		kieSession.insert(history);
		kieSession.insert(appliedFor);
		
		kieSession.getAgenda().getAgendaGroup("calculate-risk-rating").setFocus();
		kieSession.fireAllRules();
		
	}
	
}

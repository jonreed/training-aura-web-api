package com.massmutual.brms.rules.mib;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.massmutual.domain.mib.FormResponse;

public class MibInputTest {
	private final static transient Logger log = Logger.getLogger(MibInputTest.class);
	
	@Test
	public void testFormResponseConversion() {
		FormResponse response = new FormResponse("010GEN");
		
		log.info("Converted response: " + response);
		
		assertEquals("Expecting 010 diagnosis code", "010", response.getDiagnosis());
		assertEquals("Expecting 'G' for modifier", "G", response.getModifier());
		assertEquals("Expecting 'E' for source", "E", response.getSource());
		assertEquals("Expecting 'N' for timeline", "N", response.getTimeframe());
	}
}

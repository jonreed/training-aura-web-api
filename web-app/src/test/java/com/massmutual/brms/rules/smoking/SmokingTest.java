package com.massmutual.brms.rules.smoking;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.domain.PartOneRiskType;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.partone.AviationRisk;
import com.massmutual.domain.partone.AvocationRisk;
import com.massmutual.domain.partone.ForeignTravelRisk;
import com.massmutual.domain.partone.MilitaryRisk;
import com.massmutual.domain.partone.PrimaryInsured;
import com.massmutual.domain.partone.SmokingRisk;

public class SmokingTest {

	private static final Logger log = Logger.getLogger(SmokingTest.class);
	private RuleProcessor ruleProcessor = new RuleProcessor();
	
	@Test
	public void testSmokingRollup() throws Exception {
		PrimaryInsured insured = new PrimaryInsured();
		SmokingRisk risk = new SmokingRisk();
		risk.setUsedCigarsPast12Months("Y");
		risk.setUsedPast12Months("Y");
		risk.setUsedPast24Months("Y");
		risk.setAnnualCigarCount(23);
		risk.setUsedTobaccoCessationAides("Y");
		risk.setSmokingDuration(13);
		insured.setSmoking(risk);
		insured.setRiskType(PartOneRiskType.SMOKER);
		insured.setId("1");
		
		insured.setTravel(new ForeignTravelRisk());
		insured.setMilitary(new MilitaryRisk());
		insured.setAviation(new AviationRisk());
		insured.setAvocation(new AvocationRisk());
		
		insured.setRiskSummaryType(RiskSummaryType.PART_I);
		
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();	
		inputs.add(insured);
		
		RiskResponse response = ruleProcessor.calculateRiskRatingRollup(inputs);
		log.info("Response: " + response);
	}

}

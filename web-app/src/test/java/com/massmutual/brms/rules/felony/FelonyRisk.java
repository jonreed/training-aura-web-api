package com.massmutual.brms.rules.felony;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.partone.Felony;
import com.massmutual.domain.partone.PrimaryInsured;

public class FelonyRisk extends BaseTest{

	
	
	
	@Test
	public void testFelonyYes(){
		//KieSession kieSession = getKieContainer().newKieSession();
		Felony risk = new Felony();
		risk.setConvictedOfFelony("Y");
		
		
		/*kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		kieSession.insert(risk);
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		
		
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		List<RiskRating> riskRatings = new ArrayList<>();
		
		
		for (QueryResultsRow row : queryResults) {			
			RiskRating riskRslt = (RiskRating)row.get("$rating");
			
			UPNT(riskRslt, 3);
			SPNT(riskRslt, 3);
			NONT(riskRslt, 3);
			SPT(riskRslt, 3);
			TOB(riskRslt, 3);
			WP(riskRslt, 3);
			DIIO(riskRslt, 3);
			
			riskRatings.add(riskRslt );	
		}*/
		
		
		//Assert.assertTrue(!riskRatings.isEmpty());
		
		
		//kieSession.dispose();
		
		RuleProcessor rp = new RuleProcessor();
		rp.init();
		PrimaryInsured insured = new PrimaryInsured();
		insured.setId("123");
		insured.setFelony(risk);
		insured.setRiskSummaryType(RiskSummaryType.PART_I);
		
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();	
		inputs.add(insured);
		
		try {
			RiskResponse response = rp.calculateRiskRatings(inputs);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

package com.massmutual.brms.rules.blender;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.massmutual.brms.BlenderRuleProcessor;
import com.massmutual.domain.PartOneRiskType;
import com.massmutual.domain.RiskRating;
import com.massmutual.domain.RiskRatingBase;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskRatingSummary;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.blender.BlenderRiskRating;
import com.massmutual.domain.blender.BlenderRiskType;
import com.massmutual.domain.blender.FinalRollupInput;
import com.massmutual.domain.blender.FinalRollupResult;
import com.massmutual.domain.labs.Applicant;
import com.massmutual.domain.labs.ApplicantConditionType;
import com.massmutual.domain.labs.ApplicantConditions;

public class FinalRollupTests {
	private final static transient Logger log = Logger.getLogger(FinalRollupTests.class);
	
	private BlenderRuleProcessor ruleProcessor = new BlenderRuleProcessor();
	
	//@Test
	public void testValidation() {
		FinalRollupInput input = new FinalRollupInput();
		Applicant applicant = new Applicant(1L, "M", 41);
		
		input.setApplicant(applicant);
		
		FinalRollupResult result = ruleProcessor.performFinalRollup(input);
		
		log.info("result: " + result);
		
		assertTrue("Expect PART_I to be missing", result.getMissingConditions().contains(RiskSummaryType.PART_I.toString()));
		assertTrue("Expect PART_II to be missing", result.getMissingConditions().contains(RiskSummaryType.PART_II.toString()));
		assertTrue("Expect MVR to be missing", result.getMissingConditions().contains(RiskSummaryType.MVR.toString()));
		assertTrue("Expect RX_Rules to be missing", result.getMissingConditions().contains(RiskSummaryType.RX_RULES.toString()));
		
		assertTrue("Expect Lab Flag blender rating to be missing", result.getMissingConditions().contains(BlenderRiskType.LAB_FLAGS.toString()));
		assertTrue("Expect Lab Score blender points to be missing", result.getMissingConditions().contains(BlenderRiskType.LAB_POINTS.toString()));
		
		assertTrue("Expect Smoker rollup to be missing", result.getMissingConditions().contains(PartOneRiskType.SMOKER.toString()));
		assertTrue("Expect Smoker detail risks to be missing", result.getMissingConditions().contains("Nicotine usage details"));
		assertTrue("Expect Avocation rollup to be missing", result.getMissingConditions().contains(PartOneRiskType.AVOCATION.toString()));
		
		for(ApplicantConditionType type : ApplicantConditionType.values()) {
			assertTrue("Expect " + type.toString() + " to be missing", result.getMissingConditions().contains(type.toString()));
		}
		
		ApplicantConditions conditions = new ApplicantConditions(null, null, null, null, null);
		applicant.setConditions(conditions);
		result = ruleProcessor.performFinalRollup(input);
		
		for(ApplicantConditionType type : ApplicantConditionType.values()) {
			assertTrue("Expect " + type.toString() + " to be missing", result.getMissingConditions().contains(type.toString()));
		}
		
		conditions = new ApplicantConditions(false, false, false, false, false);
		applicant.setConditions(conditions);
		input = createTestInput(applicant);
		result = ruleProcessor.performFinalRollup(input);
		
		log.info("results: " + result);
		assertTrue("Expecting no missing conditions", result.getMissingConditions().isEmpty());
	}
	
	@Test
	public void testSmokingPreferredPoint() {
		Applicant applicant = new Applicant(1L, "M", 41);
		applicant.setConditions(new ApplicantConditions(false, false, false, false, false));
		FinalRollupInput input = createTestInput(applicant);
		
		FinalRollupResult result = ruleProcessor.performFinalRollup(input);
		
		checkOtherBlenderValue(result, BlenderRiskType.NICOTINE_USE, false, 1);
		
		for (RiskRatingSummary summary : input.getRatingSummaries()) {
			if (RiskSummaryType.PART_I.equals(summary.getRiskSummaryType())) {
				for (RiskRatingBase group : summary.getRiskRatingGroups()) {
					if (PartOneRiskType.SMOKER.toString().equals(((RiskRatingRollup)group).getRiskType())) {
						group.setAllRiskClasses(3);
					}
				}
			}
		}
		
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.NICOTINE_USE, true, 0);
	}
	
	//@Test
	public void testAvocationPreferredPoint() {
		Applicant applicant = new Applicant(1L, "M", 41);
		applicant.setConditions(new ApplicantConditions(false, false, false, false, false));
		FinalRollupInput input = createTestInput(applicant);
		
		FinalRollupResult result = ruleProcessor.performFinalRollup(input);
		
		checkOtherBlenderValue(result, BlenderRiskType.AVOCATION, false, 1);
		
		for (RiskRatingSummary summary : input.getRatingSummaries()) {
			if (RiskSummaryType.PART_I.equals(summary.getRiskSummaryType())) {
				for (RiskRatingBase group : summary.getRiskRatingGroups()) {
					if (PartOneRiskType.AVOCATION.toString().equals(((RiskRatingRollup)group).getRiskType())) {
						group.setAllRiskClasses(3);
					}
				}
			}
		}
		
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.AVOCATION, true, 0);
	}
	
	@Test
	public void testConditionPoints() {
		Applicant applicant = new Applicant(1L, "M", 41);
		applicant.setConditions(new ApplicantConditions(false, false, false, false, false));
		FinalRollupInput input = createTestInput(applicant);
		
		// Cardiovascular history
		applicant.getConditions().setCardiovascularParentsHistory(false);
		FinalRollupResult result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.CARDIOVASCULAR, false, 1);
		
		applicant.getConditions().setCardiovascularParentsHistory(true);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.CARDIOVASCULAR, true, 0);
		
		// Anti-Hypertensive use
		applicant.getConditions().setAntihypertensiveUse(false);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.RX_USE, false, 1);
		
		applicant.getConditions().setAntihypertensiveUse(true);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.RX_USE, true, 0);
		
		// Normal EKG, stress test in last 2 years
		applicant.getConditions().setNormalEKGLast2Years(true);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.EKG, false, 1);
		
		applicant.getConditions().setNormalEKGLast2Years(false);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.EKG, true, 0);
		
		// EBCT for credit last 2 years
		applicant.getConditions().setEbctCreditLast2Years(true);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.EBCT, false, 2);
		
		applicant.getConditions().setEbctCreditLast2Years(false);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.EBCT, true, 0);
		
		// BNPT < 125  
		applicant.getConditions().setBnptBelow125(true);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.BNPT, true, 0);
		
		applicant.getConditions().setBnptBelow125(false);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.BNPT, true, 0);
		
		// Over 59
		applicant.setAge(60);
		applicant.getConditions().setBnptBelow125(true);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.BNPT, false, 1);
		
		applicant.getConditions().setBnptBelow125(false);
		result = ruleProcessor.performFinalRollup(input);
		checkOtherBlenderValue(result, BlenderRiskType.BNPT, true, 0);
		
	}
	
	private BlenderRiskRating checkOtherBlenderValue(FinalRollupResult result, BlenderRiskType type, boolean dne, int preferredPoints) {
		BlenderRiskRating blenderRating = null;
		for (BlenderRiskRating rating : result.getOtherRatings()) {
			if (type.equals(rating.getRiskType())) {
				blenderRating = rating;
			}
		}
		
		if (dne) {
			assertTrue("Expecting no " + type.toString() + " blender rating", blenderRating == null);
		} else {
			assertTrue("Expecting " + type.toString() + " blender rating", blenderRating != null);
			assertTrue("Expecting " + preferredPoints + " preferred points from " + type.toString() + " rating", blenderRating.getPreferredPoints() == preferredPoints);
		}
		
		return blenderRating;
	}
	
	private FinalRollupInput createTestInput(Applicant applicant) {
		FinalRollupInput input = new FinalRollupInput();
		
		input.setApplicant(applicant);
		
		RiskRatingSummary summary = new RiskRatingSummary();
		summary.setAllRiskClasses(1);
		summary.setRiskSummaryType(RiskSummaryType.PART_I);
		
		RiskRatingRollup smokingRollup = new RiskRatingRollup();
		smokingRollup.setRiskType(PartOneRiskType.SMOKER.name());
		smokingRollup.setAllRiskClasses(1);
		
		RiskRating rating = new RiskRating();
		rating.setRiskType(PartOneRiskType.SMOKER.name());
		rating.setAllRiskClasses(1);
		rating.setRuleName("rule-smoker-no-cigar-use");
		smokingRollup.setRiskRatings(new ArrayList<RiskRating>());
		smokingRollup.getRiskRatings().add(rating);
		
		summary.getRiskRatingGroups().add(smokingRollup);
		
		RiskRatingRollup avocationRollup = new RiskRatingRollup();
		avocationRollup.setAllRiskClasses(1);
		avocationRollup.setRiskType(PartOneRiskType.AVOCATION.name());
		summary.getRiskRatingGroups().add(avocationRollup);
		
		input.getRatingSummaries().add(summary);
		
		summary = new RiskRatingSummary();
		summary.setAllRiskClasses(1);
		summary.setRiskSummaryType(RiskSummaryType.PART_II);
		input.getRatingSummaries().add(summary);
		
		summary = new RiskRatingSummary();
		summary.setAllRiskClasses(1);
		summary.setRiskSummaryType(RiskSummaryType.MVR);
		input.getRatingSummaries().add(summary);
		
		summary = new RiskRatingSummary();
		summary.setAllRiskClasses(1);
		summary.setRiskSummaryType(RiskSummaryType.RX_RULES);
		input.getRatingSummaries().add(summary);
		
		BlenderRiskRating blenderRating = new BlenderRiskRating();
		blenderRating.setAllRiskClasses(1);
		blenderRating.setBlenderRiskType(BlenderRiskType.LAB_FLAGS);
		input.getLabRatings().add(blenderRating);
		
		blenderRating = new BlenderRiskRating();
		blenderRating.setAllRiskClasses(1);
		blenderRating.setBlenderRiskType(BlenderRiskType.LAB_POINTS);
		input.getLabRatings().add(blenderRating);
		
		return input;
	}

	@Override
	public String toString() {
		return "FinalRollupTests [ruleProcessor=" + ruleProcessor + "]";
	}
}

package com.massmutual.brms.rules.dui;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.partone.Dui;
import com.massmutual.domain.partone.PrimaryInsured;

public class DuiRisk extends BaseTest{

	
	
	
	@Test
	public void testDui(){
		//KieSession kieSession = getKieContainer().newKieSession();
		Dui risk = new Dui();
		risk.setSuspendedLicenseDueToNonPmtChildSupport("Y");
		
	
		
		RuleProcessor rp = new RuleProcessor();
		rp.init();
		PrimaryInsured insured = new PrimaryInsured();
		insured.setId("123");
		insured.setDui(risk);
		insured.setRiskSummaryType(RiskSummaryType.PART_I);
		
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();	
		inputs.add(insured);
		try {
			RiskResponse response = rp.calculateRiskRatings(inputs);
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

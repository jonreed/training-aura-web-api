package com.massmutual.brms.rules.alir;

import java.io.File;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.massmutual.brms.AgendaListener;
import com.massmutual.brms.RuleListener;
import com.massmutual.domain.Insured;
import com.massmutual.domain.partone.PolicyAppliedFor;

public class AlirTest {
	private static final Logger logger = Logger.getLogger(AlirTest.class);
	static KieServices kieServices = KieServices.Factory.get();
	static KieContainer kieContainer = null;
	
	 @BeforeClass
	 public static void init(){
		KieRepository kr = kieServices.getRepository();
		KieFileSystem fileSystem = kieServices.newKieFileSystem();
		
		String [] fileNames  = {
				//"src/test/resources/calculate_alir_risk_multiplier.drl",
				"src/test/resources/calculate_schedule_unscheduled_alir_amount_at_risk.drl"
				,"src/test/resources/final_alir_amount_at_risk.drl"
				//,"src/test/resources/scheduled_alir_premium_risk.drl"
				//,"src/test/resources/unscheduled_alir_premium_risk.drl"
				
		};
		for (String string : fileNames) {
			Resource resource = kieServices.getResources().newFileSystemResource(new File(string));
			fileSystem.write(resource);
		}
		
		KieBuilder kb = kieServices.newKieBuilder(fileSystem);
		
		kb.buildAll();
		if (kb.getResults().hasMessages(org.kie.api.builder.Message.Level.ERROR)) {
			throw new RuntimeException("Build errors:\n" + kb.getResults().toString());
		}
		
		kieContainer = kieServices.newKieContainer(kr.getDefaultReleaseId());
		logger.info("kieContainer : "+kieContainer);
	 }
	@Test
	public void testAlirRiskInFileSystem(){
		
		//PrimaryInsured insured = new PrimaryInsured();
		//insured.setInsuredAge(41);
		//insured.setId("1");
		
		PolicyAppliedFor appliedFor = new PolicyAppliedFor();
		appliedFor.setTotalAmountAtRisk(10000l);
		appliedFor.setAlirModalPremium(1000000d);
		appliedFor.setAlirUnscheduleLumpSum(1000d);
		
		Insured insured = new Insured();
		insured.setInsuredAge(41);
		
		//appliedFor.setBaseFaceAmount(700000d); 
		
		KieSession kieSession = kieContainer.newKieSession();
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		
		kieSession.insert(insured);
		kieSession.insert(appliedFor);
		
		kieSession.getAgenda().getAgendaGroup("calculate-risk-rating").setFocus();
		
		logger.info("now firiing rules");
		
		kieSession.fireAllRules();
		
		
		
	}
}

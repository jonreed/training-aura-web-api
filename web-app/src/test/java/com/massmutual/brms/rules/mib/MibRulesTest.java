package com.massmutual.brms.rules.mib;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.mib.FormResponse;
import com.massmutual.domain.mib.MIBInput;

public class MibRulesTest {
	private final static transient Logger log = Logger.getLogger(MibRulesTest.class);
	private RuleProcessor ruleProcessor = new RuleProcessor();
	
	@Test
	public void testMibRules() {
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();
		MIBInput input = createFormResponse(Arrays.asList("011***", "110T**"));
		
		input.setRiskSummaryType(RiskSummaryType.MIB);
		inputs.add(input);
		
		try{
			RiskResponse response = ruleProcessor.calculateRiskRatingSummary(inputs);
			log.info("Output: " + response);
		}
		catch(Exception e){}
		
		log.info("MIB Test complete");
		//assertEquals("Highest rating does not match", 3, summary.getRollupRating());
		
	}
	
	private MIBInput createFormResponse(List<String> responseData) {
		MIBInput input = new MIBInput();
		
		for (String value : responseData) {
			input.getResponses().add(new FormResponse(value));
		}
		
		return input;
	}
}

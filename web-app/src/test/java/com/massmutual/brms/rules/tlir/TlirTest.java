package com.massmutual.brms.rules.tlir;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.partone.PaymentInformation;
import com.massmutual.domain.partone.PrimaryInsured;

public class TlirTest {
	private static final Logger logger = Logger.getLogger(TlirTest.class);
	
	RuleProcessor ruleProcessor = new RuleProcessor(){{
		init();
	}};
	
	@Test
	public void testPremiumSource(){
		PrimaryInsured insured = new PrimaryInsured();
		insured.setId("1");
		PaymentInformation paymentInformation = new PaymentInformation();
		paymentInformation.setPremiumSource(0);
		
		insured.setPaymentInformation(paymentInformation);
		insured.setRiskSummaryType(RiskSummaryType.PART_I);
		
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();	
		inputs.add(insured);
		
		try {
			ruleProcessor.calculateRiskRatings(inputs);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}

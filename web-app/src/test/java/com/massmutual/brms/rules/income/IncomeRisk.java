package com.massmutual.brms.rules.income;

import java.io.File;

import org.junit.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.massmutual.brms.AgendaListener;
import com.massmutual.brms.RuleListener;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.Insured;
import com.massmutual.domain.partone.FinancialHistory;
import com.massmutual.domain.partone.PolicyAppliedFor;
import com.massmutual.domain.partone.PrimaryInsured;

public class IncomeRisk extends BaseTest{

	
	KieServices kieServices = KieServices.Factory.get();
	@Test
	public void testStart(){
		try {
			testSome();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void testSome(){
		KieRepository kr = kieServices.getRepository();
		KieFileSystem fileSystem = kieServices.newKieFileSystem();
		Resource resource = kieServices.getResources().newFileSystemResource(new File("src/test/resources/truthy.drl"));
		fileSystem.write(resource);
		
		KieBuilder kb = kieServices.newKieBuilder(fileSystem);
		
		kb.buildAll();
		if (kb.getResults().hasMessages(org.kie.api.builder.Message.Level.ERROR)) {
			throw new RuntimeException("Build errors:\n" + kb.getResults().toString());
		}
		
		KieContainer kContainer = kieServices.newKieContainer(kr.getDefaultReleaseId());
		KieSession kieSession = kContainer.newKieSession();
		
		//PrimaryInsured insured = new PrimaryInsured();
		//insured.setInsuredAge(37);

		FinancialHistory history = new FinancialHistory();
		history.setEarnedIncomeCurrentYear(10000d);
		history.setUnearnedIncomeCurrentYear(20000d);
		
		PolicyAppliedFor appliedFor = new PolicyAppliedFor();
		appliedFor.setProposedPlanPremium(1501d);
		
		Insured insured = new Insured();
		insured.setInsuredAge(37);
		
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		
		kieSession.insert(insured);
		kieSession.insert(history);
		kieSession.insert(appliedFor);
		
		kieSession.getAgenda().getAgendaGroup("calculate-risk-rating").setFocus();
		kieSession.fireAllRules();
		
	}
	
	
	
	
}

package com.massmutual.brms.rules.rtr;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.partone.PolicyAppliedFor;
import com.massmutual.domain.partone.PrimaryInsured;

public class RtrRiskTest {

	private static final Logger log = Logger.getLogger(RtrRiskTest.class);
	private RuleProcessor ruleProcessor = new RuleProcessor(){{
		init();
	}};
	
	@Test
	public void testRtrRisk() throws Exception {
		PrimaryInsured insured = new PrimaryInsured();
		insured.setId("12");
		PolicyAppliedFor appliedFor = new PolicyAppliedFor();
		
		appliedFor.setBaseFaceAmount(20000d);
		appliedFor.setRtrFaceAmount(2000d);
		//appliedFor.setLisrFaceAmount(10000d);
		insured.setPolicyAppliedFor(appliedFor);
		insured.setRiskSummaryType(RiskSummaryType.PART_I);
		
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();	
		inputs.add(insured);
		
		RiskResponse response = ruleProcessor.calculateRiskRatings(inputs);
		
		
		
		
	}

}

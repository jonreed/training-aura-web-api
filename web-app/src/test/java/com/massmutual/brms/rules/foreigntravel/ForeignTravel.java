package com.massmutual.brms.rules.foreigntravel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.massmutual.brms.AgendaListener;
import com.massmutual.brms.RuleListener;
import com.massmutual.brms.RuleProcessor;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.PartOneRiskRating;
import com.massmutual.domain.partone.ForeignTravelRisk;

public class ForeignTravel extends BaseTest{

	
	
	
	@Test
	public void testTravelRiskCountries(){
		KieSession kieSession = getKieContainer().newKieSession();
		ForeignTravelRisk risk = new ForeignTravelRisk();
		risk.setCountry("Armenia");
		
		
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		kieSession.insert(risk);
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		
		
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		List<PartOneRiskRating> riskRatings = new ArrayList<>();
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//3		3		2		3	2	3	3
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			
			UPNT(riskRslt, 3);
			SPNT(riskRslt, 3);
			NONT(riskRslt, 2);
			SPT(riskRslt, 3);
			TOB(riskRslt, 2);
			WP(riskRslt, 3);
			DIIO(riskRslt, 3);
			
			riskRatings.add(riskRslt );	
		}
		
		Assert.assertTrue(!riskRatings.isEmpty());
		
		
		kieSession.dispose();
	}
	
	@Test
	public void testTravelNoneRiskCountries(){
		KieSession kieSession = getKieContainer().newKieSession();
		ForeignTravelRisk risk = new ForeignTravelRisk();
		risk.setCountry("Canada");
		
		
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		kieSession.insert(risk);
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		
		
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		List<PartOneRiskRating> riskRatings = new ArrayList<>();
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//1		1		1		1	1	1	1
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			
			UPNT(riskRslt, 1);
			SPNT(riskRslt, 1);
			NONT(riskRslt, 1);
			SPT(riskRslt, 1);
			TOB(riskRslt, 1);
			WP(riskRslt, 1);
			DIIO(riskRslt, 1);
			
			riskRatings.add(riskRslt );	
		}
		
		Assert.assertTrue(!riskRatings.isEmpty());
		
		
		kieSession.dispose();
	}
	
	@Test
	public void testTravelForeightTravelOver12Weeks(){
		KieSession kieSession = getKieContainer().newKieSession();
		ForeignTravelRisk risk = new ForeignTravelRisk();
		risk.setCountry("Armenia");
		
		
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		kieSession.insert(risk);
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		
		
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		List<PartOneRiskRating> riskRatings = new ArrayList<>();
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//3		3		2		3	2	3	3
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			
			UPNT(riskRslt, 3);
			SPNT(riskRslt, 3);
			NONT(riskRslt, 2);
			SPT(riskRslt, 3);
			TOB(riskRslt, 2);
			WP(riskRslt, 3);
			DIIO(riskRslt, 3);
			
			riskRatings.add(riskRslt );	
		}
		
		Assert.assertTrue(!riskRatings.isEmpty());
		
		
		kieSession.dispose();
	}
}

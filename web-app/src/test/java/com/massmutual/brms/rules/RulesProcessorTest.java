package com.massmutual.brms.rules;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

//import pl.zientarski.SchemaMapper;
//import org.json.JSONObject;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.domain.RiskInputBase;
import com.massmutual.domain.RiskRatingRollup;
import com.massmutual.domain.RiskResponse;
import com.massmutual.domain.RiskSummaryType;
import com.massmutual.domain.partone.PrimaryInsured;

public class RulesProcessorTest {
	
	//@Test
//	public void printPrimaryInsuredSchema(){
//		// only use this with JDK 1.8
//		SchemaMapper schemaMapper = new SchemaMapper();
//		JSONObject jsonObject = schemaMapper.toJsonSchema4(PrimaryInsured.class, true);
//		
//		System.out.println("//schema for PrimaryInsured");
//		System.out.println(jsonObject.toString(4));
//		
//		
//	}
	
	//@Test
//	public void printRiskRepsonseSchema(){
//		SchemaMapper schemaMapper = new SchemaMapper();
//		JSONObject jsonObject = schemaMapper.toJsonSchema4(PrimaryInsured.class, true);
//		
//	
//		
//		System.out.println("//schema for RiskResponse");
//		jsonObject = schemaMapper.toJsonSchema4(RiskResponse.class, true);
//		System.out.println(jsonObject.toString(4));
//	}
		
	//@Test
	public void testFindAllMarkedObjects (){
		
		RuleProcessor p = new RuleProcessor();
		//p.findAllRiskMarkedObjects();

		PrimaryInsured ins = new PrimaryInsured();
//		ins.setAviation(new AviationRisk());
//		ins.setAge(new AgeRisk());
//		ins.setSmoking(new SmokingRisk());

		/*Set<Method> methodSet = p.getRiskGetterMethodsByRiskType().get(PartOneRiskType.AVIATION);
		
		for (Method method : methodSet) {
			Class returnType = method.getReturnType();
			
			
		}*/
		Field [] fields = PrimaryInsured.class.getDeclaredFields();
		for (Field field : fields) {
			field.setAccessible(true);
			Class type  = field.getType();
			try {
				Object instance = type.newInstance();
				field.set(ins, instance);
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}
		}
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(System.out, ins);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		

	}
	
	private Object fromJson(){
		ObjectMapper mapper = new ObjectMapper();
		try {
			Object obj  = mapper.readValue(this.getClass().getResourceAsStream("/ratingJson.json"), PrimaryInsured.class);
			return obj;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) {
		try {
			new RulesProcessorTest().testRollup();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testRollup(){
		PrimaryInsured insured = (PrimaryInsured) fromJson();
		
		insured.setRiskSummaryType(RiskSummaryType.PART_I);
		
		RuleProcessor proce= new RuleProcessor();
		proce.init();
		List<RiskInputBase> inputs = new ArrayList<RiskInputBase>();	
		inputs.add(insured);
		
		try {
			RiskResponse response = proce.calculateRiskRatingRollup(inputs);
			//String val = String.valueOf(response.getRiskRollups());
			
			List<RiskRatingRollup> rollups =  response.getRiskRollups();
			for (RiskRatingRollup RiskRatingRollup : rollups) {
				try {
					System.out.println("hash : "+RiskRatingRollup.hashCode());
					System.out.println(" rollups :"+RiskRatingRollup.toString());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			//System.out.println(" val; "+val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

package com.massmutual.brms.rules.parttwo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.kie.api.runtime.KieSession;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.PartTwoRiskType;
import com.massmutual.domain.parttwo.BloodDisorder;
import com.massmutual.domain.parttwo.MedicalCondition;
@SuppressWarnings({ "serial" })
public class BloodDisorderTests extends BaseTest{
	
	@Test
	public void testStartBD_1(){
		KieSession kieSession = getKieSessionClassPath();
		
		BloodDisorder disorder = new BloodDisorder(){{

			setConditions(new ArrayList<MedicalCondition>(){{
				add(new MedicalCondition("hadBDAnemia",Y));
				add(new MedicalCondition("hasBDAnemiaResolved",Y));
				add(new MedicalCondition("hadBDAnemiaBleeding",Y));
				add(new MedicalCondition("hasBDAnemiaControlled",Y));
				add(new MedicalCondition("hasBDAnemiaSymptoms",Y));
				add(new MedicalCondition("hadBDSpleen",Y));
				add(new MedicalCondition("hadBDClotting",Y));
				add(new MedicalCondition("hadBDHemochromatosis",Y));
				add(new MedicalCondition("hadBDOther",Y));
				
			}});
		}};
		
		kieSession.insert(disorder);
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		kieSession.fireAllRules();
		//upnt, spnt nont spt tob wp diio 
		
		
		
		
		Map<String,String> ruleToRiskMap = new HashMap<String,String>(){{
			put("BD_1_Anemia","1,1,1,1,1,1");
			put("BD_1_AnemiaStable","1,1,1,1,1,1");
			put("BD_1_ChronicBleeding","2,2,2,2,2,2");
			put("BD_1_B12Supplements","1,1,1,1,1,1");
			put("BD_1_AnemiaSymptons","2,2,2,2,2,2");
			put("BD_2_EnlargedSpleen","2,2,2,2,2,2");
			put("BD_2_Hemophilia","2,2,2,2,2,2");
			put("BD_2_Hemochromatosis","1,1,1,1,1,1");
			put("BD_2_Other","2,2,2,2,2,2");
		}};
		
		assertMyPartTwoRiskType(ruleToRiskMap,PartTwoRiskType.BLOOD_DISORDER,getPartTwoRiskRatings(kieSession));
	
		//System.out.println("list "+list.size());
	}

}

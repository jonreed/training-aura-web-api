package com.massmutual.brms.rules;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.drools.core.common.DefaultFactHandle;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.io.Resource;
import org.kie.api.runtime.ClassObjectFilter;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.massmutual.brms.AgendaListener;
import com.massmutual.brms.RuleListener;
import com.massmutual.domain.PartTwoRiskRating;
import com.massmutual.domain.PartTwoRiskType;
import com.massmutual.domain.RiskRatingBase;
import com.massmutual.domain.parttwo.MedicalCondition;

@SuppressWarnings({"unchecked","rawtypes","serial"})
public abstract class BaseTest {
	protected static final Logger log = Logger.getLogger(BaseTest.class);
	protected String groupId = null;
	protected String artifactId = null;
	protected String verstion = null;
	protected String Y = "Y";
	protected String N = "N";
	private static class Factory {
		private static KieContainer kContainer;
		static {
			try {

				// if the local maven repository is not in the default
				// ${user.home}/.m2
				// need to provide the custom settings.xml
				// pass property value
				// -Dkie.maven.settings.custom={custom.settings.location.full.path}

				KieServices kServices = KieServices.Factory.get();

				ReleaseId releaseId = kServices.newReleaseId("com.massmutual",
						"mm-risk-rules", "0.6-SNAPSHOT");

				// kContainer = kServices.newKieContainer(releaseId,
				// Factory.class.getClassLoader());

				kContainer = kServices.newKieContainer(releaseId);

				// KieScanner kScanner = kServices.newKieScanner(kContainer);
				// Start the KieScanner polling the maven repository every 10
				// seconds

				// kScanner.start(10000L);

			} catch (Exception e) {

				log.error("Exception trying to initialize the Kie Container", e);
			}
		}

		public static KieContainer get() {
			return kContainer;
		}
	}

	protected KieContainer getKieContainer() {
		return Factory.get();
	}
	
	
	
	protected KieSession getKieSessionClassPath(){
		KieSession kieSession =KieServices.Factory.get().getKieClasspathContainer().newKieSession();
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		return kieSession;
	
	}
	
	protected KieSession getKieSession(){
		
		KieSession kieSession = Factory.get().newKieSession();
		kieSession.addEventListener(new AgendaListener());
		kieSession.addEventListener(new RuleListener());
		return kieSession;
	}

	private void assertVal(Integer actualValue, Integer assertVale){
		org.junit.Assert.assertEquals(actualValue, assertVale);
	}
	protected void UPNT(RiskRatingBase rating, Integer assertVale) {
		assertVal(rating.getUPNT(),assertVale);
	}

	protected void SPNT(RiskRatingBase rating, Integer assertVale) {
		assertVal(rating.getSPNT(),assertVale);
	}

	protected void SPT(RiskRatingBase rating, Integer assertVale) {
		assertVal(rating.getSPT(),assertVale);
	}

	protected void TOB(RiskRatingBase rating, Integer assertVale) {
		assertVal(rating.getTOB(),assertVale);
	}

	protected void WP(RiskRatingBase rating, Integer assertVale) {
		assertVal(rating.getWP(),assertVale);
	}

	protected void DIIO(RiskRatingBase rating, Integer assertVale) {
		assertVal(rating.getDIIO(),assertVale);
	}

	protected void NONT(RiskRatingBase rating, Integer assertVale) {
		assertVal(rating.getNONT(),assertVale);
	}
	protected List<PartTwoRiskRating> getPartTwoRiskRatings(KieSession kieSession){
		;
		List<PartTwoRiskRating> retList = new ArrayList<>();
		Collection<?> coll = kieSession.getFactHandles(new ClassObjectFilter(PartTwoRiskRating.class));
		for (Object object : coll) {
			DefaultFactHandle factHandle = (DefaultFactHandle) object;
			Object riskRating = factHandle.getObject();
			if (riskRating instanceof PartTwoRiskRating) {
				PartTwoRiskRating partTwo = (PartTwoRiskRating) riskRating;
				retList.add(partTwo);
			}
		}
		return retList;
		/*kieSession.getFactHandles(new ObjectFilter() {
			
			@Override
			public boolean accept(Object object) {
				System.out.println("object  "+object);
				return false;
			}
		});*/
		//return Collections.EMPTY_LIST;
	}
	List<Method> riskAssertMethod = new ArrayList(){{
		try {
			
			add(BaseTest.class.getDeclaredMethod("UPNT", RiskRatingBase.class,Integer.class));
			add(BaseTest.class.getDeclaredMethod("SPNT", RiskRatingBase.class,Integer.class));
			add(BaseTest.class.getDeclaredMethod("NONT", RiskRatingBase.class,Integer.class));
			add(BaseTest.class.getDeclaredMethod("SPT", RiskRatingBase.class,Integer.class));
			add(BaseTest.class.getDeclaredMethod("TOB", RiskRatingBase.class,Integer.class));
		//	add(BaseTest.class.getDeclaredMethod("UPNT", RiskRatingBase.class,Integer.class));
			add(BaseTest.class.getDeclaredMethod("WP", RiskRatingBase.class,Integer.class));
			add(BaseTest.class.getDeclaredMethod("DIIO", RiskRatingBase.class,Integer.class));
			
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}};
	protected void assertRating(PartTwoRiskRating rating,String vals){
	
		vals = vals.replaceAll(",\\s{1,}$", "");
		vals = vals.replaceAll(",$", "");
		vals = vals.trim();
		String [] valArr = vals.split(",");
		//int len = valArr.length;
		for (int i = 0; i < valArr.length; i++) {
				Method method= riskAssertMethod.get(i);
				int val = Integer.parseInt(valArr[i]);
				RiskRatingBase base = (RiskRatingBase) rating;
				try {
					method.invoke(this, base,val);
				} catch (IllegalAccessException e) {
					log.error("",e);
					logger.error("rating error on : "+base.toString()+"-valArr.length :"+valArr.length + "--vals : "+vals+"--method " +method );
					if(true){
						throw new AssertionError(e);
					}
				} catch (IllegalArgumentException e) {
					log.error("",e);
					logger.error("rating error on : "+base.toString()+"-valArr.length :"+valArr.length + "--vals : "+vals+"--method " +method );
					// TODO Auto-generated catch block
					if(true){
						throw new AssertionError(e);
					}
				} catch (InvocationTargetException e) {
					log.error("",e);
					logger.error("rating error on : "+base.toString()+"-valArr.length :"+valArr.length + "--vals : "+vals+"--method " +method );
					// TODO Auto-generated catch block
					if(true){
						throw new AssertionError(e);
					}
				}
		}
		
//		UPNT(rating, Integer.parseInt(valArr[--len]));
//		SPNT(rating, Integer.parseInt(valArr[--len]));
//		NONT(rating, Integer.parseInt(valArr[--len]));
//		SPT(rating, Integer.parseInt(valArr[--len]));
//		TOB(rating, Integer.parseInt(valArr[--len]));
//		WP(rating, Integer.parseInt(valArr[--len]));
//		DIIO(rating, Integer.parseInt(valArr[--len]));
		
	}
	
	/**
	 * Asserts all the rule and its risk value
	 * and removes the value from the map, 
	 * a empty map means all rules test passed
	 * @param ruleToRiskMap
	 * @param type
	 * @param list
	 */
	protected void assertMyPartTwoRiskType(Map<String,String> ruleToRiskMap, PartTwoRiskType type, List<PartTwoRiskRating> list){
		//List<PartTwoRiskRating> list = getPartTwoRiskRatings(kieSession);
		for (PartTwoRiskRating rating : list) {
			
			if(!rating.getPartTwoRiskType().equals(type)){
				continue;
			}
			
			String ruleName = rating.getRuleName();
			log.info("came ruleName : "+ruleName);
			if(ruleToRiskMap.get(ruleName) != null) {
				System.out.println(ruleName);
				try {
					assertRating(rating,ruleToRiskMap.get(ruleName));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ruleToRiskMap.remove(ruleName);
			}
		}
		
		log.info("ruleToRiskMap leftover : "+ruleToRiskMap);
		org.junit.Assert.assertEquals(true, ruleToRiskMap.isEmpty());
	}
	/**
	 * val is like this "hadBloodDisorder,Y,hadBDAnemia,N"


	 * @param val
	 * @return
	 */
	protected List<MedicalCondition> makeMedicalCondition(String val){
		
		List<MedicalCondition> medicalConditionList = new ArrayList<MedicalCondition>();
		String [] spltr = val.split(",");
		StringTokenizer tokenizer = new StringTokenizer(val, ",");
		while(tokenizer.hasMoreTokens()){
			String name = (String)tokenizer.nextElement();
			String found = (String)tokenizer.nextElement();
			medicalConditionList.add(new MedicalCondition(name, found));
		}
		return medicalConditionList;
	}
	
	private static final Logger logger = Logger.getLogger(BaseTest.class);
	
	//protected KieServices fileBasedkieServices = KieServices.Factory.get();
	protected KieContainer fileBasedkieContainer = null;
	 protected String [] drlNames = {};
	 protected  void fileSystemKcontainerCreate(){
		 if(fileBasedkieContainer != null){
			 return;
		 }
		KieServices kieServices = KieServices.Factory.get();
		KieRepository kr = kieServices.getRepository();
		KieFileSystem fileSystem = kieServices.newKieFileSystem();
		
//		String [] fileNames  = {
//				//"src/test/resources/calculate_alir_risk_multiplier.drl",
//				"src/test/resources/calculate_schedule_unscheduled_alir_amount_at_risk.drl"
//				,"src/test/resources/final_alir_amount_at_risk.drl"
//				//,"src/test/resources/scheduled_alir_premium_risk.drl"
//				//,"src/test/resources/unscheduled_alir_premium_risk.drl"
//				
//		};
		for (String string : drlNames) {
			Resource resource = KieServices.Factory.get().getResources().newClassPathResource(string);
			fileSystem.write(resource);
		}
		
		KieBuilder kb =  KieServices.Factory.get().newKieBuilder(fileSystem);
		
		kb.buildAll();
		if (kb.getResults().hasMessages(org.kie.api.builder.Message.Level.ERROR)) {
			throw new RuntimeException("Build errors:\n" + kb.getResults().toString());
		}
		
		fileBasedkieContainer =  KieServices.Factory.get().newKieContainer(kr.getDefaultReleaseId());
		logger.info("kieContainer : "+fileBasedkieContainer);
	 }
	 
	 public Object getMapFromJson(InputStream stream ) throws Exception, JsonMappingException, IOException{
		 ObjectMapper mapper = new ObjectMapper();
		 
		 return mapper.readValue(stream, HashMap.class);
		
	 }
	 
	 public String readTextFromFile(InputStream stream){
		 
		try {
			return  IOUtils.toString(stream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		 
	 }
}

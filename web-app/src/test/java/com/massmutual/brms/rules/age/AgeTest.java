package com.massmutual.brms.rules.age;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.PartOneRiskRating;
import com.massmutual.domain.PartOneRiskType;

public class AgeTest extends BaseTest{/*

	private static final Logger log = Logger.getLogger(AgeTest.class);
	
	@Test
//	public void testAgeTestKickout(){
//		KieSession kieSession = getKieSession();
//		
//		AgeRisk risk = new AgeRisk();
//		risk.setInsuranceAge(16);
//		
//		
//		kieSession.insert(risk);
//		
//		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
//		kieSession.fireAllRules();
//		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
//		
//		
//		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
//		//3		3		2		3	2	1	1
//		for (QueryResultsRow row : queryResults) {			
//			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
//			if( ! (  riskRslt.getRiskType().equals(PartOneRiskType.AGE) )){
//				continue;
//			}
//			log.info("asserting rule-age-1");
//			
//			UPNT(riskRslt, 3);
//			SPNT(riskRslt, 3);
//			NONT(riskRslt, 2);
//			SPT(riskRslt, 3);
//			TOB(riskRslt, 2);
//			WP(riskRslt, 1);
//			DIIO(riskRslt, 1);
//			
//			
//			
//			break;
//		}
//		kieSession.dispose();
//		
//		
//	}
	
	@Test
	public void testAgeTestKickoutAnother(){
		KieSession kieSession = getKieSession();
		
		AgeRisk risk = new AgeRisk();
		risk.setInsuranceAge(41);
		
		
		kieSession.insert(risk);
		
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//3		3		2		3	2	1	1
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			if( ! (  riskRslt.getRiskType().equals(PartOneRiskType.AGE) )){
				continue;
			}
			log.info("asserting rule-age-1");
			
			UPNT(riskRslt, 3);
			SPNT(riskRslt, 3);
			NONT(riskRslt, 2);
			SPT(riskRslt, 3);
			TOB(riskRslt, 2);
			WP(riskRslt, 1);
			DIIO(riskRslt, 1);
			
			
			
			break;
		}
		kieSession.dispose();
		
		
	}
	
	@Test
	public void testAgeTesPass(){
		KieSession kieSession = getKieSession();
		
		AgeRisk risk = new AgeRisk();
		risk.setInsuranceAge(23);
		
		
		kieSession.insert(risk);
		
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//3		3		2		3	2	1	1
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			if( ! (  riskRslt.getRiskType().equals(PartOneRiskType.AGE) )){
				continue;
			}
			log.info("asserting rule-age-1");
			
			UPNT(riskRslt, 1);
			SPNT(riskRslt, 1);
			NONT(riskRslt, 1);
			SPT(riskRslt, 1);
			TOB(riskRslt, 1);
			WP(riskRslt, 1);
			DIIO(riskRslt, 1);
			
			
			
			break;
		}
		kieSession.dispose();
		
		
	}
	
	
*/}

package com.massmutual.brms.rules.labs;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;

import com.massmutual.brms.BlenderRuleProcessor;
import com.massmutual.domain.labs.Applicant;
import com.massmutual.domain.labs.LabResult;
import com.massmutual.domain.labs.LabResultFlag;

import static com.massmutual.domain.labs.LabResultType.*;

import com.massmutual.domain.labs.LabResultType;
import com.massmutual.domain.labs.LabRulesSummary;

public class LabRuleTest {

	private final static transient Logger logger = Logger.getLogger(LabRuleTest.class);
	private BlenderRuleProcessor ruleProcessor = new BlenderRuleProcessor();
	
	@Test
	public void testLabValidation() {
		Applicant applicant = new Applicant(1l, "M", 41);
		applicant.setLabResults(new ArrayList<LabResult>());
		LabRulesSummary summary = ruleProcessor.performLabSummary(applicant);
		logger.info(summary);
		
		applicant.getLabResults().add(new LabResult(EGFR, "", "112.54"));
		
		summary = ruleProcessor.performLabSummary(applicant);
		logger.info(summary);
		
		checkForFlags(new ArrayList<LabResultType>(), summary.getFlags(), 0);
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(EGFR, "50", "213"));
		applicant.getLabResults().add(new LabResult(HIV, "", "POS"));
		applicant.getLabResults().add(new LabResult(NT_PROBNP, "50", "213"));
		summary = ruleProcessor.performLabSummary(applicant);
		checkForFlags(Arrays.asList(EGFR, HIV, NT_PROBNP), summary.getFlags(), 3);
		assertMissingFlags(Arrays.asList(TOTAL_SCORE), summary.getFlags());
		
		applicant.getLabResults().add(new LabResult(BLOOD_ALCOHOL, "16", ""));
		summary = ruleProcessor.performLabSummary(applicant);
		checkForFlags(Arrays.asList(EGFR, HIV, NT_PROBNP, TOTAL_SCORE), summary.getFlags(), 4);
		
		logger.info("Summary: " + summary);
	}
	
	@Test
	public void testCholesterolRatio() {
		List<LabResultType> types = Arrays.asList(CHOL_HDL_RATIO, CHOLESTEROL);
		Applicant applicant = new Applicant(1l, "M", 41);
		applicant.setLabResults(new ArrayList<LabResult>());
		applicant.getLabResults().add(new LabResult(CHOL_HDL_RATIO, "0", "11"));
		applicant.getLabResults().add(new LabResult(CHOLESTEROL, "0", "0"));
		LabRulesSummary summary = ruleProcessor.performLabSummary(applicant);
		checkForFlags(types, summary.getFlags(), 2);
		printFlags(types, summary.getFlags());
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(CHOL_HDL_RATIO, "0", "11"));
		applicant.getLabResults().add(new LabResult(CHOLESTEROL, "0", "240"));
		summary = ruleProcessor.performLabSummary(applicant);
		types = Arrays.asList(CHOL_HDL_RATIO);
		checkForFlags(types, summary.getFlags(), 1);
		printFlags(types, summary.getFlags());
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(CHOL_HDL_RATIO, "0", "11"));
		applicant.getLabResults().add(new LabResult(CHOLESTEROL, "0", "300"));
		summary = ruleProcessor.performLabSummary(applicant);
		types = Arrays.asList(CHOL_HDL_RATIO);
		checkForFlags(types, summary.getFlags(), 1);
		printFlags(types, summary.getFlags());
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(CHOL_HDL_RATIO, "0", "3.4"));
		applicant.getLabResults().add(new LabResult(CHOLESTEROL, "0", "300"));
		summary = ruleProcessor.performLabSummary(applicant);
		types = Arrays.asList(CHOL_HDL_RATIO);
		assertMissingFlags(Arrays.asList(CHOL_HDL_RATIO,  CHOLESTEROL), summary.getFlags());
		printFlags(types, summary.getFlags());
		
	}
	
	@Test
	public void testBloodPressureFail() {
		Applicant applicant = new Applicant(1l, "M", 41);
		applicant.setLabResults(new ArrayList<LabResult>());
		applicant.getLabResults().add(new LabResult(BLOOD_PRESSURE, "fred", "fred"));
		LabRulesSummary summary = ruleProcessor.performLabSummary(applicant);
		assertNotNull("Expecting non-null summary", summary);
	}
	
	@Test
	public void testBloodPressure() {
		Applicant applicant = new Applicant(1l, "M", 41);
		applicant.setLabResults(new ArrayList<LabResult>());
		LabRulesSummary summary = ruleProcessor.performLabSummary(applicant);
		countBloodPressureWarnings(summary.getMissingLabs(), 2);
		
		applicant.getLabResults().add(new LabResult(BLOOD_PRESSURE, "", ""));
		summary = ruleProcessor.performLabSummary(applicant);
		countBloodPressureWarnings(summary.getMissingLabs(), 1);
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(BLOOD_PRESSURE, null, "125/75"));
		summary = ruleProcessor.performLabSummary(applicant);
		countBloodPressureWarnings(summary.getMissingLabs(), 0);
		//checkForFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags(), 0);
		Set<LabResultType> found = assertMissingFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags());
		assertEquals("Expecting no BLOOD_PRESSURE flag", 0, found.size());
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(BLOOD_PRESSURE, null, "141/75"));
		summary = ruleProcessor.performLabSummary(applicant);
		countBloodPressureWarnings(summary.getMissingLabs(), 0);
		checkForFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags(), null);
		found = assertMissingFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags());
		assertEquals("Expecting 1 BLOOD_PRESSURE flag", 1, found.size());
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(BLOOD_PRESSURE, null, "141/96"));
		summary = ruleProcessor.performLabSummary(applicant);
		countBloodPressureWarnings(summary.getMissingLabs(), 0);
		checkForFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags(), null);
		found = assertMissingFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags());
		assertEquals("Expecting 1 BLOOD_PRESSURE flag", 1, found.size());
		
		applicant.getLabResults().clear();
		applicant.getLabResults().add(new LabResult(BLOOD_PRESSURE, null, "139/96"));
		summary = ruleProcessor.performLabSummary(applicant);
		countBloodPressureWarnings(summary.getMissingLabs(), 0);
		checkForFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags(), null);
		found = assertMissingFlags(Arrays.asList(BLOOD_PRESSURE), summary.getFlags());
		assertEquals("Expecting 1 BLOOD_PRESSURE flag", 1, found.size());
		
		
	}
	
	private int countBloodPressureWarnings(List<String> missingLabs, Integer expectedCount) {
		int mentionCount = 0;
		for (String missingLab : missingLabs) {
			logger.debug("missingLab: " + missingLab);
			if (missingLab.toLowerCase().indexOf("blood pressure") >= 0 || 
					missingLab.toLowerCase().indexOf("blood_pressure") >= 0) {
				mentionCount++;
			}
		}
		if (expectedCount != null) {
			assertEquals("Expecting " + expectedCount.intValue() + " missing blood pressure warnings", expectedCount.intValue(), mentionCount);
		}
		return mentionCount;
	}
	
	private Set<LabResultType> assertMissingFlags(List<LabResultType>missingTypes, List<LabResultFlag>flags) {
		Set<LabResultType> found = new HashSet<LabResultType>();
		
		for (LabResultType missingType : missingTypes) {
			for (LabResultFlag flag : flags) {
				if (missingType.equals(flag.getResultType())) {
					found.add(missingType);
				}
			}
		}
		return found;
	}
	
	private void checkForFlags(List<LabResultType>types, List<LabResultFlag>flags, Integer totalCount) {
		for (LabResultType type : types) {
			int matches = 0;
			for (LabResultFlag flag : flags) {
				if (flag.getResultType().equals(type)) {
					matches++;
				}
			}
			assertEquals("Expecting single match for type: " + type, 1, matches);
		}
		
		Set<LabResultType> unexpectedTypes = new HashSet<LabResultType>();
		for (LabResultFlag flag : flags) {
			if (!types.contains(flag.getResultType())) {
				unexpectedTypes.add(flag.getResultType());
			}
		}
		
		if (!unexpectedTypes.isEmpty()) {
			logger.info("Unexpected types: " + unexpectedTypes.toString());
		}
		
		if (totalCount != null) {
			assertEquals("Expecting " + totalCount + " flags", totalCount.intValue(), flags.size());
		}
	}
	
	private void printFlags(List<LabResultType> types, List<LabResultFlag> flags) {
		for (LabResultFlag flag : flags) {
			if (types.contains(flag.getResultType())) {
				logger.info(">>> " + flag);
			}
		}
	}
}

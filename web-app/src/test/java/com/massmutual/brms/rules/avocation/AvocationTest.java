package com.massmutual.brms.rules.avocation;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.QueryResults;
import org.kie.api.runtime.rule.QueryResultsRow;

import com.massmutual.brms.RuleProcessor;
import com.massmutual.brms.rules.BaseTest;
import com.massmutual.domain.PartOneRiskRating;
import com.massmutual.domain.PartOneRiskType;
import com.massmutual.domain.partone.AvocationRisk;

public class AvocationTest extends BaseTest{

	private static final Logger log = Logger.getLogger(AvocationTest.class);
	
	@Test
	public void testYesLifeStyleAcitivies(){
		KieSession kieSession = getKieSession();
		
		AvocationRisk risk = new AvocationRisk();
		risk.setLifeStyleActivities("Yes");
		
		
		kieSession.insert(risk);
		
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//3		3		2		3	2	3	3
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			if( ! ( riskRslt.getRuleName().equals("rule-avocation-1") && riskRslt.getRiskType().equals(PartOneRiskType.AVOCATION) )){
				continue;
			}
			log.info("asserting rule-avocation-1");
			
			UPNT(riskRslt, 3);
			SPNT(riskRslt, 3);
			NONT(riskRslt, 2);
			SPT(riskRslt, 3);
			TOB(riskRslt, 2);
			WP(riskRslt, 3);
			DIIO(riskRslt, 3);
			
			
			
			break;
		}
		kieSession.dispose();
		
		
	}
	
	@Test
	public void testNoLifeStyleAcitivies(){
		KieSession kieSession = getKieSession();
		
		AvocationRisk risk = new AvocationRisk();
		risk.setLifeStyleActivities("N");
		
		
		kieSession.insert(risk);
		
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//1		1		1		1	1	1	1
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			if( ! ( riskRslt.getRuleName().equals("rule-avocation-2") && riskRslt.getRiskType().equals(PartOneRiskType.AVOCATION) )){
				continue;
			}
			log.info("asserting rule-avocation-2");
			
			UPNT(riskRslt, 1);
			SPNT(riskRslt, 1);
			NONT(riskRslt, 1);
			SPT(riskRslt, 1);
			TOB(riskRslt, 1);
			WP(riskRslt, 1);
			DIIO(riskRslt, 1);
			
			
			
			break;
		}
		kieSession.dispose();
		
		
	}
	
	@Test
	public void testUnderWritingYesToAll(){
		KieSession kieSession = getKieSession();
		
		AvocationRisk risk = new AvocationRisk();
		risk.setUnderWaterDiving("Yes");
		risk.setUnderWaterDiving100Ft("Yes");
		risk.setUnderWaterDivingCertification("Yes");
		risk.setUnderWaterDiveWithABuddy("Yes");
		risk.setUnderWaterDiveFewerThanFifty("Y");
		
		
		
		
		kieSession.insert(risk);
		
		kieSession.getAgenda().getAgendaGroup(RuleProcessor.AGENDA_CALCULATE_RISK_RATING).setFocus();
		kieSession.fireAllRules();
		QueryResults queryResults = kieSession.getQueryResults("getRiskRatings");
		
		
		//UPNT	SPNT	NONT	SPT	TOB	WP	DIIO
		//1		1		1		1	1	1	1
		for (QueryResultsRow row : queryResults) {			
			PartOneRiskRating riskRslt = (PartOneRiskRating)row.get("$rating");
			if( ! ( riskRslt.getRuleName().equals("rule-avocation-3") && riskRslt.getRiskType().equals(PartOneRiskType.AVOCATION) )){
				continue;
			}
			log.info("asserting rule-avocation-3");
			
			UPNT(riskRslt, 1);
			SPNT(riskRslt, 1);
			NONT(riskRslt, 1);
			SPT(riskRslt, 1);
			TOB(riskRslt, 1);
			WP(riskRslt, 1);
			DIIO(riskRslt, 1);
			
			
			
			break;
		}
		kieSession.dispose();
		
		
	}
}
